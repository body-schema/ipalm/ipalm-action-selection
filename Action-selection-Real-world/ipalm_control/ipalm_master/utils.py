"""
Library of utilitary code for entropy based action-selection.
"""

from matplotlib.axes import Axes
from matplotlib.lines import Line2D
# from ipalm_dataset.measurements import ObjectMeasurements
# from ipalm_dataset.model import Model
from scipy.integrate import quad #using quadpack from FORTRAN lib
import matplotlib.pyplot as plt
import numpy as np 
import json
from scipy.stats import norm  
import os  
import copy


def check_or_create(name_json):
    stringed = "'" + name_json + "'"
    if os.path.exists(name_json):
        print("Cleaning the previous {} file.".format(stringed))
        os.remove(name_json)
    else:
        print("No {} found. New to be created.".format(stringed))

def json2dict(file_name):
    with open(file_name) as fp:
        data = fp.read()
        a = json.loads(data)
        return a

def solve(m1,m2,std1,std2):
  a = 1/(2*std1**2) - 1/(2*std2**2)
  b = m2/(std2**2) - m1/(std1**2)
  c = m1**2 /(2*std1**2) - m2**2 / (2*std2**2) - np.log(std2/std1)
  return np.roots([a,b,c])
            
def assign_equal_probs(ground, joker=False):
    """
    A helper function that just assigns equal
    probabilities to all known materials, that
    sums up to 1. Also creates a "cond_meas":None for each.

    [In] : ground   > type:dict, dictionary with all known materials
    [Out]: eq_probs > type:dict (nested), dictionary {'material': {"prob":prob_num, "cond":None}, ...}
    """
    
    n = float(len(ground["materials"]))
    eq_probs = {}
    for material in ground["materials"]:
        eq_probs[material] = {"prob":1/n, "cond":None}
    if joker:
        eq_probs["joker"] = {"prob":1/n, "cond":None}
    return eq_probs

def do_meas(ax, probs, ground, meas_type, meas_mean, meas_sigma):
    """
    Evaluates measurement in closed-form for every ground distribution.
    [In] :
    [Out]: temp_probs >type:dict (nested)
                       {'MATERIAL': {'prob': 0.05, 'cond': 0.05}, ... }
    """
    temp_probs = copy.deepcopy(probs)
    for material in ground["materials"]:
        if material == "joker":
            y_uni = uniform(ax)
            temp_probs[material]["cond"] = y_uni[0]
        else:
            mat_sigma = ground["materials"][material][meas_type]["sigma"]
            new_mat_sigma = np.sqrt(mat_sigma**2 + meas_sigma**2)
            mat_mean = ground["materials"][material][meas_type]["mean"]
            temp_probs[material]["cond"] = norm.pdf(meas_mean, mat_mean, new_mat_sigma)
    
    return temp_probs

def do_meas_sim(ax, probs, ref, meas_type, meas_mean, meas_sigma):
    """
    Evaluates measurement in closed-form for every ground distribution.
    [In] :
    [Out]: temp_probs >type:dict (nested)
                       {'MATERIAL': {'prob': 0.05, 'cond': 0.05}, ... }
    """


    temp_probs = copy.deepcopy(probs)
    materials = ref['materials'].items()
    for mat in materials:
        name = mat[0]
        props = mat[1]
        if name == "joker":
            y_uni = uniform(ax)
            temp_probs[name]["cond"] = y_uni[0]
        else:
            mat_sigma =props[meas_type]["sigma"]
            new_mat_sigma = np.sqrt(mat_sigma**2 + meas_sigma**2)
            mat_mean = props[meas_type]["mean"]
            temp_probs[name]["cond"] = norm.pdf(meas_mean, mat_mean, new_mat_sigma)
    
    return temp_probs

def complete_contin_bayes_update(probs, ground, meas_type, meas_mean, meas_sigma):
    """
    Continuous update, WARN! WITHOUT JOKER!
    """
    temp_probs = copy.deepcopy(probs)

    x = np.linspace(-200,5000,25000)
    for mat in probs:
        query_sigma = ground["materials"][mat][meas_type]["sigma"]
        query_mean = ground["materials"][mat][meas_type]["mean"]
        query_y = norm.pdf(x,query_mean,query_sigma)
        meas_y = norm.pdf(x, meas_mean, meas_sigma)
        product = sum(query_y * meas_y)  # P(e|mat)
        temp_probs[mat]["cond"] = product
        nominator = product * probs[mat]["prob"]
        to_sum = []
        for material in probs:
            mat_sigma = ground["materials"][material][meas_type]["sigma"]
            mat_mean = ground["materials"][material][meas_type]["mean"]
            mat_y = norm.pdf(x, mat_mean, mat_sigma)
            product2 = sum(mat_y * meas_y)  # discrete integration
            to_sum.append(product2 * probs[material]["prob"])
        denominator = sum(to_sum)
        query_out = nominator/denominator
        temp_probs[mat]["prob"] = query_out
    
    longest_str = 0
    for material in temp_probs:
        if len(material) > longest_str:
            longest_str = len(material)
    print()
    print("NUMERICAL solution via area -------------------------------")
    for material in temp_probs:
        padded_material = material.rjust(longest_str)
        print("Material: {} | Probabilty: {:.4f} | Area: {:.8f}".format(padded_material, temp_probs[material]["prob"], temp_probs[material]["cond"]))
        

    return temp_probs

def bayes_update(mat, probs, eps=None):
    """
    Answers query for P(mat_n|e) = P(e|mat_n)*probs[mat_n] / sum(P(e|mat_j)*probs[mat_j])
    Where:
    mat   - material for query
    probs - dict of probabilities including all known materials

    [In] : mat    > type:str, the material which the query answers
           probs  > type:dict, dictionary of pairs {'material': {"prob":prob_num, "cond":cond_prob_num}, ...}
    [Out]: updated probability for material in query
    """

    nominator = probs[mat]["cond"] * probs[mat]["prob"]
    
    to_sum = []
    for material in probs:
        if probs[material]["cond"] == None:
            print("[WARN] Some material is not measured. material['cond'] = None")
            return None
        to_sum.append(probs[material]["cond"] * probs[material]["prob"])
    denominator = sum(to_sum)
    # TODO is this epsilong thingy right?
    # if eps:
    #         if probs[mat]["cond"] < eps:
    #             query = 0
    #         else:
    #             query = nominator/denominator
    # else:
    #     query = nominator/denominator
    query = nominator/denominator

    return query

def update_belief_disc(probs, eps=None, vb=True):
    temp_probs = copy.deepcopy(probs)
    longest_str = 0
    for material in probs:
        if len(material) > longest_str:
            longest_str = len(material)
    if vb:
        print()
        print("CLOSED-FORM solution via value ----------------------------------------")
    for material in probs:
        temp_probs[material]["prob"] = bayes_update(material, probs, eps=eps)
        padded_mat_name = material.rjust(longest_str)
        if vb:
            print("Material: {} | Probabilty: {:.4f} | Cond. likelihood: {:.8f}".format(padded_mat_name, temp_probs[material]["prob"], temp_probs[material]["cond"]))

    return temp_probs

def posterior_meas_dist(axis, ground, probs, meas_sigma, property):
    """
    Creates a multimodal distrubution of priors (on 'axis', based on data from 'ground'),
    for property type 'prop_type', widened by 'meas_sigma'. The multimodal dist is created
    as a sum of modes, multiplied by the probability from the 'probs' corresponding to the material

    [In]:  axis       > type:numpy.array, array of linspaced points
           ground     > type:dict, dictionary with all known materials
           probs      > type:dict (nested), dictionary as follows:
                       {'material': {"prob":prob_num, "cond":cond_num}, ...}
           meas_sigma > type:int, sigma (error) of the measurement
           property  > type:str, name of the queried property

    [Out]: y_prior_dist_with_error > type:numpy.array, array of pdf values
    """
    y_prior_dist_with_error = None
    materials = ground["materials"]
    for mat in materials:
        try:
            mat_sigma = materials[mat][property]["sigma"]
            new_mat_sigma = np.sqrt(mat_sigma**2 + meas_sigma**2)
            mat_mean = materials[mat][property]["mean"]
            y_local = norm.pdf(axis, mat_mean, new_mat_sigma) * probs[mat]["prob"]
        except TypeError:
            y_local = uniform(axis) * probs[mat]["prob"]
        if y_prior_dist_with_error is None:
            y_prior_dist_with_error = y_local
        else:
            y_prior_dist_with_error += y_local
    
    y_prior_dist_with_error = y_prior_dist_with_error / sum(y_prior_dist_with_error)
    return y_prior_dist_with_error

def posterior_meas_dist_sim(axis, ref, probs, meas_sigma, property):
    """
    Creates a multimodal distrubution of priors (on 'axis', based on data from 'ground'),
    for property type 'prop_type', widened by 'meas_sigma'. The multimodal dist is created
    as a sum of modes, multiplied by the probability from the 'probs' corresponding to the material

    [In]:  axis       > type:numpy.array, array of linspaced points
           ref        > type:dict, dictionary with all known materials
           probs      > type:dict (nested), dictionary as follows:
                       {'material': {"prob":prob_num, "cond":cond_num}, ...}
           meas_sigma > type:int, sigma (error) of the measurement
           property  > type:str, name of the queried property

    [Out]: y_prior_dist_with_error > type:numpy.array, array of pdf values
    """
    y_prior_dist_with_error = None
    # if offset is not None:
    #     ofst = offset[property]
    # else:
    #     ofst = 0

    materials = ref['materials'].items()
    for mat in materials:
        name = mat[0]
        props = mat[1]
        try:
            mat_sigma = props[property]["sigma"]
            new_mat_sigma = np.sqrt(mat_sigma**2 + meas_sigma**2)
            mat_mean = props[property]["mean"]
            y_local = norm.pdf(axis, mat_mean, new_mat_sigma) * probs[name]["prob"]
        except TypeError:
            y_local = uniform(axis) * probs[name]["prob"]
        if y_prior_dist_with_error is None:
            y_prior_dist_with_error = y_local
        else:
            y_prior_dist_with_error += y_local
    
    y_prior_dist_with_error = y_prior_dist_with_error / sum(y_prior_dist_with_error)
    return y_prior_dist_with_error

def compute_entropy(probs, eps=0):
    ent = 0
    for material in probs:
        temp_prob = probs[material]['prob']
        if temp_prob <= eps:
            ent += 0
        else:
            ent += temp_prob * np.log2(temp_prob)
    
    return -ent

def sampler(axis,dist,num_of_samples):
    last_ax_elem = axis[-1]
    xs = []
    ys = [] 
    for i in range(num_of_samples):
        ptx = np.random.choice(axis,p=dist/sum(dist))
        if ptx == last_ax_elem:  # could cause overflow
            ptx = last_ax_elem - 1  
        pty = dist[int(np.floor(ptx))]        
        xs.append(ptx)
        ys.append(pty)
    return xs,ys

def uniform(axis):
    """
    Custom uniform distribution.
    """
    return np.ones(len(axis))*1/(len(axis))

def create_prior(ground_dict, probs, ax, property, offsets):
    y_dist=None
    materials = ground_dict["materials"]
    # the GMM

    ofst = offsets[property]
    for mat in materials:
        try:
            mat_mean = materials[mat][property]["mean"] + ofst
            mat_sigma = materials[mat][property]["sigma"]
            y_mat = norm.pdf(ax, mat_mean, mat_sigma)
        except TypeError:
            y_mat = uniform(ax)
        if y_dist is None:
            y_dist = y_mat * probs[mat]['prob'] 
        else:
            y_dist += y_mat * probs[mat]['prob'] 

    y_dist = y_dist / sum(y_dist)
    return y_dist

def create_prior_sim(ref, probs, ax, property, ):
    y_dist=None
    materials = ref['materials'].items()
    # the GMM
    for mat in materials:
        name = mat[0]
        props = mat[1]
        try:
            mat_mean = props[property]["mean"]
            mat_sigma = props[property]["sigma"]
            y_mat = norm.pdf(ax, mat_mean, mat_sigma)
        except TypeError:
            y_mat = uniform(ax)
        if y_dist is None:
            y_dist = y_mat * probs[name]['prob'] 
        else:
            y_dist += y_mat * probs[name]['prob'] 

    y_dist = y_dist / sum(y_dist)
    return y_dist


#### PLOT FUNCTIONS ####
def PMF_plot(probs, ax=None, ground=None, property=None, square=False):
    txt_len = 4
    name_prob_pairs = []
    if square:
        plt.figure(figsize=(6,6))
    else:
        plt.figure(figsize=(6,3))
    
    plt.xticks(fontsize=11)
    plt.yticks(fontsize=11)
    for mat in probs:
        if ground is not None:
            if property is not None:
                mat_mean = ground['materials'][mat][property]['mean']
                name_prob_pairs.append((mat_mean, mat, probs[mat]['prob']))
                
            else:
                print("[ERROR] ground dictionary provided, but not property type!")
                return None
        else:
            name_prob_pairs.append((None, mat, probs[mat]['prob']))

    if name_prob_pairs[0][0] is not None:
        d = sorted(name_prob_pairs)
        xs = []
        labels = []
        ys = []
        for tup in d:
            xs.append(tup[0])
            ys.append(tup[2])
            if len(tup[1]) > 5:
                labels.append(tup[1][0:txt_len].capitalize() + ".")
            else:
                labels.append(tup[1].capitalize())
            plt.vlines(tup[0],0,tup[2], color="tab:blue", lw=2)
            plt.plot(tup[0], tup[2], label=mat, marker='o', markersize=10, color="tab:blue")
            # plt.hlines(tup[2], 0, tup[0], color='gray', ls="--", dashes=(0,(5,10)), lw=0.5)

        ys.append(1)
        plt.yticks(ticks=ys)
        # plt.xticks(ticks=xs,labels=labels)
        plt.xticks( xs, labels )
        plt.grid(ls="--", dashes=(2,5), axis='y')
        # print("This is ax: ", ax)
        plt.xlim(0,len(ax))
        plt.ylim(0,1.05)
        
    
    else:
        xs= []
        labels = []
        ys = []
        # divide the axis between the materials
        spacing = 30
        offset = 30  # offset from zero
        max_x=0
        for i,tup in enumerate(name_prob_pairs):
            if len(tup[1]) > 5:
                labels.append(tup[1][0:txt_len].capitalize() + ".")
            else:
                labels.append(tup[1].capitalize())
            ys.append(tup[2])
            x = i*spacing + offset
            xs.append(x)
            plt.vlines(x, 0, tup[2], color="tab:blue", lw=2)
            plt.plot(x, tup[2], label=mat, marker='o', markersize=10, color="tab:blue")
            # plt.hlines(tup[2], 0, x, color='gray', ls="--", dashes=(0,(5,10)), lw=0.5)
            max_x=x
        
        ys.append(1)

        plt.xticks( xs, labels )
        # plt.xticks(ticks=xs,labels=labels)  # because of PREHISTORIC matplotlib v 2.1.1 for python 2.7.2
        
        # plt.yticks(ticks=ys)
        plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
        plt.ylim(0,1.05)
        plt.xlim(0,max_x+offset)
        plt.grid(ls="--", dashes=(2,5), axis='y')
        
    
    plt.xlabel("Materials")
    plt.ylabel(r"$p(x)$")
    plt.savefig(fname="PMF.pdf", format="pdf")
    plt.show()

def plot_ground(ground, add_mean=None, add_sigma=None, add_name=None, mat_type=None, ax=None):
    """
    Function to plot gaussians of elasticity and density.
    Uses scipy

    [In] : ground    > type:dict, made by ground_truth.py
           add_mean  > type:int, mean of additional gaussian
           add_sigma > type:int, sigma of additional gaussian
           add_name  > type:str, name of additional gaussian
           ax        > type:tuple, hardcodes linspace settings ax = (axmin, axmax, axnum)

    [Out]: plt       > type:matplotlib.pyplot module, a figure with [r x 1] subplots
           including all materials from dict (r = num of properties)
    """
    means = []
    sigmas = []
    units = None
    x_axs = []
    plt.figure()
    for i,property in enumerate(ground["materials"]["steel"]):
        means = []
        sigmas = []
        units = ground["materials"]["steel"][property]["unit"]
        names = []
        for material in ground["materials"]:
            temp_mean = ground["materials"][material][property]["mean"]
            temp_sigma = ground["materials"][material][property]["sigma"]
            means.append(temp_mean)
            sigmas.append(temp_sigma)
            names.append(material)
        
        # set linspace based on smallest and biggest mean
        if not ax:
            x = np.linspace(-300, max(means)+1000, 50000)
        else:
            x = np.linspace(ax[0], ax[1], ax[2])
        x_axs.append(x)
        sbplt = int("{}1{}".format(len(ground["materials"]["steel"]),i+1))
        plt.subplot(sbplt)
        plt.axvline(x=0, color="black")
        for n in range(len(means)):
            y = norm.pdf(x, means[n], sigmas[n])
            plt.plot(x,y, label=names[n])
        if add_mean is not None:
            if mat_type is None or mat_type == property:
                y = norm.pdf(x, add_mean, add_sigma)
                plt.plot(x,y, color = "black", ls="--", label=str(add_name))
            
        
        plt.legend()
        label = r"{a} [${b}$]".format(a=property, b=units)
        plt.xlabel(label)
    # plt.show()

    return plt, x_axs


#### HELPER PRINT FUNCTIONS ####
def what(property, meas_sigma):
    print("_____________________")
    print("Property: ", property.capitalize())
    print("Measurement:")
    print("    Meas. sigma:", meas_sigma)
    print("_____________________")
    print()

def print_PMF(probs):
    longest_str = 0
    for material in probs:
        if len(material) > longest_str:
            longest_str = len(material)

    print("-----------------Current PMF-----------------")
    for material in probs:
        padded_mat_name = material.rjust(longest_str)
        if probs[material]["cond"] is None:
            print("Material: {} | Probabilty: {:.4f} | Cond. likelihood: None".format(padded_mat_name, probs[material]["prob"]))    
        else:
            print("Material: {} | Probabilty: {:.4f} | Cond. likelihood: {:.8f}".format(padded_mat_name, probs[material]["prob"], probs[material]["cond"]))
    print("---------------------------------------------")


# check_or_create("item1.json")
# check_or_create("meas.json")
# ground_dict = json2dict("./ground.json")