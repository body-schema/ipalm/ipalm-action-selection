import json
import numpy as np
import utility as ut
import matplotlib.pyplot as plt
from scipy.stats import norm, gaussian_kde
from sklearn.mixture import GaussianMixture, BayesianGaussianMixture
import pymc3 as pm

ax_volume = np.linspace(0, 200, 10000)  # [cm^3]
ax_mass = np.linspace(0, 100, 100000)  # [g]
ax_density = np.linspace(0, 3, 10000) # [g/cm^3]

# uniform distribution
y_mass_uni = ut.uniform(ax_mass)

volume = {"A": {"mean": 46.592,
                "sigma": 10},
          "B": {"mean": 80,
                "sigma": 15},
          "C": {"mean": 120,
                "sigma": 10}
          }  # in [cm^3]

# densities: 4000, 8000, 12000
mass = {"A": {'mean': 39.24, "sigma": 0.006},
        "B": {'mean': 55, "sigma": 0.008},
        "C": {'mean': volume["C"]["mean"]*120, 'sigma': volume["C"]["sigma"]*120}}
print(mass)

materials = ['A', 'B', 'C']

probs_vol = {'A': 0.333,
             'B': 0.333,
             'C': 0.333}
probs_mass = {'A': 1,
             'B': 0,
             'C': 0}

for i, mat in enumerate(materials):
    if i == 0:
        y_vol = norm.pdf(ax_volume, volume[mat]["mean"], volume[mat]["sigma"]) * probs_vol[mat]
        y_mass = norm.pdf(ax_mass, mass[mat]["mean"], mass[mat]["sigma"]) * probs_mass[mat]
    else:
        y_vol += norm.pdf(ax_volume, volume[mat]["mean"], volume[mat]["sigma"]) * probs_vol[mat]
        y_mass += norm.pdf(ax_mass, mass[mat]["mean"], mass[mat]["sigma"]) * probs_mass[mat]

y_vol = y_vol / sum(y_vol)
y_mass = y_mass / sum(y_mass)

# plt.plot(ax_mass, y_mass, color="tab:blue", label="Unknown Mass", lw=2)
# plt.plot(ax_volume, y_vol, color="tab:red", label="Known Volume", lw=2)
# # plt.plot(ax, y_dens, color="tab:orange", label="Resulting Density", lw=3, ls="--")
# # plt.ylim(0,0.1)
# plt.legend()
# plt.show()

mass_samples = np.array(ut.sampler(ax_mass, y_mass, 10000)).reshape(-1, 1)
volume_samples = np.array(ut.sampler(ax_volume, y_vol, 10000)).reshape(-1, 1)
dens_samples = mass_samples / volume_samples

log_mass = np.log(mass_samples)
log_volume = np.log(volume_samples)
log_dens = log_mass / log_volume

# mass_samples = np.hstack([mass_samples, np.zeros([np.shape(mass_samples)[0],1])])
# volume_samples = np.hstack([volume_samples, np.zeros([np.shape(volume_samples)[0],1])])
# dens_samples = np.hstack([dens_samples, np.zeros([np.shape(dens_samples)[0],1])])

# plt.scatter(mass_samples, np.zeros([np.size(mass_samples),]), color="tab:blue", label="Mass")
# plt.scatter(volume_samples, np.zeros([np.size(mass_samples),]), color="tab:orange", label="Volume")
# plt.scatter(dens_samples, np.zeros([np.size(mass_samples),]), color="tab:purple", label="Density")
# plt.legend()
# plt.show()

# plt.figure()
# plt.hist(mass_samples, bins=500, label="Mass")
# plt.xlabel("Mass [g]")
# plt.ylabel("Count [-]")
# plt.legend()
# plt.tight_layout()
# plt.show()

# plt.figure()
# plt.hist(volume_samples, bins=500, label="Volume")
# plt.xlabel(r"Volume [$cm^3$]")
# plt.ylabel("Count [-]")
# plt.legend()
# plt.tight_layout()
# plt.show()

bgm = BayesianGaussianMixture(n_components=4, random_state=42).fit(dens_samples)
print("Mixture Means: ", bgm.means_)
print("Mixture Weights: ", bgm.weights_)
print("Precisions: ", bgm.mean_precision_)

for i, mean in enumerate(bgm.means_):
    if bgm.weights_[i] <= 0.05:
        continue
    print("i: {}, mean: {}, weight: {}, precision: {} and STD: {}".format(i, bgm.means_[i], bgm.weights_[i],
          bgm.precisions_[i], np.sqrt(1 / bgm.precisions_[i])))
    if i == 0:
        y_fit = norm.pdf(ax_density, mean, np.sqrt(1 / bgm.precisions_[i]))[0] * bgm.weights_[i]
    else:
        y_fit += norm.pdf(ax_density, mean, np.sqrt(1 / bgm.precisions_[i]))[0] * bgm.weights_[i]

y_fit = y_fit / sum(y_fit) * 10
print("y_fit[0:30]: ", y_fit[0:30])
print("y_fit: ", y_fit)
print("np.shape(y_fit): ", np.shape(y_fit))

weights = np.ones_like(dens_samples)/float(len(dens_samples))

plt.figure()
plt.hist(dens_samples, bins=1000, label="Density", weights=weights)
plt.plot(ax_density, y_fit, color="tab:orange", lw=2, label="Fit")
plt.xlabel(r"Density [$g/cm^3$]")
plt.ylabel("Count [-]")
plt.legend()
plt.tight_layout()
plt.show()


