Cleaning the previous 'meas.json' file.
----------------------
| ITERATION NUMBER: 0 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.2000 | Cond. likelihood: None
Material:   ceramic | Probabilty: 0.2000 | Cond. likelihood: None
Material:   plastic | Probabilty: 0.2000 | Cond. likelihood: None
Material: aluminium | Probabilty: 0.2000 | Cond. likelihood: None
Material:     joker | Probabilty: 0.2000 | Cond. likelihood: None
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 12.03286228523066 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 2.5   |
| The average sampled Entropy is: 0.9161330299770795   |
| -OUT- Discrete Information Gain: 1.5838669700229207   |
| -info- AVERAGE sampled Information Gain: 1.5838669700229204 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 12.03286228523066 |
| This is the Emulated Entropy for density: 11.00726463807476 |
| Argmax mean: 2302 (no offset)|
| -x- Diff Information Gain: 1.0255976471558998     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 2.5   |
| The average sampled Entropy is: 1.4017134630102124   |
| -OUT- Discrete Information Gain: 1.0982865369897876   |
| -info- AVERAGE sampled Information Gain: 1.0982865369897878 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
| This is the Emulated Entropy for elasticity: 7.344186158558585 |
| Argmax mean: 29 (no offset)|
| -x- Diff Information Gain: 0.6045627505276103     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  1.5838669700229207
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2697 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1401 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0006 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.2956 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.5636 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 1 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1401 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0006 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.2956 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.5636 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.671075678416331 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.3901060621875785   |
| The average sampled Entropy is: 0.6650703829499222   |
| -OUT- Discrete Information Gain: 0.7250356792376563   |
| -info- AVERAGE sampled Information Gain: 0.7250356792376563 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.671075678416331 |
| This is the Emulated Entropy for density: 9.565458737595518 |
| Argmax mean: 2316 (no offset)|
| -x- Diff Information Gain: 0.10561694082081274     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.3901060621875785   |
| The average sampled Entropy is: 0.9084104907937486   |
| -OUT- Discrete Information Gain: 0.4816955713938299   |
| -info- AVERAGE sampled Information Gain: 0.4816955713938302 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
| This is the Emulated Entropy for elasticity: 7.344186158558585 |
| Argmax mean: 29 (no offset)|
| -x- Diff Information Gain: 0.6045627505276103     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.7250356792376563
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2706 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0462 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.2058 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.7480 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 2 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0462 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.2058 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.7480 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.516726786666963 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.987788701034557   |
| The average sampled Entropy is: 0.4489256081351428   |
| -OUT- Discrete Information Gain: 0.5388630928994141   |
| -info- AVERAGE sampled Information Gain: 0.5388630928994145 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.516726786666963 |
| This is the Emulated Entropy for density: 9.39152833123676 |
| Argmax mean: 2698 (no offset)|
| -x- Diff Information Gain: 0.12519845543020303     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.987788701034557   |
| The average sampled Entropy is: 0.7081762764942127   |
| -OUT- Discrete Information Gain: 0.2796124245403443   |
| -info- AVERAGE sampled Information Gain: 0.2796124245403443 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
| This is the Emulated Entropy for elasticity: 7.344186158558585 |
| Argmax mean: 29 (no offset)|
| -x- Diff Information Gain: 0.6045627505276103     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.5388630928994141
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2724 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0133 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.1244 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.8623 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 3 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0133 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.1244 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.8623 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.309911354938958 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.6411083542411405   |
| The average sampled Entropy is: 0.2466667096729748   |
| -OUT- Discrete Information Gain: 0.3944416445681658   |
| -info- AVERAGE sampled Information Gain: 0.3944416445681658 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.309911354938958 |
| This is the Emulated Entropy for density: 9.161199565925674 |
| Argmax mean: 2702 (no offset)|
| -x- Diff Information Gain: 0.14871178901328364     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.6411083542411405   |
| The average sampled Entropy is: 0.47645081669267164   |
| -OUT- Discrete Information Gain: 0.1646575375484689   |
| -info- AVERAGE sampled Information Gain: 0.16465753754846882 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
| This is the Emulated Entropy for elasticity: 7.344186158558585 |
| Argmax mean: 29 (no offset)|
| -x- Diff Information Gain: 0.6045627505276103     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.3944416445681658
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2688 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0035 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.0701 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.9263 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 4 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0035 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.0701 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.9263 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.108279735976026 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.39993683965476273   |
| The average sampled Entropy is: 0.20655010472538804   |
| -OUT- Discrete Information Gain: 0.1933867349293747   |
| -info- AVERAGE sampled Information Gain: 0.19338673492937464 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.108279735976026 |
| This is the Emulated Entropy for density: 8.969336722991347 |
| Argmax mean: 2703 (no offset)|
| -x- Diff Information Gain: 0.13894301298467937     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.39993683965476273   |
| The average sampled Entropy is: 0.3173267805527402   |
| -OUT- Discrete Information Gain: 0.08261005910202252   |
| -info- AVERAGE sampled Information Gain: 0.08261005910202243 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
| This is the Emulated Entropy for elasticity: 7.344186158558585 |
| Argmax mean: 29 (no offset)|
| -x- Diff Information Gain: 0.6045627505276103     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.1933867349293747
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2685 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0009 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.0382 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.9609 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 5 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0009 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.0382 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.9609 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 8.93900473940061 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.244261246550304   |
| The average sampled Entropy is: 0.12162845544874239   |
| -OUT- Discrete Information Gain: 0.12263279110156163   |
| -info- AVERAGE sampled Information Gain: 0.12263279110156161 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 8.93900473940061 |
| This is the Emulated Entropy for density: 8.821311878537683 |
| Argmax mean: 2704 (no offset)|
| -x- Diff Information Gain: 0.11769286086292752     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.244261246550304   |
| The average sampled Entropy is: 0.1581362924445169   |
| -OUT- Discrete Information Gain: 0.0861249541057871   |
| -info- AVERAGE sampled Information Gain: 0.08612495410578719 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
| This is the Emulated Entropy for elasticity: 7.344186158558585 |
| Argmax mean: 29 (no offset)|
| -x- Diff Information Gain: 0.6045627505276103     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.12263279110156163
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2691 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0002 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.0204 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.9794 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 6 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0002 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.0204 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.9794 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 8.803077327114929 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.14679578589759712   |
| The average sampled Entropy is: 0.05468462329835559   |
| -OUT- Discrete Information Gain: 0.09211116259924153   |
| -info- AVERAGE sampled Information Gain: 0.09211116259924144 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 8.803077327114929 |
| This is the Emulated Entropy for density: 8.705478305826107 |
| Argmax mean: 2704 (no offset)|
| -x- Diff Information Gain: 0.09759902128882203     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.14679578589759712   |
| The average sampled Entropy is: 0.11585424990914152   |
| -OUT- Discrete Information Gain: 0.0309415359884556   |
| -info- AVERAGE sampled Information Gain: 0.030941535988455546 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
| This is the Emulated Entropy for elasticity: 7.344186158558585 |
| Argmax mean: 29 (no offset)|
| -x- Diff Information Gain: 0.6045627505276103     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.09211116259924153
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2692 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0001 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.0108 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.9891 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 7 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0001 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.0108 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.9891 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 8.693514038227576 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.08699550057019069   |
| The average sampled Entropy is: 0.04838578033435134   |
| -OUT- Discrete Information Gain: 0.03860972023583935   |
| -info- AVERAGE sampled Information Gain: 0.038609720235839334 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 8.693514038227576 |
| This is the Emulated Entropy for density: 8.611620356785647 |
| Argmax mean: 2705 (no offset)|
| -x- Diff Information Gain: 0.08189368144192954     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.08699550057019069   |
| The average sampled Entropy is: 0.0675225061864484   |
| -OUT- Discrete Information Gain: 0.019472994383742295   |
| -info- AVERAGE sampled Information Gain: 0.019472994383742302 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
| This is the Emulated Entropy for elasticity: 7.344186158558585 |
| Argmax mean: 29 (no offset)|
| -x- Diff Information Gain: 0.6045627505276103     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.03860972023583935
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2692 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0000 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.0057 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.9943 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 8 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0000 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.0057 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.9943 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 8.603103244868876 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.05093228253094088   |
| The average sampled Entropy is: 0.04703670638697375   |
| -OUT- Discrete Information Gain: 0.0038955761439671277   |
| -info- AVERAGE sampled Information Gain: 0.003895576143967112 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 8.603103244868876 |
| This is the Emulated Entropy for density: 8.532810602350809 |
| Argmax mean: 2705 (no offset)|
| -x- Diff Information Gain: 0.07029264251806744     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.05093228253094088   |
| The average sampled Entropy is: 0.040134925231507974   |
| -OUT- Discrete Information Gain: 0.010797357299432903   |
| -info- AVERAGE sampled Information Gain: 0.010797357299432892 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
| This is the Emulated Entropy for elasticity: 7.344186158558585 |
| Argmax mean: 29 (no offset)|
| -x- Diff Information Gain: 0.6045627505276103     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.010797357299432903
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 166 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0000 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.0283 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.9716 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 9 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0000 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.0283 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.9716 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 8.603103244868876 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.186597646170271   |
| The average sampled Entropy is: 0.09943120006183051   |
| -OUT- Discrete Information Gain: 0.08716644610844049   |
| -info- AVERAGE sampled Information Gain: 0.0871664461084405 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 8.603103244868876 |
| This is the Emulated Entropy for density: 8.532810602350809 |
| Argmax mean: 2705 (no offset)|
| -x- Diff Information Gain: 0.07029264251806744     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.722276465519388 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.186597646170271   |
| The average sampled Entropy is: 0.11868131740464477   |
| -OUT- Discrete Information Gain: 0.06791632876562623   |
| -info- AVERAGE sampled Information Gain: 0.06791632876562614 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.722276465519388 |
| This is the Emulated Entropy for elasticity: 6.521772869758855 |
| Argmax mean: 67 (no offset)|
| -x- Diff Information Gain: 0.20050359576053367     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.08716644610844049
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2709 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0000 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.0151 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.9849 | Cond. likelihood: 0.00217391
MODE 1 was chosen --> optimizing was done for discrete information gain 
 computed from the PMF.

