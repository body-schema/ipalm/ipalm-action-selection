from matplotlib.axes import Axes
from matplotlib.lines import Line2D
from scipy.integrate import quad #using quadpack from FORTRAN lib
import matplotlib.pyplot as plt
import numpy as np 
import json
from scipy.stats import norm  
# from scipy.stats import uniform
import csv
import os  
import copy

# path = "measurement_logs/"
path=""
tim = "time11_05_23-39-11"
pos = "position11_05_23-39-11"
eff = "effort11_05_23-39-11"

log = {}

file_name = path + tim + ".csv"
with open(file_name, mode='r') as csv_file:
    csv_reader = csv.reader(csv_file)
    time = []
    for line in csv_reader:
        if line[0] == "0.000000000000000000e+00":
            continue
        time.append(float(line[0]))
    log["time"] = time

file_name = path + pos + ".csv"
with open(file_name, mode='r') as csv_file:
    csv_reader = csv.reader(csv_file)
    pos = []
    for line in csv_reader:
        if line[0] == "0.000000000000000000e+00":
            continue
        pos.append(float(line[0]))
    log["pos"] = pos

file_name = path + eff + ".csv"
with open(file_name, mode='r') as csv_file:
    csv_reader = csv.reader(csv_file)
    eff = []
    for line in csv_reader:
        if line[0] == "0.000000000000000000e+00":
            continue
        eff.append(float(line[0]))
    log["eff"] = eff

# kinova-like piston
PLATE_HEIGHT = 0.038
PLATE_WIDTH = 0.022

CUBE_SIDE = 0.047  # measured in rviz
# CUBE_SIDE = 0.056  # computed from XML file

PLATE_DEPTH = 0.01
OFFSET = PLATE_DEPTH/2
AREA = PLATE_HEIGHT * PLATE_WIDTH
# size = CUBE_SIDE

# now compute strain
t = np.array(log['time'])
d = np.array(log['pos'])
F = np.array(log['eff'])

deltas = np.zeros(len(F))

# PLATE SIZE: 0.01 x 0.06 x 0.06 m
# CUBE SIZE: should be 0.06 x 0.06 x 0.06 m
#   but in MuJoCo counts = 10, spacing = 0.0065
#   counts - 10 capsules spaced by 0.0065 for one side 
#   this means that the cube is slightly larger when there is no gravitational pull
#   after the simulation is started, the cube is squished a little bit to the size 0.06

# check when the forces are non zero -> moment of contact, start computing strain
# take one time step after, compensation for the first contact with the sensor
# non_zeros = [ np.nonzero(F1)[0][1], np.nonzero(F2)[0][1]] 
# refs = [d1[non_zeros[0]], d2[non_zeros[1]]]

# strain1 = []
# print("size: ", size)
# F1_cut = []

# # compute strain as dL/L, where dL is the length difference and L is the base length 
# for idx in range(non_zeros[0],len(F1)):
#     value = (size - d1[idx]) / size
#     F1_cut.append(F1[idx])
#     strain1.append(value) 

# for idx in range(non_zeros[1],len(F2)):
#     strain2[idx] = (size - d2[idx]) / size

# for idx in range(non_zeros[1],len(F2)):
#     nominator = size - (abs(d1[idx]) + abs(d2[idx]))
#     deltas[idx] =  nominator / size

stress = F / AREA / 1000  # kPa

# negative = np.where(deltas<0)[0][0]


# print(deltas)




# first_nonzero_idx = np.nonzero()[0][0]
x1 = 0
y1 = 0
# x2 = deltas[np.argmax(F_together)]
x2 = np.max(deltas)
y2 = np.max(stress)

m = (y2-y1) / (x2-x1)

plt.subplot(311)
plt.xlabel("Time [s]")
plt.ylabel("Finger distance [m]")
plt.plot(t,d, label='Position', lw=2, color="tab:blue")
# plt.plot(log['time'],log['distance2'], label='Position R',lw=2, ls="-.", color="tab:orange")
plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
plt.legend()
# plt.title(name)

plt.subplot(312)
plt.xlabel("Time [s]")
plt.ylabel("Force [N]")
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
plt.plot(t,F, label='Force',lw=2,  color="tab:blue")
# plt.plot(log['time'],log['sensordata4'], label='Force Sensor R',lw=2, ls="-.", color="tab:orange")

plt.legend()

# plt.subplot(313)
# plt.xlabel("Strain [-]")
# plt.ylabel("Stress [kPa]")
# plt.xticks(fontsize=11)
# plt.yticks(fontsize=11)
# plt.scatter(deltas[20:negative], stress[20:negative], label = "Elasticity", s=1, color="tab:blue")
# plt.plot([x1,x2],[y1,y2], label="Modulus = {:.0f} [kPa]".format(m), lw=2, ls="--", color="tab:red")
# plt.ticklabel_format(axis="x", style="sci", scilimits=(0,0))
# plt.xlim(xmin=-0.1)
# plt.legend()
# # plt.savefig(fname=path + name + ".png", format="png")
plt.show()
