from __future__ import print_function
import os
import json
import numpy as np
from scipy.stats import norm  
import matplotlib.pyplot as plt

import utils
import entropy_sim

# nice colors
nice = ['#992600', '#997300', '#269900', '#009999', '#000099', '#730099', '#99004d']

# print(os.getcwd())
path = os.getcwd()
complete_path = path + "/ipalm_master/"

ref = utils.json2dict(complete_path + "./reference.json")
# print(ref)
orig_probs = utils.assign_equal_probs(ref, joker=True)
# orig_probs["A"]["prob"] = 0.7
# orig_probs["B"]["prob"] = 0.3
orig_probs["A"]["prob"] = 0.27
orig_probs["B"]["prob"] = 0.7
orig_probs["joker"]["prob"] = 0.03
# orig_probs["A"]["prob"] = 0.33
# orig_probs["B"]["prob"] = 0.33
# orig_probs["joker"]["prob"] = 0.33
print(orig_probs)
meas_sigma = 500

x = np.linspace(0,10000,100000)
rng = x[-1]-x[0]
bin_size = rng/len(x)
print("bin size is: ", bin_size)

materials = ref['materials'].items()
print(len(materials[0][1]))

# plot reference
plt.figure(figsize=(7,3))
for i,prop in enumerate(materials[0][1]):
    sbplt = 210 + (i+1)
    plt.subplot(sbplt)
    y_all = 0
    for j,mat in enumerate(materials):
        name = "Material " + mat[0].capitalize() 
        props = mat[1]

        mean = props[prop]["mean"]
        sigma = props[prop]["sigma"]
        sigma_w = np.sqrt(sigma**2 + meas_sigma**2)
        y = norm.pdf(x, mean, sigma)
        y_wide = norm.pdf(x, mean, sigma_w)
        y_all += y * orig_probs[mat[0]]["prob"]

        units= materials[0][1][prop]['unit']
        label = r"{a} [${b}$]".format(a=prop.capitalize(), b=units)
        plt.xlabel(label)
        plt.ylabel(r"$p(x)$")
        plt.plot(x,y,label=name, color=nice[j + i*2], lw=2)
        # plt.plot(x,y_wide, color="grey", ls="--", lw=2)
        
        plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
        plt.xticks(fontsize=11)
        plt.yticks(fontsize=11)
    plt.plot(x, y_all, color="tab:red", ls=":", lw=2, label="Mix")
    plt.legend()
plt.show()



# plot property beliefs
for prop in materials[0][1]:
    plt.figure(figsize=(6,3))
    y_final = None
    for mat in materials:
        name = mat[0]
        props = mat[1]
        try:
            mat_mean = props[prop]["mean"]
            mat_sigma = props[prop]["sigma"]
            y_mat = norm.pdf(x, mat_mean, mat_sigma)
        except TypeError:
            print("ADDING UNIFORM")
            y_mat = utils.uniform(x)
        if y_final is None:
            y_final = y_mat * orig_probs[name]['prob'] 
        else:
            y_final += y_mat * orig_probs[name]['prob'] 
   
    y_final = y_final / sum(y_final)
    # print("Sum via normalization: ", sum(y_final))
    plt.plot(x,y_final, color="tab:blue", lw=2)

    units= materials[0][1][prop]['unit']
    label = r"{a} [${b}$]".format(a=prop.capitalize(), b=units)
    plt.xlabel(label)
    plt.ylabel(r"$p(x)$")
    plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    plt.xticks(fontsize=11)
    plt.yticks(fontsize=11)
    plt.show()
