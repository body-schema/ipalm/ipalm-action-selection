import os
import numpy as np
import pymc3 as pm
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
import seaborn as sns
import arviz as az
from scipy.stats import norm, uniform
from matplotlib import cm
from collections import OrderedDict

from scipy.interpolate import splev, splrep

grams = np.load('../spectrograms/inputYCB.npy')
labels = np.load('../spectrograms/targetYCB.npy')

x = np.zeros(128)
y = np.zeros(128)
x_max = np.zeros(128)
y_max = np.zeros(128)

for idx, r in enumerate(grams[0]):
    y[idx] = np.sum(r)
    y_max[idx] = np.max(r)

for idx, c in enumerate(np.transpose(grams[0])):
    x[idx] = np.sum(c)
    x_max[idx] = np.max(c)
support = np.linspace(0, 128, 128)

# plt.scatter(support, x, label='x-axis marg')
# plt.plot(support, x_max, label='x-axis MAX')
# plt.legend()
# plt.show()

# plt.scatter(support, y, label='y-axis marg')
# plt.plot(support, y_max, label='y-axis MAX')
# plt.legend()
# plt.show()

# MU = np.array(62.5, 187.5, 312.5, 437.5, 562.5, 687.5, 812.5, 937.5, 1062.5, 1875, 3125, 4375, 7500, 12500, 17500)
size = 8

# for ITERS in range(1):
y /= sum(y)
ITERS = 5000

# color = iter(cm.rainbow(np.linspace(0, 1, 21)))
color = iter(cm.winter(np.linspace(0, 1, int(10050.0/50.0))))

mus = np.linspace(10, 118, 10)
# mus = np.array(norm.rvs(loc=64, scale=200, size=20))
ks = np.array(uniform.rvs(1, 5, size=10))
# for ITERS in range(50, 10050, 50):
fig, ax1 = plt.subplots()
for n in range(10):

    with pm.Model() as model:
        w = pm.Dirichlet("w", np.ones(size))
        mu = pm.Normal("mu", 0, 100.0, shape=size)
        sigma = pm.Gamma("sigs", 4.0, 1.0, shape=size)

        x_obs = pm.NormalMixture('x_obs', w, mu=mu, sigma=sigma, observed=y)
        start = pm.find_MAP()
        step = pm.Metropolis()
        # trace = pm.sample(5000, start=start, step=step, n_init=10000, tune=1000)
        trace = pm.sample(ITERS, start=start, step=step, tune=100)
        ppc_trace = pm.sample_posterior_predictive(trace)

        idata_pymc3 = az.from_pymc3(trace, posterior_predictive=ppc_trace)
        # print(idata_pymc3)
        # vars = [[], [], []]
        vars = {}
        print(idata_pymc3['posterior']['mu'][0])
        for var in idata_pymc3['posterior']:
            vars[var] = []
            for chain in idata_pymc3['posterior'][var]:
                for sample in chain.transpose():
                    vars[var].append(float(np.mean(sample)))

        weights = vars['w']
        mus = vars['mu']
        sigmas = vars['sigs']

        x = np.linspace(0, 128, 128)
        for idx in range(len(weights)):
            if idx == 0:
                y_gmm = norm.pdf(x, mus[idx], sigmas[idx]) * weights[idx]
            else:
                y_gmm += norm.pdf(x, mus[idx], sigmas[idx]) * weights[idx]

        ax1.plot(x, y_gmm, color=next(color), label="GMM")
        ax1.set_xlabel('X data')
        ax1.set_ylabel('The GMM', color='tab:blue')

        # plt.savefig("spectrograms/spectroGMMfit_{}.png".format(ITERS), dpi=300)
ax2 = ax1.twinx()
ax2.scatter(x, y, label='Observation', color="tab:orange")
ax2.set_ylabel('Observed', color='tab:orange')
plt.legend()
plt.show()

    # print("vars", vars)
    # max_prob_idx = np.argmax(vars['w'])
    # best_mean = vars['mu'][max_prob_idx]
    # best_tau = vars['tau'][max_prob_idx]
    #
    # print("max_prob: ", np.max(vars['w']))
    # print("max_prob_mean: ", best_mean)
    # print("max_prob_tau: ", best_tau)
    # print("max_prob_sigma: ", np.sqrt(1.0/best_tau))

    # az.plot_trace(idata_pymc3, var_names=["w", "mu"])
    # plt.show()

    # pm.traceplot(trace)
    # pm.plot_posterior(trace)
    # pm.autocorrplot(trace)
    # plt.show()
    # probs = []
    # mus = []
    # taus = []
    # for i, samp in enumerate(ppc_trace['w'].transpose()):
    #     probs.append(np.mean(samp))
    #
    # print("ppc_trac['mu'].T: ", ppc_trace['mu'].transpose())
    # for i, samp in enumerate(ppc_trace['mu'].transpose()):
    #     mus.append(np.mean(samp))
    #
    # print("ppc_trac['tau'].T: ", ppc_trace['tau'].transpose())
    # for i, samp in enumerate(ppc_trace['tau'].transpose()):
    #     taus.append(np.mean(samp))
    #
    # idx = np.argmax(probs)
    # print("The best fit gaussian should be: Mean {}, Tau {}, prob: {}".format(mus[idx], taus[idx], probs[idx]))




# sns.jointplot(
#     data=grams[0]
# )


# plt.show()