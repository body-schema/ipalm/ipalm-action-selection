## Terminator
`ctrl + shift + w` - close<br>
`ctrl + shift + e` - open tab on side<br>
`ctrl + shift + o` - open tab on bottom<br>
`alt + arrows` - move between underterminals<br>
`ctrl + z` - stop process<br>
`ctrl + c` - ask process to terminate<br>
`ctrl + \` - kill process<br>
[ubuntu handling processes](https://askubuntu.com/questions/14155/what-to-do-when-ctrl-c-wont-kill-running-job)<br>

## SSH 
### Kinova workstation
`sudo apt install openssh-server` - fail
`ifconfig`
`ip addr`
`dmesg`
`sudo bash`
`poweroff`
`sudo apt install openssh-server`
`vi /etc/ssh//sshd_config`
`sudo bash`
`ip addr`

### Connecting from a remote computer
`ssh -X robot3@192.168.1.250`

### Downloading files from a remote PC via SSH
1. `lsof -i -P -n` - at the bottom of the list is your PC port (something like: 168.1.250:22 (ESTABLISHED))
2. `scp -P your_port remotePCUsername@IPaddress:/path/to/remote/file/ /path/to/local/directory/to/save/the/file` <br>
for example: `scp -P 22 robot3@192.168.1.250:/home/robot3/vision_ws/src/epos_internal/scripts/andreqs.txt ~/Documents/internship2020`

### Uploading files to a remote PC via SSH
Just reverse the local address and remote pc connecting prompt as follows: <br>
1. `scp -P your_port /path/to/local/directory/to/save/the/file remotePCUsername@IPaddress:/path/to/remote/file/` <br>
If you need to upload a whole directory, put a `-r` flag after the port number
<br>

## ROS commands
`rosservice list | grep whatever` - e.g. `gripper`, get services with gripper. Same for `rostopic list` and `rosparam list`.<br>
`rosservice call /manipulator/...` - or `/my_gen3/...` or `/kinova_gen3/...`, calls service<br>
`roslaunch kortex_driver kortex_driver.launch start_rviz:=false` - starts kortex_driver, needs to be done <i>before</i> we can launch other things<br>
`roscore` - is the bare minimum that can be run of ROS<br>
`rostopic info /tf` - gives us info about the topic /tf, transforms containing translation and quaternions<br>
`rostopic echo /tf` - prints everything the topic /tf has to offer, warning: spam<br>
`rosrun tf tf_echo /parent_directory /child_directory` - replace parent and child with whatever you want to extract<br>
`kortex` - shorthand for `roslaunch kortex_driver kortex_driver.launch`<br>
`rospack find pkgname` - `package.xml` defines a package. Find if package "pkgname" package exists. Needed for launch files to find be able to create nodes in the package. Scripts and binaries sometimes need to be runnable `(sudo) chmod +x file`<br>
`rosclean check/purge` - `check` = check how much space logs are occupying | `purge` = delete .ros/log/*<br>

<br>

### Access arm camera
1. Launch kortex - `roslaunch kortex_driver kortex_driver.launch ip_address:=192.168.1.10 start_rviz:=false`
2. Launch kortex vision node - `roslaunch kinova_vision kinova_vision_color_only.launch`
3. View the stream - `rosrun image_view image_view image:=/camera/color/image_raw`
4. Record the stream - `rosrun image_view video_recorder _fps:=30 image:=/camera/color/image_raw`

Pavel's diploma [thesis](https://docs.google.com/document/d/1U4CgaH4Y1qKx_V3urmkvt6lTcuSZTuTDT19_o_ZM23E/edit)

## Building programs
`source devel setup.bash` - needs to be done when we compile new programs using `catkin_make`<br>
`sws` - Petr Svarny's shorthand for the above<br>
`catkin_make` - makes the files<br>
`catkin clean` - cleans devel and runnables, a kind of reset<br>
`ipalm` - does sws and cd ~/vision_ws<br>
<br>
## Python scripts
Just run them as python scripts.<br>
`import ipdb; ipdb.set_trace()` - step-by-step debug from the line where this is <br>
`lsof -i :PORT` - writes out the processes using this address. Useful if a python script using <b>sockets</b> doesn't shut down properly<br>
`kil PID` - kill process by ID<br>
<br>

## PyCharm charms <3
**Collapsing** everything (all folding regions), including comments, functions, classes, your lovelife and so on: <br>
`Ctrl + Shift + NumPad-` <br>

**Expanding** everything (all folding regions), just like Russia did formerly in Czechoslovakia, lately on Krym, and now possibly in Belarus. <br>
`Ctrl + Shift + NumPad+` <br>

sourced from: https://www.jetbrains.com/help/pycharm/code-folding-commands.html

## Conda & python
`deact` - deactivate conda, equivalent to `conda deactivate`<br>
`conda activate epos` - activate venv for the epos module ([original](https://github.com/thodan/epos_internal) and [ipalm](https://gitlab.fel.cvut.cz/body-schema/ipalm/epos_internal_robot3)).<br>
* Default Python version is 2.7.10, which is the one ROS runs on
* Epos and tensorflow 1.12-gpu runs on 3.6

`conda install` - only for venv<br>
`epos` - shortcut for `conda activate epos & cd ~/vision_ws/src/epos_internal/scripts` in ~/.bashrc<br>

### Markdown 
* Link [here](https://wordpress.com/support/markdown-quick-reference/)

## Emergency git
`git status` alias `gitsta` - show status of files<br>
`git log` - show commit history. Up to date when origin/master and HEAD/master are on the same level.<br>
`git add .` - add all<br>
`git commit -m` alias `gitcom` - commit with message<br>
`git push origin master` - push commited changes to remote master. `git pull` to pull.<br>
[Merge branch](https://stackoverflow.com/questions/5601931/what-is-the-best-and-safest-way-to-merge-a-git-branch-into-master)<br>
[Get one file](https://stackoverflow.com/questions/16230838/is-it-possible-to-pull-just-one-file-in-git) - usually `git fetch`, `git checkout` is enough.<br>
[Rebase](https://git-scm.com/docs/git-rebase) - apply changes of commit sha256 (= the code of the commit) to desired branch. E.g. when remote readme changed, to apply the changes offline.<br>
* E.g. `git rebase 06d12f6 master` - apply changes from `06d12f6` to master

`git update-index --assume-unchanged dir/file` - untrack file you don't want whose changes you don't want to keep track of

## Emergency VIM
`:q` - quit<br>
`:q!` - quit without saving<br>
`:x` - save and quit<br>
`:w` - save (write)<br>
`:i` - go into edit mode<br>
`u` - undo<br>
`esc` - leave edit mode<br>
`/sth` - search for sth<br>
[Find same string](https://stackoverflow.com/questions/6607630/find-next-in-vim/6607664#6607664)<br>
[Replace text](https://www.cyberciti.biz/faq/vim-text-editor-find-and-replace-all-text/)<br>
[Basic commands](https://coderwall.com/p/adv71w/basic-vim-commands-for-getting-started)<br>
<br>
<br> 

## Workflow Start-up
* turn ON the laptop manually, hold the power button on Kinova robot, until the LEDlight flickers
* to launch the ROS first in a separate terminal, type:<br>
`deact`<br>
`ipalm`<br>
`roslaunch camera_publisher.launch`
* launching 3 more scripts, all in separate terminals:
    1. `ipalm`, then `cd src/ipalm_control`, finally `python goes_to_object.py`
    2. `epos`, then `python3 infer.py --model=ycbv-cvpr20-xc65-f64`
    3. `epos`, then `python epos_camera_stream.py `
