#!/usr/bin/env python
"""
Copyright (c) 2020 Jan Behrens
All rights reserved.

This source code is licensed under the BSD-3-Clause license found in the
LICENSE file in the root directory of this source tree.

@author: Jan Behrens
"""

from __future__ import print_function
import rospy
import time
from visualization_msgs.msg import MarkerArray, Marker
from mujoco_interface_msgs.srv import GetAllObjectPoses, GetAllObjectPosesRequest, GetAllObjectPosesResponse
from urdf_parser_py.urdf import URDF
from rospkg import RosPack
import os
import random
import sys
import matplotlib.pyplot as plt
from datetime import datetime

from geometry_msgs.msg import Quaternion, PoseStamped, Point
from kortex_driver.msg import Finger, GripperMode, TwistCommand, BaseCyclic_Feedback, CartesianReferenceFrame, \
    JointSpeed
from kortex_driver.srv import SendGripperCommand, SendGripperCommandRequest, SendTwistCommand, SendTwistCommandRequest, \
    SendTwistCommandResponse, SendJointSpeedsCommand, SendJointSpeedsCommandRequest
from moveit_commander import MoveGroupCommander, RobotCommander, PlanningSceneInterface, SolidPrimitive
import moveit_commander
import rosnode
import rospy
import numpy as np
import copy
from sensor_msgs.msg import JointState
from tf.transformations import quaternion_from_euler, euler_from_quaternion
from control_msgs.msg import GripperCommandActionGoal

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class ManipulatorActions(object):
    mg = None  # type: MoveGroupCommander
    sc = None  # type: PlanningSceneInterface
    rc = None  # type: RobotCommander

    CUBE_SIZE = 0.0478  #m; 47.8 mm 

    # PLATE_HEIGHT = 0.038
    # PLATE_WIDTH = 0.022
    KINOVA_AREA = 0.000836  #m^2

    # positions 0.5 - 0.7 are usually OK

    GRIPPER_WEIGH_POS = 0.7
    GRIPPER_WEIGH_EFF = 0.7

    GRIPPER_SQZ_POS = 0.8
    GRIPPER_SQZ_EFF = 0.7

    scene_objects = []
    kinova_joints = None
    gripper_goal_publisher = None


    # declaration of used services
    send_gripper_command_full_name = '/' + 'my_gen3' + '/base/send_gripper_command'
    send_gripper_command_srv = rospy.ServiceProxy(send_gripper_command_full_name, SendGripperCommand)

    cart_vel_control_srv_full_name = '/' + 'my_gen3' + '/base/send_twist_command'
    cart_vel_control_srv = rospy.ServiceProxy(cart_vel_control_srv_full_name, SendTwistCommand)

    joint_vel_control_srv_full_name = '/' + 'my_gen3' + '/base/send_joint_speeds_command'
    joint_vel_control_srv = rospy.ServiceProxy(joint_vel_control_srv_full_name, SendJointSpeedsCommand)

    # UP = 0.3
    tau_0 = None

    """
    These three are needed:

    ManipulatorActions.move_above(obj=obj)
    ManipulatorActions.approach_grasp(obj=obj)
    ManipulatorActions.grasp(obj)
    """


    @classmethod
    def get_move_group(cls):
        # type: () -> MoveGroupCommander
        if isinstance(ManipulatorActions.mg, MoveGroupCommander):
            return ManipulatorActions.mg
        else:
            ManipulatorActions.init_mp()
            return ManipulatorActions.mg

    @staticmethod 
    def get_ns():
        nodes = rosnode.get_node_names()
        for node in nodes:  # type: str
            if 'move_group' in node:
                if 'move_group_commander_wrapper' in node:
                    continue
                split_node = node.split('/')
                i = split_node.index('move_group')
                ns = '/{}'.format(split_node[i-1])
                print(ns)
                return ns
        rospy.logerr('Node move_group node is not running. Exiting.')

    @staticmethod
    def get_robdesc_ns():  
        import rosparam
        names = rosparam.get_param_server().getParamNames()
        for name in names: # type: str
            # if name.endswith('robot_description'):
            if name.endswith('robot_description'):
                return name

    @staticmethod
    def with_planning_scene():
        ManipulatorActions.sc.remove_world_object()
        ManipulatorActions.add_table_plane()
        ManipulatorActions.avoid(*ManipulatorActions.scene_objects)
        time.sleep(0.3)

    @classmethod
    def add_table_plane(cls):
        table_pose = PoseStamped()
        table_pose.header.frame_id = 'base_link'
        table_pose.pose.orientation.w = 1.0
        ManipulatorActions.sc.add_plane('table', table_pose)
    
    @classmethod
    def init_mp(cls):
        movegroup_ns = ManipulatorActions.get_ns()
        # movegroup_ns = "/move_group/"
        robot_description = ManipulatorActions.get_robdesc_ns()
        sc = PlanningSceneInterface(ns=movegroup_ns)
        mg = MoveGroupCommander('arm', ns=movegroup_ns, robot_description=robot_description)
        mg.set_max_acceleration_scaling_factor(0.5)
        mg.set_max_velocity_scaling_factor(0.6)
        mg.set_end_effector_link('tool_frame')
        rospy.sleep(1.0)
        sc.remove_world_object()
        ManipulatorActions.rc = RobotCommander(ns=movegroup_ns)
        ManipulatorActions.mg = mg
        ManipulatorActions.sc = sc
        ManipulatorActions.add_table_plane()

        # ManipulatorActions.measure_weight(calibrate=True)
        # ManipulatorActions.move_up(home=True)
        # ManipulatorActions.measure_weight(calibrate=True)
        return mg, sc
    
    # -------------------------------
    #              MINE
    # -------------------------------

    @classmethod
    def getMujocoPoses(cls):
        """
        Extracts all objects from MuJoCo via mujoco_interface_msgs.srv GetAllObjectPoses;
        Shows the Objects in RViz;
        Returns the GetAllObjectPosesResponse.
        """
        # pull state from mujoco
        req = GetAllObjectPosesRequest()
        res = STATE_SRV.call(req)
        assert isinstance(res, GetAllObjectPosesResponse)
        
        """
        names: 
        - softCube
        poses: 
        - 
            header: 
            seq: 0
            stamp: 
                secs: 1620204179
                nsecs: 600090974
            frame_id: "world"
            pose: 
            position: 
                x: 0.389278160977
                y: -0.0556070735416
                z: 0.0254048832484
            orientation: 
                x: 2.50446854153e-06
                y: -4.71451099285e-07
                z: 0.0479816209858
                w: 0.99884821872
        mesh_resources: []
        success: True
        """
        return res  # returns the marker array for further use

    @classmethod
    def kinovaGoRetracted(cls):

        mg = ManipulatorActions.get_move_group()
        current_pose = mg.get_current_pose()
        retracted_pose = copy.deepcopy(current_pose)
        retracted_pose.pose.position.x = 0.134933766536
        retracted_pose.pose.position.y = 0.0013105497169
        retracted_pose.pose.position.z = 0.211024091753
        retracted_pose.pose.orientation.x = 0.706796927226
        retracted_pose.pose.orientation.y = 0.706254025222
        retracted_pose.pose.orientation.z = 0.0286551333637
        retracted_pose.pose.orientation.w = 0.0286747075461

        mg.set_pose_target(retracted_pose)
        plan = mg.plan()
        print(bcolors.OKBLUE + "[AS-INFO] Moving to retracted position. " + bcolors.ENDC)
        mg.execute(plan)
        print(bcolors.OKGREEN + "[AS-INFO] FINISHED moving to retracted position. " + bcolors.ENDC)
        """
        pose: 
            position: 
                x: 0.134933766536
                y: 0.0013105497169
                z: 0.211024091753
            orientation: 
                x: 0.706796927226
                y: 0.706254025222
                z: 0.0286551333637
                w: 0.0286747075461
        """
        return True

    @classmethod
    def kinovaGoHome(cls):
        mg = ManipulatorActions.get_move_group()
        current_pose = mg.get_current_pose()
        
        joint_angles = [0.0, 0.0, np.pi, -np.pi/2, 0.0, -np.pi/2, np.pi/2]
        plan = mg.plan(joint_angles)
        print("[AS-INFO] Moving to HOME.")
        done = mg.execute(plan)
        if done:
            print(bcolors.OKGREEN + "[AS-INFO] FINISHED moving to HOME." + bcolors.ENDC)
            return True
        else:
            # print(bcolors.WARNING + "[AS-WARN] Execution of the plan to move to HOME is not COMPLETE, probably due to possition error." + bcolors.ENDC)
            return False
        """
        pose: 
            position: 
                x: 0.552390746474
                y: -0.0048099558279
                z: 0.314102253505
            orientation: 
                x: 0.502033272959
                y: 0.501781280892
                z: 0.497784159367
                w: 0.49838646618
        """
        
        
    @classmethod
    def sim_gripper_control(cls, position, max_effort):
        
        # if gripper_goal.goal.command.position is None:
        if ManipulatorActions.gripper_goal_publisher is None:
            pub = rospy.Publisher("/robotiq_2f_85_gripper_controller/gripper_cmd/goal", GripperCommandActionGoal, queue_size=1)
            ManipulatorActions.gripper_goal_publisher = pub
            rospy.sleep(0.5)
        else:
            pub = ManipulatorActions.gripper_goal_publisher

        gripper_goal = GripperCommandActionGoal()
        gripper_goal.goal.command.position = position
        gripper_goal.goal.command.max_effort = max_effort
        if position < 0.1:
            print("[AS-INFO] Opening gripper to position: {}.".format(position))
        else:
            print("[AS-INFO] Closing gripper to position: {}.".format(position))
        pub.publish(gripper_goal)
        print(bcolors.OKGREEN +"[AS-INFO] Gripper goal published." + bcolors.ENDC)

        # no waiting until completion, it halts the gripper movement reading while squeezing!
        return True

    @classmethod
    def update_joint_state(cls, msg):
        ManipulatorActions.kinova_joints = msg

    @classmethod
    def kinovaGoAboveObject(cls, obj_pose):
        # TODO - the object_pose quaternion is somehow upside down, fix it
        # also check Jan's code, there was something up with the object orientation

        mg = ManipulatorActions.get_move_group()
        current_pose = mg.get_current_pose()

        """basic grasping pose orientation: 
        x: -0.706403790128
        y: -0.7077936173
        z: 0.00298439291102
        w: 0.00360194409273
        """

        pose = copy.deepcopy(obj_pose)  # this is needed, else pose is edited also out of the function...
        pose.position.z += 0.30000
        pose.orientation.x = -0.706403790128
        pose.orientation.y = -0.7077936173
        pose.orientation.z = 0.00298439291102
        pose.orientation.w = 0.00360194409273

        mg.set_pose_target(pose)
        plan = mg.plan()
        print("[AS-INFO] Moving ABOVE.")
        done = mg.execute(plan)
        if done:
            print(bcolors.OKGREEN + "[AS-INFO] FINISHED moving ABOVE." + bcolors.ENDC)
            return True
        else:
            # print(bcolors.FAIL + "[AS-FAIL] FAILED to move ABOVE." + bcolors.ENDC)
            # try again 2 more times:
            for i in range(2):
                print(bcolors.WARNING + "[AS-WARN] Retrying movement ABOVE."+ bcolors.ENDC)
                mg.set_pose_target(pose)
                plan = mg.plan()
                done = mg.execute(plan)
                if done:
                    print(bcolors.OKGREEN + "[AS-INFO] FINISHED moving ABOVE after {} retrials.".format(i+1) + bcolors.ENDC)
                    return True


            return False

    @classmethod
    def kinovaGo2GraspPosition(cls, obj_pose):
        """ Assert gripper == opened """
        # ManipulatorActions.sim_gripper_control(0.0, 0.9)
        mg = ManipulatorActions.get_move_group()
        pose = copy.deepcopy(obj_pose)

        pose.position.z += 0.0200

        pose.orientation.x = -0.706403790128
        pose.orientation.y = -0.7077936173
        pose.orientation.z = 0.00298439291102
        pose.orientation.w = 0.00360194409273
        mg.set_pose_target(pose)
        plan = mg.plan()
        print("[AS-INFO] MOVING to grasp position.")
        done = mg.execute(plan)
        if done:
            print(bcolors.OKGREEN + "[AS-INFO] FINISHED moving to grasp position." + bcolors.ENDC)
            return True
        else:
            # print(bcolors.FAIL + "[AS-FAIL] FAILED moving to grasp position." + bcolors.ENDC)
            return False
        
    
    @classmethod
    def exploratoryAction(cls, action, obj_pose, calibrate=False):
        """
        1) Go above object
        2) Do series of needed steps for the action
        3) Return measurement
        """
        print(bcolors.OKBLUE + "[AS-INFO] Starting the sequence for: {}".format(action) + bcolors.ENDC)
        grasp_complete = False

        # Get the MoveGroupCommander and deepcopy the pose
        mg = ManipulatorActions.get_move_group()
        current_pose = mg.get_current_pose()
        pose = copy.deepcopy(obj_pose)
        
        if action == 'density' or action == 'weigh':

            if ManipulatorActions.tau_0 is None:
                calibrate = True

            if calibrate:
                print(bcolors.OKBLUE + "[AS-CALIB] Start of the calibrating sequence." + bcolors.ENDC)
                ManipulatorActions.kinovaGoHome()
                
                #  wait until possible oscillations stop
                rospy.sleep(2)  

                # read 10 torques (efforts), create average
                J4_torques = np.zeros(10)
                for i in range(10):  
                    J4_torques[i] = ManipulatorActions.kinova_joints.effort[4]
                    rospy.sleep(0.1)
                
                ManipulatorActions.tau_0 = J4_torques.mean()
                print(bcolors.OKGREEN + "[AS-CALIB] Calibrated tau_0 = {}.".format(ManipulatorActions.tau_0) + bcolors.ENDC)

                # Move above the object
                ManipulatorActions.kinovaGoAboveObject(pose)

                """ THIS IS HOW THE JOINTS ARE LISTED
                name: [finger_joint, joint_1, joint_2, joint_3, joint_4, joint_5, joint_6, joint_7,
                left_inner_finger_joint, left_inner_knuckle_joint, right_inner_finger_joint,
                right_inner_knuckle_joint, right_outer_knuckle_joint]

                to listen, call this in sourced terminal: `rostopic echo /kinova_mujoco/joint_states`
                """
        
            # Move the manipulator to the grasping position
            edited_pose = copy.deepcopy(pose)
            edited_pose.position.z+= 0.005
            ManipulatorActions.kinovaGo2GraspPosition(edited_pose)     
            rospy.sleep(2)
            # Squeeze the object for good grip for weight measurement
            pos = ManipulatorActions.GRIPPER_WEIGH_POS
            max_eff = ManipulatorActions.GRIPPER_WEIGH_EFF
            previous_obj_pos = copy.deepcopy(obj_pose)

            ManipulatorActions.sim_gripper_control(pos, max_eff)
            rospy.sleep(2)

            # Go to Home position for torque measurement
            ManipulatorActions.kinovaGoHome()
            tau_0 = ManipulatorActions.tau_0

            # Measure the average torque of the 4th joint in the Home position
            J4_torques = np.zeros(10)
            for i in range(10):
                J4_torques[i] = ManipulatorActions.kinova_joints.effort[4]
                rospy.sleep(0.1)
            
            # Tau = r x F
            # Tau = r * mass * g
            # mass = Tau / (r * g)
            r = 0.3143  # 4th to 6th joint
            g = 9.81    # m.s^-2
            mass = (J4_torques.mean() - tau_0) / (r * g)
            
            print(bcolors.OKGREEN + "[AS-INFO] The measured mass is: {} kg".format(mass) + bcolors.ENDC)

            # Place back the object
            ManipulatorActions.kinovaGoAboveObject(previous_obj_pos)
            ManipulatorActions.kinovaGo2GraspPosition(previous_obj_pos)
            ManipulatorActions.sim_gripper_control(0.0, ManipulatorActions.GRIPPER_WEIGH_EFF)
            rospy.sleep(2)
            ManipulatorActions.kinovaGoAboveObject(previous_obj_pos)

            print(bcolors.OKGREEN + "[AS-RESULT] The object's mass is {} kg.".format(mass) + bcolors.ENDC)
            return mass

            # finger.finger_identifier = 0  
            # finger.value = GRIPPER_CLOSED_POSITION
            # req.input.gripper.finger.append(finger)
            # req.input.mode = GripperMode.GRIPPER_POSITION
            # rospy.loginfo("Sending the gripper command...")

            # # Call the service
            # try:
            #     ManipulatorActions.send_gripper_command_srv.call(req)
            # except rospy.ServiceException:
            #     rospy.logerr("Failed to call SendGripperCommand")
            #     grasp_complete = False
            #     print("Gripper not closed!")
            # else:
            #     time.sleep(0.5)
            #     grasp_complete = True
            #     print("Gripper successfully closed!")
                    
        elif action == 'elasticity' or action == 'squeeze':
            
            pos = ManipulatorActions.GRIPPER_SQZ_POS
            max_eff = ManipulatorActions.GRIPPER_SQZ_EFF

            # Open the gripper, to be sure
            ManipulatorActions.sim_gripper_control(0.0, max_eff)
            rospy.sleep(2)

            # Go above the object
            ManipulatorActions.kinovaGoAboveObject(pose)

            # Go to the grasping position
            
            ManipulatorActions.kinovaGo2GraspPosition(pose)

            effort_time_series = np.zeros(1)
            time_series = np.zeros(1)
            position_time_series = np.zeros(1)
            strain = np.zeros(1)

            started = False
            t_duration = 0.0
            t_max = 5.0
            epsilon = 0.01
            print("[AS-INFO] Recording squeezing.")
            while abs(pos-ManipulatorActions.kinova_joints.position[0]) >= epsilon:
                if not started:
                    ManipulatorActions.sim_gripper_control(pos, max_eff)
                    started = True
                    t_start = time.time() 
                t_duration = time.time() - t_start
                effort_time_series = np.append(effort_time_series, ManipulatorActions.kinova_joints.effort[0])
                time_series = np.append(time_series, t_duration)
                
                
                # constant of proportionality between percentage of openness and finger distance
                K = 85/0.8  # 85mm range / 0.8 
                actual_dist = (0.8 - ManipulatorActions.kinova_joints.position[0]) * K
                actual_dist /= 1000
                
                position_time_series = np.append(position_time_series, actual_dist)
                delta = ManipulatorActions.CUBE_SIZE - actual_dist
                if delta >= 0:
                    strain = np.append(strain, delta/ManipulatorActions.CUBE_SIZE)
                else:
                    strain = np.append(strain, 0.0)

                if t_duration > t_max:
                    print(bcolors.WARNING + "[AS-WARN] Squeezing took {} seconds. Stopped.".format(t_max) + bcolors.ENDC)
                    break

            print(bcolors.OKGREEN + "[AS-INFO] Squeezing finished in {} seconds.".format(t_duration) + bcolors.ENDC)

            # reopen the gripper
            ManipulatorActions.sim_gripper_control(0.0, ManipulatorActions.GRIPPER_SQZ_EFF)
            rospy.sleep(2)

            # go back above the object
            ManipulatorActions.kinovaGoAboveObject(pose)

            params = "_pos{}_eff{}".format(ManipulatorActions.GRIPPER_SQZ_POS, ManipulatorActions.GRIPPER_SQZ_EFF)
            now = datetime.now()
            date_time = now.strftime("_%d-%m_%H-%M-%S")


            np.savetxt("effort" + date_time + params + ".csv", effort_time_series, delimiter=",")
            
            np.savetxt("time" + date_time + params + ".csv", time_series, delimiter=",")

            np.savetxt("position" + date_time + params + ".csv", position_time_series, delimiter=",")
            stress = effort_time_series / ManipulatorActions.KINOVA_AREA / 1000  # [kPa]

            x1 = 0
            y1 = stress[ np.nonzero(strain)[0][0] ]  # returns the first nonzero element
            # x2 = deltas[np.argmax(F_together)]
            x2 = np.max(strain)
            y2 = np.max(stress)

            m = (y2-y1) / (x2-x1)

            plt.subplot(311)
            plt.scatter(time_series[1:], position_time_series[1:], color="tab:blue", label="Position", s=1)
            plt.xlabel("Time [s]")
            plt.ylabel("Fingers distance [mm]")
            plt.legend()

            plt.subplot(312)
            plt.scatter(time_series[1:], effort_time_series[1:], color="tab:blue", label="Force", s=1)
            plt.xlabel("Time [s]")
            plt.ylabel("Force (effort) [N]")
            plt.legend()
            
            plt.subplot(313)
            plt.scatter(strain, stress, color="tab:blue", label="Elasticity", s=1)
            plt.plot([x1,x2],[y1,y2], label="Modulus = {} [kPa]".format(m), lw=1, ls="--", color="tab:red")
            plt.xlabel("Strain [-]")
            plt.ylabel("Stress [kPa]")
            plt.legend()
            plt.show()

        return True



URDF_PKG = "kinova_mujoco"
DISPLAY_TOPIC = 'scene_objects'
STATE_SRV_NAME = '/kinova_mujoco/getAllObjectPoses'
URDF_FILE = "world_model/kinova_fel_with_objects_NoDae.urdf"
STATE_SRV = None

DISPLAY_PUB = None

robot = None
MODEL_SCALE = 1.0



if __name__=="__main__":
   
    rospack = RosPack()  
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('master')
    robot_commander = moveit_commander.RobotCommander()

    SCENE_PATH = os.path.join(rospack.get_path(URDF_PKG), URDF_FILE)
    robot = URDF.from_xml_file(SCENE_PATH)
    STATE_SRV = rospy.ServiceProxy(STATE_SRV_NAME, GetAllObjectPoses)
    rospy.wait_for_service(STATE_SRV_NAME)

    # poses = ManipulatorActions.getMujocoPoses()
    sub = rospy.Subscriber("/joint_states", JointState, callback=ManipulatorActions.update_joint_state, queue_size=1)
    time.sleep(2) # wait for the subscriber to initialize
    
    # ManipulatorActions.sim_gripper_control(0.0, 0.9)
    
    # print(poses)
    
    while True:
        poses = ManipulatorActions.getMujocoPoses() 
        for i,pose in enumerate(poses.poses):
            print(pose)
            # planned_action = random.choice(["density", "elasticity"])
            planned_action = 'elasticity'
            ManipulatorActions.kinovaGoHome()  # WAITS for completion
            rospy.sleep(0.5)
            ManipulatorActions.sim_gripper_control(0.0, 0.9)
            rospy.sleep(2)
            
            ManipulatorActions.exploratoryAction(planned_action, pose.pose)

        rospy.sleep(1)


    rospy.spin()



