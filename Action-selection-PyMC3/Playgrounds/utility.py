import copy

import matplotlib.pyplot as plt
from pymc3.sampling import sample
from scipy.stats import norm
import json
import numpy as np
import pymc3 as pm
import queue
from bayesNetFramework import BayesNet, json2dict


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def sample_PMF(PMF, n_samples, seed=False):
    """
    ## NOTE: the PMF must be an array with fixed indices, i.e. an index_i must
    correspond to category_i or material_i each time.
    """
    if seed:
        # not np.random.seed(seed), because it sets the seed for the whole utility.py script
        rng = np.random.default_rng(seed)
    obs = []
    for i in range(n_samples):
        rand_event_idx = rng.choice([idx for idx in range(len(PMF))], p=PMF)
        trial = [0 for num in PMF]
        trial[rand_event_idx] = 1
        obs.append(trial)
    return np.array(obs)


def get_property_names(materials):
    a = []
    for prop in materials[0]['properties']:
        a.append(prop['prop_name'])
    return a


def uniform(axis):
    """
    Custom uniform distribution.
    """
    return np.ones(len(axis))*1/(len(axis))


def get_material(mat_name, materials):
    for mat in materials:
        print("this is mat: ", mat)
        if mat['name'] == mat_name:
            return mat


def get_names(materials):
    names = []
    for mat in materials:
        names.append(mat["name"])
    return names


def get_property(material, property_name):
    for prop in material['properties']:
        if prop['prop_name'] == property_name:
            return prop
    print("Probably a TYPO? You wanted: material: {} and property name: {}".format(material, property_name))
    return None


def get_ticks_and_labels(materials, prop, short=True):
    """
    returns: xticks, label
    """
    xticks = []
    labels = []
    for mat in materials:
        mean = get_property(mat, prop)['mean']
        if mean is not None:
            xticks.append(mean)
        else:
            continue
        labels.append(mat['name'])

    if short:
        for i, lab in enumerate(labels):
            labels[i] = lab[:3].capitalize() + "."

    xticks = np.array(xticks)
    labels = np.array(labels)
    indices = np.argsort(xticks)  # get indices for sorting of names and ticks

    xticks = xticks[indices]
    labels = labels[indices]

    return xticks, labels


def create_axes(materials):
    axes = {}
    properties = get_property_names(materials)
    for prop in properties:
        means = []
        sigmas = []
        for mat in materials:

            mean = get_property(mat, prop)['mean']
            sigma = get_property(mat, prop)['sigma']
            if mean is not None and sigma is not None:
                means.append(mean)
                sigmas.append(sigma)
            else:
                print("[WARN] Material {} has 'None' as a parameter in Property {}".format(mat["name"].capitalize(),
                                                                                           prop.capitalize()))
        axes[prop] = np.linspace(min(means)-3*max(sigmas), max(means)+3*max(sigmas), 50000)

    return axes


def create_beliefs(materials, axes):
    properties = get_property_names(materials)
    beliefs = {}
    for prop in properties:
        beliefs[prop] = create_prior_dist(materials, axes[prop], prop)
    return beliefs


def create_prior_dist(materials, ax, prop):
    y_dist = None
    # the GMM
    for mat in materials:
        try:
            mat_mean = get_property(mat, prop)['mean']
            mat_sigma = get_property(mat, prop)['sigma']
            if mat_mean is None or mat_sigma is None:
                continue
            y_mat = norm.pdf(ax, mat_mean, mat_sigma)
        except TypeError:
            y_mat = uniform(ax)
        if mat['prob'] is None:
            mat['prob'] = 1.0 / len(materials)  # if None, probabilities are not initialized, set to uniform
        if y_dist is None:
            y_dist = y_mat * mat['prob']
        else:
            y_dist += y_mat * mat['prob']

    y_dist = y_dist / sum(y_dist)
    return y_dist


def plot_beliefs(materials, axes, beliefs, show=True):
    """
    in:
        materials - list of dictionaries
        axes - dictionary of axes keyed by property name
        beliefs - list of dictionaries, each dict (belief) contains name (property), and the distribution (y)
    out:
        fig - returns the whole fig (but also shows it before)
    """
    # fig = plt.figure()
    plt.xticks(fontsize=11)
    plt.yticks(fontsize=11)
    fig = plt.figure(figsize=(6, 8))
    plt.subplots_adjust(hspace=0.6)

    for i, prop in enumerate(beliefs):  # for dict in a list of dicts

        axis = axes[prop]
        units = get_property(materials[0], prop)['unit']
        y = beliefs[prop]
        subplot = 10 + 100*len(beliefs) + i+1
        xticks, labels = get_ticks_and_labels(materials, prop)
        ax = plt.subplot(subplot)
        ax.plot(axis, y, color="tab:blue", lw=1.5, label="Belief")
        # ax.legend()
        
        ax.axvline(lw=1.5)
        label = r"{a} [${b}$]".format(a=prop.capitalize(), b=units)
        ax.set_xlabel(label)
        ax.ticklabel_format(axis="y", style="sci", scilimits=(0, 0))
        # ax.grid(ls="--", dashes=(2, 5), axis='y')
        ax2 = ax.secondary_xaxis("top")
        ax2.set_xticks(xticks)
        ax2.set_xticklabels(labels, rotation=60)
        for tick in xticks:
            ax.vlines(tick, ymin=0, ymax=max(y), ls="--", lw=1.5, color="#494949", alpha=0.5)

    if show:
        plt.show()

    return plt  # TODO not sure whether right


def plot_dist(ax, y, title=r"Distribution", xlabel=r"Distribution", ylabel=r"p(x)", legend=None):
    plt.figure(figsize=(5,3))
    plt.plot(ax, y, color="tab:blue", lw=1.5, label=legend)
    if legend is not None:
        plt.legend()
    plt.title(title)
    
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()


def plot_posterior_sequence(ax, meas_list, current_belief):
    posterior = current_belief
    plt.figure(figsize=(5,3))
    plt.plot(ax, posterior, color="tab:red", lw=1.5, ls="--", label="PMF Belief")
    for i, meas in enumerate(meas_list):
        posterior = posterior * meas
        posterior = posterior / sum(posterior)
        plt.plot(ax, posterior, color="tab:blue", lw=1.5, alpha=(i+1)/len(meas_list), label="Posterior")
        plt.legend()
        plt.title("Numerical post. after n measurements.")
    plt.show()


def plot_materials(axes, materials):
    props = get_property_names(materials)
    plt.xticks(fontsize=11)
    plt.yticks(fontsize=11)
    fig = plt.figure(figsize=(6, 8))
    plt.subplots_adjust(hspace=0.9)

    for i, prop in enumerate(props):
        subplot = 10 + 100 * len(props) + i + 1
        xticks, labels = get_ticks_and_labels(materials, prop)
        ax = plt.subplot(subplot)
        unit = get_property(materials[0], prop)['unit']
        rng = axes[prop][-1] - axes[prop][0]
        bin_size = rng / len(axes[prop])


        for mat in materials:
            mean = get_property(mat, prop)['mean']
            sigma = get_property(mat, prop)['sigma']
            if mean is None or sigma is None:
                continue
            y = norm.pdf(axes[prop], mean, sigma) * bin_size
            ax.plot(axes[prop], y, lw=1.5, label=mat['name'].capitalize())

        label = r"{a} [${b}$]".format(a=prop.capitalize(), b=unit)
        ax.set_xlabel(label)
        ax.set_xticks(xticks)
        ax.set_xticklabels(xticks, rotation=60)
        ax2 = ax.secondary_xaxis("top")
        ax2.set_xticks(xticks)
        ax2.set_xticklabels(labels, rotation=60)
        ax.ticklabel_format(axis="y", style="sci", scilimits=(0, 0))
        ax.axvline(lw=1.5)

        if prop == 'density':
            ax.set_xlim(xmin=0.0, xmax=2800.0)
            arr = ax.text(2500, 0.001, 'Metal', rotation=0, size=10, color="white",
                          bbox=dict(boxstyle="rarrow,pad=0.3", fc="tab:blue", ec="#494949", lw=1))

        if prop == 'elasticity':
            ax.set_xlim(xmin=0.0, xmax=600.0)
            arr = ax.text(450, 0.04, 'Unmeasurable', rotation=0, size=10, color="white",
                          bbox=dict(boxstyle="rarrow,pad=0.3", fc="tab:blue", ec="#494949", lw=1))

    plt.show()


def plot_material_category(materials):
    plt.figure(figsize=(6, 6))
    plt.xticks(fontsize=11)
    plt.yticks(fontsize=11)

    txt_len = 5
    spacing = 30
    xs = []
    labels = []
    ys = []
    for idx, mat in enumerate(materials):
        x = spacing * (idx + 1)
        name = mat['name']
        if len(name) > 5:
            labels.append(name[:txt_len].capitalize()+".")
        else:
            labels.append(name.capitalize())
        if mat['prob'] is None:
            print("[ERR] Cannot plot None probs.")
            return None
        xs.append(x)
        ys.append(mat['prob'])
        plt.vlines(x, 0, mat['prob'], color="tab:blue", lw=2)
        plt.plot(x, mat['prob'], marker='o', markersize=10, color="tab:blue")
    ys.append(1)
    plt.xticks(ticks=xs, labels=labels)
    plt.yticks(ticks=ys)
    plt.ticklabel_format(axis="y", style="sci", scilimits=(0, 0))
    plt.ylim(0, 1.05)
    plt.xlim(0, x + spacing)
    plt.grid(ls="--", dashes=(2, 5), axis='y')
    plt.show()

    return plt

# ------- ENTROPY STUFF ---------


def discrete_entropy(materials):
    ent_sum = 0
    for mat in materials:
        if mat['prob'] is not None:
            if mat['prob'] == 0.0:
                mat['prob'] = 0.00000000000000001
            ent_sum += mat['prob'] * np.log2(mat['prob'])
    disc_cat_entropy = - ent_sum
    return disc_cat_entropy


def differential_entropy(axes, materials, measurements, prop_name):
    """
    In: Measurements must be an array of values representing gaussian, not actual observations
    """
    ax = axes[prop_name]
    beliefs = create_beliefs(materials, axes)
    curr_belief = beliefs[prop_name]
    if len(measurements) != 0:
        relevant_measurements = measurements[prop_name]
    else:
        relevant_measurements = []
    for meas in relevant_measurements:
        curr_belief = curr_belief * meas
        curr_belief = curr_belief / sum(curr_belief)

    rng = ax[-1] - ax[0]
    bin_size = rng / len(ax)

    diff_prop_entropy = - sum(bin_size * curr_belief * np.log2(curr_belief))
    return diff_prop_entropy


def overall_differential_entropy(axes, materials, measurements, vb=True):
    """
    Returns: dict of entropies keyed by property names
    """
    props = get_property_names(materials)
    entropies = {}
    for prop in props:
        entropies[prop] = differential_entropy(axes, materials, measurements, prop)

    if vb:
        de = "Differential Entropies:"
        txt = "".join('-' for i in range(30))
        print(txt)
        print(de)
        for prop in props:
            print("{a:<10}{b:^10}{c:>5.7f}".format(a=prop.capitalize(), b=":", c=entropies[prop]))
        print(txt)

    return entropies


def generate_norm_rvs(loc, scale, n_samples, mmd=None):
    """
    This function generates samples needed because of the numerical nature of PyMC3.
    [In]:
        - loc          > the mean of a gaussian
        - scale        > the sigma of a gaussian
        - n_samples    > n of samples to draw from a gaussian (for each observation)
        - n_obs        > n of observations (emulated measurements) in the list
        - mmd          > maximum mean deviation - how far the mean can deviate
    [Out]:
        - observation  > list of samples from the distribution created in this function
        - params_of_ob > list of two parameters, mean and std of the underlying distribution
    """
    if mmd is None:
        mmd = scale/4
    rand_mean = norm.rvs(loc=loc, scale=mmd, size=1)

    observation = norm.rvs(loc=rand_mean, scale=scale, size=n_samples)
    params_of_ob = [rand_mean[0], scale]
    return observation, params_of_ob


def diff_ent_simulated_meas(axes, materials, measurements, prop_name, sim_meas_sigma, plot=True, vb=True):
    """
    [Out]:
        - y_posterior    > np.array() containing the posterior distribution
        - prior_entropy  > float32
        - emul_entropy   > float32
        - diff_info_gain > float32 differential information gain computed from prior and emul. entropy
    """

    ax = axes[prop_name]
    beliefs = create_beliefs(materials, axes)
    curr_belief = beliefs[prop_name]
    if len(measurements) != 0:
        relevant_measurements = measurements[prop_name]
    else:
        relevant_measurements = []
    for meas in relevant_measurements:
        curr_belief = curr_belief * meas
        curr_belief = curr_belief / sum(curr_belief)

    rng = ax[-1] - ax[0]
    bin_size = rng / len(ax)

    # put really small numbers instead of zeros
    curr_belief = np.where(curr_belief > 0, curr_belief, 10 ** -10)
    prior_entropy = -sum(bin_size * curr_belief * np.log2(curr_belief))

    # the emulation
    middle = (ax[-1] + ax[0]) / 2
    print("last of axis: ", ax[-1])
    print("middle: ", middle)

    y_meas = norm.pdf(ax, middle, sim_meas_sigma) * bin_size
    y_wide = np.convolve(curr_belief, y_meas, mode='same')
    y_posterior = curr_belief * y_wide
    y_posterior = y_posterior / sum(y_posterior)

    # put really small numbers instead of zeros
    y_posterior = np.where(y_posterior > 0, y_posterior, 10 ** -10)
    emul_entropy = -sum(bin_size * y_posterior * np.log2(y_posterior))
    diff_info_gain = prior_entropy - emul_entropy

    # ---- PLOT ----
    if plot:
        plt.figure(figsize=(8, 4))
        unit = get_property(materials[0], prop_name)['unit']
        plt.ticklabel_format(axis="y", style="sci", scilimits=(0, 0))
        plt.xticks(fontsize=11)
        plt.yticks(fontsize=11)
        label = r"{a} [${b}$]".format(a=prop_name.capitalize(), b=unit)
        plt.xlabel(label)
        plt.ylabel(r"$p(x)$")
        plt.tight_layout()

        plt.plot(ax, curr_belief, ls="--", color="tab:grey", lw=1.5, label="Prior")
        plt.plot(ax, y_meas, ls="-.", color="tab:pink", lw=1.5, label="Sigma")
        plt.plot(ax, y_wide, ls="-", color="black", lw=1.5, label="Emulation")
        plt.plot(ax, y_posterior, ls="-", color="tab:blue", lw=2, label="Posterior")
        plt.legend()
        plt.title(r"$H(Prior)$ = {:.4f}, $H(Post)$ = {:.4f}".format(prior_entropy, emul_entropy))
        plt.show()

    if vb:
        print("----------SIMULATED DIFF. ENTROPY----------")
        print("| This is the H(prop) for {}: {} |".format(prop_name.capitalize(), prior_entropy))
        print("| This is the Emulated Entropy for {}: {} |".format(prop_name.capitalize(), emul_entropy))
        print("| -x- Diff Information Gain: {}     |".format(diff_info_gain))
        print("--------------------------------------------")
        print()

    return y_posterior, prior_entropy, emul_entropy, diff_info_gain


def disc_ent_simulated_meas(axes, materials, prop_name, sim_meas_sigma, plot=True, n_samples=100, vb=True):

    ax = axes[prop_name]
    ent_before = discrete_entropy(materials)
    middle = (ax[-1] + ax[0]) / 2
    rng = ax[-1] - ax[0]
    bin_size = rng / len(ax)

    y_meas = norm.pdf(ax, middle, sim_meas_sigma) * bin_size
    current_belief = create_beliefs(materials, axes)[prop_name]
    y_wide = np.convolve(y_meas, current_belief, mode='same')

    y_wide = y_wide / sum(y_wide)

    observation = sampler(ax, y_wide, n_samples)
    if vb:
        print("--------------SIMULATED DISC. ENTROPY--------------")
        print("| Discrete Material entropy before: {}   |".format(ent_before))
        print("| The sampled Entropy is: {}   |".format(ent_after))
        print("| -OUT- Discrete Information Gain: {}   |".format(info_gain))
        print("--------------------------------------------------")
        print()

    return ent_before, ent_after, info_gain


def sampler(ax, dist, n_samples):
    samples = []
    for i in range(n_samples):
        ptx = np.random.choice(ax, p=dist)
        if ptx == ax[-1]:
            ptx = ax[-2]
        samples.append(ptx)
    return samples


def plot_meas_with_belief(axes, materials, prop, meas_mean, meas_sigma):
    ax = axes[prop]
    bels = create_beliefs(materials, axes)
    curr_bel = bels[prop]
    plt.plot(ax, curr_bel, color="tab:blue", label="Curr. Belief", lw=2)
    m = norm.pdf(ax, meas_mean, meas_sigma)
    plt.plot(ax, m, color="#494949", label="Meas", lw=2, ls="--")
    plt.legend()
    plt.title("Belief & Measurement")
    plt.show()


def reference(materials):
    """
    ### IN:
    - dictionary of materials  

    ### OUT:
    - dictionary of reference mus and sigmas only, keyed by property name
    """
    
    # TODO should add MASS_MUS and MASS_SIGS?

    VOLUME_MUS = np.array([])
    VOLUME_SIGS = np.array([])
    ELASTICITY_MUS = np.array([])
    ELASTICITY_SIGS = np.array([])
    DENSITY_MUS = np.array([])
    DENSITY_SIGS = np.array([])

    # extract parameters from the materials dictionary
    for mat in materials:

        VOLUME_MUS = np.append(VOLUME_MUS, float(get_property(mat, "volume")["mean"]))
        VOLUME_SIGS = np.append(VOLUME_SIGS, float(get_property(mat, "volume")["sigma"]))
        if get_property(mat, "elasticity")["mean"] is not None:
            ELASTICITY_MUS = np.append(ELASTICITY_MUS, float(get_property(mat, "elasticity")["mean"]))
            ELASTICITY_SIGS = np.append(ELASTICITY_SIGS, float(get_property(mat, "elasticity")["sigma"]))
        DENSITY_MUS = np.append(DENSITY_MUS, float(get_property(mat, "density")["mean"]))
        DENSITY_SIGS = np.append(DENSITY_SIGS, float(get_property(mat, "density")["sigma"]))
    
    reference = {}
    reference['volume'] = {'mu':VOLUME_MUS, 'sigma':VOLUME_SIGS}
    reference['density'] = {'mu':DENSITY_MUS/1000.0, 'sigma':DENSITY_SIGS/1000.0}
    reference['elasticity'] = {'mu':ELASTICITY_MUS, 'sigma':ELASTICITY_SIGS}

    return reference


def get_obs(obs_of_prop):
    obs = []
    while True:
        try:
            obs.append(obs_of_prop.get(block=False))
        except queue.Empty:
            if obs == []:
                return None
            else:
                return np.array(obs)


def argmax_of_prop(materials, prop_name, param_obs):
    if prop_name == 'mass':
        # TODO make more general
        # with information shared by pm.Model(), extracted from the trace
        # possibly save the trace for this
        # this is just temporary
        pass

    axes = create_axes(materials)
    # belief solely from the material category (from the PMF)
    current_belief = create_beliefs(materials, axes)[prop_name]

    # belief with measurements added numerically
    ax = axes[prop_name]
    rng = ax[-1] - ax[0]
    bin_size = rng / len(ax)
    for params in param_obs[prop_name]:
        y_meas = norm.pdf(ax, params[0], params[1])
        current_belief *= y_meas
        current_belief /= sum(current_belief * bin_size)
    
    plt.plot(ax, current_belief, label=prop_name)
    plt.legend()
    plt.show()
    # print("current_belief: ", current_belief)
    zero_idx = None
    diff = float('inf')
    for idx, x in enumerate(ax):
        if x > 20.0:
            break
        if x == 0.0:
            zero_idx = idx
            break
        elif abs(x - 0.0) < diff:
            diff = abs(x - 0.0)
            zero_idx = idx


    argmax_mean = np.argmax(current_belief) - zero_idx
    argmax_mean *= bin_size
    print("[INFO] Argmax_mean for {}: ".format(prop_name), argmax_mean)
    return argmax_mean


def smart_flatten(obs):
    if obs == np.array([]):
        return None
    else:
        return np.array(obs).flatten()


def confuse_PMF(pmf_vec, uniform_error):
    cnm = confusion_noise_matrix(pmf_vec, uniform_error)
    pmf_vec = np.reshape(pmf_vec, [len(pmf_vec), 1])
    confused_PMF = np.matmul(cnm, pmf_vec)
    return confused_PMF


def confusion_noise_matrix(pmf_vec_len, uniform_error):
    assert(uniform_error >= 0 and uniform_error <=1)
    CNM = np.eye(pmf_vec_len) * (1 - uniform_error)
    piecewise_error = uniform_error / (pmf_vec_len - 1)
    for r in range(pmf_vec_len):
        for c in range(pmf_vec_len):
            if r == c: continue
            CNM[r,c] = piecewise_error
    return CNM

def set_material_dir_alphas(materials, desired_PMF, plot=False, vb=False):
    """
    Parameters
    ----------
    materials    : dict  
        `materials` is a dict of subdicts with material name, material prob and iterable properties
    desired_PMF  : dict  
        `desired_PMF` is a dict with mat.name as key and its prob as a value

    Returns
    -------
    (updated_mats, alphas) : tuple  
    updated_mats : dict
        `updated_mats` is a the `materials` dictionary containing dirichlet approximated probs
    alphas       : numpy.array()  
        `alphas` is a numpy array of alpha values to initiate dirichlet by `desired_PMF` values

    """
    updated_materials = copy.deepcopy(materials)
    PMF = []
    PMF_names = []
    for name in get_names(materials):
        PMF.append(desired_PMF[name])
        PMF_names.append(name)
    
    n_samples = len(PMF) * 100
    obs = sample_PMF(PMF, n_samples, seed=123)
    
    N, K = obs.shape  # N - num of obs, K - num of categories (components)
    cum_error = 0
    with pm.Model() as dirFit:

        # dirichlet distribution
        alpha = pm.Gamma('alpha', alpha=2, beta=0.5, shape=K)
        w = pm.Dirichlet('weights', a=alpha)
        y = pm.Multinomial('y', 1, w, observed=obs)

        trace = pm.sample(5000, tune=5000, target_accept=0.9)
        burn_in = 500
        chain = trace[burn_in:]
        alphas = []
        fit_probs = []
        ppc = pm.sample_posterior_predictive(chain, var_names=['alpha', 'weights'], model=dirFit)
        for i, samp in enumerate(ppc['alpha'].transpose()):
            alphas.append(np.mean(samp))

        for j, samp in enumerate(ppc['weights'].transpose()):
            prob_mean = np.mean(samp)
            fit_probs.append(prob_mean)
            if vb:
                print("| Desired prob: {:.4f}. |\n| Fit prob: {:.4f}. |\n".format(PMF[j], prob_mean))
            cum_error += abs(PMF[j] - prob_mean)
            updated_materials[i]['prob'] = prob_mean
        
        # mandatory safety info, if PyMC3 managed to fit the alphas correctly
        print("[INFO]: | The dirichlet alpha fit cumulative error = {:.3f} |".format(cum_error))

        if plot:
            pm.traceplot(chain)
            pm.plot_posterior(chain)
            pm.autocorrplot(chain)
            plt.show()
        
    return updated_materials, np.array(alphas)


def build_and_infer_from_model(materials, observations, param_obs, alphas=None, plot=True, vb=True):
    """
    ### IN:
    
    """
    temp_materials = copy.deepcopy(materials)
    ref = reference(temp_materials)

    with pm.Model() as bnet:
        if alphas is None:
            mat_cat = pm.Dirichlet('matCat', np.ones(len(temp_materials)))
        else:
            mat_cat = pm.Dirichlet('matCat', alphas)  # init dirichlet with a prior dist

        mass_meas_error = 10
        mass_prior = pm.Uniform(lower=0, upper=2000)
        obsv_mass = pm.Normal('obs_mass', mu=mass_prior, sigma=mass_meas_error, observed=smart_flatten(observations['mass']))

        volume = pm.NormalMixture('volu', w=mat_cat, mu=ref['volume']['mu'], sigma=ref['volume']['sigma'],
                                    observed=smart_flatten(observations['volume']))
        elasticity = pm.NormalMixture('elas', w=mat_cat, mu=ref['elasticity']['mu'], sigma=ref['elasticity']['sigma'],
                                      observed=smart_flatten(observations['elasticity']))
        density = pm.NormalMixture('dens', w=mat_cat, mu=ref['density']['mu'], sigma=ref['density']['sigma'],
                                   observed=smart_flatten(observations['density']))
        sound_classif = pm.Multinomial('sound', 1, w=mat_cat, observed=TODO)
        vision_mat_classif = pm.Multinomial('vision_mat', 1, w=mat_cat, observed=TODO)
        

        # start = pm.find_MAP()
        step = pm.Metropolis()
        trace = pm.sample(2000, step=step)
        burn_in = 500  # skips the n iters, where the MCMC is just wandering
        chain = trace[burn_in:]
        ppc = pm.sample_posterior_predictive(chain, var_names=['matCat'], model=bnet)
        for i, samp in enumerate(ppc['matCat'].transpose()):
            prob = np.mean(samp)
            if vb:
                print("Prob for {} is {}.".format(temp_materials[i]['name'], prob))
            temp_materials[i]['prob'] = prob

        if plot:
            pm.traceplot(trace)
            pm.plot_posterior(chain)
            pm.autocorrplot(chain)
            plt.show()

    return temp_materials


def add_obs_smart(observations, prop, current_obs):
    observations[prop].append(current_obs)
    observations[prop] = list(np.array(observations[prop]).flatten())
    return observations


# noinspection PyDictCreation,PyTypeChecker
def infer_with_pymc3(materials, prop, observation, plot=True, vb=True):
    """
    ** deprecated **
    Performs independent single-instance inference.
    """

    temp_materials = copy.deepcopy(materials)
    VOLUME_MUS = np.array([])
    VOLUME_SIGS = np.array([])
    ELASTICITY_MUS = np.array([])
    ELASTICITY_SIGS = np.array([])
    DENSITY_MUS = np.array([])
    DENSITY_SIGS = np.array([])

    observations = {
        'elasticity': None,
        'density': None,
        'volume': None
    }

    observations[prop] = observation

    # extract parameters from the materials dictionary
    for mat in materials:

        VOLUME_MUS = np.append(VOLUME_MUS, float(get_property(mat, "volume")["mean"]))
        VOLUME_SIGS = np.append(VOLUME_SIGS, float(get_property(mat, "volume")["sigma"]))
        if get_property(mat, "elasticity")["mean"] is not None:
            ELASTICITY_MUS = np.append(ELASTICITY_MUS, float(get_property(mat, "elasticity")["mean"]))
            ELASTICITY_SIGS = np.append(ELASTICITY_SIGS, float(get_property(mat, "elasticity")["sigma"]))
        DENSITY_MUS = np.append(DENSITY_MUS, float(get_property(mat, "density")["mean"]))
        DENSITY_SIGS = np.append(DENSITY_SIGS, float(get_property(mat, "density")["sigma"]))

    with pm.Model() as bnet:
        mat_cat = pm.Dirichlet('matCat', np.ones(len(materials)))

        volume = pm.NormalMixture('volu', w=mat_cat, mu=VOLUME_MUS, sigma=VOLUME_SIGS,
                                    observed=observations['volume'])
        elasticity = pm.NormalMixture('elas', w=mat_cat, mu=ELASTICITY_MUS, sigma=ELASTICITY_SIGS,
                                      observed=observations['elasticity'])
        density = pm.NormalMixture('dens', w=mat_cat, mu=DENSITY_MUS, sigma=DENSITY_SIGS,
                                   observed=observations['density'])

        start = pm.find_MAP()
        step = pm.Metropolis()
        trace = pm.sample(1000, start=start, step=step)
        burn_in = 500  # skips the n iters, where the MCMC is just wandering
        chain = trace[burn_in:]
        ppc = pm.sample_posterior_predictive(chain, var_names=['matCat'], model=bnet)
        for i, samp in enumerate(ppc['matCat'].transpose()):
            prob = np.mean(samp)
            if vb:
                print("Prob for {} is {}.".format(temp_materials[i]['name'], prob))
            temp_materials[i]['prob'] = prob

        if plot:
            pm.traceplot(trace)
            pm.plot_posterior(chain)
            pm.autocorrplot(chain)
            plt.show()

    return temp_materials


def mat_given_cat(crawl_data, obj_name, ipalm_names_flag=True):
    object_names = ['bakingtray', 'bottle', 'bowl', 'fork', 'glass', 'hammer', 'kettle', 'knife', 'mug', 'pan', 'plate', 'sodacan', 'spoon', 'wineglass']
    prop_list = ['Material', 'Colour', 'Weight', 'Volume', 'Length', 'Width', 'Height', 'Functionality']  # shape?, not very informative
    mat_names = ["ceramic", "glass", "metal", "other", "plastic", "silicone", "stone", "wood"]
    ipalm_mat_names = ["ceramic", "metal", "soft-plastic", "hard-plastic", "wood", "paper", "foam"]
    if obj_name not in object_names:
        print("[ERR] Unknown object.")
        return None
    
    prop_query = None
    probs = {}

    for obj in crawl_data:
        if obj["name"] == obj_name:
            for prop in obj["properties"]:
                if prop["prop_name"] == "Material":
                    prop_query = prop
                    break
            break

    n_obs = sum(prop_query["ground"])
    for i, mat in enumerate(mat_names):
        probs[mat] = prop_query["ground"][i] / n_obs

    ipalm_probs = {}

    # now translate the mat_names from london partners to ipalm_mat_names
    other = 0
    used_mat_names = []
    for name in mat_names:
        if name in ipalm_mat_names:
            ipalm_probs[name] = probs[name]
            used_mat_names.append(name)
        elif name == "plastic":
            ipalm_probs["soft-plastic"] = probs[name]/2
            ipalm_probs["hard-plastic"] = probs[name]/2 
            used_mat_names.append(name)
        else:
            other += probs[name]
    unused_mat_names = []
    for mat_name in ipalm_mat_names:
        if mat_name not in used_mat_names:
            unused_mat_names.append(mat_name)
    for unused in unused_mat_names:
        ipalm_probs[unused] = other / len(unused_mat_names)

    if ipalm_names_flag:
        return ipalm_probs
    else:
        return probs


def get_london_property(obj, property_name):
    for prop in obj['properties']:
        if prop['prop_name'] == property_name:
            return prop
    print("Probably a TYPO? You wanted: object: {} and property name: {}".format(obj['name'], property_name))
    return None


def get_london_object(crawl_data, obj_name):
    for obj in crawl_data:
        if obj['name'] == obj_name:
            return obj
    print("Probably a TYPO? You wanted: object: {}".format(obj_name))
    return None


def compute_std_and_mean(crawl_data, single_property=None):
    """
    Computes Standard deviation for properties with available numeric bins and 'accuracy' for categoricals.

    Parameters
    ----------
    crawl_data : dict
        crawl_data is a dictionary with the available data, originaly in json format. It is iterable and best done soe with iterating via object_names
    single_property : None/string
        if None, returns the std or 'accuracy' equivalent for all properties in a dict. Else returns a single tuple in
        format ("std"/"acc" - string, std/acc - float32, mean - if "std" - float32, elif "acc" - string)

    Returns
    -------
    std : dict / tuple of arrays
        return is based on the property flag, see above 
    """
    def _single_prop(_prop, _errs, _stds, _means):
        if type(_prop['bins'][0]) == int:
            bins = _prop['bins']
            err = "std"
            bin_size = bins[1] - bins[0]  # force the static binsize

            # the real value is assumed to lie in the middle of the bin: e.g. for 0 - 50 bin,
            # the assumed property value is 25, so bin[0]= 0 + (bin_size/2)
            ground_truth = _prop['ground']
            samples = []
            for i in range(len(ground_truth)):
                if ground_truth[i] * (bins[i] + bin_size / 2) == 0:
                    continue
                else:
                    samples.append(ground_truth[i] * (bins[i] + bin_size / 2))
            mean = np.mean(samples)
            std = np.std(samples)
            _errs.append(err)
            _stds.append(std)
            _means.append(mean)

        elif type(_prop['bins'][0]) == str:
            err = "acc"
            bins = _prop['bins']
            ground_truth = _prop['ground']
            normalized_gt = np.array(ground_truth) / sum(ground_truth)
            normalized_gt = normalized_gt[normalized_gt != 0]
            normalized_log2_entropy = -np.sum(normalized_gt * np.log2(normalized_gt)) / len(normalized_gt) / 2

            # this is an ad hoc accuracy error factor (measures whether are
            # the categories EVENLY or SINGULARLY distributed)
            ent_based_accuracy = 1 - normalized_log2_entropy
            argmax = bins[np.argmax(ground_truth)]

            _errs.append(err)
            _stds.append(ent_based_accuracy)
            _means.append(argmax)
        else:
            print("Unknown bin type: ", type(_prop['bins'][0]))
            return None

        return _errs, _stds, _means
        
    object_names = ['bakingtray', 'bottle', 'bowl', 'fork', 'glass', 'hammer', 'kettle', 'knife', 'mug', 'pan', 'plate', 'sodacan', 'spoon', 'wineglass']
    prop_names = ['Material', 'Colour', 'Weight', 'Volume', 'Length', 'Width', 'Height', 'Functionality']
    # check whether are bins numerical
    errs = []
    stds = []
    means = []
    all_stds = {}
    if single_property is None:
        # all properties
        for prop_name in prop_names:
            for obj_name in object_names:
                obj = get_london_object(crawl_data, obj_name)
                prop = get_london_property(obj, prop_name)
                out = _single_prop(prop, [], [], [])
                errs.append(out[0])
                stds.append(out[1])
                means.append(out[2])
            print("errs: \n", errs)
            print("stds: \n", stds)
            if errs[0][0] == 'std':
                means = np.mean(means)
            else:
                means = None
            all_stds[prop_name] = (errs[0][0], np.mean(stds), means)
            errs = []
            stds = []
            means = []
        return all_stds

    else:
        # only for one property "single_property"
        for obj_name in object_names:
            # print("Object: {}, property: {}".format(obj_name, single_property))
            obj = get_london_object(crawl_data, obj_name)
            prop = get_london_property(obj, single_property)
            errs, stds, means = _single_prop(prop, errs, stds, means)

        return errs, stds, means

        # print("single_prop_stats: \n ------------------ \n", single_prop_stats)


def cat_given_mat(crawl_data, mat_name):

    if mat_name == "soft-plastic" or mat_name == "hard-plastic":
        mat_name = "plastic"
    if mat_name == "foam" or mat_name == "paper":
        print("Material '{}' is not in the London dataset. Cannot return probabilities.".format(mat_name))
        return None
    object_names = ['bakingtray', 'bottle', 'bowl', 'fork', 'glass', 'hammer', 'kettle', 'knife', 'mug', 'pan', 'plate', 'sodacan', 'spoon', 'wineglass']
    prop_names = ['Material', 'Colour', 'Weight', 'Volume', 'Length', 'Width', 'Height', 'Functionality']  # shape?, not very informative
    mat_names = ["ceramic", "glass", "metal", "other", "plastic", "silicone", "stone", "wood"]
    ipalm_mat_names = ["ceramic", "metal", "soft-plastic", "hard-plastic", "wood", "paper", "foam"]
    if mat_name not in ipalm_mat_names and mat_name not in mat_names:
        print("[ERR] Unknown material.")
        return None

    category_probs = {}  # also known as object probs
    for obj_name in object_names:
        
        # retrieve p(obj)
        total_n_objects = 0
        for obj in crawl_data:
            if obj["name"] == obj_name:
                obj_query_count = sum(obj["properties"][0]["ground"])
            total_n_objects += sum(obj["properties"][0]["ground"])
        
        p_cat_i = obj_query_count / total_n_objects
        if p_cat_i > 1.0 or p_cat_i < 0.0:
            print("Something went wrong. The p_cat is: ", p_cat_i)

        # retrieve p(mat|cat_i) from dict of p(mat_i|cat_i)
        materials_given_cat_i = mat_given_cat(crawl_data, obj_name, ipalm_names_flag=False)
        p_mat_given_cat_i = materials_given_cat_i[mat_name]

        nominator = p_mat_given_cat_i * p_cat_i

        # retrieve normalizing denominator sum_{j=0_to_n}( p(mat|cat_j) * p(cat_j) )
        denominator = 0
        for obj in crawl_data:
            materials_given_cat_i = mat_given_cat(crawl_data, obj["name"], ipalm_names_flag=False)
            for obj in crawl_data:
                if obj["name"] == obj_name:
                    obj_query_count = sum(obj["properties"][0]["ground"])
                
            p_cat_j = obj_query_count / total_n_objects

            denominator += (materials_given_cat_i[mat_name] * p_cat_j)

        category_probs[obj_name] = nominator / denominator
    
# crawl_data = json2dict("./crawl-data.json")
# # compute_std(crawl_data, "Colour")
# mat_given_cat(crawl_data, )
# print(get_london_object(crawl_data, 'bakingtray'))





        
        

    


    
