Cleaning the previous 'meas.json' file.
----------------------
| ITERATION NUMBER: 0 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.4490 | Cond. likelihood: None
Material:   ceramic | Probabilty: 0.3500 | Cond. likelihood: None
Material:   plastic | Probabilty: 0.1000 | Cond. likelihood: None
Material: aluminium | Probabilty: 0.1000 | Cond. likelihood: None
Material:     joker | Probabilty: 0.0010 | Cond. likelihood: None
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 10.830366873442362 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.7231424935670523   |
| The average sampled Entropy is: 0.42568006647587836   |
| -OUT- Discrete Information Gain: 1.2974624270911739   |
| -info- AVERAGE sampled Information Gain: 1.2974624270911743 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 10.830366873442362 |
| This is the Emulated Entropy for density: 10.196949092972625 |
| Argmax mean: 2301 (no offset)|
| -x- Diff Information Gain: 0.6334177804697365     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.7231424935670523   |
| The average sampled Entropy is: 0.7506647665073041   |
| -OUT- Discrete Information Gain: 0.9724777270597482   |
| -info- AVERAGE sampled Information Gain: 0.972477727059748 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.745632708173032 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.22865721469542688     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.6334177804697365
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2704 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.6190 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0008 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.3731 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0071 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 1 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.6190 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0008 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.3731 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0071 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.162208447637791 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0181436963766242   |
| The average sampled Entropy is: 0.7652314598906248   |
| -OUT- Discrete Information Gain: 0.25291223648599936   |
| -info- AVERAGE sampled Information Gain: 0.25291223648599886 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.162208447637791 |
| This is the Emulated Entropy for density: 8.940992523528317 |
| Argmax mean: 2313 (no offset)|
| -x- Diff Information Gain: 0.22121592410947422     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0181436963766242   |
| The average sampled Entropy is: 0.765530932184242   |
| -OUT- Discrete Information Gain: 0.2526127641923822   |
| -info- AVERAGE sampled Information Gain: 0.2526127641923819 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.745632708173032 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.22865721469542688     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.22865721469542688
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 174 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.4584 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0001 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.5394 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0020 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 2 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.4584 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0001 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.5394 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0020 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.162208447637791 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0161089355619237   |
| The average sampled Entropy is: 0.7867178110081862   |
| -OUT- Discrete Information Gain: 0.22939112455373756   |
| -info- AVERAGE sampled Information Gain: 0.2293911245537379 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.162208447637791 |
| This is the Emulated Entropy for density: 8.940992523528317 |
| Argmax mean: 2313 (no offset)|
| -x- Diff Information Gain: 0.22121592410947422     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.376517960428295 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0161089355619237   |
| The average sampled Entropy is: 0.7932682362804063   |
| -OUT- Discrete Information Gain: 0.2228406992815174   |
| -info- AVERAGE sampled Information Gain: 0.22284069928151756 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.376517960428295 |
| This is the Emulated Entropy for elasticity: 6.142083421397058 |
| Argmax mean: 34 (no offset)|
| -x- Diff Information Gain: 0.2344345390312368     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.2344345390312368
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 168 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.3032 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.6963 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0005 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 3 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.3032 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.6963 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0005 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.162208447637791 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8915742442001808   |
| The average sampled Entropy is: 0.6422077968632885   |
| -OUT- Discrete Information Gain: 0.24936644733689228   |
| -info- AVERAGE sampled Information Gain: 0.2493664473368924 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.162208447637791 |
| This is the Emulated Entropy for density: 8.940992523528317 |
| Argmax mean: 2313 (no offset)|
| -x- Diff Information Gain: 0.22121592410947422     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.306156084088499 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8915742442001808   |
| The average sampled Entropy is: 0.6836225833007554   |
| -OUT- Discrete Information Gain: 0.2079516608994254   |
| -info- AVERAGE sampled Information Gain: 0.2079516608994255 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.306156084088499 |
| This is the Emulated Entropy for elasticity: 6.177543106991452 |
| Argmax mean: 38 (no offset)|
| -x- Diff Information Gain: 0.12861297709704722     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.22121592410947422
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2691 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1709 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.8280 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0012 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 4 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1709 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.8280 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0012 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.394577178581711 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.6723920051621284   |
| The average sampled Entropy is: 0.5419917368284406   |
| -OUT- Discrete Information Gain: 0.13040026833368779   |
| -info- AVERAGE sampled Information Gain: 0.1304002683336877 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.394577178581711 |
| This is the Emulated Entropy for density: 9.329689692283642 |
| Argmax mean: 2325 (no offset)|
| -x- Diff Information Gain: 0.06488748629806906     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.306156084088499 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.6723920051621284   |
| The average sampled Entropy is: 0.5653233872081833   |
| -OUT- Discrete Information Gain: 0.10706861795394507   |
| -info- AVERAGE sampled Information Gain: 0.10706861795394527 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.306156084088499 |
| This is the Emulated Entropy for elasticity: 6.177543106991452 |
| Argmax mean: 38 (no offset)|
| -x- Diff Information Gain: 0.12861297709704722     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.12861297709704722
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 170 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0956 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9042 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0002 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 5 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0956 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9042 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0002 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.394577178581711 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.45819064804804727   |
| The average sampled Entropy is: 0.42170598176011226   |
| -OUT- Discrete Information Gain: 0.03648466628793501   |
| -info- AVERAGE sampled Information Gain: 0.0364846662879351 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.394577178581711 |
| This is the Emulated Entropy for density: 9.329689692283642 |
| Argmax mean: 2325 (no offset)|
| -x- Diff Information Gain: 0.06488748629806906     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.203592920944308 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.45819064804804727   |
| The average sampled Entropy is: 0.4052763571100658   |
| -OUT- Discrete Information Gain: 0.052914290937981445   |
| -info- AVERAGE sampled Information Gain: 0.05291429093798148 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.203592920944308 |
| This is the Emulated Entropy for elasticity: 6.106043925717451 |
| Argmax mean: 68 (no offset)|
| -x- Diff Information Gain: 0.09754899522685712     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.09754899522685712
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 176 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0514 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9486 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0001 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 6 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0514 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9486 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0001 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.394577178581711 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.29304066572279475   |
| The average sampled Entropy is: 0.23632019596236054   |
| -OUT- Discrete Information Gain: 0.05672046976043421   |
| -info- AVERAGE sampled Information Gain: 0.05672046976043425 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.394577178581711 |
| This is the Emulated Entropy for density: 9.329689692283642 |
| Argmax mean: 2325 (no offset)|
| -x- Diff Information Gain: 0.06488748629806906     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.066132416626175 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.29304066572279475   |
| The average sampled Entropy is: 0.2745942006878227   |
| -OUT- Discrete Information Gain: 0.018446465034972048   |
| -info- AVERAGE sampled Information Gain: 0.01844646503497213 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.066132416626175 |
| This is the Emulated Entropy for elasticity: 5.968844335379347 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.09728808124682775     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.09728808124682775
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 165 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0270 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9730 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 7 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0270 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9730 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.394577178581711 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.17930945249445665   |
| The average sampled Entropy is: 0.13300182503633365   |
| -OUT- Discrete Information Gain: 0.046307627458123   |
| -info- AVERAGE sampled Information Gain: 0.04630762745812302 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.394577178581711 |
| This is the Emulated Entropy for density: 9.329689692283642 |
| Argmax mean: 2325 (no offset)|
| -x- Diff Information Gain: 0.06488748629806906     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.917398156088119 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.17930945249445665   |
| The average sampled Entropy is: 0.16162792672626142   |
| -OUT- Discrete Information Gain: 0.017681525768195233   |
| -info- AVERAGE sampled Information Gain: 0.0176815257681952 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.917398156088119 |
| This is the Emulated Entropy for elasticity: 5.817741305181467 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.09965685090665222     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.09965685090665222
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 174 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0140 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9860 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 8 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0140 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9860 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.394577178581711 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.10642535200974783   |
| The average sampled Entropy is: 0.10223899522313415   |
| -OUT- Discrete Information Gain: 0.004186356786613679   |
| -info- AVERAGE sampled Information Gain: 0.004186356786613694 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.394577178581711 |
| This is the Emulated Entropy for density: 9.329689692283642 |
| Argmax mean: 2325 (no offset)|
| -x- Diff Information Gain: 0.06488748629806906     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.773752421708219 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.10642535200974783   |
| The average sampled Entropy is: 0.08826317750053399   |
| -OUT- Discrete Information Gain: 0.01816217450921384   |
| -info- AVERAGE sampled Information Gain: 0.01816217450921379 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.773752421708219 |
| This is the Emulated Entropy for elasticity: 5.676074478751838 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.09767794295638055     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.09767794295638055
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 168 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0072 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9928 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 9 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0072 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9928 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.394577178581711 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.061827491865396   |
| The average sampled Entropy is: 0.06018452295500512   |
| -OUT- Discrete Information Gain: 0.0016429689103908823   |
| -info- AVERAGE sampled Information Gain: 0.0016429689103908676 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.394577178581711 |
| This is the Emulated Entropy for density: 9.329689692283642 |
| Argmax mean: 2325 (no offset)|
| -x- Diff Information Gain: 0.06488748629806906     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.642404613062734 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.061827491865396   |
| The average sampled Entropy is: 0.06152783621229809   |
| -OUT- Discrete Information Gain: 0.0002996556530979119   |
| -info- AVERAGE sampled Information Gain: 0.00029965565309791024 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.642404613062734 |
| This is the Emulated Entropy for elasticity: 5.550183624918946 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.09222098814378832     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.09222098814378832
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 166 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0037 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9963 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
MODE 2 was chosen --> optimizing was done for differential information gain.
The argmax mean for density : 2325
The argmax mean for elasticity : 69

