#!/usr/bin/env python
from __future__ import print_function
# python version = 2.7
import threading
# import sys
import rospy, rosservice
# import rosservice
import math, os, signal, time, tf
import copy, yaml, argparse
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import distance, transform
from itertools import permutations
import angles_n_matrix

from geometry_msgs.msg import Point, Pose, Twist, Vector3
from std_msgs.msg import Float32, String, Float64, Int32, Bool
from tf2_msgs.msg import TFMessage

from kortex_driver.msg import BaseCyclic_Feedback, ActionType, ActionNotification, ConstrainedPose, ActionEvent, Finger, \
    GripperMode, CartesianSpeed, JointAngle, Wrench, WrenchCommand, PayloadInformation
from kortex_driver.srv import Base_ClearFaults, ExecuteAction, ExecuteActionRequest, SetCartesianReferenceFrame, \
    OnNotificationActionTopic, OnNotificationActionTopicRequest, \
    SendGripperCommand, SendGripperCommandRequest, GetMeasuredCartesianPose, SendTwistCommand, SendTwistCommandRequest, \
    SendWrenchCommand, SendWrenchCommandRequest, Stop, PlayJointTrajectory, PlayJointTrajectoryRequest, \
    GetPayloadInformation, \
    ResetPayloadInformation, SetPayloadInformation  # , StringString

import plot_youngs_modulus
from grasp_lib import gimme_grasp, gimme_all_grasps

"""
ARGUMENT FLAGS:
"""

measure = False
weightEstimation = False
grid = False
stiffnessSort = False

"""
YAML CONFIG:
"""

path_to_yaml = "/home/robot3/vision_ws/config/"
with open(os.path.join(path_to_yaml, "kinova_config.yaml"), "r") as file:
    yaml_config = yaml.load(file)
visualize = False  # visualization flag
"""plot_youngs_modulus.py HERE"""

grip = plot_youngs_modulus.GrippingSessions()
# grip.load_all("2020_08_25/")
# grip.load_all("12_objectsx5/")
grip.init_clean_data(max_cur=1.00, slope=8e4, resolution=500, limit=0.05, window_size=1, show=False)
grip.init_linear_class(show=False)

# grip.display_raw(idx=2, block=False, color=(0,0,1))
# grip.display_raw(pos=grip.grips[2]["position"], cur=grip.filter_current(grip.grips[2]["current"]))
# grip_name = "ipalm-grasping/Kinova_Gen3/Measurements/2f-85/2020_08_24/2020-Aug-24-13-01--1-3.npy"
# matt =  grip.npy2grip(grip_name)

#  black cube position: 0.42529; 0.058792; 0.20714
#  black cube orientation: 0.70715; 0.70706; 0.0021613; -0.00045053
#  black cube rel pos: 0; 0; -0.061525
#  black cube rel ori: 1; 0; -5.4969e-33; 1.6155e-15

# white marble block position: 0.42595; -0.06103; 0.2168
# white marble block orientation: 0.72479; 0.68892; -0.0028575; 0.0078871
# white marble block rel pos: 0; 0; -0.061525
# white marble block rel ori: 1; 0; -5.4969e-33; 1.6155e-15

class IpalmGrasping:

    # Initialization functions

    def __init__(self):

        self.robot_name = check_name()
        if self.robot_name is None:
            rospy.logerr('unrecognized robot name')
        self.ARUCO_MARKER_IDS = [0]
        self.OBJ_IDS = yaml_config['obj_ids']  # TODO generalize ??? = 0-99
        self.BASE_FRAME = 1
        self.TOOL_FRAME = 2
        self.OPENED_GRIPPER_POSITION = 0.01
        self.CLOSED_GRIPPER_POSITION = 98.0
        self.GRIPPER_CURRENT_LIMIT = 0.05  # f.e. 0.6 for measurement
        self.gripper_feedback = None

        self.TRANSLATION_SPEED = yaml_config['speed_limit']  # 0.30 seems to be the limit
        # self.ORIENTATION_SPEED = 30
        self.ORIENTATION_SPEED = 30

        self.BIG_RADIUS = 0.25
        self.MEDIUM_RADIUS = 0.10
        self.SMALL_RADIUS = 0.01
        self.NULL_RADIUS = 0.001
        self.SMALL_RADIUS_ANGLE = 1
        self.ANGULAR_TOLERANCE = 2
        self.ANGULAR_SPEED_LIMIT = 10
        self.RESTRICTED_RADIUS = 0.16  # 0.16m from the origin
        self.WORKSPACE_AREA = [0.20, 0.90, -0.40,
                               0.40]  # list of [x1, x2, y1, y2], 2 coordinates defining the workspace area

        # TODO put to config
        self.bop2ycb_dict = {
            '1': '002_master_chef_can',
            '2': '003_cracker_box',
            '3': '004_sugar_box',
            '4': '005_tomato_soup_can',
            '5': '006_mustard_bottle',
            '6': '007_tuna_fish_can',
            '7': '008_pudding_box',
            '8': '009_gelatin_box',
            '9': '010_potted_meat_can',
            '10': '011_banana',
            '11': '019_pitcher_base',
            '12': '021_bleach_cleanser',
            '13': '024_bowl',
            '14': '025_mug',
            '15': '035_power_drill',
            '16': '036_wood_block',
            '17': '037_scissors',
            '18': '040_large_marker',
            '19': '051_large_clamp',
            '20': '052_extra_large_clamp',
            '21': '061_foam_brick',
        }

        # Grasping speed

        self.CLOSING_SPEED = - CLOSING_SPEED

        # Object lists
        self.top_poses = []
        self.grasp_poses = []
        self.object_poses = []  # 6D object poses
        self.current_feedback_list = []  # this is for the gripper only
        self.gripper_position_list = []
        self.weight_joint_current = []
        self.joint_currents = []
        self.general_measurement_list = []
        self.weight_flag = False  # writing to the self.weight_joint_current list
        self.weightCalibration = False
        self.gripper_weight = 0.8992 - 0.0352857505809
        self.gripper_tau_0 = None
        self.redo_measurement = True

        # Cartesian poses we need
        self.ignored_areas = []

        self.pose_starting = coords2twist(yaml_config['pose_starting'])
        # self.pose_starting = Twist(Vector3(0.352, -0.032, 0.410), Vector3(179.900, 0.001, 90.000))
        self.pose_basket = coords2twist(yaml_config['basket_mid'])
        # self.pose_basket = Twist(Vector3(0.0, -0.2, 0.40), Vector3(179.900, 0.001, 90.000)) # Current Basket size is 24 x 24 cm

        basket_coords = yaml_config['basket_mid'][0]
        # centres of baskets: (x1: 0.32 y1: -0.28) white - the softest, (x2: 0.65 y2: -0.28) blue - middle soft,  (x3:0.65 y3:0.28) brown - middle stiff, (x4: 0.32, y4: +0.28) red - stiff;
        # basket size 24 x 24 cm
        self.mids_grid = np.array([[[33.0, -29.5], [32.2, -10.2], [32.8, 8.8], [33.0, 28.0]],
                                   [[50.5, -29.7], [50.2, -10.2], [50.0, 9.3], [50.2, 28.8]],
                                   [[68.2, -30.3], [68.2, -10.2], [68.2, 9.7], [69.0, 31.5]]]) / 100

        self.sorting_baskets_centers = [[0.30, -0.28, 0.1], [0.60, -0.28, 0.1], [0.60, 0.28, 0.1], [0.30, 0.28, 0.1]]
        self.sorting_baskets_sizes = [[0.24, 0.24, 0.185], [0.24, 0.24, 0.185], [0.24, 0.24, 0.185],
                                      [0.24, 0.24, 0.185]]

        self.set_ignore_area(basket_coords[0] - 0.14, basket_coords[1] - 0.14, basket_coords[0] + 0.14,
                             basket_coords[1] + 0.14)
        self.pose_transition = Twist(Vector3(0.0, -0.2, 0.25), Vector3(179.900, 0.001, 90.000))

        self.obj_trans = [0.352, -0.032, 0.410]
        self.obj_rot = [179.900, 0.001, 90.000]

        # Angular pose we need

        self.pose_angle_starting = yaml_config['start_angles']  # in a seven shape
        self.current_joint_angle = [0.0, 0.0, 180.0, 270.0, 0.0, 270.0, 90.0]


        # Gripper positions we need
        self.gripper_opened = 0.00
        self.gripper_closed = 0.85
        self.dont_look_ID = "-1"
        # self.set_ignore_area(0.58, -0.19, 0.4, 0.19) # ignores the middle rectangle

        # Statistics
        self.start_time = time.time()
        self.elapsed_time = self.start_time

        # Services
        self.init_services()

        # Subscribers

        self.actual_tool_pose = Twist()
        self.basefeedback = rospy.Subscriber("/" + self.robot_name + "/base_feedback", BaseCyclic_Feedback,
                                             self.base_feedback_callback, buff_size=1)

        self.tf_feedback = rospy.Subscriber("/tf", TFMessage, self.tf_callback)

        # Events
        self.action_type = ActionType.UNSPECIFIED_ACTION
        self.is_action_successful = False
        self.is_action_completed = threading.Event()
        self.is_action_completed.clear()

        # Subscribe to the ActionNotification
        req = OnNotificationActionTopicRequest()
        rospy.loginfo("Activating the action notifications...")
        try:
            self.activate_publishing_of_action_notification(req)
        except rospy.ServiceException:
            rospy.logerr("Failed to call OnNotificationActionTopic")
        else:
            rospy.loginfo("Successfully activated the Action Notifications!")

        rospy.Subscriber("/" + self.robot_name + "/action_topic", ActionNotification, self.action_notification_callback)

        self.actuator_current_motor = [0 for _ in range(yaml_config['dof'])]

    """
    ROS-TRANSLATION FUNCTIONS:
    """

    def init_services(self):
        # Services
        print("Starting to clear")
        clear_faults_full_name = '/' + self.robot_name + '/base/clear_faults'
        print("Waiting for service")
        rospy.wait_for_service(clear_faults_full_name)
        print("Clearing faults")
        self.clear_faults = rospy.ServiceProxy(clear_faults_full_name, Base_ClearFaults)
        # print(self.clear_faults)

        execute_action_full_name = '/' + self.robot_name + '/base/execute_action'
        rospy.wait_for_service(execute_action_full_name)
        self.execute_action = rospy.ServiceProxy(execute_action_full_name, ExecuteAction)
        #
        set_cartesian_reference_frame_full_name = '/' + self.robot_name + '/control_config/set_cartesian_reference_frame'
        rospy.wait_for_service(set_cartesian_reference_frame_full_name)
        self.set_cartesian_reference_frame = rospy.ServiceProxy(set_cartesian_reference_frame_full_name,
                                                                SetCartesianReferenceFrame)
        #
        activate_publishing_of_action_notification_full_name = '/' + self.robot_name + '/base/activate_publishing_of_action_topic'
        rospy.wait_for_service(activate_publishing_of_action_notification_full_name)
        self.activate_publishing_of_action_notification = rospy.ServiceProxy(
            activate_publishing_of_action_notification_full_name, OnNotificationActionTopic)
        #
        send_gripper_command_full_name = '/' + self.robot_name + '/base/send_gripper_command'
        rospy.wait_for_service(send_gripper_command_full_name)
        self.send_gripper_command = rospy.ServiceProxy(send_gripper_command_full_name, SendGripperCommand)
        #
        # Found out this can be only used for adding and getting info about "custom" payload information
        get_payload_information = '/' + self.robot_name + '/control_config/get_payload_information'
        rospy.wait_for_service(get_payload_information)
        self.get_payload_information = rospy.ServiceProxy(get_payload_information, GetPayloadInformation)
        #
        get_measured_cartesian_pose_full_name = '/' + self.robot_name + '/base/get_measured_cartesian_pose'
        rospy.wait_for_service(get_measured_cartesian_pose_full_name)
        self.get_measured_cartesian_pose = rospy.ServiceProxy(get_measured_cartesian_pose_full_name,
                                                              GetMeasuredCartesianPose)
        #
        stop_action_full_name = '/' + self.robot_name + '/base/stop'
        rospy.wait_for_service(stop_action_full_name)
        self.stop_the_robot = rospy.ServiceProxy(stop_action_full_name, Stop)
        #
        self.twist_command = SendTwistCommandRequest()
        send_twist_command_full_name = '/' + self.robot_name + '/base/send_twist_command'
        rospy.wait_for_service(send_twist_command_full_name)
        self.send_twist_command = rospy.ServiceProxy(send_twist_command_full_name, SendTwistCommand)
        #
        self.wrench_command = SendWrenchCommandRequest()
        send_wrench_command_full_name = '/' + self.robot_name + '/base/send_wrench_command'
        rospy.wait_for_service(send_wrench_command_full_name)
        self.send_wrench_command = rospy.ServiceProxy(send_wrench_command_full_name, SendWrenchCommand)

        play_joint_trajectory_full_name = '/' + self.robot_name + '/base/play_joint_trajectory'
        rospy.wait_for_service(play_joint_trajectory_full_name)
        self.play_joint_trajectory = rospy.ServiceProxy(play_joint_trajectory_full_name, PlayJointTrajectory)

        # self.obj_feedback = rospy.Service("/" + self.robot_name + "/ignored_ids", StringString, self.id_handler) # a Service instance for string comm

        # Callbacks

    def tf_callback(self, feedback):  # gets called really fast
        """
        This function is called everytime there is a change in the /tf, listen to the transform between the grasping pose
        and the base of the robot, this pose can be send directly to the reach_cartesian_pose() function. The tfs can be
        visualize in the Terminal with the command `rostopic echo tf` when the kortex is launched
        Args:
            feedback: class with all the tfs, it is better to use the listener

        Returns: self.grasping_position is filled with poses
        """
        # print(feedback.transforms[5])
        self.top_poses = []  # a LIST OF TUPLES [(((translation), (rotation)), obj_id), ... ]
        if measure:  # IN ARUCO_MARKER MODE, sends -1 as the object_id, for test purposes
            for object_id in range(len(self.ARUCO_MARKER_IDS)):
                try:
                    (self.obj_trans,
                     self.obj_rot) = listener.lookupTransform(yaml_config["base"],
                                                              yaml_config["grasp_above_cam"].format(0, object_id),
                                                              rospy.Time(0))  # rotation output in quaternions
                    rotation_rad = tf.transformations.euler_from_quaternion(self.obj_rot)  # using only camera 0
                    rotation = tuple([x * 180 / math.pi for x in rotation_rad])  # conversion in degrees

                    # self.grasping_position.append((
                    #     Twist(Vector3(self.marker_trans[0], self.marker_trans[1], self.marker_trans[2]),
                    #           Vector3(rotation[0], rotation[1], rotation[2])), -1))
                    self.top_poses.append((((self.obj_trans[0], self.obj_trans[1], self.obj_trans[2]), (rotation[0], rotation[1], rotation[2])), -1))

                except(tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                    pass
        else:  # IN VISION MODE (NO ARUCO)
            for object_id in self.OBJ_IDS:
                # not going for the object, if it is being ignored, might be solved with "all knowing" self.is_ignored

                try:
                    (self.obj_trans, self.obj_rot) = listener.lookupTransform(yaml_config["base"],
                                                                              yaml_config["grasp_above"].format(object_id),
                                                                              rospy.Time(0))  # rotation output in quaternions
                    # Converts quaternions to degrees
                    rotation_rad = tf.transformations.euler_from_quaternion(self.obj_rot)
                    rotation = tuple([x * 180 / math.pi for x in rotation_rad])
                    # print("obj_{}".format(object_id), self.marker_trans)  # reading OK
                    # Fill the object list
                    if not self.is_ignored(self.obj_trans):
                        # self.grasping_position.append((Twist(
                        #     Vector3(self.marker_trans[0], self.marker_trans[1], self.marker_trans[2]),
                        #     Vector3(rotation[0], rotation[1], rotation[2])), object_id))
                        self.top_poses.append((((self.obj_trans[0], self.obj_trans[1],
                                                 self.obj_trans[2]),
                                                (rotation[0], rotation[1], rotation[2])), object_id))
                    else:
                        if not hasattr(self, "ig_msg_counter"):
                            self.ig_msg_counter = 0
                        self.ig_msg_counter += 1
                        if self.ig_msg_counter >= 1000:
                            self.ig_msg_counter = 0
                            print(bcolors.OKBLUE + "[INFO] This object {} is in an IGNORED AREA".format(
                                object_id) + bcolors.ENDC)

                except(tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                    pass

                try:
                    (obj_t, obj_r) = listener.lookupTransform(yaml_config["base"],
                                                              yaml_config["6Dpose"].format(object_id),
                                                              rospy.Time(0))  # rotation output in quaternions
                    # Converts quaternions to degrees
                    rotation_rad = tf.transformations.euler_from_quaternion(obj_r)
                    rotation = tuple([x * 180 / math.pi for x in rotation_rad])
                    # print("obj_{}".format(object_id), self.marker_trans)  # reading OK
                    # Fill the object list
                    self.object_poses.append((((obj_t[0], obj_t[1],
                                             obj_t[2]),
                                             (rotation[0], rotation[1], rotation[2])), object_id))
                    if self.is_ignored(self.obj_trans):
                        if not hasattr(self, "ig_msg_counter"):
                            self.ig_msg_counter = 0
                        self.ig_msg_counter += 1
                        if self.ig_msg_counter >= 1000:
                            self.ig_msg_counter = 0
                            print(bcolors.OKBLUE + "[INFO] This object {} is in an IGNORED AREA".format(
                                object_id) + bcolors.ENDC)
                except(tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                    pass

                try:
                    (obj_t, obj_r) = listener.lookupTransform(yaml_config["base"],
                                                              yaml_config["gripper"].format(object_id),
                                                              rospy.Time(0))  # rotation output in quaternions
                    # Converts quaternions to degrees
                    rotation_rad = tf.transformations.euler_from_quaternion(obj_r)
                    rotation = tuple([x * 180 / math.pi for x in rotation_rad])
                    # print("obj_{}".format(object_id), self.marker_trans)  # reading OK
                    # Fill the object list
                    self.grasp_poses.append((((obj_t[0], obj_t[1],
                                             obj_t[2]),
                                             (rotation[0], rotation[1], rotation[2])), object_id))

                except(tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                    pass

    def action_notification_callback(self, notif):
        if self.action_type == notif.handle.action_type:
            if notif.action_event == ActionEvent.ACTION_END:
                self.is_action_completed.set()
                self.is_action_successful = True
            elif notif.action_event == ActionEvent.ACTION_ABORT:
                self.is_action_completed.set()
                self.is_action_successful = False

    def base_feedback_callback(self, feedback):
        """
        This function returns the feedback from topic manipulator/base, can be visualized in the terminal using the
        command `rostopic echo manipulator/base_feedback` when the kortex is launched. Used to know the current gripper
        position and current, and the current joints angle
        Args:
            feedback: class with all the attributes from base_feedback
        """
        self.actual_tool_pose.linear.x = feedback.base.tool_pose_x
        self.actual_tool_pose.linear.y = feedback.base.tool_pose_y
        self.actual_tool_pose.linear.z = feedback.base.tool_pose_z
        self.actual_tool_pose.angular.x = feedback.base.tool_pose_theta_x
        self.actual_tool_pose.angular.y = feedback.base.tool_pose_theta_y
        self.actual_tool_pose.angular.z = feedback.base.tool_pose_theta_z
        self.gripper_feedback = feedback.interconnect.oneof_tool_feedback.gripper_feedback
        if self.gripper_feedback is not None:
            self.gripper_current = self.gripper_feedback[0].motor[0].current_motor
        self.current_gripper_position = self.gripper_feedback[0].motor[0].position

        self.weighing_joint_torque = feedback.actuators[3].torque

        temp_joint_currents = []
        for i in range(7):
            temp_joint_currents.append(feedback.actuators[i].current_motor)
        self.joint_currents = temp_joint_currents

        if self.weight_flag:  # turns on writing the current of the joints and the [n] joint
            self.weight_joint_current.append(feedback.actuators[5].current_motor)

        temp_joint_angles = []
        for i in range(7):
            temp_joint_angles.append(feedback.actuators[i].position)
        self.current_joint_angle = temp_joint_angles

        # self.actual_tool_pose is the 6D orientation

    def broadcast_tf(self, translation, rotation, target, parent=yaml_config["base"]):  # rotation is quaternion x,y,z,w
        """
        Broadcast the transform from target to parent, creates target if doesn't exist
        Args:
            translation: array 3x1, vector of translation in x,y,z
            rotation: array 4x1, angles in quaternions
            target: string, name of target topic
            parent: string, name of parent topic
        """

        if not hasattr(self, "br"):
            self.br = tf.TransformBroadcaster()

        if len(rotation) == 4:
            self.br.sendTransform(translation,
                                  rotation,
                                  rospy.Time.now(),  # also broadcasts rostime
                                  target,
                                  parent)
        elif len(rotation) == 3:
            if np.shape(rotation) == (3, 3):
                # r = transform.rotation.Rotation.from_dcm(rotation)
                r = angles_n_matrix.rotation_matrix_to_euler_angles(np.array(rotation))

                # print("r.as_euler('xyz', degrees=True):", r.as_euler("xyz", degrees=True))
                # eul = r.as_euler("xyz", degrees=True)
                # rot = tf.transformations.quaternion_from_euler(eul[0], eul[1], eul[2])
                rot = tf.transformations.quaternion_from_euler(r[0], r[1], r[2])
            else:
                rot = tf.transformations.quaternion_from_euler(rotation)

            self.br.sendTransform(translation,
                                  rot,
                                  rospy.Time.now(),  # also broadcasts rostime
                                  target,
                                  parent)
        else:
            print(bcolors.FAIL+"[ERROR] broadcast_tf: rotation entered wrong."+bcolors.ENDC)

    """
    SAFETY FUNCTIONS:
    """

    def is_in_workspace(self, point):
        if (min(self.WORKSPACE_AREA[0:2]) < point[0] < max(self.WORKSPACE_AREA[0:2])) and (
                min(self.WORKSPACE_AREA[2:4]) < point[1] < max(self.WORKSPACE_AREA[2:4])):
            return True
        return False

    def add_classif_area(self, x1, x2, y1, y2, areaLabel):
        """

        :param x1:
        :param x2:
        :param y1:
        :param y2:
        :param areaLabel:
        :return:
        """
        if areaLabel not in self.CLASSIF_AREAS:
            self.CLASSIF_AREAS[areaLabel] = [x1, x2, y1, y2]
        else:
            print(bcolors.WARNING + '[WARN] The label {} is already in the CLASSIF_AREAS dict. Cannot add.'.format(
                areaLabel) + bcolors.ENDC)

    def set_ignore_area(self, xpos1, ypos1, xpos2, ypos2):
        self.ignored_areas.append((xpos1, xpos2, ypos1, ypos2))

    def is_ignored(self, place):
        for area in self.ignored_areas:
            if (min(area[0:2]) < place[0] < max(area[0:2])) and (min(area[2:4]) < place[1] < max(area[2:4])):
                return True
        return False

    def is_in_restricted_area(self, x_m, y_m, z_m):
        """
        Function that returns True, if the point set by the manipulator is in the restricted area.
        Args:
            x_m: x coord of the manipulator
            y_m: y coord of the manipulator
            z_m: z coord of the manipulator (for future uses)

        Returns:
            bool: True if the point is restricted
                : False otherwise
        """

        if distance.sqeuclidean((x_m, y_m), (0, 0)) <= self.RESTRICTED_RADIUS ** 2:  # 0.17:
            return True
        return False

    def will_cross_restricted_area(self, a, b, c, radius, x=0, y=0):
        """
        Args:
            The following are parametres for a general line equation ax + by + c = 0
            a: (a*x)
            b: (b*y)
            c: (c)
            radius: circular distance from the NONO point
            x: x of the NONO point
            y: y of the NONO point

        Returns:
            bool: True if the line crosses a restricted area

        """
        dist = ((abs(a * x + b * y + c)) /
                math.sqrt(a * a + b * b))

        if dist <= radius:
            print(bcolors.WARNING + "[WARN] Planned trajectory intersects a restricted area!" + bcolors.ENDC)
            return True

        print(bcolors.OKBLUE + "[INFO] Planned trajectory should be safe." + bcolors.ENDC)

        return False

    """
    UTILITY and MEASURE FUNCTIONS
    """

    def get_ycb_pose(self, grasp_pos, bop_object_id=None, lookup_name=yaml_config["6Dpose"]):
        """
            :param grasp_pos: Pose wrt base in form: ( ( (coords: float x, y, z), (rots : float a_x, a_y, a_z) ), objectID )
            :param bop_object_id: ID of object, if you want to separate position from id
            :param lookup_name: Name of transform to lookup w/o object ID
        :return: relative position & rotation of gripper to YCB object ((coords: float x, y, z), (rots : float a_x, a_y, a_z))
        """
        print("getting a convenient pose")
        if bop_object_id is None:
            bop_object_id = grasp_pos[1]
        # print(bop_object_id)
        # ycb_object_id = get_bop2ycb(bop_object_id)
        if bop_object_id is not None:
            object_string_id = lookup_name.format(bop_object_id)
            # print(object_string_id)
            (pos, rot) = well_behaved_lookup_transform(parent=yaml_config["base"], child=object_string_id)
            nrot = (rot[2], rot[0], rot[1])  # swap of the axis
            print(bcolors.HEADER + "FOLLOWING INFORMATION FROM get_ycb_pose." + bcolors.ENDC)
            print("The original position: ")
            print(pos)
            print("The original rotation: ")
            print(rot)
            print("Those are the switched rotations: ", nrot)
            q = angles_n_matrix.euler_angles_to_rotation_matrix(nrot)
            relative_pose = gimme_grasp(q, bop_object_id)

            # r = transform.Rotation.from_euler(seq="xyz", angles=nrot, degrees=True)  # (seq="xyz", angles=rot, degrees=False)  # dcm = rotation matrix
            # (gripper_pos, gripper_rot)
            # print(bcolors.OKBLUE+"gripper_pos, gripper_rot", gripper_pos, gripper_rot, bcolors.ENDC)
            # print(bcolors.OKBLUE+"gripper_pos, gripper_rot", relative_pose[0], relative_pose[1], bcolors.ENDC)
            # new_gripper_coord = coords2twist((gripper_pos, gripper_rot))
            # print("THIS IS GRIPPER coords: ", new_gripper_coord)
            # return new_gripper_coord

            return relative_pose
        else:
            print(bcolors.OKBLUE + "[INFO] Using original pose." + bcolors.ENDC)
            return grasp_pos

    def shubhan_broadcaster(self, grasp_pos, bop_object_id=None, lookup_name=yaml_config["6Dpose"]):
        print(bcolors.OKBLUE + "[INFO] Broadcasting all grasps" + bcolors.ENDC )
        if bop_object_id is None:
            bop_object_id = grasp_pos[1]
        if bop_object_id is not None:
            object_string_id = lookup_name.format(bop_object_id)
            (pos, rot) = well_behaved_lookup_transform(parent=yaml_config["base"], child=object_string_id)
            r = transform.Rotation.from_euler(seq="xyz", angles=rot,
                                              degrees=True)  # (seq="xyz", angles=rot, degrees=False)  # dcm = rotation matrix
            # (gripper_pos, gripper_rot)
            all_grasps = gimme_all_grasps(r.as_dcm(), bop_object_id)
            print("All grasps: ", all_grasps)
        for i, current_grasp in enumerate(all_grasps):
            best_gripper_rot = current_grasp["Rotation"]
            best_gripper_trans = current_grasp["Translation"]
            gripper_wrt_obj = np.matmul(r.as_dcm(), best_gripper_trans)
            gripper_rot_mat = np.matmul(r.as_dcm(), best_gripper_rot)
            for _ in range(1000):
                self.broadcast_tf(gripper_wrt_obj, gripper_rot_mat, "/A{0}_{1}".format(i, bop_object_id),
                                  yaml_config["6Dpose"].format(bop_object_id))

    def visualize(self, x, y, itemid):

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        # xs = []  # x values array
        # ys = []  # y values array
        ax.clear()
        ax.plot(x, y, label=itemid)
        plt.show(block=False)

    def calculate_camera_x_y(self, z_cloud):
        # Linear regression found experimentally
        # Probably needs to be altered for different setups
        x = 0.00004 * (z_cloud * z_cloud) + 0.00001 * z_cloud + 0.317
        y = 0.00018 * (z_cloud * z_cloud * z_cloud) - 0.00333 * (z_cloud * z_cloud) + 0.009 * z_cloud + 0.385

        return (x, y)

    def get_camera_target_point(self):
        coordinates = self.srv_get_xyz_coordinates().point
        x_pixels = coordinates.x
        y_pixels = coordinates.y
        z_cloud = coordinates.z

        # Convert the pixels
        # This could be much much better
        x = x_pixels * 0.001
        y = y_pixels * 0.001

        return (x, y, z_cloud)

    def normalize_vector3(self, x, y, z):

        temp = (x * x) + (y * y) + (z * z)

        if temp == 0:
            temp = 0.001

        norm = math.sqrt(temp)

        x_normalized = x / norm
        y_normalized = y / norm
        z_normalized = z / norm

        return (x_normalized, y_normalized, z_normalized)

    def is_twist_command_null(self):
        return self.twist_command.input.twist.linear_x == 0 and \
               self.twist_command.input.twist.linear_y == 0 and \
               self.twist_command.input.twist.linear_z == 0 and \
               self.twist_command.input.twist.angular_x == 0 and \
               self.twist_command.input.twist.angular_y == 0 and \
               self.twist_command.input.twist.angular_z == 0

    def process_and_send_wrench_to_robot(self, wrench, use_normalized_wrench, translation_speed_limit,
                                         orientation_speed_limit, reference_frame):

        if use_normalized_wrench:
            (direction_x, direction_y, direction_z) = self.normalize_vector3(wrench.force_x, wrench.force_y,
                                                                             wrench.force_z)
            (direction_x_theta, direction_y_theta, direction_z_theta) = self.normalize_vector3(wrench.torque_x,
                                                                                               wrench.torque_y,
                                                                                               wrench.torque_z)
            target = Twist(Vector3(direction_x, direction_y, direction_z),
                           Vector3(direction_x, direction_y_theta, direction_z_theta))
        else:
            target = wrench
            p_gain = 1
            minimum_translation_speed = 0.01
            minimum_orientation_speed = 1

        # Fill the wrench command for the arm
        self.wrench_command.input.wrench.force_x = wrench.force_x
        self.wrench_command.input.wrench.force_y = wrench.force_y
        self.wrench_command.input.wrench.force_z = wrench.force_z
        self.wrench_command.input.wrench.torque_x = 0.0
        self.wrench_command.input.wrench.torque_y = 0.0
        self.wrench_command.input.wrench.torque_z = 0.0

        # Send the command to the arm
        self.wrench_command.input.reference_frame = reference_frame
        self.wrench_command.input.duration = 1000  # ms
        # print(bcolors.BOLD + "this is wrench BEFORE SENDING: {}".format(wrench) + bcolors.ENDC)
        # print(bcolors.BOLD + "this is wrench COMMAND BEFORE SENDING: {}".format(self.wrench_command) + bcolors.ENDC)
        self.send_wrench_command(self.wrench_command)

    def sort_to_baskets(self, stiffness, baskets_list, baskets_sizes):
        """
        :param stiffness: which basket to choose based on stiffness
        :param baskets_list: list of basket coordinates, sorted in a manner of stiffness
        :return: puts currently held object into the chosen basket
        """
        # TODO: add basket sizes to ignored areas?
        # TODO 2: switch between basket settings according to N_OF_SORT_BASKETS

        basket_pose = (baskets_list[stiffness], (179.900, 0.001, 90.000))
        twisted_basket_pose = coords2twist(basket_pose)
        near_pose = self.calculate_near_pose(twisted_basket_pose, 0.2)
        self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED)
        self.send_gripper_command_and_wait_for_completion(self.gripper_opened)

    """
    MOVEMENT FUNCTIONS
    """

    def send_gripper_command_speed(self, value, current):
        """
        Send speed to open or close the gripper using SendGripperCommand service
        Args:
            value: float, speed in m/s, the gripper open when > 0 and closes when < 0
            current: float, current gripper current in self.gripper current
        Returns: bool, False if the object is grasped
        """
        # Initialize request
        req = SendGripperCommandRequest()
        temp_finger = Finger()
        temp_finger.finger_identifier = 0
        if current < self.GRIPPER_CURRENT_LIMIT:  # 0.05 to grip an object is enough
            temp_finger.value = value
        else:
            temp_finger.value = 0
        req.input.gripper.finger.append(temp_finger)
        req.input.mode = GripperMode.GRIPPER_SPEED
        # Try to send request
        try:
            self.send_gripper_command(req)
        except rospy.ServiceException:
            rospy.logerr("Failed to call SendGripperCommand")

        rospy.sleep(0.01)
        if current < self.GRIPPER_CURRENT_LIMIT:
            return True
        else:
            return False

    def send_gripper_command_and_wait_for_completion(self, value):
        """
        Move the gripper to an opening position
        Args:
            value: float, relative opening of the gripper, opened when = 0 and closed when = 1
        """
        # Create the gripper command request
        req = SendGripperCommandRequest()
        finger = Finger()
        finger.finger_identifier = 0.0
        finger.value = value
        req.input.gripper.finger.append(finger)
        req.input.mode = GripperMode.GRIPPER_POSITION
        # Call the service
        try:
            self.send_gripper_command(req)
        except rospy.ServiceException:
            rospy.logerr("Failed to call SendGripperCommand")
            return False
        else:
            rospy.loginfo("Sending the gripper position...")

            # Wait a bit
        rospy.sleep(0.5)
        return True

    def send_joint_angles_fct(self, target_angles, meas=False):
        """
        Move the joints of the robot to the starting position, can unstick the robot after multiple movements
        """
        # Initialize request
        req = PlayJointTrajectoryRequest()
        for i in range(7):
            temp_angle = JointAngle()
            temp_angle.joint_identifier = i
            temp_angle.value = target_angles[i]  # [0,0,0,0,0,0,0] = robot vertical position
            req.input.joint_angles.joint_angles.append(temp_angle)
        succeded = False
        # Wait for completion
        b = np.array(target_angles)
        measured_voltage = []
        while not succeded:
            try:
                # while len(self.current_joint_angle) < 7:
                #     pass
                self.play_joint_trajectory(req)
                succeded = True
                # Compare old and new value of angles
                a = np.array(self.current_joint_angle)

                count = 0
                c = np.sum(np.abs(a - b)) % 360
                while c > self.ANGULAR_TOLERANCE * 2 and not c > 360.0 - self.ANGULAR_TOLERANCE and \
                        count < 30:
                    count += 1
                    rospy.sleep(0.2)
                    a = np.array(self.current_joint_angle)
                    c = np.sum(np.abs(a - b)) % 360

                if count > 30:
                    succeded = False

            except rospy.ServiceException:
                rospy.logerr("Failed to send pose")
                rospy.sleep(0.5)
                succeded = False

    def reach_cartesian_pose_and_wait_for_completion(self, target_pose, trans_speed):
        """
        Move the robot to a cartesian pose with any mean possible

        :param target_pose: Twist(), Target pose
        :param trans_speed: float, translation speed
        :return bool: True if action successful
        """
        # Clear the faults
        self.clear_faults()
        rospy.sleep(0.5)

        pose_speed = CartesianSpeed()
        pose_speed.translation = trans_speed
        pose_speed.orientation = 0.9  # self.ORIENTATION_SPEED

        # Create the pose and the request
        my_constrained_pose = ConstrainedPose()
        # https://github.com/Kinovarobotics/ros_kortex/tree/kinetic-devel/kortex_examples/src/full_arm
        my_constrained_pose.constraint.oneof_type.speed.append(pose_speed)

        last_point = [self.actual_tool_pose.linear.x, self.actual_tool_pose.linear.y]
        planned_point = [target_pose.linear.x, target_pose.linear.y]
        current_line = self.line_from_points(last_point, planned_point)

        if self.will_cross_restricted_area(current_line[0], current_line[1], current_line[2], self.RESTRICTED_RADIUS):  # is a bit more sensitive
            if self.project_on_line(self.RESTRICTED_RADIUS, last_point, planned_point):
                print(bcolors.WARNING + "[WARN] Trajectory on a collision course, halting." + bcolors.ENDC)
                return False


        # if not (self.is_in_workspace(lastPoint) and self.is_in_workspace(plannedPoint)):
        #     if self.will_cross_restricted_area(currentLine[0], currentLine[1], currentLine[2], self.RESTRICTED_RADIUS):
        #         print(bcolors.WARNING + "[WARN] Trajectory on a collision course, halting." + bcolors.ENDC)
        #
        #         return False
        # else:
        #     print(bcolors.OKBLUE + "[INFO] Trajectory OK, both points in the workspace area." + bcolors.ENDC)

        my_constrained_pose.target_pose.x = target_pose.linear.x
        my_constrained_pose.target_pose.y = target_pose.linear.y
        my_constrained_pose.target_pose.z = target_pose.linear.z
        my_constrained_pose.target_pose.theta_x = target_pose.angular.x
        my_constrained_pose.target_pose.theta_y = target_pose.angular.y
        my_constrained_pose.target_pose.theta_z = target_pose.angular.z

        req = ExecuteActionRequest()
        req.input.oneof_action_parameters.reach_pose.append(my_constrained_pose)

        # Tell the callback to look for a REACH_POSE action type
        self.action_type = ActionType.REACH_POSE

        # Send the pose
        rospy.loginfo("Sending pose...")
        try:
            self.execute_action(req)
        except rospy.ServiceException:
            rospy.logerr("Failed to send pose")
        else:
            rospy.loginfo("Waiting for pose to finish...")

        # Wait for the pose to finish

        success = self.is_action_completed.wait()

        # Clear the notifications variables afterwards
        self.is_action_completed.clear()
        self.action_type = ActionType.UNSPECIFIED_ACTION

        # Return if action was successful or aborted
        return self.is_action_successful

    def process_and_send_twist_to_robot(self, twist, use_normalized_twist, translation_speed_limit,
                                        orientation_speed_limit, reference_frame):

        if use_normalized_twist:
            (direction_x, direction_y, direction_z) = self.normalize_vector3(twist.linear.x, twist.linear.y,
                                                                             twist.linear.z)
            (direction_x_theta, direction_y_theta, direction_z_theta) = self.normalize_vector3(twist.angular.x,
                                                                                               twist.angular.y,
                                                                                               twist.angular.z)
            target = Twist(Vector3(direction_x, direction_y, direction_z),
                           Vector3(direction_x, direction_y_theta, direction_z_theta))
        else:
            target = twist
            p_gain = 1
            minimum_translation_speed = 0.01
            minimum_orientation_speed = 1

        if use_normalized_twist:
            pass
        else:
            # Validation: put to 0 if lower than minimum, cap at max if above max, multiply by a proportional gain
            target.linear.x *= p_gain
            target.linear.x = target.linear.x if abs(target.linear.x) > minimum_translation_speed else 0.0
            target.linear.x = math.copysign(min(abs(target.linear.x), translation_speed_limit), target.linear.x)
            target.linear.y *= p_gain
            target.linear.y = target.linear.y if abs(target.linear.y) > minimum_translation_speed else 0.0
            target.linear.y = math.copysign(min(abs(target.linear.y), translation_speed_limit), target.linear.y)
            target.linear.z *= p_gain
            target.linear.z = target.linear.z if abs(target.linear.z) > minimum_translation_speed else 0.0
            target.linear.z = math.copysign(min(abs(target.linear.z), translation_speed_limit), target.linear.z)
            target.angular.z *= p_gain
            target.angular.z = target.angular.z if abs(target.angular.z) > minimum_orientation_speed else 0.0
            target.angular.z = math.copysign(min(abs(target.angular.z), orientation_speed_limit), target.angular.z)
            target.angular.z *= -1  # hardcoded -1 here

        # Fill the twist command for the arm
        self.twist_command.input.twist.linear_x = target.linear.x
        self.twist_command.input.twist.linear_y = target.linear.y
        self.twist_command.input.twist.linear_z = target.linear.z
        self.twist_command.input.twist.angular_x = 0.0  # TODO why 0?
        self.twist_command.input.twist.angular_y = 0.0
        self.twist_command.input.twist.angular_z = target.angular.z

        # Send the command to the arm
        self.twist_command.input.reference_frame = reference_frame
        self.twist_command.input.duration = 0
        self.send_twist_command(self.twist_command)

    def calculate_near_pose(self, pose, distance):
        """
        Calculate a position near the grasping point with the same orientation and at a certain distance

        :param pose: Twist, grasping point of the object
        :param distance: float, distance from the grasping point
        :return: Twist, shifted 'distance' in z-axis
        """
        near_pose = Twist()
        near_pose.linear.x = pose.linear.x
        near_pose.linear.y = pose.linear.y
        near_pose.linear.z = pose.linear.z + distance
        near_pose.angular.x = pose.angular.x  # -177.40
        near_pose.angular.y = pose.angular.y  # -0.05
        near_pose.angular.z = pose.angular.z  # -179.34
        return near_pose

    """
    MOVEMENT FUNCTIONS
    """

    def print_pos(self):
        # for i in range(7):
        #     rospy.loginfo(self.actuator_current_motor[i])
        if self.gripper_feedback is not None:
            rospy.loginfo(self.gripper_feedback[0].motor[0].current_motor)

    def fill_current_and_gripper_list(self):
        """
        Fill arrays for grasping measurement
        """
        self.current_feedback_list.append(self.gripper_current)
        self.gripper_position_list.append(self.current_gripper_position)

    def line_from_points(self, P, Q):
        """

        Args:
            P: [x,y] point1
            Q: [x,y] point2

        Returns:
            three element tuple returning coeffs of a line in a form ax + by + c = 0
            from a previous computation result ax + by = c, thats why the c in return is -c

        """
        a = Q[1] - P[1]
        b = P[0] - Q[0]
        c = a * (P[0]) + b * (P[1])

        return (a, b, -c)

    def project_on_line(self, radius, point_a, point_b):
        point_a = np.array(point_a)
        point_b = np.array(point_b)
        d = np.linalg.norm(np.cross(point_b - point_a, point_a)) / np.linalg.norm(point_b - point_a)
        orth_ab0 = np.array([-(point_b[1] - point_a[1]), (point_b[0] - point_a[0])])
        orth_ab1 = np.array([(point_b[1] - point_a[1]), -(point_b[0] - point_a[0])])
        orthd0 = np.dot(orth_ab0, orth_ab0) - np.dot(orth_ab1, orth_ab1)
        orth_ab = orth_ab0 if orthd0 < 0 else orth_ab1
        norm_ab = orth_ab / np.linalg.norm(orth_ab)
        # print(d * norm_ab)
        basedAB = np.array((point_a, point_b)) + (norm_ab * d)
        # print(basedAB)
        based_ab = basedAB[1] - basedAB[0]
        inside = False
        radius_sq = np.power(radius, 2)
        if distance.sqeuclidean(point_a, [0, 0]) < radius_sq or \
                distance.sqeuclidean(point_b, [0, 0]) < radius_sq:
            inside = True
        elif distance.sqeuclidean(basedAB[0], [0, 0]) + \
                distance.sqeuclidean(basedAB[1], [0, 0]) < np.dot(
            based_ab, based_ab) + 1e-3:
            inside = True
        return inside

    def save_data(self, object_id, order_id, lookup_id=None):
        """
        Save datas in .npy files of relative current and gripper position for grasping measurements

        :param: object_id: int, ID of object
        :param: order_id: int, order id in running
        :param: lookup_id: int, used only if the id of the transform isn't -1
        :returns: Generated file name
        """
        # print(object_id)
        print(bcolors.OKBLUE + "[INFO] saving data" + 'object_id: {}  order: {}'.format(object_id,
                                                                                        order_id) + bcolors.ENDC)
        # TODO assuming the relative translation and rotation always exist
        if lookup_id is not None:
            trans, rot = well_behaved_lookup_transform(yaml_config["end_effector"],
                                                            yaml_config["grasp_above"].format(-1))
        else:
            trans, rot = well_behaved_lookup_transform(yaml_config["end_effector"],
                                                            yaml_config["grasp_above"].format(object_id))
        file_name = 'saved_data/' + time.strftime("%Y-%b-%d-%H-%M", time.gmtime()) \
                    + '-{}-{}.npy'.format(object_id, order_id)

        # trans = [0.0, 0.0, 0.0]
        # rot = [0.0, 0.0, 0.0]
        with open(file_name, 'wb') as f:  # TODO correct saving format
            # np.save(f, np.transpose(np.array(self.gripper_position_list)))
            # np.save(f, np.transpose(np.array(self.current_feedback_list)))
            np.save(f, np.array((trans, rot, self.gripper_position_list, self.current_feedback_list)))
            # print(np.array((trans, rot, self.gripper_position_list, self.current_feedback_list)))
            print(bcolors.OKBLUE + "[INFO] successfully saved data" + bcolors.ENDC)
        x = self.gripper_position_list
        y = self.current_feedback_list

        if visualize:
            self.visualize(x, y, object_id)

        # matt = grip.npy2grip(file_name)
        # print(str(matt['position'])+"\n"+str(matt['current']))

        """
        matt = grip.npy2grip(file_name)
        print(str(matt['position'])+"\n"+str(matt['current']))
        grip.display_raw(pos=matt['position'], cur=matt['current'], block=False, keep=True)
        grip.display_last_grip(block=False)
        """
        # print(bcolors.OKGREEN+"[RESULT] Rank of grabbed object is: "+str(grip.get_linear_rank(matt['position'], matt['current']))+"/"+str(grip.file_num)+bcolors.ENDC)

        self.current_feedback_list = []
        self.gripper_position_list = []
        # print("#######################")
        # print("Printing object properties")
        # print(np.transpose(np.array(self.gripper_position_list)))
        # print(np.transpose(np.array(self.current_feedback_list)))
        # print("#######################")
        return file_name

    def go_through_squish_grid(self):
        object_count = 0

        """ begin grid measurement """  # TODO
        # translation_list = [[0.3, -0.314, 0.10], [0.459, -0.314, 0.11], [0.582, -0.293, 0.09],
        #                     [0.295, -0.103, 0.08], [0.4483, -0.103, 0.105], [0.594, -0.103, 0.105],
        #                     [0.296, 0.084, 0.105], [0.449, 0.084, 0.105], [0.605, 0.084, 0.09],
        #                     [0.299, 0.266, 0.105], [0.453, 0.269, 0.105], [0.600, 0.267, 0.084]]
        #  black cube position: 0.42529; 0.058792; 0.20714
        #  black cube orientation: 0.70715; 0.70706; 0.0021613; -0.00045053
        #  black cube rel pos: 0; 0; -0.061525
        #  black cube rel ori: 1; 0; -5.4969e-33; 1.6155e-15

        # white marble block position: 0.42595; -0.06103; 0.2168

        # two demo objects
        translation_list = [[0.42529, 0.058792, 0.20714], [0.42595, -0.06103, 0.2168]]
        translation_list[0][2] -= 0.11
        translation_list[1][2] -= 0.11
        # mes_rot = [179.900, 0.001, 0.000000]
        mes_rot = [179.900, 0.001, 90.000000]
        n_squishes = 1
        for tr in translation_list:
            measure_count = 0

            for i in range(n_squishes):

                no_object = True
                closing_threshold = False
                new_twist = coords2twist(tr, mes_rot)
                new_near_pose = self.calculate_near_pose(new_twist, 0.1)
                self.reach_cartesian_pose_and_wait_for_completion(new_near_pose, self.TRANSLATION_SPEED)
                self.reach_cartesian_pose_and_wait_for_completion(new_twist, self.TRANSLATION_SPEED)

                starting_time = 0

                while no_object and self.current_gripper_position < self.CLOSED_GRIPPER_POSITION:
                    if self.gripper_current > 0.051:
                        closing_threshold = True
                    print("Current gripper current: ", self.gripper_current)
                    print("State of closing_threshold: ", closing_threshold)
                    no_object = self.send_gripper_command_speed(self.CLOSING_SPEED, self.gripper_current)
                    self.fill_current_and_gripper_list()
                    if self.gripper_current < 0.051 and closing_threshold:
                        if starting_time == 0:
                            starting_time = time.time()
                        else:
                            if time.time() - starting_time > 5.0:
                                print(bcolors.WARNING +
                                      "[WARN] The gripper couldn't decide, decision made artificially."
                                      + bcolors.ENDC)
                                break
                self.send_gripper_command_and_wait_for_completion(self.gripper_opened)

                self.reach_cartesian_pose_and_wait_for_completion(new_near_pose, self.TRANSLATION_SPEED)

                # self.send_gripper_command_and_wait_for_completion(self.gripper_opened)

                self.save_data(object_count, measure_count)
                measure_count += 1
            object_count += 1
        exit()

        """ arg: end grid measurement """  # TODO

    def squish_many_times_n_save(self):

        self.CLOSING_SPEED = -0.5
        mes_rot = [179.900, 0.001, 0.000000]
        curr_object_id = 0
        reps = 5
        # shifts_up = [-0.007, -0.021, -0.031, -0.022, -0.028,
        #              0.015, -0.0225, -0.028, -0.0175, 0.012,
        #              -0.00, -0.023, -0.026, -0.016, -0.005,
        #              0.012, -0.018, 0.025, -0.024, -0.017]
        shifts_up = [0.044, 0.044, 0.044, 0.044, 0.044, 0.044, 0.044, 0.044, 0.044, 0.044, 0.044, 0.044, 0.044, 0.044, 0.044, 0.044]
        failed_grip = False
        while True:
            self.send_joint_angles_fct(self.pose_angle_starting)

            while self.top_poses:
                print("got a top pose")
                grasp_pos = self.top_poses.pop()
                object_count = 0
                # Goes to starting position

                object_grasp = coords2twist(grasp_pos[0])  # select the first object seen
                object_grasp.angular.x = mes_rot[0]
                object_grasp.angular.y = mes_rot[1]
                object_grasp.angular.z = mes_rot[2]
                object_grasp.linear.z += shifts_up[curr_object_id]
                print("object_grasp translation: ", object_grasp.linear)
                near_pose = self.calculate_near_pose(object_grasp, 0.2)
                self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED)
                # Open Gripper
                self.send_gripper_command_and_wait_for_completion(self.gripper_opened)

                for i in range(reps):
                    # Calculate and go to near pose
                    near_pose = self.calculate_near_pose(object_grasp, 0.2)
                    self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED)
                    # Goes to object
                    # originally /5 # speed must be lower when distance is small
                    self.reach_cartesian_pose_and_wait_for_completion(object_grasp, self.TRANSLATION_SPEED / 3)

                    # Initialize object grasping and measurement lists
                    no_object = True
                    # self.current_feedback_list = []  # not necessarily necessary
                    # self.gripper_position_list = []
                    # Grasp object
                    print('Closing Gripper...')
                    starting_time = 0
                    closing_threshold = False
                    while no_object and self.current_gripper_position < self.CLOSED_GRIPPER_POSITION:
                        if self.gripper_current > 0.051:
                            closing_threshold = True
                        no_object = self.send_gripper_command_speed(self.CLOSING_SPEED, self.gripper_current)
                        self.fill_current_and_gripper_list()
                        if self.gripper_current < 0.051 and closing_threshold:
                            if starting_time == 0:
                                starting_time = time.time()
                            elif time.time() - starting_time > 5.0:
                                print(
                                    bcolors.WARNING + "[WARN] The gripper couldn't decide, decision made artificially." + bcolors.ENDC)
                                break
                    listener.clear()  # clear the buffer of the subscriber

                    # Save data in .npy file
                    latest_grip_file = self.save_data(curr_object_id, object_count, lookup_id=-1)
                    matt = grip.npy2grip(latest_grip_file)

                    if no_object:
                        # print(grip.display_raw(pos=matt["position"], cur=matt["current"]))
                        # if self.current_gripper_position > 98.5:
                        #     exit()
                        pass

                    if object_count == reps - 1:  # put away object when done with it
                        try:
                            obj_rank = grip.get_anchor_rank(matt["position"], matt["current"])
                            near_pose = self.calculate_near_pose(object_grasp, 0.2)
                            self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED)
                            object_count = 0
                            self.sort_to_baskets(obj_rank, self.sorting_baskets_centers, self.sorting_baskets_sizes)
                            # self.sort_to_baskets(0,
                            #                      self.sorting_baskets_centers, self.sorting_baskets_sizes)
                            # self.send_gripper_command_and_wait_for_completion(self.gripper_opened)
                            near_pose = self.calculate_near_pose(object_grasp, 0.2)
                            # print("going to near pose: " + str(near_pose))
                            self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED)
                        except TypeError as e:
                            object_count -= 1
                            failed_grip = True
                            print(e)
                            self.send_gripper_command_and_wait_for_completion(self.gripper_opened)

                    else:
                        self.send_gripper_command_and_wait_for_completion(self.gripper_opened)
                        near_pose = self.calculate_near_pose(object_grasp, 0.2)
                        # print("going to near pose: "+str(near_pose))
                        self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED)
                    object_count += 1
                if not failed_grip:
                    curr_object_id += 1
                else:
                    failed_grip = False

    def weigh_with_torque(self, object_grasp, near_pose, calib=False):

        straight_pose = [0.0, 0.0, 180.0, 270.0, 0.0, 270.0, 90.0]
        r = 0.3143
        zero_gripper_torque = self.gripper_tau_0
        if zero_gripper_torque is None:
            calib=True

        if calib:
            print(bcolors.OKBLUE + "[INFO] Zero weight calibration in progress." + bcolors.ENDC)
            self.send_joint_angles_fct(self.pose_angle_starting)
            time.sleep(0.5)  # TODO probably should be rospy.sleep() or something!
            self.send_gripper_command_and_wait_for_completion(self.gripper_closed)
            time.sleep(0.5)
            self.send_joint_angles_fct(straight_pose)
            # wait for the vibrations and swings to stop
            time.sleep(3)
            gripper_torques = []
            print("Zero payload torque: ")
            for i in range(50):
                print(self.weighing_joint_torque)
                gripper_torques.append(self.weighing_joint_torque)
                time.sleep(0.1)

            zero_gripper_torque = sum(gripper_torques) / len(gripper_torques)
            # where 0.3143 m is cca the r in M = r x F = r x (m * g)

            self.gripper_weight = -zero_gripper_torque / (r * 9.81)
            self.gripper_tau_0 = zero_gripper_torque
            print(bcolors.OKBLUE + "Torque is: {}".format(zero_gripper_torque) + bcolors.ENDC)
            print(bcolors.OKGREEN + "Gripper zero weight is {}".format(
                self.gripper_weight) + bcolors.ENDC)
            time.sleep(0.1)
            self.send_gripper_command_and_wait_for_completion(self.gripper_opened)
            self.send_joint_angles_fct(self.pose_angle_starting)
            print(bcolors.OKGREEN + "[INFO] Weight calibration successful." + bcolors.ENDC)
            self.weightCalibration = True
            with open("weight_meas.txt", "a") as wf:
                time_rn = time.strftime("%Y-%b-%d-%H-%M", time.gmtime())
                log = "Time: {0}, Calibration: Estimated empty gripper weight: {1}.\n".format(time_rn, self.gripper_weight)
                wf.write(log)
            # return


        print(bcolors.OKBLUE + "[INFO] Starting the weighing sequence." + bcolors.ENDC)
        self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED)
        # time.sleep(0.5)
        self.reach_cartesian_pose_and_wait_for_completion(object_grasp, self.TRANSLATION_SPEED)
        self.send_gripper_command_and_wait_for_completion(self.gripper_closed)
        # time.sleep(0.5)
        self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED)
        self.send_joint_angles_fct(straight_pose)
        # wait for the vibrations and swings to stop
        time.sleep(3)
        hernando_torques = []
        print("Payload torque: ")
        for i in range(50):
            print(self.weighing_joint_torque)
            hernando_torques.append(self.weighing_joint_torque)
            time.sleep(0.1)
        final_torque = sum(hernando_torques)/len(hernando_torques)

        # where 0.305 m is cca the r in M = r x F = r x (m * g)
        computed_weight = -(final_torque - zero_gripper_torque) / (r * 9.81)  # TODO, zero_gripper_torque_ should be +?
        self.general_measurement_list.append(computed_weight)
        print(bcolors.OKBLUE + "Torque is: {}".format(final_torque) + bcolors.ENDC)
        print(bcolors.OKGREEN + "So the estimated weight is {}".format(computed_weight) + bcolors.ENDC)
        time.sleep(0.1)
        self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED)
        self.reach_cartesian_pose_and_wait_for_completion(object_grasp, self.TRANSLATION_SPEED / 3)
        self.send_gripper_command_and_wait_for_completion(self.gripper_opened)
        self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED)

        return computed_weight

    def test(self):
        def print_info():
            enums = ["TOP", "FRONT", "RIGHT", "LEFT"]
            print(bcolors.OKBLUE + "\n\nTesting pose orientation: {0} from base POV.\nAt coordinates: {1}\n\n".format(enums[pose_type], testing_pose[0]) + bcolors.ENDC)

        # TODO write to JSON or CSV file, not txt!
        def write_info(lvl1, lvl2, h1, h2):
            with open("spatial_poses_success.txt", "w") as wf:
                time_rn = time.strftime("%Y-%b-%d-%H-%M", time.gmtime())

                str1 = "Success Table Level 1 with height {} is:\n".format(h1)
                print("lvl1 succes: ", lvl1)
                for row in lvl1:
                    for col in row:
                        for elem in col:
                            if elem == None:
                                str1 += "None "
                            elif elem == True:
                                str1 += "True "
                            else:
                                str1 += "False "
                        str1 += "; "


                    str1 += '\n'
                str2 = "\nSuccess Table Level 2 with height {} is:\n".format(h2)
                print("lvl2 succes: ", lvl2)
                for row in lvl2:
                    for col in row:
                        for elem in col:
                            if elem == None:
                                str2 += "None "
                            elif elem == True:
                                str2 += "True "
                            else:
                                str2 += "False "
                        str2 += "; "

                    str2 += '\n'
                log = str1 + str2
                wf.write(log)

        mids = [[[33.0, -29.5], [32.2, -10.2], [32.8, 8.8], [33.0, 28.0]],
                [[50.5, -29.7], [50.2, -10.2], [50.0, 9.3], [50.2, 28.8]],
                [[68.2, -30.3], [68.2, -10.2], [68.2, 9.7], [69.0, 31.5]]]

        self.lvl_1_success = [
            [[None, None, None, None], [None, None, None, None], [None, None, None, None], [None, None, None, None]],
            [[None, None, None, None], [None, None, None, None], [None, None, None, None], [None, None, None, None]],
            [[None, None, None, None], [None, None, None, None], [None, None, None, None], [None, None, None, None]]]

        self.lvl_2_success = [
            [[None, None, None, None], [None, None, None, None], [None, None, None, None], [None, None, None, None]],
            [[None, None, None, None], [None, None, None, None], [None, None, None, None], [None, None, None, None]],
            [[None, None, None, None], [None, None, None, None], [None, None, None, None], [None, None, None, None]]]

        success_tables = [self.lvl_1_success, self.lvl_2_success]

        lvl_1 = copy.deepcopy(mids)
        lvl_2 = copy.deepcopy(mids)
        lvl_1_height = 12  # in cm
        lvl_2_height = 70  # in cm
        for row in range(3):
            for col in range(4):
                lvl_1[row][col].append(lvl_1_height)
                lvl_2[row][col].append(lvl_2_height)

        lvl_1 = np.array(lvl_1)/100
        lvl_2 = np.array(lvl_2) / 100
        levels = [lvl_1, lvl_2]
        # top, front, right, left grasp (from the point of view of the robot)
        rot_poses = [[179.9, 0.001, 0.0], [89.999, 0.001, 89.89], [89.999, 0.001, 179.899], [89.990, 0.001, 0.001]]

        for i, lvl in enumerate(levels):
            print(bcolors.OKBLUE + "LEVEL n {}".format(i + 1) + bcolors.ENDC)
            for row in range(3):
                for col in range(4):
                    for pose_type, pose in enumerate(rot_poses):
                        # home_pose = [[0.55, 0, 0.7], [89.990, 0.001, 89.990]]
                        # home_pose = coords2twist(home_pose)
                        straight_pose = [0.0, 0.0, 180.0, 270.0, 0.0, 360.0, 90.0]
                        self.send_joint_angles_fct(straight_pose)

                        # self.reach_cartesian_pose_and_wait_for_completion(home_pose, self.TRANSLATION_SPEED)
                        testing_pose = [[0.4, -0.2, 0.4], [179.900, 0.001, 0.0]]  # safe random
                        testing_pose[0][0] = lvl[row][col][0]
                        testing_pose[0][1] = lvl[row][col][1]
                        testing_pose[0][2] = lvl[row][col][2]
                        testing_pose[1] = pose
                        print_info()
                        write_info(self.lvl_1_success, self.lvl_2_success, lvl_1_height, lvl_2_height)
                        testing_pose = coords2twist(testing_pose)
                        success = self.reach_cartesian_pose_and_wait_for_completion(testing_pose, self.TRANSLATION_SPEED)
                        success_tables[i][row][col][pose_type] = success

                        if success:
                            print(bcolors.OKGREEN + "Reached Destination." + bcolors.ENDC)
                        else:
                            print(bcolors.WARNING + "Pinch-point warning!" + bcolors.ENDC)
                            print(bcolors.FAIL + "ABORTED! FAILED reaching the destination." + bcolors.ENDC)

        testing_pose = coords2twist(testing_pose)
        self.reach_cartesian_pose_and_wait_for_completion(testing_pose, self.TRANSLATION_SPEED)


    def main(self):
        """
        Demo to pick up objects seen with infer.py and epos_camera_stream.py IF MEASURE = FALSE
        Demo to measure object properties with arucomarkers.py IF MEASURE = TRUE
        """
        # Start the demo
        object_counter = 0
        while not rospy.is_shutdown():
            self.send_joint_angles_fct(
                self.pose_angle_starting)  # reset joint position instead of cartesian so that the robot doesnt get stuck
            self.send_gripper_command_and_wait_for_completion(self.gripper_opened)
            self.print_pos()
            object_count = 0
            # Grasp objects
            # print(self.grasping_position)
            # testing_pose = ((0.4, -0.2, 0.4),(179.900, 0.001, 0.0))
            #
            # self.test()
            # exit()

            # if weightEstimation and not self.weightCalibration:
            #     self.weigh_with_torque(None, None, calib=True)

            if grid and not weightEstimation:
                # after completion, this function exits the program
                self.go_through_squish_grid()

            if grid and weightEstimation:
                translation_list = [[0.42529, 0.058792, 0.20714], [0.42595, -0.06103, 0.2168]]
                translation_list[0][2] -= 0.11
                translation_list[1][2] -= 0.11
                # mes_rot = [179.900, 0.001, 0.000000]
                mes_rot = [179.900, 0.001, 90.000000]
                n_squishes = 1
                for tr in translation_list:
                    new_twist = coords2twist(tr, mes_rot)
                    new_near_pose = self.calculate_near_pose(new_twist, 0.1)
                    object_weight = self.weigh_with_torque(new_twist, new_near_pose)
                    object_counter += 1
                    self.general_measurement_list.append(object_weight)
                    print(bcolors.OKGREEN + "Adding Object number {0} with weight {1} to the general list.".format(
                        object_counter, object_weight) + bcolors.ENDC)

                    with open("weight_meas_NEW.txt", "a") as wf:
                        time_rn = time.strftime("%Y-%b-%d-%H-%M", time.gmtime())
                        log = "Time: {2}, Measurement number {0} with weight {1}\n".format(object_counter,
                                                                                           object_weight, time_rn)
                        wf.write(log)
                exit()



            if stiff_rep and measure:
                self.squish_many_times_n_save()
            elif stiff_rep and not measure:
                print(bcolors.WARNING+"squish_many_times_n_save: needs argument --mes to be also set to run"+bcolors.ENDC)

            while self.top_poses:
                np_top = np.array(get_translations_from_6di(self.top_poses))
                # print(np_top)
                # print(np.shape(np_top))
                # print(np_top[:,0][0])
                cli = get_closest_point_to(np_top, index=True)  # take from translation
                print(bcolors.OKBLUE+"[INFO] Closest item is obj {}".format(self.top_poses[cli][1])+bcolors.ENDC)
                grasp_pos = self.top_poses[cli]
                del self.top_poses[cli]
                # grasp_pos = self.top_poses.pop()

                curr_object_id = grasp_pos[1]  # select the first object's ID

                if grasp and self.object_poses:
                    obj_pos = self.object_poses.pop()  # TODO this is becoming messy, sometimes crashes
                    # self.shubhan_broadcaster(obj_pos[0], curr_object_id)
                    # print("I hope you found what you were looking for. RAGE QUITTING!")
                    # exit()
                    (gripper_wrt_obj, gripper_rot_mat) = self.get_ycb_pose(obj_pos[0], curr_object_id)
                    print("gripper_wrt_obj", gripper_wrt_obj)
                    # print("gripper_rot_mat", angles_n_matrix.rotation_matrix_to_euler_angles(gripper_rot_mat))
                    print("gripper_wrt_obj_mat", gripper_wrt_obj)
                    print("gripper_rot_mat", gripper_rot_mat)
                    self.broadcast_tf(gripper_wrt_obj, gripper_rot_mat, yaml_config["gripper"].format(curr_object_id),
                                      yaml_config["6Dpose"].format(curr_object_id))
                    trrt = well_behaved_lookup_transform(child=yaml_config["gripper"].format(curr_object_id))
                    trrt = (trrt[0], (trrt[1][0], trrt[1][1], trrt[1][2]))
                    print(trrt, )
                    object_grasp = coords2twist(trrt)  # select the first object seen
                else:
                    object_grasp = coords2twist(grasp_pos[0])  # select the first object seen

                self.send_joint_angles_fct(self.pose_angle_starting)
                self.send_gripper_command_and_wait_for_completion(self.gripper_opened)

                if measure:
                    print(bcolors.OKBLUE + "[INFO] Going to the left of the ArUco marker." + bcolors.ENDC)
                else:
                    print(bcolors.OKBLUE + "[INFO] Going to the object." + bcolors.ENDC)

                near_pose = self.calculate_near_pose(object_grasp, 0.1)
                self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED)
                self.reach_cartesian_pose_and_wait_for_completion(object_grasp, self.TRANSLATION_SPEED / 3)

                # Initialize object grasping and measurement lists
                no_object = True
                self.current_feedback_list = []  # not necessarily necessary
                self.gripper_position_list = []
                # Grasp object
                print('Closing Gripper...')
                starting_time = 0
                closing_threshold = False
                while no_object and self.current_gripper_position < self.CLOSED_GRIPPER_POSITION:
                    if self.gripper_current > 0.051:
                        closing_threshold = True
                    no_object = self.send_gripper_command_speed(self.CLOSING_SPEED, self.gripper_current)
                    self.fill_current_and_gripper_list()
                    if self.gripper_current < 0.051 and closing_threshold:
                        if starting_time == 0:
                            starting_time = time.time()
                        elif time.time() - starting_time > 5.0:
                            print(bcolors.WARNING + "[WARN] The gripper couldn't decide, decision made artificially.")
                            break
                listener.clear()  # clear the buffer of the subscriber

                if weightEstimation:
                    object_weight = self.weigh_with_torque(object_grasp, near_pose)
                    object_counter += 1
                    self.general_measurement_list.append(object_weight)
                    print(bcolors.OKGREEN + "Adding Object number {0} with weight {1} to the general list.".format(object_counter, object_weight) + bcolors.ENDC)

                    with open("weight_meas.txt", "a") as wf:
                        time_rn = time.strftime("%Y-%b-%d-%H-%M", time.gmtime())
                        log = "Time: {2}, Measurement number {0} with weight {1}\n".format(object_counter, object_weight, time_rn)
                        wf.write(log)

                if measure:
                    # Save data in .npy files only if caught object
                    latest_grip_file = self.save_data(curr_object_id, object_count)
                    object_count += 1
                    matt = grip.npy2grip(latest_grip_file)

                if not no_object:
                    if stiffnessSort and measure:
                        near_pose = self.calculate_near_pose(object_grasp, 0.3)
                        self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED)
                        self.sort_to_baskets(grip.get_anchor_rank(matt["position"], matt["current"]),
                                             self.sorting_baskets_centers, self.sorting_baskets_sizes)
                    if measure:
                        self.send_gripper_command_and_wait_for_completion(self.gripper_opened)
                        self.reach_cartesian_pose_and_wait_for_completion(self.pose_starting, self.TRANSLATION_SPEED)
                    elif not measure:

                        self.reach_cartesian_pose_and_wait_for_completion(self.pose_basket, self.TRANSLATION_SPEED)
                        self.send_gripper_command_and_wait_for_completion(self.gripper_opened)
                else:
                    if stiffnessSort and measure:
                        near_pose = self.calculate_near_pose(object_grasp, 0.3)
                        self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED)
                        # print(grip.display_raw(pos=matt["position"], cur=matt["current"]))
                        if self.current_gripper_position < 98.5:
                            self.sort_to_baskets(grip.get_anchor_rank(matt["position"], matt["current"]),
                                                 self.sorting_baskets_centers, self.sorting_baskets_sizes)
                    else:
                        self.send_gripper_command_and_wait_for_completion(self.gripper_opened)
                        self.reach_cartesian_pose_and_wait_for_completion(self.pose_starting, self.TRANSLATION_SPEED)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def get_closest_point_to(arr, origin=(0.0, 0.0, 0.0), index=False):
    # print(arr, np.array([origin]*len(arr)))
    d = distance.cdist(arr, np.array([origin]*len(arr)))[..., 0]
    # print(d)
    if index:
        return np.argmin(d)
    else:
        return arr[np.argmin(d)]

def get_translations_from_6di(poses):
    ret = []
    for el in poses:
        ret.append(el[0][0])
        # print(el[0][0])
    return ret

def well_behaved_lookup_transform(parent=yaml_config["base"], child=yaml_config["end_effector"]):
    countdown = 100000
    while True:
        try:
            (local_position, local_rot) = listener.lookupTransform(parent, child, rospy.Time(0))
            rotation_rad = tf.transformations.euler_from_quaternion(local_rot)
            local_rot = tuple([x * 180 / math.pi for x in rotation_rad])
            return local_position, local_rot
        except(tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
            if countdown == 0:
                print(
                    bcolors.FAIL + "[ERROR] well_behaved_lookup_transform: couldn't find transform" + bcolors.ENDC)
                return (0.5, 0.0, 0.4), (179.9, 0.001, 90.01)
            # print(countdown)
            countdown -= 1
            pass

def coords2twist(x, y=None, z=None, rotx=None, roty=None, rotz=None):
    """
    This function takes general input, ideally from the YAML file, either a tuple of tuples, or a simple tuple
    or simply just naked coordinates.

    :param x: coord x
    :param y: coord y
    :param z: coord z
    :param rotx: rot of x
    :param roty: rot of y
    :param rotz: rot of z
    :return: product of Twist fnc
    """
    if y is None:  # then x is a tuple of 2 tuples of 3 floats ((x, y, z), (rotx, roty, rotz)) format from the yaml file
        tempx = x
        x = tempx[0][0]
        y = tempx[0][1]
        z = tempx[0][2]
        rotx = tempx[1][0]
        roty = tempx[1][1]
        rotz = tempx[1][2]
    elif z is None:
        tempx = x
        tempy = y
        x = tempx[0]
        y = tempx[1]
        z = tempx[2]
        rotx = tempy[0]
        roty = tempy[1]
        rotz = tempy[2]

    return Twist(Vector3(x, y, z), Vector3(rotx, roty, rotz))

def twist2coords(twist):
    """
    Convert Twist to twople of truples. Translation, rotation.

    :param: Twist
    :return: translation (x y z), rotation (x y z)
    """
    if twist is None:  # then x is a tuple of 2 tuples of 3 floats ((x, y, z), (rotx, roty, rotz)) format from the yaml file
        return None
    return (twist.linear.x, twist.linear.y, twist.linear.z), (twist.angular.x, twist.angular.y, twist.angular.z)

def check_name():
    service_list = rosservice.get_service_list()
    for element in service_list:
        if 'manipulator' in element:
            return 'manipulator'
        elif 'my_gen3' in element:
            return 'my_gen3'
    return None

def shut_down_node(sig=2, frame=None):  # TODO? make the shut down functions an import
    """
    Close the ros node
    """
    print(sig)
    nodes = os.popen("rosnode list").readlines()
    for i, node in enumerate(nodes):
        nodes[i] = nodes[i].replace("\n", "")
        if nodes[i] == node_name:
            os.system("rosnode kill " + node)
    exit()

signal.signal(signal.SIGINT, shut_down_node)  # when receiving ctrl+c
signal.signal(signal.SIGTERM, shut_down_node)  # idk what this does exactly
signal.signal(20, shut_down_node)  # when receiving ctrl+z
# ctrl+z - https://stackoverflow.com/questions/11886812/what-is-the-difference-between-sigstop-and-sigtstp
node_name = 'pickup_object'

if __name__ == '__main__':
    """
    print(str(os.getpid()))
    kinova_components = "/tmp/kinova_components.pid"
    # if os.path.isfile(kinova_components):
    with open(kinova_components, "a") as f:
        f.write(str(os.getpid()))
        # exit()
    """
    max_current = 0.06
    parser = argparse.ArgumentParser()
    parser.add_argument("--mes", nargs='?', default="off", required=False, const=1.0, help="turn on the measure mode")
    parser.add_argument("--vis", nargs='?', default="off", required=False, help="turn on the visualization")
    parser.add_argument("--wght", nargs='?', default="off", required=False,
                        help="turn on the weighing sequence after each grasp")
    parser.add_argument("--grid", nargs='?', default="off", required=False, const=0.055,
                        help="turn on the grid measuring mode for stiffness")
    parser.add_argument("--stiff", nargs='?', default="off", required=False, const=4,
                        help="turn on the stiffness sorting into determined number of baskets")
    parser.add_argument("--stiffrep", nargs='?', default="off", required=False, const=5,
                        help="Repeated stiffness measurement with aruco then put away into basket")
    parser.add_argument("--grasp", nargs='?', default="off", required=False, const=4,
                        help="Adjust grasps for ycb objects")

    args = parser.parse_args()
    print("args", args)
    if args.mes == "off":
        measure = False

        print(bcolors.OKBLUE + "[INFO] Starting program in vision recog. mode." + bcolors.ENDC)

    else:
        measure = True
        max_current = float(args.mes)
        if max_current > 1:
            max_current = 1
        if max_current < 0:
            max_current = 0.01
        print(bcolors.OKBLUE + "[INFO] Starting program in MEASURE mode with {} as the max current".format(
            max_current) + bcolors.ENDC)

    if args.vis == "off":
        visualize = False
    else:
        visualize = True
        print(bcolors.OKBLUE + "[INFO] Visualisation mode ON." + bcolors.ENDC)

    if args.wght == "off":
        weightEstimation = False
    else:
        weightEstimation = True
        print(bcolors.OKBLUE + "[INFO] Weight estimation mode ON." + bcolors.ENDC)

    if args.grid == "off":
        grid = False
        CLOSING_SPEED = 0.05
    else:
        grid = True
        CLOSING_SPEED = float(args.grid)
        if CLOSING_SPEED > 1:
            CLOSING_SPEED = 1
        if CLOSING_SPEED <= 0:
            CLOSING_SPEED = 0.01
        print(bcolors.OKBLUE + "[INFO] Grid Measuring is ON, with CLOSING_SPEED at {}.".format(
            CLOSING_SPEED) + bcolors.ENDC)

    if args.stiff == "off":
        stiffnessSort = False
        N_OF_SORT_BASKETS = 4
    else:
        stiffnessSort = True
        N_OF_SORT_BASKETS = int(args.stiff)
        if N_OF_SORT_BASKETS < 0:
            N_OF_SORT_BASKETS = 1
        if N_OF_SORT_BASKETS > 4:
            N_OF_SORT_BASKETS = 4
        print(bcolors.OKBLUE + "[INFO] Stiffness sort is ON, with number of sorting baskets at {}.".format(
            N_OF_SORT_BASKETS) + bcolors.ENDC)

    if args.stiffrep == "off":
        stiff_rep = False
        repetitions = 5
    else:
        stiff_rep = True
        repetitions = int(args.stiffrep)
        if repetitions < 1:
            repetitions = 1
        print(bcolors.OKBLUE + "[INFO] Repeated stiffness measurement (aruco & sort) is ON with {} repetitions.".format(
            repetitions) + bcolors.ENDC)

    if args.grasp == "off":
        grasp = False
    else:
        grasp = True
        print(bcolors.OKBLUE + "[INFO] Adapting grasps for ycb objects." + bcolors.ENDC)

    rospy.init_node(node_name)
    listener = tf.TransformListener()
    rate = rospy.Rate(10)  # the rate changes the amount of measurements for grasping

    # Create the experiment object
    experiment = IpalmGrasping()
    try:
        experiment.GRIPPER_CURRENT_LIMIT = max_current
        experiment.CLOSING_SPEED = -CLOSING_SPEED
        experiment.main()
    except KeyboardInterrupt:
        print("Shutting down")
    shut_down_node()
