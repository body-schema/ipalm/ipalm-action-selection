from pymc3.sampling import sample
from scipy.stats import norm
import numpy as np
import pymc3 as pm
import arviz as az
import matplotlib.pyplot as plt
from sklearn.mixture import GaussianMixture

TRUE_MUS = np.array([100, 200, 300, 500])
TRUE_SIGS = np.array([50, 60, 10, 20])


# creation of samples from a previously created PyMC3 model
with pm.Model() as model:
    w = np.array([1.0/len(TRUE_MUS) for _ in TRUE_MUS])
    y = pm.NormalMixture('y', w, mu=TRUE_MUS, sigma=TRUE_SIGS)

    trace = pm.sample(800)
    # print(trace['posterior']['y'])
    burn_in = 500  # skips the n iters, where the MCMC is just wandering
    chain = trace[burn_in:]
    ppc = pm.sample_posterior_predictive(chain, var_names=['y'], model=model)
    data = ppc['y'].transpose()
    print(len(data))

    n = len(data)
    bins = int(np.sqrt(n))

    plt.hist(data, bins=bins, label="Data")
    plt.legend()
    # plt.show()

# X = np.array([[1, 2], [1, 4], [1, 0], [10, 2], [10, 4], [10, 0]])
# x = [1, 1, 1, 10, 10, 10]
# y = [2, 4, 0, 2, 4, 0]
# plt.scatter(x,y)
# plt.show()

data = data.reshape(-1, 1)
gm = GaussianMixture(n_components=len(TRUE_MUS), random_state=0).fit(data)
print(gm.means_)
print(gm.weights_)
print(gm.covariances_)
print(gm.precisions_)

mus_fit = gm.means_.flatten()
sigs_fit = np.sqrt(gm.covariances_.flatten())
w_fit = gm.weights_.flatten()
x = np.linspace(0, 600, 600)

for i in range(len(mus_fit)):
    if i == 0:
        y = norm.pdf(x, mus_fit[i], sigs_fit[i]) * w_fit[i]
    else:
        y += norm.pdf(x, mus_fit[i], sigs_fit[i]) * w_fit[i]

y *= 14900
plt.plot(x, y, label="Rec. GMM", color="tab:red", lw=2, ls="--")
plt.hist(data, bins=bins, label="Data")
plt.legend()
plt.show()



# # fitting of a Gaussian Mixture Model based on samples from previously built model
# with pm.Model() as GMMfit:
#     starting_mu = np.mean(data)
#     print("Starting point is: ", starting_mu)
#     w = pm.Dirichlet('w', np.ones(2))
#     mus = pm.Normal('mus', np.array([100, 300]), np.array([50,60]), shape=2)
#     sigs = pm.HalfCauchy('sigs', beta=5.0, shape=2)
#     GMM = pm.NormalMixture('gmm', w=w, mu=mus, sigma=sigs, observed=data)
#
#     trace = pm.sample(1000, step=pm.Metropolis(), return_inferencedata=True)
#     # burn_in = 500  # skips the n iters, where the MCMC is just wandering
#     # chain = trace[burn_in:]
#
#     ppc = pm.sample_posterior_predictive(trace, var_names=['w', 'mus', 'sigs', 'gmm'], model=GMMfit)
#     idata_pymc3 = az.from_pymc3(trace, posterior_predictive=ppc)
#     print(ppc)
#     print("w: ", ppc["w"].transpose())
#     print("mus: ", ppc["mus"].transpose())
#     print("sigs: ", ppc["sigs"].transpose())
#
#     mus_infered = []
#     sigs_infered = []
#     weights_infered = []
#
#     for i in range(2):
#         mus_infered.append(np.mean(ppc['mus'].transpose()[i]))
#         sigs_infered.append(np.mean(ppc['sigs'].transpose()[i]))
#         weights_infered.append(np.mean(ppc['w'].transpose()[i]))
#
#     print("mus_infered: ", mus_infered)
#     print("sigs_infered: ", sigs_infered)
#     print("weights_infered: ", weights_infered)
#
#     pm.traceplot(trace)
#     pm.plot_posterior(chain)
#     pm.autocorrplot(chain)
#     plt.show()
#
#     x = np.linspace(0,600,600)
#     # print(idata_pymc3)
#     # print(idata_pymc3['posterior'])
#     # print(idata_pymc3['posterior_predictive'])
#     # print(idata_pymc3['log_likelihood'])
#     # print(idata_pymc3['sample_stats'])
#     # print(idata_pymc3['observed_data'])
#     for i in range(2):
#         if i == 0:
#             y = norm.pdf(x, mus_infered[i], sigs_infered[i]) * weights_infered[i]
#         else:
#             y += norm.pdf(x, mus_infered[i], sigs_infered[i]) * weights_infered[i]
#
#     y *= 14900
#     plt.plot(x, y, label="Rec. GMM", color="tab:red", lw=2, ls="--")
#     plt.hist(data, bins=bins, label="Data")
#     plt.legend()
#     az.plot_ppc(idata_pymc3)
#     plt.show()



