"""
'pip3 install pymc3' <-- newest version: 3.10.0
'pip3 install arviz==0.11.0'
<-- older but compatible version due to python 3.6 not 3.8

python 3.6.9
"""

from scipy import stats
from scipy.stats import norm

import numpy as np
import arviz as az
import pymc3 as pm
import theano
import seaborn as sn
import matplotlib.pyplot as plt
from bayesNetFramework import BayesNet, json2dict
import utility as ut
import nicePrint as nice
import queue


# az.style.use("arviz-darkgrid")
np.random.seed(123)

BN = BayesNet("BN")
BN.load_from_yaml("graph-def.yaml")
materials = json2dict("./materials.json")  # new iterable json

for prop in materials[0]['properties']:
    for name in prop:
        print(name)

axes = ut.create_axes(materials)

measurements = {
    "observations": {

        # n long vectors of observed values are queued
        'volume': queue.Queue(),
        'elasticity': queue.Queue(),
        "density": queue.Queue(),
        "mass": queue.Queue()
    },
    "parameters": {

        # tuples (mean, sigma) are queued
        'volume': queue.Queue(),
        'elasticity': queue.Queue(),
        "density": queue.Queue(),
        "mass": queue.Queue()
    },
    "errors": {

        # here should be the measurement errors
        'volume': 147,
        'elasticity': 400,
        'density': 550,
        'mass': 100
    }
}

manual_PMF = {
    "metal" : 0.5,
    "ceramic" : 0.1,
    "soft-plastic" : 0.1,
    "hard-plastic" : 0.2,
    "wood" : 0.025,
    "paper" : 0.025,
    "foam" : 0.05,
}

materials, alphas = ut.set_material_dir_alphas(materials, manual_PMF)


# N_OBS = 5  # five observations for each property
# ut.create_prior_dist(materials, axes['density'], 'density')
# for prop_name in measurements["observations"]:
#
#     # simulation of the measured material
#     true_mat_mu = ut.get_property(ut.get_material('metal', materials), prop_name)['mean']  # TODO just temporary
#     if true_mat_mu is None:
#         continue
#
#     for n in range(N_OBS):
#
#         # generate n observations and also return parameterers used for the obsv. creation
#         ob, params_of_ob = ut.generate_norm_rvs(loc=true_mat_mu, scale=sig_of_meas[prop_name], n_samples=30)
#         observations[prop_name].put((ob, params_of_ob))

"""
---------------------
MAIN ACTION SELECTION
---------------------
"""

MODE = 1
RAND_CHOICE = False
VERBOSITY = True


# iterations = 2
ut.plot_materials(axes, materials)

# taken_params = queue.Queue()
# observed_obs = queue.Queue()

# gets a new observation (values) and parameters from the queue
# ob = measurements["observations"]['volume'].get()
# params_of_ob = measurements['parameters']
# meas_mean = params_of_ob[0]
# meas_sigma = params_of_ob[1]

# taken_params.put(params_of_ob)
# observed_obs.put(ob)

ob_vol, pars_ob_vol = ut.generate_norm_rvs(loc=40, scale=10, n_samples=30, mmd=0)  # ceramic volume
print("pars_ob_vol", pars_ob_vol)

ob_mass, pars_ob_mass = ut.generate_norm_rvs(loc=90, scale=10, n_samples=30, mmd=0)  # ceramic mass
print("pars_ob_mass", pars_ob_mass)

pars = {"volume": [pars_ob_vol], "mass": [pars_ob_mass]}
obs = {"volume": [ob_vol], "mass": [ob_mass]}


local_obs = {'volume': [], 'mass': [], 'density': [], 'elasticity': []}
local_param_obs = {}

for action in ['volume', 'mass']:
    local_obs[action] = obs[action]
    local_param_obs[action] = pars[action]
    if action == 'volume':
        density_obs = 500.0 / np.array(obs['volume'])  # just temporary, 500 is the overall mean prior for the mass
    elif action == 'mass':
        density_obs = np.array(obs['mass']) / ut.argmax_of_prop(materials, 'volume', local_param_obs)
    local_obs['density'] = density_obs

    print("Observed: \n", local_obs)
    materials = ut.build_and_infer_from_model(materials, local_obs, local_param_obs)




# puts them into a list of already "taken" measurements
# taken_param_meas[chosen_action].append(norm.pdf(axes[chosen_action], chosen_mean, chosen_sigma))
# materials = ut.infer_with_pymc3(materials, chosen_action, ob, plot=True, vb=True)


# for i in range(iterations):
#
#     nice.iteration_number(i)
#     nice.pmf(materials)
#     chosen_action = None
#     max_info_gain = 0
#     for prop in ut.get_property_names(materials):
#
#         # MODE 1
#         # out_disc captures all returned by disc_ent_simulated_meas
#         out_disc = ut.disc_ent_simulated_meas(axes, materials, prop, sig_of_meas[prop], plot=True, vb=True)
#         prior_disc_ent = out_disc[0]
#         post_disc_ent = out_disc[1]
#         disc_info_gain = out_disc[2]
#
#         # MODE 2
#         # out_diff captures all returned by diff_ent_simulated_meas
#         out_diff = ut.diff_ent_simulated_meas(axes, materials, taken_param_meas, prop, sig_of_meas[prop],
#                                              plot=True, vb=True)
#         y_post = out_diff[0]
#         prior_diff_ent = out_diff[1]
#         post_diff_ent = out_diff[2]
#         diff_info_gain = out_diff[3]
#
#         if MODE == 1:
#             if disc_info_gain > max_info_gain:
#                 max_info_gain = disc_info_gain
#                 chosen_action = prop
#         elif MODE == 2:
#             if diff_info_gain > max_info_gain:
#                 max_info_gain = diff_info_gain
#                 chosen_action = prop
#         elif MODE == 3:
#             if (diff_info_gain + disc_info_gain) > max_info_gain:
#                 max_info_gain = (diff_info_gain + disc_info_gain)
#                 chosen_action = prop
#
#         if RAND_CHOICE and chosen_action is not None:
#             chosen_action = np.random.choice(ut.get_property_names(materials))
#
#     # taking the 'virtual' measurement
#     if chosen_action is not None:
#
#         # prints the planned action nicely
#         nice.planned_action(chosen_action, max_info_gain, vb=VERBOSITY)
#
#         # gets a new observation (values) and parameters from the queue
#         ob, params_of_ob = observations[chosen_action].get()
#         chosen_mean = params_of_ob[0]
#         chosen_sigma = params_of_ob[1]
#
#         # puts them into a list of already "taken" measurements
#         taken_param_meas[chosen_action].append(norm.pdf(axes[chosen_action], chosen_mean, chosen_sigma))
#         materials = ut.infer_with_pymc3(materials, chosen_action, ob, plot=True, vb=True)






