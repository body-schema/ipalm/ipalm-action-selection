from scipy import stats
import numpy as np
import arviz as az
import pymc3 as pm
import theano
import seaborn as sn
import matplotlib as mpl
import matplotlib.pyplot as plt
az.style.use("arviz-darkgrid")
np.random.seed(123)

"""

# a simple "moving" of a gaussian prior based on the incoming measurement

obs = np.random.normal(20,3,50)
with pm.Model() as model:
    prior_mu = pm.Normal('prior_mu', mu=10, sigma=1)  # prior for the mean of the gaussian
    prior_sig = pm.HalfCauchy('prior_sig', beta=5)  # prior for the sigma of the gaussian
    
    # fitting the prior params above to the likelihood function
    # based on the data observer and the information, that the likelihood function is thought to be a gaussian
    y = pm.Normal('y', mu=prior_mu, sigma=prior_sig, observed=obs)  

    start = pm.find_MAP()
    step = pm.Metropolis()
    trace = pm.sample(1000, step=step, start=start)
    burnin = 500  # skips the n iters, where the MCMC is just wandering
    chain = trace[burnin:]
    pm.plot_posterior(chain, ref_val=20)
    plt.show()

"""


# def from_posterior(param, samples):
#     smin, smax = np.min(samples), np.max(samples)
#     width = smax - smin
#     x = np.linspace(smin, smax, 1000)
#     fig, ax = plt.subplots(figsize=(8, 6))
#     print("len of samples: ", len(samples))
#     ax.hist(samples, bins=120, density=True, lw=0)
#     plt.show()
#     y = stats.gaussian_kde(samples)(x)
#
#     # what was never sampled should have a small probability but not 0,
#     # so we'll extend the domain and use linear approximation of density on it
#     x = np.concatenate([[x[0] - 3 * width], x, [x[-1] + 3 * width]])  # just widen start and end
#
#     y = np.concatenate([[0], y, [0]])
#     return pm.distributions.Interpolated(param, x, y)


W = np.array([0.5, 0.2, 0.3])
MU = np.array([8.0, 20.0, 25.0])
SIGMA = np.array([3.0, 5.0, 4.0])
N = 1000
# component = np.random.choice(MU.size, size=N, p=W)
# x = np.random.normal(MU[component], SIGMA[component], size=N)

x = np.random.normal(21, 5, size=N)

fig, ax = plt.subplots(figsize=(8, 6))
ax.hist(x, bins=120, density=True, lw=0)
plt.show()

with pm.Model() as model:

    w = pm.Dirichlet("w", np.ones_like(W))  # TODO how to get dirichlet parameters from the posterior?
    x_obs = pm.NormalMixture("x_obs", w, mu=MU, sigma=SIGMA, observed=x)

with model:

    trace = pm.sample(1000)
    ppc = pm.sample_posterior_predictive(trace, var_names=['w'], model=model)

    print("Weights from ppc: ", ppc['w'].transpose())
    print("mean weights of Dirichlet dist:")
    print(w.__dict__)
    for w_ in ppc['w'].transpose():
        print(np.mean(w_))
    # print("mean weights: ", np.mean(ppc['w'].transpose()[0]), np.mean(ppc['w'].transpose()[1]))
traces = [trace]

exit()
for it in range(10):

    #  generate new data
    component = np.random.choice(MU.size, size=N, p=W)
    x = np.random.normal(MU[component], SIGMA[component], size=N)

    with model:
        # print("Number of iterations: ", it)
        # print("trace['w']", trace['w'])
        w = pm.Dirichlet("w", np.ones_like(W))
        # mu = from_posterior('mu', trace['mu'])
        # tau = from_posterior('tau', trace['tau'])

        x_obs = pm.NormalMixture("x_obs", w, mu=MU, sigma=SIGMA, observed=x)
        trace = pm.sample(1000)
        traces.append(trace)

print("Posterior distributions after " + str(len(traces)) + " iterations.")
cmap = mpl.cm.autumn
for param in ["w", "mu", "tau"]:
    plt.figure(figsize=(8, 2))
    for update_i, trace in enumerate(traces):
        samples = trace[param]
        smin, smax = np.min(samples), np.max(samples)
        x = np.linspace(smin, smax, 100)
        y = stats.gaussian_kde(samples)(x)
        plt.plot(x, y, color=cmap(1 - update_i / len(traces)))
    # plt.axvline({"alpha": alpha_true, "beta0": beta0_true, "beta1": beta1_true}[param], c="k")
    plt.ylabel("Frequency")
    plt.title(param)

plt.tight_layout()

# trace.add_groups(posterior_predictive=ppc_trace)  # what is this?
# az.plot_trace(trace, var_names=["w", "mu", 'tau'])
# az.plot_posterior(trace, var_names=["w", "mu", 'tau'])
# az.plot_ppc(trace)

plt.show()

    #
    # # priors for the GMM
    # mu_1 = pm.Normal('mu_1', mu=10, sigma=3)
    # mu_2 = pm.Normal('mu_2', mu=15, sigma=3)
    #
    # sig_1 = pm.HalfCauchy('sig_1', beta=2)
    # sig_2 = pm.HalfCauchy('sig_2', beta=5)

