import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import rcParams
import numpy as np
from scipy.stats import norm
import utility as ut

density = {"A": {"mean": 200,
                 "sigma": 40},
           "B": {"mean": 300,
                 "sigma": 60},
           "C": {"mean": 800,
                 "sigma": 55}
           }

elasticity = {"A": {"mean": 200,
                    "sigma": 55},
              "B": {"mean": 700,
                    "sigma": 100},
              "C": {"mean": 800,
                    "sigma": 40}
              }

materials = ['A', 'B', 'C']

probs = {'A': 0.33333,
         'B': 0.33333,
         'C': 0.33333}

fig, axs = plt.subplots(2)
# print(rcParams)

x = np.linspace(0, 1000, 1000)

y_dens = None
y_elas = None

for mat in materials:
    density_i = density[mat]
    elasticity_i = elasticity[mat]

    # construct partial pdfs
    y_dens_i = norm.pdf(x, density_i["mean"], density_i["sigma"])
    y_elas_i = norm.pdf(x, elasticity_i["mean"], elasticity_i["sigma"])

    # plot partial pdfs
    axs[0].plot(x, y_dens_i, label=mat)
    axs[1].plot(x, y_elas_i, label=mat)
    if y_dens is None:
        y_dens = y_dens_i * probs[mat]
    else:
        y_dens += y_dens_i * probs[mat]

    if y_elas is None:
        y_elas = y_elas_i * probs[mat]
    else:
        y_elas += y_elas_i * probs[mat]

# normalize - NOTE: generally should use bin_size here, although it is now unity
# y_elast = y_elast / sum(y_elast)
# y_dens = y_dens / sum(y_dens)

# print(sum(y_dens))
# print(sum(y_elas))

axs[0].plot(x, y_dens, label="Density GMM", color="tab:blue")
axs[1].plot(x, y_elas, label="Elasticity GMM", color="tab:blue")
# rcParams['figure.subplot.hspace'] = 0.5

# compute discrete entropy
ent_d = 0
for mat in probs:
    prob = probs[mat]
    ent_d -= prob * np.log2(prob)

# compute differential entropy
ent_dens = - sum(y_dens * np.log2(y_dens))
ent_elas = - sum(y_elas * np.log2(y_elas))

print("Discrete entropy is:\n   ", ent_d)
print("Contiunous entropies are:")
print("Density: \n   ", ent_dens)
print("Elasticity: \n   ", ent_elas)

axs[0].set_title("Density")
axs[0].set(xlabel=r'\rho [kgm^{-3}]', ylabel=r'p(x)')
axs[1].set_title("Elasticity")
axs[1].set(xlabel=r'E [Pa]', ylabel=r'p(x)')

plt.show()







