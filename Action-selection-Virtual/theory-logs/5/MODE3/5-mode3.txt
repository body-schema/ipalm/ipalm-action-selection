Cleaning the previous 'meas.json' file.
----------------------
| ITERATION NUMBER: 0 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.4490 | Cond. likelihood: None
Material:   ceramic | Probabilty: 0.3500 | Cond. likelihood: None
Material:   plastic | Probabilty: 0.1000 | Cond. likelihood: None
Material: aluminium | Probabilty: 0.1000 | Cond. likelihood: None
Material:     joker | Probabilty: 0.0010 | Cond. likelihood: None
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 10.830366873442362 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.7231424935670523   |
| The average sampled Entropy is: 0.5545553175615451   |
| -OUT- Discrete Information Gain: 1.168587176005507   |
| -info- AVERAGE sampled Information Gain: 1.168587176005507 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 10.830366873442362 |
| This is the Emulated Entropy for density: 10.476452484004698 |
| Argmax mean: 2300 (no offset)|
| -x- Diff Information Gain: 0.35391438943766396     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.7231424935670523   |
| The average sampled Entropy is: 0.9646122979675822   |
| -OUT- Discrete Information Gain: 0.7585301955994701   |
| -info- AVERAGE sampled Information Gain: 0.7585301955994701 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  1.522501565443171
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2724 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.7160 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0184 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.2576 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0080 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 1 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.7160 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0184 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.2576 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0080 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.045710542614522 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0110562276018884   |
| The average sampled Entropy is: 0.8658552849199921   |
| -OUT- Discrete Information Gain: 0.14520094268189632   |
| -info- AVERAGE sampled Information Gain: 0.145200942681896 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.045710542614522 |
| This is the Emulated Entropy for density: 8.84039110462788 |
| Argmax mean: 2304 (no offset)|
| -x- Diff Information Gain: 0.20531943798664187     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0110562276018884   |
| The average sampled Entropy is: 0.9294308097287859   |
| -OUT- Discrete Information Gain: 0.08162541787310251   |
| -info- AVERAGE sampled Information Gain: 0.08162541787310262 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.3505203806685382
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2700 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.6672 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0015 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3022 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0291 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 2 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.6672 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0015 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3022 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0291 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.050385897806926 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0741455380041622   |
| The average sampled Entropy is: 0.862771199547702   |
| -OUT- Discrete Information Gain: 0.21137433845646025   |
| -info- AVERAGE sampled Information Gain: 0.21137433845646073 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.050385897806926 |
| This is the Emulated Entropy for density: 8.927853385239976 |
| Argmax mean: 2308 (no offset)|
| -x- Diff Information Gain: 0.1225325125669503     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0741455380041622   |
| The average sampled Entropy is: 0.9540415110389999   |
| -OUT- Discrete Information Gain: 0.12010402696516231   |
| -info- AVERAGE sampled Information Gain: 0.12010402696516254 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.33390685102341056
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2691 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.5744 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0001 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3277 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0978 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 3 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.5744 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0001 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3277 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0978 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.139284997020425 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.3164083155993322   |
| The average sampled Entropy is: 0.9711601354600421   |
| -OUT- Discrete Information Gain: 0.34524818013929015   |
| -info- AVERAGE sampled Information Gain: 0.34524818013929 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.139284997020425 |
| This is the Emulated Entropy for density: 9.04902982095341 |
| Argmax mean: 2311 (no offset)|
| -x- Diff Information Gain: 0.09025517606701605     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.3164083155993322   |
| The average sampled Entropy is: 1.1545934539508311   |
| -OUT- Discrete Information Gain: 0.16181486164850112   |
| -info- AVERAGE sampled Information Gain: 0.16181486164850128 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.4355033562063062
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2714 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.4196 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3014 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.2790 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 4 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.4196 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3014 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.2790 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.232245882669368 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.5611546791640851   |
| The average sampled Entropy is: 1.0122252204496895   |
| -OUT- Discrete Information Gain: 0.5489294587143956   |
| -info- AVERAGE sampled Information Gain: 0.5489294587143955 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.232245882669368 |
| This is the Emulated Entropy for density: 9.166022297750708 |
| Argmax mean: 2315 (no offset)|
| -x- Diff Information Gain: 0.0662235849186601     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.5611546791640851   |
| The average sampled Entropy is: 1.2962053278797592   |
| -OUT- Discrete Information Gain: 0.26494935128432595   |
| -info- AVERAGE sampled Information Gain: 0.2649493512843255 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.6151530436330557
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2685 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.2222 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.2009 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.5769 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 5 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.2222 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.2009 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.5769 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.312173301257838 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.4052409417504363   |
| The average sampled Entropy is: 0.9181230903979108   |
| -OUT- Discrete Information Gain: 0.4871178513525255   |
| -info- AVERAGE sampled Information Gain: 0.4871178513525254 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.312173301257838 |
| This is the Emulated Entropy for density: 9.265640198629677 |
| Argmax mean: 2318 (no offset)|
| -x- Diff Information Gain: 0.04653310262816035     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.4052409417504363   |
| The average sampled Entropy is: 1.0939425826074765   |
| -OUT- Discrete Information Gain: 0.3112983591429599   |
| -info- AVERAGE sampled Information Gain: 0.3112983591429595 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.5336509539806858
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2718 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0814 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0927 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.8258 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 6 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0814 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0927 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.8258 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.372976439336195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8408219133193136   |
| The average sampled Entropy is: 0.5633872599464842   |
| -OUT- Discrete Information Gain: 0.27743465337282935   |
| -info- AVERAGE sampled Information Gain: 0.27743465337282947 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.372976439336195 |
| This is the Emulated Entropy for density: 9.340914325383649 |
| Argmax mean: 2321 (no offset)|
| -x- Diff Information Gain: 0.03206211395254677     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8408219133193136   |
| The average sampled Entropy is: 0.6073072394351677   |
| -OUT- Discrete Information Gain: 0.23351467388414582   |
| -info- AVERAGE sampled Information Gain: 0.23351467388414546 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.3094967673253761
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2720 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0238 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0341 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.9421 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 7 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0238 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0341 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.9421 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.412279880719503 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.3756018513894724   |
| The average sampled Entropy is: 0.26289864922086975   |
| -OUT- Discrete Information Gain: 0.11270320216860263   |
| -info- AVERAGE sampled Information Gain: 0.11270320216860254 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.412279880719503 |
| This is the Emulated Entropy for density: 9.388721342432074 |
| Argmax mean: 2325 (no offset)|
| -x- Diff Information Gain: 0.023558538287428377     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.3756018513894724   |
| The average sampled Entropy is: 0.31128235475243954   |
| -OUT- Discrete Information Gain: 0.06431949663703285   |
| -info- AVERAGE sampled Information Gain: 0.06431949663703283 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.136261740456031
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2710 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0063 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0115 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.9822 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 8 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0063 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0115 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.9822 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.429972381003061 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.14572711875333594   |
| The average sampled Entropy is: 0.13084725226270205   |
| -OUT- Discrete Information Gain: 0.014879866490633886   |
| -info- AVERAGE sampled Information Gain: 0.014879866490633916 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.429972381003061 |
| This is the Emulated Entropy for density: 9.409237230479834 |
| Argmax mean: 2328 (no offset)|
| -x- Diff Information Gain: 0.02073515052322783     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.14572711875333594   |
| The average sampled Entropy is: 0.13591695062605647   |
| -OUT- Discrete Information Gain: 0.009810168127279473   |
| -info- AVERAGE sampled Information Gain: 0.009810168127279487 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.047433222519766394
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 163 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.0150 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.0323 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.9528 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 9 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.0150 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.0323 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.9528 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.429972381003061 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.3171122106366123   |
| The average sampled Entropy is: 0.2162342425030117   |
| -OUT- Discrete Information Gain: 0.10087796813360062   |
| -info- AVERAGE sampled Information Gain: 0.10087796813360052 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.429972381003061 |
| This is the Emulated Entropy for density: 9.409237230479834 |
| Argmax mean: 2328 (no offset)|
| -x- Diff Information Gain: 0.02073515052322783     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.7926393591345855 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.3171122106366123   |
| The average sampled Entropy is: 0.2482695515246209   |
| -OUT- Discrete Information Gain: 0.06884265911199142   |
| -info- AVERAGE sampled Information Gain: 0.06884265911199139 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.7926393591345855 |
| This is the Emulated Entropy for elasticity: 6.44184824693757 |
| Argmax mean: 31 (no offset)|
| -x- Diff Information Gain: 0.3507911121970153     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.4196337713090067
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 179 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.0336 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.0865 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.8799 | Cond. likelihood: 0.00217391
MODE 3 was chosen --> optimizing was done for hybrid sum of 
 differential and discrete information gain.
The argmax mean for density : 2328
The argmax mean for elasticity : 31

