import os
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn import datasets, linear_model
from scipy.signal import savgol_filter
import pandas as pd
import matplotlib.pyplot as plt

FILE_NAME = None
####################### Robotiq + 2F-85 Functions ###############################


# This function analyses the filename for metadata: foam, speed, thickness and extracts the raw data from the file.
# F = 87.596 * i**3 -216*i**2 + 191.397*i + 0.176

def get_raw_data(filename):

	#file_address=filename.split('/')
	#speed_string=file_address[7]
	#foam_name=file_address[8]
	#metadata=file_address[9].split('.')[0]+'.'+file_address[9].split('.')[1]
	#thickness=float(metadata.split('-')[7])
	# print("THIS IS FILE NAME: ")
	# print(filename)
	df=pd.read_csv(filename,sep=' ')
	time=list()
	position=list()
	force=list()
	coeffs=[87.596, -216, 191.397, 0.176]
	cubic_model=np.poly1d(coeffs)

	for counter in range(0,len(df['//'])):
		time.append(float(df['//'][counter]))
		position.append(float(df['Time'][counter]))
		force.append(cubic_model(df['Position'][counter]))
    
	return  time, position, force #metadata, foam_name, thickness, speed_string,


#This function gets the starting and ending points of the cycle.
#num_cycles has been set to 1.0, in case the number of cycles is different please change the value of the variable accordingly.

def find_cycle_pts(position):

	min_pos=min(position)
	temp_peaks=list()
	data_length=float(len(position))
	num_cycles=1
	cycle_pts=list()
    
	for counter in range(0,len(position)):
		if abs(position[counter]-min_pos)<2:
			temp_peaks.append(counter)
    
	for counter in range(0,len(temp_peaks)-1):
		gap=temp_peaks[counter+1]-temp_peaks[counter]
		if(gap>50):
			cycle_pts.append(temp_peaks[counter])

	cycle_pts.append(temp_peaks[len(temp_peaks)-1])
	return cycle_pts

#This function calls the find_cycle_pts function, and extracts and returns only the first cycle data.

def get_C1_data(time, position, force):

	cycle_pts=find_cycle_pts(position)
	print(cycle_pts)
	C1_time=list()
	C1_position=list()
	C1_force=list()

	for counter in range(cycle_pts[0]+1,len(position)-1):
		C1_time.append(time[counter])
		C1_position.append(position[counter])
		C1_force.append(force[counter])

	return C1_time, C1_position, C1_force

#Data filtering function

def implement_savgol(x):
	return savgol_filter(x,51,2)

# This function gets the stress and strain from the raw data.

def format_data(position, force, thickness): 

	finger_area=0.000825
	min_pos=min(position)
	strain=list()
	stress=list()

	for temp_counter in range(0,len(position)):
		strain.append(((position[temp_counter]-min_pos)/1000.0)/thickness)
		stress.append(force[temp_counter]/finger_area)      

	return strain, stress

#This function splits the data into the compression cycle and the release cycle.
#Only the compression cycle is returned

def get_compression_cycle(time, strain, stress):

	temp_time=list()
	temp_strain=list()
	temp_stress=list()
	switch=0
	for counter in range(0,len(stress)):
		if(switch==0):
			temp_time.append(time[counter])
			temp_strain.append(strain[counter])
			temp_stress.append(stress[counter])
		if(stress[counter]==max(stress)):
			switch=1

	return temp_time, temp_strain, temp_stress

#Function that calculates the modulus value

def get_modulus(strain, stress):

	temp_stress=[]
	temp_strain=[]
	for counter in range(0,len(strain)):
		temp_stress.append(stress[counter])
		temp_strain.append(strain[counter])
	temp_stress=np.asarray(temp_stress)
	temp_strain=np.asarray(temp_strain).reshape(-1,1)
	temp_lin_reg_model=LinearRegression().fit(temp_strain,temp_stress)
	regr = linear_model.LinearRegression()
	regr.fit(temp_strain, temp_stress)
	y = regr.predict(temp_strain)
	fig = plt.figure(figsize=(6,3))
	# plt.scatter(temp_strain, temp_stress, color="tab:blue", s=1)
	# plt.plot(temp_strain, y, color="tab:red", lw=2, ls="--", label="Regression")
	# plt.ticklabel_format(axis="y", style="sci", scilimits=(0, 0))
	# plt.xlabel("Strain [-]", fontsize=11)
	# plt.ylabel("Stress [Pa]", fontsize=11)
	# plt.legend()
	# plt.show()


	modulus=temp_lin_reg_model.coef_
	intercept=temp_lin_reg_model.intercept_
	R2=temp_lin_reg_model.score(temp_strain,temp_stress)
	# print("The Intercept is: ", intercept)


	return modulus, intercept, R2

def get_calibrated_modulus(strain, stress):

	mod, intercept, R2= get_modulus(strain, stress)
	actual_modulus=(0.4628*(mod/1000.0))-9.847

	return actual_modulus, R2

######################################### Main Body #######################################################


def yield_results(file_name, verbosity=False):
	# file_name=("/home/shubhan/2021-May-20-09-02-ObjectNum_1.txt")
	# print("THIS IS THE FILE NAME IN THE SHUBHANS CODE: ", file_name)

	sigma = 4.091  # kPa for RobotiQ 2F-85
	FILE_NAME = os.path.abspath(file_name)

	time, position, force = get_raw_data(FILE_NAME)
	thickness=0.056

	C_time, C1_position, C1_force= get_C1_data(time, position, force)
	C_strain, C_stress=format_data(C1_position, C1_force, thickness)
	if len(C_strain) < 52 or len(C_stress) < 52:
		print("TOO SMALL TIME SERIES SAMPLE: {}".format(min(len(C_strain), len(C_stress))))
		return -10000, sigma

	filtered_C_strain=implement_savgol(C_strain)
	filtered_C_stress=implement_savgol(C_stress)



	foam_modulus, R2 = get_calibrated_modulus(filtered_C_strain,filtered_C_stress)

	# if verbosity:



	if foam_modulus < 0.05:
		print("Unusable data, the modulus is negative!")
	print("Value of Modulus is "+str(foam_modulus)+"kPa, the sigma is " + str(4.091) + "kPa")
	return foam_modulus, sigma
