Cleaning the previous 'meas.json' file.
----------------------
| ITERATION NUMBER: 0 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0490 | Cond. likelihood: None
Material:   ceramic | Probabilty: 0.3500 | Cond. likelihood: None
Material:   plastic | Probabilty: 0.1000 | Cond. likelihood: None
Material: aluminium | Probabilty: 0.5000 | Cond. likelihood: None
Material:     joker | Probabilty: 0.0010 | Cond. likelihood: None
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 10.414960097409153 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.585461851850611   |
| The average sampled Entropy is: 0.6996530076758475   |
| -OUT- Discrete Information Gain: 0.8858088441747635   |
| -info- AVERAGE sampled Information Gain: 0.885808844174763 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 10.414960097409153 |
| This is the Emulated Entropy for density: 9.666231106196143 |
| Argmax mean: 2304 (no offset)|
| -x- Diff Information Gain: 0.7487289912130102     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.0613755325850995 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.585461851850611   |
| The average sampled Entropy is: 1.0678433899593658   |
| -OUT- Discrete Information Gain: 0.5176184618912452   |
| -info- AVERAGE sampled Information Gain: 0.5176184618912452 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.0613755325850995 |
| This is the Emulated Entropy for elasticity: 6.689350232235342 |
| Argmax mean: 31 (no offset)|
| -x- Diff Information Gain: 0.37202530034975734     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  1.6345378353877735
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2713 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.2483 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0003 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.7485 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0029 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 1 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.2483 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0003 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.7485 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0029 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8398009814434119   |
| The average sampled Entropy is: 0.5977407868801566   |
| -OUT- Discrete Information Gain: 0.24206019456325523   |
| -info- AVERAGE sampled Information Gain: 0.24206019456325506 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.0613755325850995 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8398009814434119   |
| The average sampled Entropy is: 0.6298479487782178   |
| -OUT- Discrete Information Gain: 0.20995303266519405   |
| -info- AVERAGE sampled Information Gain: 0.20995303266519444 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.0613755325850995 |
| This is the Emulated Entropy for elasticity: 6.689350232235342 |
| Argmax mean: 31 (no offset)|
| -x- Diff Information Gain: 0.37202530034975734     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.5819783330149514
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 171 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.1452 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.8541 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0006 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 2 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.1452 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.8541 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0006 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.6059299113689526   |
| The average sampled Entropy is: 0.42177514788317233   |
| -OUT- Discrete Information Gain: 0.18415476348578025   |
| -info- AVERAGE sampled Information Gain: 0.18415476348578014 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.510872494366296 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.6059299113689526   |
| The average sampled Entropy is: 0.46548359381371335   |
| -OUT- Discrete Information Gain: 0.14044631755523923   |
| -info- AVERAGE sampled Information Gain: 0.14044631755523926 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.510872494366296 |
| This is the Emulated Entropy for elasticity: 6.354018980489982 |
| Argmax mean: 68 (no offset)|
| -x- Diff Information Gain: 0.15685351387631474     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.297299831431554
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 178 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0801 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9198 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0001 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 3 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0801 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9198 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0001 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.40454551230479857   |
| The average sampled Entropy is: 0.32128483521012896   |
| -OUT- Discrete Information Gain: 0.08326067709466961   |
| -info- AVERAGE sampled Information Gain: 0.08326067709466979 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.234527955828286 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.40454551230479857   |
| The average sampled Entropy is: 0.3542702124907749   |
| -OUT- Discrete Information Gain: 0.05027529981402368   |
| -info- AVERAGE sampled Information Gain: 0.05027529981402368 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.234527955828286 |
| This is the Emulated Entropy for elasticity: 6.0840304940747965 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.15049746175348933     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.200772761567513
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 172 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0427 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9573 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 4 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0427 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9573 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.25504399807629713   |
| The average sampled Entropy is: 0.2067389555499805   |
| -OUT- Discrete Information Gain: 0.04830504252631662   |
| -info- AVERAGE sampled Information Gain: 0.04830504252631655 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.008348865591114 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.25504399807629713   |
| The average sampled Entropy is: 0.23011613120945715   |
| -OUT- Discrete Information Gain: 0.02492786686683998   |
| -info- AVERAGE sampled Information Gain: 0.02492786686684 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.008348865591114 |
| This is the Emulated Entropy for elasticity: 5.869669713129925 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.13867915246118923     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.1636070193280292
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 165 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0223 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9776 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 5 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0223 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9776 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.15452180924077602   |
| The average sampled Entropy is: 0.16334299396330845   |
| -OUT- Discrete Information Gain: -0.008821184722532432   |
| -info- AVERAGE sampled Information Gain: -0.00882118472253239 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.823059426557183 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.15452180924077602   |
| The average sampled Entropy is: 0.15144514790045613   |
| -OUT- Discrete Information Gain: 0.0030766613403198917   |
| -info- AVERAGE sampled Information Gain: 0.003076661340319864 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.823059426557183 |
| This is the Emulated Entropy for elasticity: 5.70071596889448 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.12234345766270316     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.12542011900302305
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 161 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0116 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9884 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 6 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0116 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9884 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.09108026079436987   |
| The average sampled Entropy is: 0.08710443832780554   |
| -OUT- Discrete Information Gain: 0.003975822466564333   |
| -info- AVERAGE sampled Information Gain: 0.003975822466564312 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.6708856754043415 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.09108026079436987   |
| The average sampled Entropy is: 0.0733177440865529   |
| -OUT- Discrete Information Gain: 0.017762516707816978   |
| -info- AVERAGE sampled Information Gain: 0.017762516707816946 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.6708856754043415 |
| This is the Emulated Entropy for elasticity: 5.564490323650635 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.10639535175370618     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.12415786846152316
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 163 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0060 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9940 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 7 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0060 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9940 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.05264905601913278   |
| The average sampled Entropy is: 0.050607126041300514   |
| -OUT- Discrete Information Gain: 0.0020419299778322644   |
| -info- AVERAGE sampled Information Gain: 0.002041929977832277 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.544322112365512 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.05264905601913278   |
| The average sampled Entropy is: 0.04304033528355534   |
| -OUT- Discrete Information Gain: 0.00960872073557744   |
| -info- AVERAGE sampled Information Gain: 0.009608720735577442 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.544322112365512 |
| This is the Emulated Entropy for elasticity: 5.451585354895648 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.0927367574698641     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.10234547820544154
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 174 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0031 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9969 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 8 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0031 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9969 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.029999940652370553   |
| The average sampled Entropy is: 0.02960969195082903   |
| -OUT- Discrete Information Gain: 0.0003902487015415221   |
| -info- AVERAGE sampled Information Gain: 0.00039024870154153927 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.437210575437024 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.029999940652370553   |
| The average sampled Entropy is: 0.03320535680541556   |
| -OUT- Discrete Information Gain: -0.0032054161530450063   |
| -info- AVERAGE sampled Information Gain: -0.0032054161530450163 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.437210575437024 |
| This is the Emulated Entropy for elasticity: 5.355637030287308 |
| Argmax mean: 70 (no offset)|
| -x- Diff Information Gain: 0.08157354514971527     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.08736975024885668
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2713 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0015 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.9985 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 9 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0015 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.9985 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.448122636516208 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.015809057938260814   |
| The average sampled Entropy is: 0.014580493461389992   |
| -OUT- Discrete Information Gain: 0.001228564476870822   |
| -info- AVERAGE sampled Information Gain: 0.0012285644768708175 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.448122636516208 |
| This is the Emulated Entropy for density: 9.312169184784498 |
| Argmax mean: 2700 (no offset)|
| -x- Diff Information Gain: 0.13595345173171047     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.437210575437024 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.015809057938260814   |
| The average sampled Entropy is: 0.012424713736989142   |
| -OUT- Discrete Information Gain: 0.003384344201271672   |
| -info- AVERAGE sampled Information Gain: 0.003384344201271669 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.437210575437024 |
| This is the Emulated Entropy for elasticity: 5.355637030287308 |
| Argmax mean: 70 (no offset)|
| -x- Diff Information Gain: 0.08157354514971527     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.13718201620858128
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2700 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0007 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.9993 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
MODE 3 was chosen --> optimizing was done for hybrid sum of 
 differential and discrete information gain.
The argmax mean for density : 2700
The argmax mean for elasticity : 70

