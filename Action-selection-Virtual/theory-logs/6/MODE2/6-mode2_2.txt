Cleaning the previous 'meas.json' file.
----------------------
| ITERATION NUMBER: 0 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.2000 | Cond. likelihood: None
Material:   ceramic | Probabilty: 0.2000 | Cond. likelihood: None
Material:   plastic | Probabilty: 0.2000 | Cond. likelihood: None
Material: aluminium | Probabilty: 0.2000 | Cond. likelihood: None
Material:     joker | Probabilty: 0.2000 | Cond. likelihood: None
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 12.03286228523066 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 2.5   |
| The average sampled Entropy is: 1.24600984130283   |
| -OUT- Discrete Information Gain: 1.25399015869717   |
| -info- AVERAGE sampled Information Gain: 1.25399015869717 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 12.03286228523066 |
| This is the Emulated Entropy for density: 11.199517956926307 |
| Argmax mean: 2301 (no offset)|
| -x- Diff Information Gain: 0.8333443283043529     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 2.5   |
| The average sampled Entropy is: 1.7555113659655752   |
| -OUT- Discrete Information Gain: 0.7444886340344248   |
| -info- AVERAGE sampled Information Gain: 0.7444886340344244 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
| This is the Emulated Entropy for elasticity: 7.577236055365909 |
| Argmax mean: 30 (no offset)|
| -x- Diff Information Gain: 0.37151285372028564     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.8333443283043529
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2717 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1599 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0144 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.2013 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.6244 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 1 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1599 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0144 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.2013 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.6244 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.966018557745054 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.400786728515491   |
| The average sampled Entropy is: 0.7400555016547227   |
| -OUT- Discrete Information Gain: 0.6607312268607682   |
| -info- AVERAGE sampled Information Gain: 0.6607312268607685 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.966018557745054 |
| This is the Emulated Entropy for density: 9.720356463054879 |
| Argmax mean: 2305 (no offset)|
| -x- Diff Information Gain: 0.24566209469017508     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.400786728515491   |
| The average sampled Entropy is: 1.0309372353856423   |
| -OUT- Discrete Information Gain: 0.3698494931298486   |
| -info- AVERAGE sampled Information Gain: 0.36984949312984894 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
| This is the Emulated Entropy for elasticity: 7.577236055365909 |
| Argmax mean: 30 (no offset)|
| -x- Diff Information Gain: 0.37151285372028564     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.37151285372028564
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 166 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.2397 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0146 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.3604 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.3853 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 2 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.2397 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0146 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.3604 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.3853 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.966018557745054 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.6438883221570633   |
| The average sampled Entropy is: 1.140802351014492   |
| -OUT- Discrete Information Gain: 0.5030859711425713   |
| -info- AVERAGE sampled Information Gain: 0.5030859711425707 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.966018557745054 |
| This is the Emulated Entropy for density: 9.720356463054879 |
| Argmax mean: 2305 (no offset)|
| -x- Diff Information Gain: 0.24566209469017508     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.266989727546608 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.6438883221570633   |
| The average sampled Entropy is: 1.3187839193347568   |
| -OUT- Discrete Information Gain: 0.32510440282230646   |
| -info- AVERAGE sampled Information Gain: 0.32510440282230746 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.266989727546608 |
| This is the Emulated Entropy for elasticity: 6.9821010891992845 |
| Argmax mean: 31 (no offset)|
| -x- Diff Information Gain: 0.2848886383473239     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.2848886383473239
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 160 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.2859 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0118 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.5132 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.1891 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 3 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.2859 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0118 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.5132 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.1891 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.966018557745054 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.5403064021724648   |
| The average sampled Entropy is: 1.058307577983549   |
| -OUT- Discrete Information Gain: 0.48199882418891593   |
| -info- AVERAGE sampled Information Gain: 0.48199882418891593 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.966018557745054 |
| This is the Emulated Entropy for density: 9.720356463054879 |
| Argmax mean: 2305 (no offset)|
| -x- Diff Information Gain: 0.24566209469017508     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.992124777584703 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.5403064021724648   |
| The average sampled Entropy is: 1.325076223839324   |
| -OUT- Discrete Information Gain: 0.21523017833314073   |
| -info- AVERAGE sampled Information Gain: 0.21523017833314115 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.992124777584703 |
| This is the Emulated Entropy for elasticity: 6.842111868348414 |
| Argmax mean: 33 (no offset)|
| -x- Diff Information Gain: 0.15001290923628918     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.24566209469017508
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2712 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1711 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0006 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3866 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.4417 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 4 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1711 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0006 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3866 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.4417 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.744453846163172 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.4932660978991095   |
| The average sampled Entropy is: 0.946369764192204   |
| -OUT- Discrete Information Gain: 0.5468963337069055   |
| -info- AVERAGE sampled Information Gain: 0.5468963337069054 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.744453846163172 |
| This is the Emulated Entropy for density: 9.649309695370164 |
| Argmax mean: 2309 (no offset)|
| -x- Diff Information Gain: 0.09514415079300775     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.992124777584703 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.4932660978991095   |
| The average sampled Entropy is: 1.228562185271487   |
| -OUT- Discrete Information Gain: 0.2647039126276225   |
| -info- AVERAGE sampled Information Gain: 0.26470391262762283 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.992124777584703 |
| This is the Emulated Entropy for elasticity: 6.842111868348414 |
| Argmax mean: 33 (no offset)|
| -x- Diff Information Gain: 0.15001290923628918     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.15001290923628918
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 167 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.2099 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0005 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.5665 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.2231 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 5 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.2099 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0005 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.5665 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.2231 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.744453846163172 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.4258262949721676   |
| The average sampled Entropy is: 0.9879349879854267   |
| -OUT- Discrete Information Gain: 0.43789130698674095   |
| -info- AVERAGE sampled Information Gain: 0.4378913069867405 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.744453846163172 |
| This is the Emulated Entropy for density: 9.649309695370164 |
| Argmax mean: 2309 (no offset)|
| -x- Diff Information Gain: 0.09514415079300775     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.839727062030601 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.4258262949721676   |
| The average sampled Entropy is: 1.2172728644093462   |
| -OUT- Discrete Information Gain: 0.20855343056282138   |
| -info- AVERAGE sampled Information Gain: 0.2085534305628215 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.839727062030601 |
| This is the Emulated Entropy for elasticity: 6.736609169274303 |
| Argmax mean: 34 (no offset)|
| -x- Diff Information Gain: 0.10311789275629835     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.10311789275629835
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 164 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.2146 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0004 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.6912 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0938 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 6 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.2146 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0004 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.6912 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0938 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.744453846163172 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.1691711826858513   |
| The average sampled Entropy is: 0.8803413706758962   |
| -OUT- Discrete Information Gain: 0.28882981200995506   |
| -info- AVERAGE sampled Information Gain: 0.288829812009955 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.744453846163172 |
| This is the Emulated Entropy for density: 9.649309695370164 |
| Argmax mean: 2309 (no offset)|
| -x- Diff Information Gain: 0.09514415079300775     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.722276465519388 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.1691711826858513   |
| The average sampled Entropy is: 1.0061157757306345   |
| -OUT- Discrete Information Gain: 0.1630554069552168   |
| -info- AVERAGE sampled Information Gain: 0.1630554069552172 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.722276465519388 |
| This is the Emulated Entropy for elasticity: 6.636683220329012 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.0855932451903767     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.09514415079300775
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2700 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1478 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.5997 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.2525 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 7 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1478 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.5997 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.2525 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.681321673860992 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.3518280624114944   |
| The average sampled Entropy is: 0.8166589789290609   |
| -OUT- Discrete Information Gain: 0.5351690834824335   |
| -info- AVERAGE sampled Information Gain: 0.5351690834824329 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.681321673860992 |
| This is the Emulated Entropy for density: 9.624958274513535 |
| Argmax mean: 2313 (no offset)|
| -x- Diff Information Gain: 0.05636339934745749     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.722276465519388 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.3518280624114944   |
| The average sampled Entropy is: 1.1621384644357118   |
| -OUT- Discrete Information Gain: 0.18968959797578266   |
| -info- AVERAGE sampled Information Gain: 0.18968959797578278 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.722276465519388 |
| This is the Emulated Entropy for elasticity: 6.636683220329012 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.0855932451903767     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.0855932451903767
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 173 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.1528 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.7399 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.1073 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 8 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.1528 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.7399 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.1073 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.681321673860992 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.081556032847827   |
| The average sampled Entropy is: 0.8016317684033267   |
| -OUT- Discrete Information Gain: 0.27992426444450036   |
| -info- AVERAGE sampled Information Gain: 0.2799242644445007 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.681321673860992 |
| This is the Emulated Entropy for density: 9.624958274513535 |
| Argmax mean: 2313 (no offset)|
| -x- Diff Information Gain: 0.05636339934745749     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.618734936030629 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.081556032847827   |
| The average sampled Entropy is: 0.908179647098587   |
| -OUT- Discrete Information Gain: 0.17337638574924008   |
| -info- AVERAGE sampled Information Gain: 0.1733763857492401 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.618734936030629 |
| This is the Emulated Entropy for elasticity: 6.542693917782143 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.0760410182484863     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.0760410182484863
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 173 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.1414 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.8177 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0409 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 9 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.1414 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.8177 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0409 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.681321673860992 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8252590020281397   |
| The average sampled Entropy is: 0.675400945982716   |
| -OUT- Discrete Information Gain: 0.14985805604542368   |
| -info- AVERAGE sampled Information Gain: 0.1498580560454235 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.681321673860992 |
| This is the Emulated Entropy for density: 9.624958274513535 |
| Argmax mean: 2313 (no offset)|
| -x- Diff Information Gain: 0.05636339934745749     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.524783335366958 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8252590020281397   |
| The average sampled Entropy is: 0.722378333836203   |
| -OUT- Discrete Information Gain: 0.10288066819193675   |
| -info- AVERAGE sampled Information Gain: 0.102880668191937 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.524783335366958 |
| This is the Emulated Entropy for elasticity: 6.455537766211712 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.06924556915524605     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.06924556915524605
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 177 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.1247 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.8605 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0148 | Cond. likelihood: 0.00217391
MODE 2 was chosen --> optimizing was done for differential information gain.
The argmax mean for density : 2313
The argmax mean for elasticity : 69

