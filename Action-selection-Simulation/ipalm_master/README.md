## SIMULATION action selection

In this repository you can find the core code for the action selection in Simulation Setting. The main functions are in the `entropy_sim.py` and utilitary functions can be found in `utils.py`.

The reference for measurements is stored as `reference.json` and another reference can be created with `create_sim_reference.py`.  

The logs from the Simulation experiments can be found in sim-logs.  
The Folders numbered 1,2,..,6 stand for number of context and inside each context are folders named Mode 1-3 and a measurement description `meas_description.txt`





