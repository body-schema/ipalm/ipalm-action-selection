/*
 * KINOVA (R) KORTEX (TM)
 *
 * Copyright (c) 2019 Kinova inc. All rights reserved.
 *
 * This software may be modified and distributed
 * under the terms of the BSD 3-Clause license.
 *
 * Refer to the LICENSE file for details.
 *
 */
#include <thread>

#include "ros/ros.h"
#include <kortex_driver/Base_ClearFaults.h>
#include <kortex_driver/PlayCartesianTrajectory.h>
#include <kortex_driver/CartesianSpeed.h>
#include <kortex_driver/BaseCyclic_Feedback.h>
#include <kortex_driver/ReadAction.h>
#include <kortex_driver/ExecuteAction.h>
#include <kortex_driver/PlayJointTrajectory.h>
#include <kortex_driver/SetCartesianReferenceFrame.h>
#include <kortex_driver/CartesianReferenceFrame.h>
#include <kortex_driver/SendGripperCommand.h>
#include <kortex_driver/GripperMode.h>
#include <ros/callback_queue.h>

#include <cstdlib>
#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <iostream>
#include <fstream>
#include <string> 
#define HOME_ACTION_IDENTIFIER 2
using namespace std;


bool is_gripper_present = true;
bool should_continue = true;

float actual_gripper_position;
float actual_gripper_current;
float actual_gripper_voltage;
float actual_gripper_temp;

float oldCur;
float newCur;

double currents[] = {0.0, 0.0, 0.0, 0.0, 0.0};
int mesCounter=0;
double curCounter=0;
double derivative=0.0;
double oldDerivative=0.0;
double newDerivative=0.0;
double secondDerivative=0.0;

double newPosition=0.0;
double oldPosition=0.0;
double closingSpeed=0.0;

double startTime;
double oldTime;
double newTime;
double t;
std::string robot_name = "my_gen3";


double firstDerivative(double currentData[5],double t)
{
    double firstDer(0.0);
    //double t(0.001);

    // no loop here as 5 points is just enough to calculate the derivative
    // of one elem (the middle one). If you do use a loop, it has to start
    // at 2 and end 2 elems before the end (so i - 2 and i + 2 are always
    // valid indexes for the array.)

    size_t i(2);

    firstDer = (   4.0 / 3.0 * (currentData[i + 1] - currentData[i - 1]) / (2.0 * t)
                 - 1.0 / 3.0 * (currentData[i + 2] - currentData[i - 2]) / (4.0 * t) );
    // Rather than use (double)4 you can just use 4.0. The .0 tells the
    // compiler you mean a double (if you need a float instead, append
    // an f, like 3.0f )

    return firstDer;
}
bool gripDetected(){
	bool ret=false;
	if(mesCounter>10){
		if(currents[4]>currents[3]){
			if(currents[3]>=currents[2]){
				if(currents[2]>=currents[1]){
					ret=true;
				}
			}
		}
	}
	return ret;
}
void feedbackCallback(const kortex_driver::BaseCyclic_Feedback::ConstPtr& feedback)
{
  
  
  /** Getting the stats we want   */ 
  actual_gripper_position = (1-(feedback->interconnect.oneof_tool_feedback.gripper_feedback[0].motor[0].position / 100.0))*85.0; 
  actual_gripper_current = feedback->interconnect.oneof_tool_feedback.gripper_feedback[0].motor[0].current_motor;
  actual_gripper_voltage = feedback->interconnect.oneof_tool_feedback.gripper_feedback[0].motor[0].voltage;
  actual_gripper_temp = feedback->interconnect.oneof_tool_feedback.gripper_feedback[0].motor[0].temperature_motor;
  
  /*
  if(actual_gripper_current<=0.015){
		actual_gripper_current=0.0;
  }*/
  
  
}

void clear_faults(ros::NodeHandle n, std::string robot_name)
{
  ros::ServiceClient service_client_clear_faults = n.serviceClient<kortex_driver::Base_ClearFaults>("/" + robot_name + "/base/clear_faults");
  kortex_driver::Base_ClearFaults service_clear_faults;

  // Clear the faults
  if (!service_client_clear_faults.call(service_clear_faults))
  {
    std::string error_string = "Failed to clear the faults";
    ROS_ERROR("%s", error_string.c_str());
    throw new std::runtime_error(error_string);
  }

  // Wait a bit
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
}


void send_gripper_command_position(ros::NodeHandle n, std::string robot_name, double value)
{
  // Initialize the ServiceClient
  ros::ServiceClient service_client_send_gripper_command = n.serviceClient<kortex_driver::SendGripperCommand>("/" + robot_name + "/base/send_gripper_command");
  kortex_driver::SendGripperCommand service_send_gripper_command;

  // Initialize the request
  kortex_driver::Finger finger;
  finger.finger_identifier = 0;
  finger.value = value;
  service_send_gripper_command.request.input.gripper.finger.push_back(finger);
  //service_send_gripper_command.request.input.duration=10;
  service_send_gripper_command.request.input.mode = kortex_driver::GripperMode::GRIPPER_POSITION;

  if (service_client_send_gripper_command.call(service_send_gripper_command))  
  {
    ROS_INFO("The gripper position command was sent to the robot.");
  }
  else
  {
    std::string error_string = "Failed to call SendGripperCommand position";
    ROS_ERROR("%s", error_string.c_str());
    throw new std::runtime_error(error_string);
  }
}

void send_gripper_command_speed(ros::NodeHandle n, std::string robot_name, double value, double wait)
{
  // Initialize the ServiceClient
  ros::ServiceClient service_client_send_gripper_command = n.serviceClient<kortex_driver::SendGripperCommand>("/" + robot_name + "/base/send_gripper_command");
  kortex_driver::SendGripperCommand service_send_gripper_command;

  // Initialize the request
  kortex_driver::Finger finger;
  finger.finger_identifier = 0;
  finger.value = value;
  service_send_gripper_command.request.input.gripper.finger.push_back(finger);
  //service_send_gripper_command.request.input.duration=2;
  service_send_gripper_command.request.input.mode = kortex_driver::GripperMode::GRIPPER_SPEED;
  ros::Duration(wait).sleep();
  if (service_client_send_gripper_command.call(service_send_gripper_command))  
  {
    ROS_INFO("The gripper speed command was sent to the robot.");
  }
  else
  {
    std::string error_string = "Failed to call SendGripperCommand speed";
    ROS_ERROR("%s", error_string.c_str());
    throw new std::runtime_error(error_string);
  }
}




void mySigintHandler(int sig)
{
	/** Do some custom action.
	* In my case stop the while loop
	*/
	should_continue = false;
}

int main(int argc, char **argv)
{
	
	ros::init(argc, argv, "kinova_gripping_node",ros::init_options::NoSigintHandler);

	/*******************************************************************************/
	// ROS Parameters
	ros::NodeHandle n;
	ros::CallbackQueue my_queue;
	n.setCallbackQueue(&my_queue);
	std::thread gripping;
	int degrees_of_freedom = 7;
	double gripper_speed=0.0;
	string object_name="";
	double threshold1=0.0;
	double threshold2=0.0;
	
	// Parameter robot_name
	if (!ros::param::get("~robot_name", robot_name))
	{
		std::string error_string = "Parameter robot_name was not specified, defaulting to " + robot_name + " as namespace";
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using robot_name " + robot_name + " as namespace";
		ROS_INFO("%s", error_string.c_str());
	}

	// Parameter degrees_of_freedom
	if (!ros::param::get("/" + robot_name + "/degrees_of_freedom", degrees_of_freedom))
	{
		std::string error_string = "Parameter /" + robot_name + "/degrees_of_freedom was not specified, defaulting to " + std::to_string(degrees_of_freedom) + " as degrees of freedom";
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using degrees_of_freedom " + std::to_string(degrees_of_freedom) + " as degrees_of_freedom";
		ROS_INFO("%s", error_string.c_str());
	}

	// Parameter is_gripper_present
	if (!ros::param::get("/" + robot_name + "/is_gripper_present", is_gripper_present))
	{
		std::string error_string = "Parameter /" + robot_name + "/is_gripper_present was not specified, defaulting to " + std::to_string(is_gripper_present);
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using is_gripper_present " + std::to_string(is_gripper_present);
		ROS_INFO("%s", error_string.c_str());
	}
	  
	// Parameter gripper_speed
	if (!ros::param::get("~gripper_speed", gripper_speed))
	{
		std::string error_string = "Parameter gripper_speed was not specified, defaulting to " + std::to_string(gripper_speed) + " as a gripper speed";
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using gripper_speed " + std::to_string(gripper_speed) + " as as a gripper speed";
		ROS_INFO("%s", error_string.c_str());
	}
	if (!ros::param::get("~object_name", object_name))
	{
		std::string error_string = "Parameter object_name was not specified, defaulting to " + object_name + " as an object name";
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using object_name " + object_name + " as an object name";
		ROS_INFO("%s", error_string.c_str());
	}
	if (!ros::param::get("~threshold", threshold1))
	{
		std::string error_string = "Parameter threshold was not specified, defaulting to " + std::to_string(threshold1) + " as an threshold";
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using threshold " + std::to_string(threshold1) + " as an threshold";
		ROS_INFO("%s", error_string.c_str());
	}
	/*if (!ros::param::get("~v_threshold", threshold2))
	{
		std::string error_string = "Parameter v_threshold was not specified, defaulting to " + std::to_string(threshold2) + " as an v_threshold";
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using v_threshold " + std::to_string(threshold2) + " as an v_threshold";
		ROS_INFO("%s", error_string.c_str());
	}*/
	
	/********************************************************************************/
	
	
	/*******************************************************************************
	* Make sure to clear the robot's faults else it won't move if it's already in fault
	* */
	clear_faults(n, robot_name);
	/*******************************************************************************/

	 
	
	
	ros::param::del("~gripper_speed");
	startTime = ros::Time::now().toSec();
  /** Subscribing to the topic	 */
	ros::Subscriber sub = n.subscribe("/my_gen3/base_feedback", 3, feedbackCallback);

	/** setting the sigint handler  */
	signal(SIGINT, mySigintHandler);
	
	
	if (is_gripper_present)	{	
		//send_gripper_command_speed(n, robot_name, gripper_speed);    
		//send_gripper_command_force(n, robot_name, gripper_speed);
		gripping=std::thread(send_gripper_command_speed, n, robot_name, gripper_speed,0.0);
	}
	ros::Rate r(100);
	while(should_continue){
		my_queue.callAvailable();
		
		if(actual_gripper_current>=(float)0.01){
	curCounter+=actual_gripper_current;
  }else{
	curCounter=0;
  }
  
  if(mesCounter<1){
		newCur=actual_gripper_current;
		newPosition=actual_gripper_position;
		newTime=ros::Time::now().toSec()-startTime;
  }else if(mesCounter==1){
	oldCur=newCur;
	newCur=actual_gripper_current;
	
	oldPosition=newPosition;
	newPosition=actual_gripper_position;
	
	
	oldTime=newTime;	
	newTime=ros::Time::now().toSec()-startTime;
	t=newTime-oldTime;
	
	derivative=(newCur-oldCur)/(t);
	closingSpeed=(oldPosition-newPosition)/(t);
	newDerivative=derivative;
	
  }else if(mesCounter>1){
	oldCur=newCur;
	newCur=actual_gripper_current;
	
	oldPosition=newPosition;
	newPosition=actual_gripper_position;
	
	oldTime=newTime;	
	newTime=ros::Time::now().toSec()-startTime;
	t=newTime-oldTime;
	
	closingSpeed=(oldPosition-newPosition)/(t);
	derivative=(newCur-oldCur)/(t);
	oldDerivative=newDerivative;
	newDerivative=derivative;
	secondDerivative=(newDerivative-oldDerivative)/(t);
  }
  mesCounter++;
  
  
  /*
  if(mesCounter<5){
	currents[mesCounter-1]=actual_gripper_current;
  }else if(mesCounter==5){
	//calculate derivative
	derivative=firstDerivative(currents,t);
  }else if(mesCounter>5){
	//shift
	for(int i=0;i<4;i++){
		currents[i]=currents[i+1];
	}
	currents[4]=actual_gripper_current;
	//calculate derivative
	derivative=firstDerivative(currents,t);
  }*/
  ROS_INFO("Position: %f Current: %f 1stD: %f v: %f Time: %f c: %f",actual_gripper_position,actual_gripper_current,derivative,closingSpeed,t,curCounter);
		
		
		/**here we can check for the position*/
		if(actual_gripper_current>=0.03||curCounter>=threshold1){
			send_gripper_command_speed(n, robot_name, 0.0,0.0);
			should_continue=false;
			ROS_INFO("The object width is %f", actual_gripper_position);
			
		}
		ros::spinOnce();
		r.sleep();
		/**maybe if the speed is 0.0 then stop*/
	}
	/** here save the log*/

	ros::param::del("~object_name");
	
	gripping.join();
	
	if (is_gripper_present)
	{
		if(gripper_speed!=0.0){
			send_gripper_command_speed(n, robot_name, 0.0,0.0);   
		} 
	}
	
	ROS_INFO("Shutting down");
	ros::shutdown();

	return 0;
}
