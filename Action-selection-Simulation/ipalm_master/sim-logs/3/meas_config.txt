
GRIPPER_WEIGH_POS = 0.7
GRIPPER_WEIGH_EFF = 0.7

GRIPPER_SQZ_POS = 0.8
GRIPPER_SQZ_EFF = 0.7

# uniform prior
orig_probs = utils.assign_equal_probs(ref, joker=True)

# good measurement
meas_sigma = 360 # for both
