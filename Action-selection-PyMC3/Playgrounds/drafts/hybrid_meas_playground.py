from __future__ import print_function, division
import numpy as np

from scipy.stats import norm
import matplotlib.pyplot as plt

meas_limit = 320

mat_means = [100, 150, 220, 305]
mat_sigmas = [30, 45, 25, 120]
names = ['A', 'B', 'C', 'D']

# fig, axs = plt.subplots(1)

x = np.linspace(0, 1500, 3000)


def uninformative_prior(ax, gamma, threshold):
    rng = ax[-1] - ax[0]
    bin_size = rng / len(ax)
    space = 1/bin_size

    new_ax = ax.copy()
    new_ax[0:int(threshold*space)] = 0
    temp = np.exp(gamma * new_ax[int(threshold*space):])
    new_ax[int(threshold*space):] = temp
    homo_pnt1 = np.array([int(threshold*space-50*space), 0, 1])
    homo_pnt2 = np.array([int(threshold*space), new_ax[int(threshold*space)], 1])
    a, b, c = np.cross(homo_pnt1, homo_pnt2)
    new_ax[int((threshold-50)*space):int(threshold*space)] = -a/b * ax[int((threshold-50)*space):int(threshold*space)] - c/b

    # print(new_ax[threshold-10:threshold+10])
    new_ax /= sum(new_ax)
    return new_ax


def uniform(ax, _threshold):
    rng = ax[-1] - ax[0]
    bin_size = rng / len(ax)
    space = 1 / bin_size
    threshold = int(_threshold * space)

    new_ax = ax.copy()
    new_ax[0:threshold] = 0
    temp = np.ones(len(new_ax[threshold:]))*1/(len(new_ax[threshold:]))
    new_ax[threshold:] = temp
    height = (ax[-1] - _threshold) / (len(new_ax[threshold:]) * space)
    homo_pnt1 = np.array([0, 0, 1])
    homo_pnt2 = np.array([_threshold, new_ax[threshold], 1])
    a, b, c = np.cross(homo_pnt1, homo_pnt2)
    new_ax[0:threshold] = -a / b * ax[0:threshold] - c / b

    new_ax /= sum(new_ax)
    return new_ax


y_mat = None

for idx in range(len(mat_means)):

    # construct partial pdfs
    y_mat_i = norm.pdf(x, mat_means[idx], mat_sigmas[idx])
    # plot partial pdfs
    plt.plot(x, y_mat_i, label=names[idx], lw=2, alpha=0.3)

    if y_mat is None:
        y_mat = y_mat_i * 1/(len(mat_means)+4)
    else:
        y_mat += y_mat_i * 1/(len(mat_means)+4)



plt.title("Property")
plt.xlabel(r'Prop [unit]')
plt.ylabel(r'p(x)')
plt.plot(x, y_mat, label='GMM', color="tab:blue", lw=2, ls="--")

plt.axvline(meas_limit, lw=2, ls="--", color="tab:red", label="Meas Maximum")
plt.legend()
plt.show()


gam = -0.02
print("gamma: ", gam)
uninf_prior = uninformative_prior(x, gam, meas_limit)
uniform_prior = uniform(x, meas_limit)


# y_uninf = y_mat + uninf_prior * 4/(len(mat_means)+4)
# y_uninf = y_uninf/sum(y_uninf)
#
# plt.title(r"$e^{-\lambda x}$ Threshold Assumption")
# plt.xlabel(r'Prop [unit]')
# plt.ylabel(r'p(x)')
# plt.plot(x, y_mat, label='GMM', color="#898989", lw=2)
# plt.plot(x, uninf_prior, label="Uninform. meas", color="black", lw=2, ls="--")
# plt.plot(x, y_uninf, label="Posterior", color="tab:red", lw=2)
# plt.axvline(meas_limit, lw=2, ls="-.", color="tab:red", label="Meas Maximum")
# plt.legend()
# plt.show()

y_uniform = y_mat + uniform_prior * 4/(len(mat_means)+4)
y_uniform = y_uniform/sum(y_uniform)

plt.title("Uniform Threshold Assumption")
plt.xlabel(r'Prop [unit]')
plt.ylabel(r'p(x)')
plt.plot(x, y_mat, label='GMM', color="#898989", lw=2)
plt.plot(x, uniform_prior, label="Uniform. meas", color="black", lw=2, ls="--")
plt.plot(x, y_uniform, label="Posterior", color="tab:red", lw=2)
plt.axvline(meas_limit, lw=2, ls="-.", color="tab:red", label="Meas Maximum")
plt.legend()
plt.show()

# inference ------------------------

# y_uninf = y_uninf * uninf_prior
# y_uninf = y_uninf/sum(y_uninf)
#
# plt.title(r"$e^{-\lambda x}$ Threshold Assumption")
# plt.xlabel(r'Prop [unit]')
# plt.ylabel(r'p(x)')
# plt.plot(x, y_mat, label='GMM', color="#898989", lw=2)
# plt.plot(x, uninf_prior, label="Uninform. meas", color="black", lw=2, ls="--")
# plt.plot(x, y_uninf, label="Posterior", color="tab:red", lw=2)
# plt.axvline(meas_limit, lw=2, ls="-.", color="tab:red", label="Meas Maximum")
# plt.legend()
# plt.show()

y_uniform = y_uniform * uniform_prior
y_uniform = y_uniform/sum(y_uniform)

plt.title("Uniform Threshold Assumption")
plt.xlabel(r'Prop [unit]')
plt.ylabel(r'p(x)')
plt.plot(x, y_mat, label='GMM', color="#898989", lw=2)
plt.plot(x, uniform_prior, label="Uniform. meas", color="black", lw=2, ls="--")
plt.plot(x, y_uniform, label="Posterior", color="tab:red", lw=2)
plt.axvline(meas_limit, lw=2, ls="-.", color="tab:red", label="Meas Maximum")
plt.legend()
plt.show()

y_uniform = y_uniform * uniform_prior
y_uniform = y_uniform/sum(y_uniform)

plt.title("Uniform Threshold Assumption")
plt.xlabel(r'Prop [unit]')
plt.ylabel(r'p(x)')
plt.plot(x, y_mat, label='GMM', color="#898989", lw=2)
plt.plot(x, uniform_prior, label="Uniform. meas", color="black", lw=2, ls="--")
plt.plot(x, y_uniform, label="Posterior", color="tab:red", lw=2)
plt.axvline(meas_limit, lw=2, ls="-.", color="tab:red", label="Meas Maximum")
plt.legend()
plt.show()

# y_uniform = y_uniform * uniform_prior
# y_uniform = y_uniform/sum(y_uniform)
#
# plt.title("Uniform Threshold Assumption")
# plt.xlabel(r'Prop [unit]')
# plt.ylabel(r'p(x)')
# plt.plot(x, y_mat, label='GMM', color="#898989", lw=2)
# plt.plot(x, uniform_prior, label="Uniform. meas", color="black", lw=2, ls="--")
# plt.plot(x, y_uniform, label="Posterior", color="tab:red", lw=2)
# plt.axvline(meas_limit, lw=2, ls="-.", color="tab:red", label="Meas Maximum")
# plt.legend()
# plt.show()
#
# y_uniform = y_uniform * uniform_prior
# y_uniform = y_uniform/sum(y_uniform)
#
# plt.title("Uniform Threshold Assumption")
# plt.xlabel(r'Prop [unit]')
# plt.ylabel(r'p(x)')
# plt.plot(x, y_mat, label='GMM', color="#898989", lw=2)
# plt.plot(x, uniform_prior, label="Uniform. meas", color="black", lw=2, ls="--")
# plt.plot(x, y_uniform, label="Posterior", color="tab:red", lw=2)
# plt.axvline(meas_limit, lw=2, ls="-.", color="tab:red", label="Meas Maximum")
# plt.legend()
# plt.show()
#
# y_uniform = y_uniform * uniform_prior
# y_uniform = y_uniform/sum(y_uniform)
#
# plt.title("Uniform Threshold Assumption")
# plt.xlabel(r'Prop [unit]')
# plt.ylabel(r'p(x)')
# plt.plot(x, y_mat, label='GMM', color="#898989", lw=2)
# plt.plot(x, uniform_prior, label="Uniform. meas", color="black", lw=2, ls="--")
# plt.plot(x, y_uniform, label="Posterior", color="tab:red", lw=2)
# plt.axvline(meas_limit, lw=2, ls="-.", color="tab:red", label="Meas Maximum")
# plt.legend()
# plt.show()
#
# y_uniform = y_uniform * uniform_prior
# y_uniform = y_uniform/sum(y_uniform)
#
# plt.title("Uniform Threshold Assumption")
# plt.xlabel(r'Prop [unit]')
# plt.ylabel(r'p(x)')
# plt.plot(x, y_mat, label='GMM', color="#898989", lw=2)
# plt.plot(x, uniform_prior, label="Uniform. meas", color="black", lw=2, ls="--")
# plt.plot(x, y_uniform, label="Posterior", color="tab:red", lw=2)
# plt.axvline(meas_limit, lw=2, ls="-.", color="tab:red", label="Meas Maximum")
# plt.legend()
# plt.show()
#
# y_uniform = y_uniform * uniform_prior
# y_uniform = y_uniform/sum(y_uniform)
#
# plt.title("Uniform Threshold Assumption")
# plt.xlabel(r'Prop [unit]')
# plt.ylabel(r'p(x)')
# plt.plot(x, y_mat, label='GMM', color="#898989", lw=2)
# plt.plot(x, uniform_prior, label="Uniform. meas", color="black", lw=2, ls="--")
# plt.plot(x, y_uniform, label="Posterior", color="tab:red", lw=2)
# plt.axvline(meas_limit, lw=2, ls="-.", color="tab:red", label="Meas Maximum")
# plt.legend()
# plt.show()

