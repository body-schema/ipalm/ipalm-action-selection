import json
import numpy as np

object_names = ['bakingtray', 'bottle', 'bowl', 'fork', 'glass', 'hammer', 'kettle', 'knife', 'mug', 'pan', 'plate', 'sodacan', 'spoon', 'wineglass']
prop_list = ['Material', 'Colour', 'Weight', 'Volume', 'Length', 'Width', 'Height', 'Functionality']  # shape?, not very informative
bool_questions = ['button', 'lid', 'fillable', 'movable', 'washable', 'dissasemblable', 'handle']
questions_full_name = ['Does it has a button?', 'Does it has a lid?', 'Could it be filled?', 'Could it be moved?', 'Could it be washed?', 'Could it be dismantled?', 'Does it has a handle?']

objects = []
for obj_name in object_names:
    obj = {}
    obj['name'] = obj_name
    properties = []
    for prop_name in prop_list:
        _prop = {}
        _prop['prop_name'] = prop_name
        _prop['bins'] = None
        _prop['ground'] = None
        _prop['prediction'] = None
        properties.append(_prop)
    obj['properties'] = properties

    questions = []
    for quest in bool_questions:
        _quest = {}
        _quest['quest_name'] = quest
        _quest['bins'] = None
        _quest['ground'] = None
        _quest['prediction'] = None
        questions.append(_quest)
    obj['questions'] = questions
    objects.append(obj)



with open('web-crawl-objects.json', 'w') as fp:
    json.dump(objects, fp, indent=4)

