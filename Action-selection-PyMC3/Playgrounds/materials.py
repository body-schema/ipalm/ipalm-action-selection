import json
import numpy as np

materials = [
    {
        "name": "metal",
        "prob": None,
        "properties": [
            {
                "prop_name": "density",
                "mean": 7900,
                "sigma": 250,
                "unit": "kg.m^{-3}"
            },
            {
                "prop_name": "elasticity",
                "mean": 2000,
                "sigma": 2000,
                "unit": "kPa"
            },
            {
                "prop_name": "volume",
                "mean": 70.37,
                "sigma": 92.65,
                "unit": "cm^3"
            }
        ]
    },
    {
        "name": "ceramic",
        "prob": None,
        "properties": [
            {
                "prop_name": "density",
                "mean": 2300,
                "sigma": 50,
                "unit": "kg.m^{-3}"
            },
            {
                "prop_name": "elasticity",
                "mean": 2000,
                "sigma": 2000,
                "unit": "kPa"
            },
            {
                "prop_name": "volume",
                "mean": 38.49,
                "sigma": 8.19,
                "unit": "cm^3"
            }
        ]
    },
    {
        "name": "soft-plastic",
        "prob": None,
        "properties": [
            {
                "prop_name": "density",
                "mean": 1300,
                "sigma": 250,
                "unit": "kg.m^{-3}"
            },
            {
                "prop_name": "elasticity",
                "mean": 300,
                "sigma": 100,
                "unit": "kPa"
            },
            {
                "prop_name": "volume",
                "mean": 85.85,
                "sigma": 13.93,
                "unit": "cm^3"
            }
        ]
    },
    {
        "name": "hard-plastic",
        "prob": None,
        "properties": [
            {
                "prop_name": "density",
                "mean": 1500,
                "sigma": 250,
                "unit": "kg.m^{-3}"
            },
            {
                "prop_name": "elasticity",
                "mean": 500,
                "sigma": 100,
                "unit": "kPa"
            },
            {
                "prop_name": "volume",
                "mean": 93.03,
                "sigma": 13.93,
                "unit": "cm^3"
            }
        ]
    },
    {
        "name": "wood",
        "prob": None,
        "properties": [
            {
                "prop_name": "density",
                "mean": 646,
                "sigma": 183.6,
                "unit": "kg.m^{-3}"
            },
            {
                "prop_name": "elasticity",
                "mean": 2000,
                "sigma": 2000,
                "unit": "kPa"
            },
            {
                "prop_name": "volume",
                "mean": 1093.82,
                "sigma": 660.763,
                "unit": "cm^3"
            }
        ]
    },
    {
        "name": "paper",
        "prob": None,
        "properties": [
            {
                "prop_name": "density",
                "mean": 756.84,
                "sigma": 223.76,
                "unit": "kg.m^{-3}"
            },
            {
                "prop_name": "elasticity",
                "mean": 2000,
                "sigma": 2000,
                "unit": "kPa"
            },
            {
                "prop_name": "volume",
                "mean": 92.88,
                "sigma": 86.99,
                "unit": "cm^3"
            }
        ]
    },
    {
        "name": "foam",
        "prob": None,
        "properties": [
            {
                "prop_name": "density",
                "mean": 91.95,
                "sigma": 127.65,
                "unit": "kg.m^{-3}"
            },
            {
                "prop_name": "elasticity",
                "mean": 4.1,
                "sigma": 1.7,
                "unit": "kPa"
            },
            {
                "prop_name": "volume",
                "mean": 548.83,
                "sigma": 150.06,
                "unit": "cm^3"
            }
        ]
    }
]


with open('materials.json', 'w') as fp:
    json.dump(materials, fp, indent=4)

