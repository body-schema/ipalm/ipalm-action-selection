# TODO proposals:
* Create a class for each grip so the members are visible by the IDE
* Create a method to classify objects based on
  * youngs modulus if object has been squished at least 40% -> get_global_modulus does not return None, args: option=1
  * if get_global_modulus returns None, use custom method to classify objects
    * calculate modulus from point of contact to maximum squished position - args: option 0
    * calculate the distance pressed
    * etc...
* Add parameter, e.g. *comb=True/False* to turn off list combing inside get_anchor_rank, get_linear_rank, init_clean_data,

## possible grip dictionary members

position - positions of gripper 0-1, open-closed
current - 0-1, min to max
From Pavel only so far
normal_pos - position normalized to the size of object - when loading txt or mat from Pavel's files
object_id - name of object - .mat/.txt
object_width - object width - .mat/.txt
closing_speed - used gripper closing speed - .mat/.txt

clean_cur - current cleaned using the get_combed_lists method
clean_pos - positions corresponding to clean_cur
filter_current - current filtered using the GrippingSessions.update_filter(), then .filter_current() method
proper - true if current reaches at least .current_tolerance*gripper_max_current
l_mod - last calculated modulus in .init_linear_class(); display using .plot_moduli()

# Examples
## 1) Loading files
`grip = GrippingSessions()  # instance to save the grips`

`grip.load_all(path_to_folder, extensions=(".mat", ".txt", ".npy"))`
Loads all files with extensions given in the *extensions* argument

`grip.load_mats(list_of_mat_files)`
`grip.load_txts(list_of_txt_files)`
`grip.load_npys(list_of_npy_files)`
Loads files and save in grip.grips list of grip dictionaries.
Modifies grip.files, grip.file_num, grip.mat_files, grip.txt_files, grip.npy_files.

`grip.mat2grip(mat_file)`
`grip.txt2grip(txt_file)`
`grip.npy2grip(npy_file)`
These return dictionary of single grip

## 2) Initialization
`grip = GrippingSessions()  # instance to save the grips`

`grip.init_clean_data(max_cur=1.00, slope=1e5, resolution=500, limit=0.05, window_size=1, filter=False,
                         show=False)  # good values`
filter = bool, use filter on data - only good for high closing speeds, e.g. >0.2, for closing speed 0.5 ~ possible filter up to 10
show = bool, show graph with all loaded data
`grip.init_linear_class(show=True, filter=False, option=1, local_limit=False)`
* Configurations:
  * option=0 and local_limit=True: scientifically correct way to do it, modulus at 40% deformation - should return `None` if it fails bad enough
  * option=1 and local_limit=True: take modulus as 40% *distance* from beginning of object to stopped gripper and set the limit as, e.g. 0.05\* *distance*

## 3) Ranking
`grip.get_linear_rank(position, current)` - see TODO proposals
Return rank relative to the other loaded files based on the linear estimate of the youngs modulus.

`grip.get_anchor_rank(position, current)`
Return rank relative to hardcoded 'anchor' values; fickle.

## 4) Displaying
### a) Repeatability
```
folders = [folder1,folder2]
grip.get_repeatability(folders, None)
```

`def get_repeatability(self, folders, reference_results=None, id_index=-2, extensions=(".npy",), show=False):`
folders = list of paths to folders
reference_results = dictionary where keys are integer ids of objects and values are the estimated stiffness categories 0-3 hard to soft
id_index = python-way index of id of object
* e.g. id here is 13, so id_index = -2 "ipalm_control/2020_09_04_0055/2020-Sep-04-08-23-13-0.npy"<br>
show = bool, show a simple graph with the results
Returns a list with the ranks for each object according to ID with the reference results at index 0.

### b) Debug & presentation
```
matt=grip.npy2grip(npy_file)
keep=True  # doesn't show the window yet, and block doesn't do anything
grip.display_raw(pos=matt["position"], cur=matt["current"], color=(1.0, 1.0, 0.0), s=20, keep=True)

# option=1, local_limit=False is technically incorrect. option=0, local_limit=True is the correct configuration.
grip.get_anchor_rank(pos=matt["position"], cur=matt["current"], option=1, local_limit=False)
# keep is False, so the window gets displayed
# block is False, so the execution of the program continues even after this line
grip.display_last_grip(keep=False, block=False)
```


## Useful Numpy tricks
arr[...,:] = create array from last dimension
arr[(condition1) & (condition2)] = same as in matlab; choose elements that satisfy condition

