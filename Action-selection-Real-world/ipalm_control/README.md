## REAL action selection

In this repository you can find the core code for the action selection in Real-World Setting. The main functions are in the `action-selection-master.py` and utilitary functions can be found in `utils.py` and `entropy_sim.py` in the `ipalm_master` folder.

The logs from the Real experiments can be found in real-logs.  
The Folders are named based on the type of prior provided and inside each context are folders named Mode 1-3 and a measurement description `measurement_config.txt`





