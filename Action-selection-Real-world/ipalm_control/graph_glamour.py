import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import seaborn as sns
import random
from scipy.interpolate import spline
import numpy as np
from plot_youngs_modulus import GrippingSessions

gradient_turq_colors = ["#2a4858", "#246e81", "#0c98a4", "#15c3bc", "#4feec8"]
kinova_gradient = ["#78c0d6", "#4faacd", "#1993c5", "#007bbb", "#0064b0", "#004ba2", "#003290", "#00147a"]
kinova_short = ["#78c0d6",  "#007bbb", "#0064b0", "#00147a"]
kinova_short = ["#00147a", "#0064b0", "#007bbb", "#78c0d6"]
# kinova_short = ["#78c0d6", "#006fb6", "#00147a"]


sns.set(font='sans-serif',
        rc={
 'axes.axisbelow': False,
 'axes.edgecolor': 'lightgrey',
 'axes.facecolor': 'None',
 'axes.grid': False,
 'axes.labelcolor': 'dimgrey',
 'axes.spines.right': False,
 'axes.spines.top': False,
 'figure.facecolor': 'white',
 'lines.solid_capstyle': 'round',
 'patch.edgecolor': 'w',
 'patch.force_edgecolor': True,
 'text.color': 'dimgrey',
 'xtick.bottom': False,
 'xtick.color': 'dimgrey',
 'xtick.direction': 'out',
 'xtick.top': False,
 'ytick.color': 'dimgrey',
 'ytick.direction': 'out',
 'ytick.left': False,
 'ytick.right': False})


# sns.despine(left=True, bottom=True)

sns.set_context("notebook", rc={"font.size":16,
                                "axes.titlesize":20,
                                "axes.labelsize":18})



# grip = GrippingSessions()
folders = ["2020_09_04_0055", "2020_09_04_02_10_again", "2020_09_04_strawberry_cube_reverse"]
# stiffnes_classifications = grip.display_repeatability(folders, None, show=False)
# index 0 is Matej's classification
# 20 objects
# print(stiffnes_classifications)
# exit()

for _ in range(1):
    data = [1, 5, 8, 8.6, 8.8, 10, 10.5, 10.4, 11, 10.2, 8.9, 8, 7, 6.5, 5.8, 5.2, 4,  3.21, 3.1, 2.8, 2.2, 1]
    data2 = []
    data3 = []
    data4 = []
    dats = [data2, data3, data4]

    for data_set in dats:
        for point in data:
            div = random.randrange(-5, 5)
            # div = div/10
            # print("this is div: ", div/10.0)
            data_set.append(point+(div/10.0))


    data = np.array(data)
    plt.gca().set_color_cycle(kinova_short)
    # plt.plot(data, 'o')
    # plt.plot(data2, 'ro')
    # plt.plot(data3, 'o')
    # plt.plot(data4, 'o')
    # histogram = [[0, 0, 0, 0] for x in range(len(stiffnes_classifications[1]))]
    # for i, subplot in enumerate(stiffnes_classifications[1:]):
    #     print("this is subplot: ", subplot)
    #     for j, n in enumerate(subplot):
    #         histogram[j][n-1] += 1
    #
    # print(histogram)
    # temps = [[], [], [], []]
    # for i, point_package in enumerate(histogram):
    #     for j, num in enumerate(point_package):
    #         temps[j].append(((num)*4)**2)
    #
    # print("this is temps: ", temps)

    # plt.plot(stiffnes_classifications[0], 'ro', markersize=18)



plt.rcParams['legend.loc'] = 'upper left'
# ax = plt.axes(projection='3d')

x1 = x2 = x3 = x4 = [x for x in range(20)]
y1 = [1 for _ in range(20)]
y2 = [2 for _ in range(20)]
y3 = [3 for _ in range(20)]
y4 = [4 for _ in range(20)]

# plt.scatter(x1, y1, s=1)


x = [1,2,3,4,5,6,7,8]
y = [4,1,3,6,1,3,5,2]
size = [100,500,100,500,100,500,100,500]

plt.scatter(x, y, s=size)


plt.hlines(y=1, xmin=0, xmax=20, linewidth=0.5, color='b', linestyles=(0, (5, 20)))
plt.hlines(y=2, xmin=0, xmax=20, linewidth=0.5, color='b', linestyles=(0, (5, 20)))
plt.hlines(y=3, xmin=0, xmax=20, linewidth=0.5, color='b', linestyles=(0, (5, 20)))
plt.hlines(y=4, xmin=3, xmax=20, linewidth=0.5, color='b', linestyles=(0, (5, 20)))

plt.xlabel("Object ID as in video")
plt.ylabel("Stiffness Categories")
plt.title("Repeatability Figure")
plt.legend(frameon=False)
plt.legend(["Hardest", "Hard", "Soft", "Softest"])

plt.show()




