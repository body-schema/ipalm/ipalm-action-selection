# Action Selection crossroad
## Andrej Kružliak - Bachelor Thesis

This repository serves as a crossroad for various action selection editions. The core of the code is basically the same in all three repositories, but with minor compatibility and utility tweaks tailored for each environment.  

`Action-selection-Virtual` folder has the bare code for action selection. Here the measurements are only artificially constructed.  
`Action-selection-Simulation` folder contains the code with tweaks to account for ROS and MuJoCo integration. It is not able to run on its own, because it needs a thorough and lengthy installation. The same applies for the real setup, which is not able to run independently for obvious reasons.
`Action-selection-Real-world` folder contains the code with tweaks to account for ROS and implementation of the Kinova control.

The `Simulation` and `Real-world` parts only serve for code overview.


# Testing Logs

In each Folder there is a `nameofthefolder-logs`, which contains the logs from the terminal and exported graphs of the final classification. If only one graph is provided, it means that all of the classifications ended in the same graph.
