import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import yaml
import json
import utility


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class BayesNet:
    def __init__(self, name):
        self.name = name
        self.actions = []
        self.nodes = []
        self.roots = []
        self.leaves = []
        self.action2node = {}
        self.yaml_dict = {}
        self.G = nx.DiGraph()  # directed graph from NetworkX library
        self.prereq_dict = {}

    def __str__(self):
        txt = "The BayesNet with name '{}' contains:\n".format(self.name)
        for i, node in enumerate(self.nodes):
            txt += "  {}. '{}' \n".format(i+1, node.name)

        txt += "The roots are: \n"
        for node in self.roots:
            txt += "  - '{}'\n".format(node.name)

        txt += "The leaves are: \n"
        for node in self.leaves:
            txt += "  - '{}'\n".format(node.name)

        return txt

    def load_from_yaml(self, path):
        with open(path, 'r') as stream:
            try:
                self.yaml_dict = yaml.safe_load(stream)
                nodes = self.yaml_dict['nodes']
                edges = self.yaml_dict['edges']  # [parent, child]
                self.actions = self.yaml_dict['actions']
                self.prereq_dict = self.yaml_dict['prerequisites']
                self.action2node = self.yaml_dict['action2node']
                for node_name in nodes:
                    self.add_node(node_name)
                    self.add_prerequisite(node_name)
                for edge in edges:
                    self.connect(edge[0], edge[1])

            except yaml.YAMLError as exc:
                print(exc)

    def add_node(self, name):
        new_node = self.Node(name)
        self.nodes.append(new_node)
        self.G.add_node(new_node)
        print("[BN] Added node: '{}'".format(name))

    def update(self):
        self.leaves = []
        self.roots = []
        for node in self.nodes:
            if len(node.parents) == 0:
                self.roots.append(node)
            elif len(node.children) == 0:
                self.leaves.append(node)
        print("[BN] BayesNet object updated.")

    def get_node(self, name):
        for node in self.nodes:
            if node.name == name:
                return node
        print("[BN-ERR] No Node with name '{}' found.")
        return None

    def connect(self, parent_name, child_name):
        print("[BN] Added parent and child edge.")
        self.add_parent(child_name, parent_name)
        self.add_child(parent_name, child_name)
        self.G.add_edge(self.get_node(parent_name), self.get_node(child_name))

    def add_prerequisite(self, node_name):
        connected_actions = self.action2node[node_name]
        node = self.get_node(node_name)
        connected_actions = np.array([connected_actions]).flatten()
        for action in connected_actions:
            if action is None:
                continue
            if self.prereq_dict[action] is None:
                node.actions[action] = True
            else:
                node.actions[action] = False
            print("[BN] Added Action: {} with prerequisite state: {} for Node: {} ".format(action, node.actions[action], node))

    def add_child(self, node_name, child_name):
        node = None
        child = None
        for node_temp in self.nodes:
            if node_temp.name == node_name:
                node = node_temp
            if node_temp.name == child_name:
                child = node_temp
            if node is not None and child is not None:
                break
        if node is None:
            print("[BN-ERR] No parent node with name '{}' found!".format(node_name))
            print("Create the parent node first!")
            return
        if child is None:
            print("[BN-ERR] No child node with name '{}' found!".format(node_name))
            print("Create the child node first!")
            return

        node.children.append(child)
        print("[BN] For node '{}':".format(node.name))
        print("  - Child n. {}, name: '{}' added.".format(len(node.children), child.name))
        self.update()

    def tree_from_node(self, node_name, check_prereq=False):
        node = self.get_node(node_name)
        if check_prereq:
            node_available = False
            if len(node.actions) != 0:
                for key in node.actions:
                    if node.actions[key]:
                        node_available = True
            else:
                node_available = True
            if node_available is False:
                return None
        tree = [node.name]
        for child in node.children:
            tree.append(self.tree_from_node(child.name, check_prereq))
        return tree

    def show_tree(self, tree, depth=-1, show_actions=False):
        depth += 1
        txt = ""
        if depth != 0:
            txt = depth * "   " + "|__"

        if tree is None:
            txt += bcolors.FAIL + "None\n" + bcolors.ENDC
            return txt

        for branch in tree:
            if type(branch) == str:
                txt += branch
                if show_actions:
                    actions = self.get_node(branch).actions
                    for act in actions:
                        if actions[act] == True:
                            txt += bcolors.OKGREEN + "-*-{}; ".format(act) + bcolors.ENDC
                        else:
                            txt += bcolors.FAIL + "-*-{}; ".format(act) + bcolors.ENDC
                txt += "\n"
                continue
            txt += self.show_tree(branch, depth, show_actions=show_actions)
        return txt

    def add_parent(self, node_name, parent_name):
        node = None
        parent = None
        for node_temp in self.nodes:
            if node_temp.name == node_name:
                node = node_temp
            if node_temp.name == parent_name:
                parent = node_temp
            if node is not None and parent is not None:
                break
        if node is None:
            print("[BN-ERR] No child node with name '{}' found!".format(node_name))
            print("Create the child node first!")
            return
        if parent is None:
            print("[BN-ERR] No parent node with name '{}' found!".format(node_name))
            print("Create the parent node first!")
            return

        node.parents.append(parent)
        print("[BN] For node '{}':".format(node.name))
        print("  - Parent n. {}, name: '{}' added.".format(len(node.parents), parent.name))
        self.update()

    def plot_graph(self):
        pos = nx.planar_layout(self.G)  # TODO custom node placement for BayesNet
        nx.draw(self.G, with_labels=True, pos=pos, node_size=500, label="BayesNet", font_weight="bold")
        plt.show()

    class Node:
        def __init__(self, name):
            self.name = name
            self.parents = []
            self.children = []
            self.actions = {}
            self.prereq = {}
            self.data = {}

        def __repr__(self):
            return self.info()

        def __str__(self):
            return self.name

        def info(self):
            txt = "".join('-' for i in range(len(self.name)+19))
            txt += "\n" + "    " + "Node name is '{0}'\n".format(self.name)
            to_add = "    " + "Children:\n"
            if len(self.children) == 0:
                to_add += "    " + "  This Node has no children.\n"
            else:
                for n, child in enumerate(self.children):
                    to_add += "    " + "  " + str(n + 1) + ". " + child.name + "\n"

            to_add += "    " + "Parents:\n"
            if len(self.parents) == 0:
                to_add += "    " + "  This Node has no parents.\n"
            else:
                for n, parent in enumerate(self.parents):
                    to_add += "    " + "  " + str(n + 1) + ". " + parent.name + "\n"
            txt += to_add
            txt += "    " + "Available actions:\n"
            for action in self.actions:
                txt += "    " + "- {}\n".format(action)
            txt += "    " + "Node Prerequisites:\n"
            for pr in self.actions:
                txt += "    " + "- {}: {}\n".format(pr, self.actions[pr])
            txt += "    " + "Data content: {}".format(self.data)

            return txt


def json2dict(file_name):
    with open(file_name) as fp:
        data = fp.read()
        a = json.loads(data)
        return a
# TODO take care of accidental duplicate instantiations of nodes with the same name and
#  duplicate edges with the same child and parent


BN = BayesNet("BN")  # global BN

file_path = 'graph-def-2022.yaml'
BN.load_from_yaml(file_path)
materials = json2dict("./materials.json")  # new iterable json
# for mat in materials:
#     print(mat)

# print(BN.show_tree(BN.tree_from_node('mat')))
# print(BN.show_tree(BN.tree_from_node('cat')))
# print(BN.show_tree(BN.tree_from_node('volume')))

# print(BN.tree_from_node('cat'))
# print("with prerequisites now:\n")
# restricted_tree = BN.tree_from_node('volume', check_prereq=True)

print(BN.show_tree(BN.tree_from_node('cat', check_prereq=True), show_actions=True))
print(BN.show_tree(BN.tree_from_node('mat', check_prereq=True), show_actions=True))
print(BN.show_tree(BN.tree_from_node('volume', check_prereq=True), show_actions=True))
print(BN.show_tree(BN.tree_from_node('density', check_prereq=True), show_actions=True))
print(BN.show_tree(BN.tree_from_node('mass', check_prereq=True), show_actions=True))
print(BN.show_tree(BN.tree_from_node('elasticity', check_prereq=True), show_actions=True))
print(BN.get_node('cat').info())
print(BN.get_node('mat').info())
# print(BN.get_node('mass').info())
# print(BN.get_node('volume').info())
# print(BN.get_node('density').info())
# print(BN.get_node('elasticity').info())

# print(BN.G.successors('cat'))
# print(BN)
# pos = nx.planar_layout(BN.G)  # TODO custom node placement for BayesNet
# nx.draw(BN.G, with_labels=True, pos=pos, node_size=500, label="BayesNet", font_weight="bold")
# plt.show()
