import numpy as np

MAX_OBJECT = 5

class ObjectParams:
    def __init__(self):
        self.id = 0
        self.current = 0
        self.position = 0

def open_file():
    '''
    Open .npy file saved in goes_to_obejct.py to create an array of classes
    :return: a list of ObjectParams
    '''

    dataset = []
    for object_id in range(MAX_OBJECT):
        try:
            with open('saved_datas/size_object_{}_numpy.npy'.format(object_id),'rb') as f:
                obj = ObjectParams()
                obj.id = object_id
                obj.current = np.load(f)
                obj.position = (100 - np.load(f))*85/100
                dataset.append(obj)
        except:
            pass
    return  dataset

def get_objects_size(dataset):
    '''
    Calulate size gripped of all objects in dataset using threshold
    :param dataset:
    :return: list of sizes in mm
    '''

    size = []
    for obj in dataset:
        opening_id = np.where(obj.current > 0.03)[0]
        size.append(obj.position[opening_id])

        # for i in range(len(obj.current)-2):
        #     mov_av = (obj.current[i] + obj.current[i+1] + obj.current[i+2])/3
        #     # print(mov_av)
        #     if mov_av > 0.03:
        #         size.append(obj.position[i])
        #         break


    return size

def get_object_young_modulus(dataset,size):
  #DOESN T WORK HAHA
    point40 = 0.50
    limits = 0.06
    gripper_area = 825/1e6
    E = []

    for i,obj in enumerate(dataset):
        cur1 = obj.current*235/gripper_area
        pos1 = obj.position/size[i]
        obj_idx_up  = np.where(obj.position > 35)
        obj_idx_low = np.where(obj.position < 45)
        obj_idx = np.intersect1d(obj_idx_low,obj_idx_up)

        print(obj_idx_up)
        print(obj_idx_low)
        print(pos1[obj_idx])
        print(cur1[obj_idx])
        E.append(np.polyfit(pos1[obj_idx], cur1[obj_idx],1))
    return E


if __name__ == '__main__':

    dataset = open_file()
    print(dataset[0].position)
    #size = get_objects_size(dataset)
    #E = get_object_young_modulus(dataset,size)
    print(size)
