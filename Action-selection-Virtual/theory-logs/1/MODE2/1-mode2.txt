Cleaning the previous 'meas.json' file.
----------------------
| ITERATION NUMBER: 0 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0490 | Cond. likelihood: None
Material:   ceramic | Probabilty: 0.3500 | Cond. likelihood: None
Material:   plastic | Probabilty: 0.1000 | Cond. likelihood: None
Material: aluminium | Probabilty: 0.5000 | Cond. likelihood: None
Material:     joker | Probabilty: 0.0010 | Cond. likelihood: None
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 10.414960097409153 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.585461851850611   |
| The average sampled Entropy is: 0.7549046794657309   |
| -OUT- Discrete Information Gain: 0.83055717238488   |
| -info- AVERAGE sampled Information Gain: 0.83055717238488 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 10.414960097409153 |
| This is the Emulated Entropy for density: 9.666231106196143 |
| Argmax mean: 2304 (no offset)|
| -x- Diff Information Gain: 0.7487289912130102     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.0613755325850995 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.585461851850611   |
| The average sampled Entropy is: 1.0236255044596136   |
| -OUT- Discrete Information Gain: 0.5618363473909973   |
| -info- AVERAGE sampled Information Gain: 0.561836347390997 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.0613755325850995 |
| This is the Emulated Entropy for elasticity: 6.689350232235342 |
| Argmax mean: 31 (no offset)|
| -x- Diff Information Gain: 0.37202530034975734     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.7487289912130102
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2713 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.2483 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0003 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.7485 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0029 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 1 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.2483 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0003 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.7485 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0029 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8398009814434119   |
| The average sampled Entropy is: 0.6325281107589658   |
| -OUT- Discrete Information Gain: 0.2072728706844461   |
| -info- AVERAGE sampled Information Gain: 0.20727287068444622 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.0613755325850995 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8398009814434119   |
| The average sampled Entropy is: 0.6628850153690273   |
| -OUT- Discrete Information Gain: 0.17691596607438453   |
| -info- AVERAGE sampled Information Gain: 0.17691596607438492 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.0613755325850995 |
| This is the Emulated Entropy for elasticity: 6.689350232235342 |
| Argmax mean: 31 (no offset)|
| -x- Diff Information Gain: 0.37202530034975734     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.37202530034975734
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 161 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.1452 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.8541 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0006 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 2 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.1452 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.8541 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0006 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.6059299113689526   |
| The average sampled Entropy is: 0.5274107976421301   |
| -OUT- Discrete Information Gain: 0.07851911372682252   |
| -info- AVERAGE sampled Information Gain: 0.0785191137268223 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.510872494366296 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.6059299113689526   |
| The average sampled Entropy is: 0.46685602272716387   |
| -OUT- Discrete Information Gain: 0.1390738886417887   |
| -info- AVERAGE sampled Information Gain: 0.13907388864178855 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.510872494366296 |
| This is the Emulated Entropy for elasticity: 6.354018980489982 |
| Argmax mean: 68 (no offset)|
| -x- Diff Information Gain: 0.15685351387631474     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.15685351387631474
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 163 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0801 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9198 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0001 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 3 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0801 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9198 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0001 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.40454551230479857   |
| The average sampled Entropy is: 0.3159292652921644   |
| -OUT- Discrete Information Gain: 0.08861624701263415   |
| -info- AVERAGE sampled Information Gain: 0.08861624701263424 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.234527955828286 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.40454551230479857   |
| The average sampled Entropy is: 0.36116561844361506   |
| -OUT- Discrete Information Gain: 0.043379893861183516   |
| -info- AVERAGE sampled Information Gain: 0.043379893861183516 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.234527955828286 |
| This is the Emulated Entropy for elasticity: 6.0840304940747965 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.15049746175348933     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.15049746175348933
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 167 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0427 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9573 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 4 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0427 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9573 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.25504399807629713   |
| The average sampled Entropy is: 0.2080724685061896   |
| -OUT- Discrete Information Gain: 0.04697152957010753   |
| -info- AVERAGE sampled Information Gain: 0.04697152957010757 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.008348865591114 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.25504399807629713   |
| The average sampled Entropy is: 0.21190430033312627   |
| -OUT- Discrete Information Gain: 0.04313969774317086   |
| -info- AVERAGE sampled Information Gain: 0.04313969774317084 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.008348865591114 |
| This is the Emulated Entropy for elasticity: 5.869669713129925 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.13867915246118923     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.13867915246118923
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 168 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0223 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9776 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 5 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0223 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9776 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.15452180924077602   |
| The average sampled Entropy is: 0.1449821730657328   |
| -OUT- Discrete Information Gain: 0.00953963617504322   |
| -info- AVERAGE sampled Information Gain: 0.009539636175043255 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.823059426557183 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.15452180924077602   |
| The average sampled Entropy is: 0.12055143748577381   |
| -OUT- Discrete Information Gain: 0.03397037175500221   |
| -info- AVERAGE sampled Information Gain: 0.033970371755002095 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.823059426557183 |
| This is the Emulated Entropy for elasticity: 5.70071596889448 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.12234345766270316     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.12234345766270316
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 165 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0116 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9884 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 6 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0116 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9884 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.09108026079436987   |
| The average sampled Entropy is: 0.0871097395321894   |
| -OUT- Discrete Information Gain: 0.003970521262180476   |
| -info- AVERAGE sampled Information Gain: 0.003970521262180482 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.6708856754043415 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.09108026079436987   |
| The average sampled Entropy is: 0.07348325044600398   |
| -OUT- Discrete Information Gain: 0.017597010348365888   |
| -info- AVERAGE sampled Information Gain: 0.017597010348365874 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.6708856754043415 |
| This is the Emulated Entropy for elasticity: 5.564490323650635 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.10639535175370618     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.10639535175370618
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 170 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0060 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9940 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 7 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0060 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9940 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.05264905601913278   |
| The average sampled Entropy is: 0.03799594187401668   |
| -OUT- Discrete Information Gain: 0.014653114145116101   |
| -info- AVERAGE sampled Information Gain: 0.014653114145116091 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.544322112365512 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.05264905601913278   |
| The average sampled Entropy is: 0.03843550888189135   |
| -OUT- Discrete Information Gain: 0.014213547137241427   |
| -info- AVERAGE sampled Information Gain: 0.01421354713724142 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.544322112365512 |
| This is the Emulated Entropy for elasticity: 5.451585354895648 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.0927367574698641     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.0927367574698641
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 174 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0031 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9969 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 8 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0031 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9969 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.611919765794047 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.029999940652370553   |
| The average sampled Entropy is: 0.022861464617976956   |
| -OUT- Discrete Information Gain: 0.007138476034393597   |
| -info- AVERAGE sampled Information Gain: 0.007138476034393593 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.611919765794047 |
| This is the Emulated Entropy for density: 9.524940264246732 |
| Argmax mean: 2693 (no offset)|
| -x- Diff Information Gain: 0.08697950154731515     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.437210575437024 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.029999940652370553   |
| The average sampled Entropy is: 0.02583356746751715   |
| -OUT- Discrete Information Gain: 0.004166373184853401   |
| -info- AVERAGE sampled Information Gain: 0.0041663731848534 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.437210575437024 |
| This is the Emulated Entropy for elasticity: 5.355637030287308 |
| Argmax mean: 70 (no offset)|
| -x- Diff Information Gain: 0.08157354514971527     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.08697950154731515
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2714 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0015 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.9985 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 9 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0015 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.9985 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.448122636516208 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.015809057938260814   |
| The average sampled Entropy is: 0.016919594705929325   |
| -OUT- Discrete Information Gain: -0.0011105367676685109   |
| -info- AVERAGE sampled Information Gain: -0.0011105367676685165 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.448122636516208 |
| This is the Emulated Entropy for density: 9.312169184784498 |
| Argmax mean: 2700 (no offset)|
| -x- Diff Information Gain: 0.13595345173171047     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.437210575437024 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.015809057938260814   |
| The average sampled Entropy is: 0.012441590475696991   |
| -OUT- Discrete Information Gain: 0.003367467462563823   |
| -info- AVERAGE sampled Information Gain: 0.003367467462563826 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.437210575437024 |
| This is the Emulated Entropy for elasticity: 5.355637030287308 |
| Argmax mean: 70 (no offset)|
| -x- Diff Information Gain: 0.08157354514971527     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.13595345173171047
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2692 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0007 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.9993 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
MODE 2 was chosen --> optimizing was done for differential information gain.
The argmax mean for density : 2700
The argmax mean for elasticity : 70

