from __future__ import print_function
import json
from master import ManipulatorActions
side = ManipulatorActions.CUBE_SIZE
weight_A = 0.2  #kg
weight_B = 0.8  #kg

sim_ref = {
    "materials": {
        "A":
        {
            "density":{
                "mean":int( weight_A /(side**3) ),
                "sigma":300,
                "unit":"kg.m^{-3}"
            },
            "elasticity":{
                "mean":5814,
                "sigma":350,
                "unit":"Pa"
            }
        },
        "B":
        {
            "density":{
                "mean":int( weight_B / (side**3) ),
                "sigma":360,
                "unit":"kg.m^{-3}"
            },
            "elasticity":{
                "mean":6522,
                "sigma":420,
                "unit":"Pa"
            }
        }
    }
}


with open('ipalm_master/reference.json', 'w') as fp:
    json.dump(sim_ref, fp, indent=4)
