import os
import json
import copy
import random


import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

import numpy as np 
from scipy.stats import norm  

from measurements import ObjectMeasurements
import utils

"""
These functions are edited for easier use (a change was made
to ground_dict layout, which disables the whole original script).
"""


def h_prop(ax, bin_size, ref, prior, property, vb = True, prnt = False, square = False):
        """
        Compute differential entropy for each property
        without the explicit relation to the material. Differential
        entropy computed from the Gaussian Mixture Model made from .

        [In] :  ax          > type:numpy.array, evenly spaced linspace
                bin_size    > type:float, width of a bin for the histogram approx
                ref         > type:dict, dictionary with all known materials
                prior       > type:np.array, the current prior belief
                property    > type:string, name of the property for keying the ref
                vb          > type:bool, short for 'verboity', toggles prints and plots
                prnt        > type:bool, toggles ONLYE prints
                square      > type:bool, toggles SQUARE plot creation
        [Out]:  diff_prop_entropy
                            > type:float, value of the differential entropy of a single property
        """
        prior = np.where(prior > 0, prior, 10**-10)
        diff_prop_entropy = -sum(bin_size * prior * np.log2(prior))

        if prnt and not vb:
            print("------------------PART A----------------------------")
            print("| This is the H(prop) for {}: {} |".format(property, diff_prop_entropy))
            print("----------------------------------------------------")
            print()

        if vb:
            print("------------------PART A----------------------------")
            print("| This is the H(prop) for {}: {} |".format(property, diff_prop_entropy))
            print("----------------------------------------------------")
            print()
            if square:
                plt.figure(figsize=(6,6))
            else:
                plt.figure(figsize=(6,3))
            
            materials = ref['materials'].items()
            units= materials[0][1][property]['unit']
            plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
            plt.xticks(fontsize=11)
            plt.yticks(fontsize=11)
            label = r"{a} [${b}$]".format(a=property.capitalize(), b=units)
            plt.xlabel(label)
            plt.ylabel(r"$p(x)$")
            plt.plot(ax, prior, label="Prior Belief: {}".format(property.capitalize()), color="tab:blue", lw=2)
            # if property == "density":
            #     plt.xticks([1350,2705,4000,7900], ["Plas.","Alum.","Cera.","Steel"])
            # else:
            #     plt.xticks([2,70,300,200], ["Plas.","Alum.","Cera.","Steel"])
            # plt.grid(ls="--", dashes=(2,5), axis="x")
            plt.ylim(bottom=0)
            plt.legend()
            # plt.savefig(fname="A-sqr.pdf", format="pdf")
            plt.show()
            
        return diff_prop_entropy

def h_discrete(ax, ref, probs, meas_sigma, property, vb=True, prnt=False, square = False):
    """
    Compute discrete entropy of the material with
    relation to a property (via sampling). (before named: h_mater_given_prop)

    [In] :  ax          > type:numpy.array, evenly spaced linspace
            ref > type:dict, dictionary with all known materials
            probs       > type:dict (nested), dictionary {'material': {"prob":prob_num, "cond":cond_num}, ...}
            meas_sigma  > type:float, measurement standard deviation for widening the normal distributions
            property    > type:string, name of the property for keying the ref
            vb          > type:bool, short for 'verboity', toggles prints and plots
    
    [Out]:  ent_before  > type:float, entropy of the material prior Probability Mass Function (PMF)
            avg_entropy > type:float, average expected entropy of the updated PMF
            difference  > type:float, difference between ent_before and avg_entropy, INFORMATION GAIN
    """

    # sampling from the gaussian mixture first
    ent_before = utils.compute_entropy(probs)
    y_cp = utils.posterior_meas_dist_sim(ax, ref, probs, meas_sigma, property)
    xs, ys = utils.sampler(ax, y_cp, 100)  # the 'ys' are dependent on a "scaling factor of linspace(a,b,c) c/b"

    #compute entropy for each emulated measurement sample
    entropies = []
    info_gains = []
    for i in range(len(xs)):
        meas_mean = xs[i]  # sample measurement  # is already offsetedddd
        temp_probs = utils.do_meas_sim(ax, probs, ref, property , meas_mean, meas_sigma)
        temp_probs = utils.update_belief_disc(temp_probs, vb=False)
        ent_now = utils.compute_entropy(temp_probs)
        entropies.append(ent_now)
        info_gains.append(ent_before - ent_now)

    avg_entropy = sum(entropies)/len(entropies)
    info_gain_difference = sum(info_gains)/len(info_gains)
    difference = ent_before-avg_entropy
    if prnt and not vb:
        print("---------------------PART B-----------------------")
        print("| Discrete Material entropy before: {}   |".format(ent_before))
        print("| The average sampled Entropy is: {}   |".format(avg_entropy))
        print("| -OUT- Discrete Information Gain: {}   |".format(difference))
        print("| -info- AVERAGE sampled Information Gain: {} |".format(info_gain_difference))
        print("--------------------------------------------------")
        print()

    if vb:    
        print("---------------------PART B-----------------------")
        print("| Discrete Material entropy before: {}   |".format(ent_before))
        print("| The average sampled Entropy is: {}   |".format(avg_entropy))
        print("| -OUT- Discrete Information Gain: {}   |".format(difference))
        print("| -info- AVERAGE sampled Information Gain: {} |".format(info_gain_difference))
        print("--------------------------------------------------")
        print()

        zeros = np.zeros(len(xs))
        max_y_cp = max(y_cp)
        for i in range(len(zeros)):
            zeros[i] = zeros[i]+0.000001*max_y_cp
        # lw = 2, nice_gray="$#464646"
        if square:
            plt.figure(figsize=(6,6))
        else:
            plt.figure(figsize=(6,3))
        materials = ref['materials'].items()
        units= materials[0][1][property]['unit']
        plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
        plt.xticks(fontsize=11)
        plt.yticks(fontsize=11)
        label = r"{a} [${b}$]".format(a=property.capitalize(), b=units)
        plt.xlabel(label)
        plt.ylabel(r"$p(x)$")
        plt.plot(ax,y_cp, lw=2,c="tab:blue", label="{}".format(property.capitalize()) + ' prior + ' + r'$\sigma_{\varepsilon}^2$')
        plt.scatter(xs,zeros, c="tab:red", label="Samples")
        # if property == "density":
        #     plt.xticks([1350,2705,4000,7900], ["Plas.","Alum.","Cera.","Steel"])
        # else:
        #     plt.xticks([2,70,300,200], ["Plas.","Alum.","Cera.","Steel"])

        # plt.grid(ls="--", dashes=(2,5), axis="x")
        plt.ylim(bottom=0)
        plt.legend()
        # plt.savefig(fname="B-sampling-sqr.pdf", format="pdf")
        plt.show()
    
    return ent_before, avg_entropy, difference

def h_contin(ax, bin_size, ref, y_prior, property, meas_sigma, offsets, vb=True, prnt=False, square = False):
    """
        Compute differential entropy for each property
        given expected property measurement.
        Compute also differential information gain.

        [In] :  ax          > type:numpy.array, evenly spaced linspace
                bin_size    > type:float, width of a bin for the histogram approx
                ref > type:dict, dictionary with all known materials
                y_prior     > type:np.array, the current prior belief
                property    > type:string, name of the property for keying the refp
                vb          > type:bool, short for 'verboity', toggles prints and plots
                prnt        > type:bool, toggles ONLYE prints
                square      > type:bool, toggles SQUARE plot creation
        [Out]:  argmax_mean > type:float, argument of the posterior distribution maximum
                prior_entropy > type:float, entropy of the prior distribution
                emul_entropy  > type:float, entropy of the posterior distribution
                difference  > type:float, the difference between prior_entropy and emul_entropy,
                              also refered to as information_gain
        """
    # TODO move the gaussians to the right for ELASTICITY

    # create gaussian for convolution that would add the measurement sigma into the distribution
    middle = int((len(ax)-1)/2)
    y_meas = norm.pdf(ax, middle, meas_sigma)

    # the emulation
    y_wide = np.convolve(y_prior, y_meas, mode='same')

    y_prior = np.where(y_prior > 0, y_prior, 10**-10)
    prior_entropy = -sum(bin_size * y_prior * np.log2(y_prior))

    # compute the posterior as an element-wise multiplication
    y_posterior = y_prior * y_wide
    y_posterior = y_posterior / sum(y_posterior)
    y_posterior = np.where(y_posterior > 0, y_posterior, 10**-10)
  
    emul_entropy = -sum(bin_size * y_posterior * np.log2(y_posterior))

    difference = prior_entropy - emul_entropy
    argmax_mean = np.argmax(y_posterior) 
    if offsets is not None:
        argmax_mean -= offsets[property]

    if prnt and not vb:
        print("------------------PART F--------------------")
        print("| This is the H(prop) for {}: {} |".format(property, prior_entropy))
        print("| This is the Emulated Entropy for {}: {} |".format(property, emul_entropy))
        print("| Argmax mean: {} (no offset)|".format(argmax_mean))
        print("| -x- Diff Information Gain: {}     |".format(difference))
        print("--------------------------------------------")
        print()

    if vb:
        print("------------------PART F--------------------")
        print("| This is the H(prop) for {}: {} |".format(property, prior_entropy))
        # print("| Number of measurements included: {}    |".format(cnt))
        print("| This is the Emulated Entropy for {}: {} |".format(property, emul_entropy))
        print("| Argmax mean: {} (no offset)|".format(argmax_mean))
        print("| -x- Diff Information Gain: {}     |".format(difference))
        print("--------------------------------------------")
        print()

        if square:
            plt.figure(figsize=(6,6))
        else:
            plt.figure(figsize=(6,3))
        materials = ref['materials'].items()
        units= materials[0][1][property]['unit']
        plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
        plt.xticks(fontsize=11)
        plt.yticks(fontsize=11)
        label = r"{a} [${b}$]".format(a=property.capitalize(), b=units)
        plt.xlabel(label)
        plt.ylabel(r"$p(x)$")
        plt.plot(ax, y_prior, color="tab:blue", label="prior", lw=2)
        plt.plot(ax, y_wide, color="tab:blue", label="widened", ls="--", lw=2)
        plt.plot(ax, y_posterior, color="tab:purple", label="posterior", lw=2)
        plt.legend()
        plt.title("PART F")
        # plt.savefig(fname="E-2000sigma-wide.pdf", format="pdf")
        plt.show()
        
    return argmax_mean, prior_entropy, emul_entropy, difference

if __name__=="__main__":

    # -------- SETUP --------

    """
    MODES can be 1, 2 and MODE 3
    MODE 1: judge by discrete information gain
    MODE 2: judge by differential information gain
    MODE 3: judge by sum of discrete and information gain
    - note - the MODE 3 is a hybrid and is experimental and 
    not mathematically correct. It only serves as proxy metric 
    """

    
    # ----- RETRIEVE THE REFERENCE DICTIONARY -----
    path = os.getcwd()
    complete_path = path + "/ipalm_master/" 
    ref = utils.json2dict("./reference.json")


    # ---- STORAGE OBJECT for MEASUREMENTS ----

    utils.check_or_create(complete_path + "meas.json")
    MEAS = ObjectMeasurements(complete_path + "./meas.json","1", "item")
    MEAS.create()


    MODE = 2
    RAND_CHOICE = False
    NICE_GRAY= "#464646"
    VERBOSITY = False
    PRNT = True
    SQUARE_FIG = False

    # ---- PRIOR SETUP ----
    orig_probs = utils.assign_equal_probs(ref, joker=True)
    # orig_probs["ceramic"]["prob"] = 0.35
    # orig_probs["aluminium"]["prob"] = 0.1
    # orig_probs["plastic"]["prob"] = 0.1
    # orig_probs["steel"]["prob"] = 0.449
    # orig_probs["joker"]["prob"] = 0.001

    orig_probs["ceramic"]["prob"] = 0.35
    orig_probs["aluminium"]["prob"] = 0.5
    orig_probs["plastic"]["prob"] = 0.1
    orig_probs["steel"]["prob"] = 0.049
    orig_probs["joker"]["prob"] = 0.001


    offsets = {
        "density":0,
        "elasticity":100
    }

    # ---- EXAMPLE MEASUREMENT ----
    ## MEAS FOR ALUMINIUM
    temp_meas_dict = {
        'density':{
            'mean':2705 + offsets["density"],
            'sigma':540
        },
        'elasticity':{
            'mean':70 + offsets["elasticity"],
            'sigma':60
        }
    }

    # ---- ADD OFFSETS IF NEEDED ----
    for mat in ref["materials"]:
        name=mat[0]
        props=mat[1]
        props["elasticity"]["mean"]+=offsets["elasticity"]
        props["density"]["mean"]+=offsets["density"]
    # add joker for the unknown materials
    ref['materials']["joker"] = "joker"

    # ---- PREPARING TOOLS for ACTION SELECTION ----
    argmax_mean_dict = {"density":0, "elasticity":0}
    probs = copy.deepcopy(orig_probs) 
    priors_0 = {}
    axes = {}
    materials = ref['materials']
    for property in materials[0][1]:
        max_mean = 0
        max_sigma = 0
        for mat in materials:
            name = mat[0]
            props = mat[1]
            if name == "joker":
                continue
            else:
                if props[property]["mean"] >= max_mean:
                    max_mean = props[property]["mean"]
                if props[property]["sigma"] >= max_sigma:
                    max_sigma = props[property]["sigma"]
        ax = np.linspace(0, max_mean+8*max_sigma, max_mean+8*max_sigma)   
        axes[property] = ax 
        priors_0[property] = utils.create_prior_sim(ref, probs, ax, property)

    distributions = {
        "density":[priors_0["density"]],
        "elasticity":[priors_0["elasticity"]]
    }


    # ---- MAIN ACTION SELECTION ----
    iterations = 10
    for i in range(iterations):
        # meas_bank = utils.json2dict("./meas.json")
        print("----------------------")
        print("| ITERATION NUMBER: {} |".format(i))
        print("| MODE NUMBER: {} |".format(MODE))
        print("----------------------")
        print()
        utils.print_PMF(probs)
        planned_action = None
        max_info_gain = 0

        for property in materials[0][1]:
            meas_mean = temp_meas_dict[property]['mean']
            meas_sigma = temp_meas_dict[property]['sigma']

            # ---- AXIS SETUP ----
            ax = axes[property]
            rng = ax[-1]-ax[0]
            bin_size = rng/len(ax)

            if VERBOSITY or PRNT:
                utils.what(property, meas_sigma)

            # choose the newest prior (the previous posterior, or if first iteration, it is the prior_0) ----
            prior = distributions[property][-1]  # [-1] to choose the latest added
            
            # ---- SECTION A -> H(property) ----
            diff_prop_entropy = h_prop(ax, bin_size, ref, prior, property, vb=VERBOSITY, prnt=PRNT)

            # ---- SECTION B -> H(material | property_hat ) ----
            mat_entropy, avg_expected_entropy, info_gain = h_discrete(ax, ref, probs, meas_sigma, property, vb=VERBOSITY, prnt=PRNT)

            # ---- SECTION F -> H(prop | prop_hat) ----
            argmax_mean_no_offset, diff_prop_entropy_duplicit, emulation_ent, diff_info_gain = h_contin(ax, bin_size, ref, prior, property, meas_sigma, offsets, vb=VERBOSITY, prnt=PRNT)
            argmax_mean_dict[property] = argmax_mean_no_offset 

            if MODE == 1:
                if info_gain > max_info_gain:
                    max_info_gain = info_gain
                    planned_action = property
            elif MODE == 2:
                if diff_info_gain > max_info_gain:
                    max_info_gain = diff_info_gain
                    planned_action = property
            elif MODE == 3:
                if (diff_info_gain + info_gain) > max_info_gain:
                    max_info_gain = (diff_info_gain + info_gain)
                    planned_action = property

            if RAND_CHOICE and planned_action is not None:
                planned_action = random.choice(['density', 'elasticity'])

        print("---------Planned action------------")
        print("Property chosen to be measured is :")
        print("    > ", planned_action) 
        print("With expected information gain : ")
        print("    > ", max_info_gain)
        print("-----------------------------------")
        print()

        if RAND_CHOICE:
            print()
            print("RANDOM-RANDOM-RANDOM-RANDOM-RANDOM")
            print("Action is planned RANDOMLY!")
            print("RANDOM-RANDOM-RANDOM-RANDOM-RANDOM")
            print()

        if planned_action is None:
            print("xXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxX")
            print("x  No action planned, expected information gain for  x")
            print("x  all properties was below zero.     x")
            print("xXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxXxX")
            print("The current best PMF guess is:")
            print()
            utils.print_PMF(probs)
            break
        
        else:
            # ---- CREATING SIMULATED "REAL" MEASUREMENTS ----
            if planned_action == "density":
                perturbed_meas_mean = temp_meas_dict[planned_action]['mean'] + np.random.randint(-20, 20)
            else:
                perturbed_meas_mean = temp_meas_dict[planned_action]['mean'] + np.random.randint(-10, 10)
                

            MEAS.add_meas(planned_action, {"mean":perturbed_meas_mean, "sigma":temp_meas_dict[planned_action]['sigma']})

        print("..........................................................")
        print("'Real' measurement mean: {} (contains offset={}) and sigma: {} for property: {}".format(perturbed_meas_mean, offsets[planned_action], temp_meas_dict[planned_action]['sigma'], planned_action))
        print("..........................................................")
        print()


        # ---- TAKING THE SIMULATED "REAL" MEASUREMENTS ----
        meas_mean=temp_meas_dict[planned_action]['mean'] 
        meas_probs = utils.do_meas_sim(ax, probs, ref, meas_type=planned_action, meas_mean=temp_meas_dict[planned_action]['mean'], meas_sigma=temp_meas_dict[planned_action]['sigma'])
        ax = axes[planned_action]
        rng = ax[-1]-ax[0]
        bin_size = rng/len(ax)


        # ---- UPDATE THE PMF (probabilities) ----
        probs = utils.update_belief_disc(meas_probs, vb=True)
        prior = distributions[planned_action][-1]
        # utils.PMF_plot(probs)
        

        # ---- UPDATE THE PDF (for the measured property) ---- 

        mean = temp_meas_dict[planned_action]['mean']
        sigma = temp_meas_dict[planned_action]['sigma']
        real_meas = norm.pdf(ax, mean, sigma)
        posterior = prior * real_meas  # numerical Bayes update
        posterior = posterior / sum(posterior)  # normalize
        distributions[planned_action].append(posterior)


    if MODE==1:
        print("MODE 1 was chosen --> optimizing was done for discrete information gain \n computed from the PMF.")
    elif MODE ==2:
        print("MODE 2 was chosen --> optimizing was done for differential information gain.")
        for prop in ref['materials']['steel']:
            print("The argmax mean for {} : {}".format(prop, argmax_mean_dict[prop]))
    elif MODE ==3:
        print("MODE 3 was chosen --> optimizing was done for hybrid sum of \n differential and discrete information gain.")
        for prop in ref['materials']['steel']:
            print("The argmax mean for {} : {}".format(prop, argmax_mean_dict[prop]))
    print()

    density_priors = distributions["density"]
    elasticity_priors = distributions["elasticity"]
    CTU_colors = ["#4f8bf0", "#447fe3", "#3670d1", "#2e67c7", "#245ab5", "#1f53ab", "#19499c", "#123e8a", "#0c3478", "#061e61"]

    utils.PMF_plot(probs)
    # # ---- SETUP ALL DISTS PLOT FOR DENSITY ---
    # N = len(density_priors)
    # ax = axes["density"]
    # plt.figure(figsize=(6,3))
    # for i,distrib in enumerate(density_priors):
    #     alp=0.3 + i*(0.7/N)
    #     # plt.xlim(xmin=500,xmax=5000)
    #     plt.plot(ax, distrib, lw=3 - 1*(i+1)/N, color=CTU_colors[i], alpha=alp)
    #     units= materials[0][1][property]['unit']
    #     plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    #     plt.xticks(fontsize=11)
    #     plt.yticks(fontsize=11)
    #     label = r"{a} [${b}$]".format(a="Chronology of density priors", b=units)
    #     plt.xlabel(label)
    #     plt.ylabel(r"$p(x)$")
    # plt.show()


    # # ---- SETUP ALL DISTS PLOT FOR ELASTICITY ---
    # N = len(elasticity_priors)
    # ax = axes["elasticity"]
    # plt.figure(figsize=(6,3))
    # for i,distrib in enumerate(elasticity_priors):
    #     alp=0.3 + i*(0.7/N)
    #     plt.plot(ax, distrib, lw=3 - 1*(i+1)/N, color=CTU_colors[i], alpha=alp)
    #     units= materials[0][1][property]['unit']
    #     plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    #     plt.xticks(fontsize=11)
    #     plt.yticks(fontsize=11)
    #     # plt.xlim(xmin=30, xmax=250)
    #     label = r"{a} [${b}$]".format(a="Chronology of elasticity priors", b=units)
    #     plt.xlabel(label)
    #     plt.ylabel(r"$p(x)$")
    # plt.show()