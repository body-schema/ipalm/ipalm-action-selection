args Namespace(grasp='off', grid=0.055, mes='1', stiff='off', stiffrep='off', vis='off', wght=None)
[94m[INFO] Starting program in MEASURE mode with 1.0 as the max current[0m
[94m[INFO] Weight estimation mode ON.[0m
[94m[INFO] Grid Measuring is ON, with CLOSING_SPEED at 0.055.[0m
Starting to clear
Waiting for service
Clearing faults
[INFO] [1621541974.146938]: Activating the action notifications...
[INFO] [1621541974.148883]: Successfully activated the Action Notifications!
/home/robot3/vision_ws/src/ipalm_control
Cleaning the previous '/home/robot3/vision_ws/src/ipalm_control/meas.json' file.
-----------------Current PMF-----------------
Material:       Kwhite | Probabilty: 0.2500 | Cond. likelihood: None
Material:        joker | Probabilty: 0.2500 | Cond. likelihood: None
Material: SpongeYellow | Probabilty: 0.2500 | Cond. likelihood: None
Material:       Kblack | Probabilty: 0.2500 | Cond. likelihood: None
---------------------------------------------
[INFO] [1621541974.231038]: Sending the gripper position...
[INFO] [1621541974.734696]: 0.509999990463
----------------------
| ITERATION NUMBER: 0 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:       Kwhite | Probabilty: 0.2500 | Cond. likelihood: None
Material:        joker | Probabilty: 0.2500 | Cond. likelihood: None
Material: SpongeYellow | Probabilty: 0.2500 | Cond. likelihood: None
Material:       Kblack | Probabilty: 0.2500 | Cond. likelihood: None
---------------------------------------------
_____________________
('Property: ', u'Elasticity')
Measurement:
('    Meas. sigma:', 4091)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 2.0   |
| The average sampled Entropy is: 1.3201206901   |
| -OUT- Discrete Information Gain: 0.679879309898   |
| -info- AVERAGE sampled Information Gain: 0.679879309898 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for elasticity: 16.1393706678 |
| This is the Emulated Entropy for elasticity: 15.0711244424 |
| Argmax mean: 10159 (no offset)|
| -x- Diff Information Gain: 1.06824622534     |
--------------------------------------------
()
_____________________
('Property: ', u'Density')
Measurement:
('    Meas. sigma:', 29.64)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 2.0   |
| The average sampled Entropy is: 0.553840158515   |
| -OUT- Discrete Information Gain: 1.44615984149   |
| -info- AVERAGE sampled Information Gain: 1.44615984149 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for density: 9.22313511481 |
| This is the Emulated Entropy for density: 8.51304684736 |
| Argmax mean: 25 (no offset)|
| -x- Diff Information Gain: 0.710088267443     |
--------------------------------------------
()
---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  1.0682462253351979
-----------------------------------

[94m[AS-INFO] Starting exploratory action.[0m
[INFO] [1621541977.936559]: Sending pose...
[INFO] [1621541977.955260]: Waiting for pose to finish...
[INFO] [1621541980.111375]: Sending pose...
[INFO] [1621541980.128623]: Waiting for pose to finish...
Current gripper current:  0.00999999977648
State of closing_threshold:  False
Current gripper current:  0.00999999977648
State of closing_threshold:  False
Current gripper current:  0.019999999553
State of closing_threshold:  False
Current gripper current:  0.0
State of closing_threshold:  False
Current gripper current:  0.0599999986589
State of closing_threshold:  True
Current gripper current:  0.189999997616
State of closing_threshold:  True
Current gripper current:  0.300000011921
State of closing_threshold:  True
Current gripper current:  0.419999986887
State of closing_threshold:  True
[INFO] [1621541983.531349]: Sending the gripper position...
[INFO] [1621541984.556106]: Sending pose...
[INFO] [1621541984.581483]: Waiting for pose to finish...
[94mSaving data![0m
[94m[INFO] saving dataobject_id: 1.[0m
/home/robot3/vision_ws/src/ipalm_control/saved_data/andrej/2021_May_20_20_19_ObjectNum_1.txt
[4]
Value of Modulus is [15.85451305]kPa, the sigma is 4.091kPa
[INFO] [1621541986.155013]: Sending pose...
[INFO] [1621541986.178697]: Waiting for pose to finish...
[INFO] [1621541986.731064]: Sending pose...
[INFO] [1621541986.754898]: Waiting for pose to finish...
Current gripper current:  0.00999999977648
State of closing_threshold:  False
Current gripper current:  0.00999999977648
State of closing_threshold:  False
Current gripper current:  0.019999999553
State of closing_threshold:  False
Current gripper current:  0.00999999977648
State of closing_threshold:  False
Current gripper current:  0.0500000007451
State of closing_threshold:  False
Current gripper current:  0.180000007153
State of closing_threshold:  True
Current gripper current:  0.300000011921
State of closing_threshold:  True
Current gripper current:  0.409999996424
State of closing_threshold:  True
Current gripper current:  0.0900000035763
State of closing_threshold:  True
Current gripper current:  0.0500000007451
State of closing_threshold:  True
Current gripper current:  0.990000009537
State of closing_threshold:  True
Current gripper current:  0.0399999991059
State of closing_threshold:  True
Current gripper current:  0.019999999553
State of closing_threshold:  True
Current gripper current:  1.0
State of closing_threshold:  True
[INFO] [1621541991.603826]: Sending the gripper position...
[INFO] [1621541992.631937]: Sending pose...
[INFO] [1621541992.655818]: Waiting for pose to finish...
[94mSaving data![0m
[94m[INFO] saving dataobject_id: 1.[0m
/home/robot3/vision_ws/src/ipalm_control/saved_data/andrej/2021_May_20_20_19_ObjectNum_1.txt
[4]
Value of Modulus is [5.31461982]kPa, the sigma is 4.091kPa
[INFO] [1621541994.255516]: Sending pose...
[INFO] [1621541994.279453]: Waiting for pose to finish...
[INFO] [1621541994.832427]: Sending pose...
[INFO] [1621541994.853958]: Waiting for pose to finish...
Current gripper current:  0.019999999553
State of closing_threshold:  False
Current gripper current:  0.0
State of closing_threshold:  False
Current gripper current:  0.00999999977648
State of closing_threshold:  False
Current gripper current:  0.00999999977648
State of closing_threshold:  False
Current gripper current:  0.019999999553
State of closing_threshold:  False
Current gripper current:  0.129999995232
State of closing_threshold:  True
Current gripper current:  0.280000001192
State of closing_threshold:  True
Current gripper current:  0.019999999553
State of closing_threshold:  True
Current gripper current:  0.0900000035763
State of closing_threshold:  True
[INFO] [1621541998.403784]: Sending the gripper position...
[INFO] [1621541999.433255]: Sending pose...
[INFO] [1621541999.456762]: Waiting for pose to finish...
[94mSaving data![0m
[94m[INFO] saving dataobject_id: 1.[0m
/home/robot3/vision_ws/src/ipalm_control/saved_data/andrej/2021_May_20_20_20_ObjectNum_1.txt
[4]
Value of Modulus is [10.88035123]kPa, the sigma is 4.091kPa
[92mThese are the Young's moduli for the squeezed object:[0m
[array([15.85451305]), array([5.31461982]), array([10.88035123])]
The average modulus is: [10.68316136] 

Added elasticity to the meas. database.
Added elasticity to the meas. database.
()
CLOSED-FORM solution via value ----------------------------------------
Material:       Kwhite | Probabilty: 0.0068 | Cond. likelihood: 0.00000700
Material:        joker | Probabilty: 0.9767 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.0001 | Cond. likelihood: 0.00000012
Material:       Kblack | Probabilty: 0.0164 | Cond. likelihood: 0.00001676
----------------------
| ITERATION NUMBER: 1 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:       Kwhite | Probabilty: 0.0068 | Cond. likelihood: 0.00000700
Material:        joker | Probabilty: 0.9767 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.0001 | Cond. likelihood: 0.00000012
Material:       Kblack | Probabilty: 0.0164 | Cond. likelihood: 0.00001676
---------------------------------------------
_____________________
('Property: ', u'Elasticity')
Measurement:
('    Meas. sigma:', 4091)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 0.18106544403   |
| The average sampled Entropy is: 0.142075995794   |
| -OUT- Discrete Information Gain: 0.0389894482362   |
| -info- AVERAGE sampled Information Gain: 0.0389894482362 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for elasticity: 13.908930076 |
| This is the Emulated Entropy for elasticity: 13.6381242727 |
| Argmax mean: 10551 (no offset)|
| -x- Diff Information Gain: 0.270805803297     |
--------------------------------------------
()
_____________________
('Property: ', u'Density')
Measurement:
('    Meas. sigma:', 29.64)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 0.18106544403   |
| The average sampled Entropy is: 0.117801137828   |
| -OUT- Discrete Information Gain: 0.0632643062018   |
| -info- AVERAGE sampled Information Gain: 0.0632643062018 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for density: 9.22313511481 |
| This is the Emulated Entropy for density: 8.51304684736 |
| Argmax mean: 25 (no offset)|
| -x- Diff Information Gain: 0.710088267443     |
--------------------------------------------
()
---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.7100882674434477
-----------------------------------

[94m[AS-INFO] Starting exploratory action.[0m
[94m[INFO] Zero weight calibration in progress.[0m
[INFO] [1621542005.778762]: Sending the gripper position...
Zero payload torque: 
Reading number 0 : -8.92935848236
Reading number 10 : -8.92832946777
Reading number 20 : -8.9360408783
Reading number 30 : -8.92327308655
Reading number 40 : -8.94240474701
[94mTorque is: -8.93350549698[0m
[92mGripper zero weight is 2.89740043226[0m
[INFO] [1621542017.953605]: Sending the gripper position...
[92m[INFO] Weight calibration successful.[0m
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621542019.004820]: Sending pose...
[INFO] [1621542019.034956]: Waiting for pose to finish...
[INFO] [1621542021.154229]: Sending pose...
[INFO] [1621542021.183927]: Waiting for pose to finish...
[INFO] [1621542022.229341]: Sending the gripper position...
[INFO] [1621542023.254756]: Sending pose...
[INFO] [1621542023.278405]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.19627380371
Reading number 10 : -9.1655216217
Reading number 20 : -9.17749595642
Reading number 30 : -9.15966701508
Reading number 40 : -9.19185352325
[94mTorque is: -9.17826984406[0m
[92mSo the estimated weight is 0.0793843273797[0m
[INFO] [1621542035.180823]: Sending pose...
[INFO] [1621542035.203320]: Waiting for pose to finish...
[INFO] [1621542037.354131]: Sending pose...
[INFO] [1621542037.378516]: Waiting for pose to finish...
[INFO] [1621542038.679808]: Sending the gripper position...
[INFO] [1621542039.714149]: Sending pose...
[INFO] [1621542039.728532]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621542041.279821]: Sending pose...
[INFO] [1621542041.311007]: Waiting for pose to finish...
[INFO] [1621542041.859915]: Sending pose...
[INFO] [1621542041.881521]: Waiting for pose to finish...
[INFO] [1621542042.928346]: Sending the gripper position...
[INFO] [1621542043.955321]: Sending pose...
[INFO] [1621542043.978926]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.19054794312
Reading number 10 : -9.19297599792
Reading number 20 : -9.22648429871
Reading number 30 : -9.19281196594
Reading number 40 : -9.18657302856
[94mTorque is: -9.18993289948[0m
[92mSo the estimated weight is 0.083167001698[0m
[INFO] [1621542055.864287]: Sending pose...
[INFO] [1621542055.878918]: Waiting for pose to finish...
[INFO] [1621542058.005571]: Sending pose...
[INFO] [1621542058.030007]: Waiting for pose to finish...
[INFO] [1621542059.328719]: Sending the gripper position...
[INFO] [1621542060.354438]: Sending pose...
[INFO] [1621542060.379373]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621542061.928934]: Sending pose...
[INFO] [1621542061.956765]: Waiting for pose to finish...
[INFO] [1621542062.505398]: Sending pose...
[INFO] [1621542062.530192]: Waiting for pose to finish...
[INFO] [1621542063.578620]: Sending the gripper position...
[INFO] [1621542064.606160]: Sending pose...
[INFO] [1621542064.631064]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.18125915527
Reading number 10 : -9.2111082077
Reading number 20 : -9.18552017212
Reading number 30 : -9.1834564209
Reading number 40 : -9.15099811554
[94mTorque is: -9.18284984589[0m
[92mSo the estimated weight is 0.0808697576277[0m
[INFO] [1621542078.129605]: Sending pose...
[INFO] [1621542078.154001]: Waiting for pose to finish...
[INFO] [1621542080.306711]: Sending pose...
[INFO] [1621542080.329711]: Waiting for pose to finish...
[INFO] [1621542081.654968]: Sending the gripper position...
[INFO] [1621542082.679666]: Sending pose...
[INFO] [1621542082.704685]: Waiting for pose to finish...
Added density to the meas. database.
Added density to the meas. database.
()
CLOSED-FORM solution via value ----------------------------------------
Material:       Kwhite | Probabilty: 0.0000 | Cond. likelihood: 0.00000003
Material:        joker | Probabilty: 0.8644 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:       Kblack | Probabilty: 0.1356 | Cond. likelihood: 0.00935945
----------------------
| ITERATION NUMBER: 2 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:       Kwhite | Probabilty: 0.0000 | Cond. likelihood: 0.00000003
Material:        joker | Probabilty: 0.8644 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:       Kblack | Probabilty: 0.1356 | Cond. likelihood: 0.00935945
---------------------------------------------
_____________________
('Property: ', u'Elasticity')
Measurement:
('    Meas. sigma:', 4091)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 0.572637709111   |
| The average sampled Entropy is: 0.422061082464   |
| -OUT- Discrete Information Gain: 0.150576626647   |
| -info- AVERAGE sampled Information Gain: 0.150576626647 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for elasticity: 13.908930076 |
| This is the Emulated Entropy for elasticity: 13.6381242727 |
| Argmax mean: 10551 (no offset)|
| -x- Diff Information Gain: 0.270805803297     |
--------------------------------------------
()
_____________________
('Property: ', u'Density')
Measurement:
('    Meas. sigma:', 29.64)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 0.572637709111   |
| The average sampled Entropy is: 0.273383965016   |
| -OUT- Discrete Information Gain: 0.299253744095   |
| -info- AVERAGE sampled Information Gain: 0.299253744095 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for density: 6.92598461767 |
| This is the Emulated Entropy for density: 6.5748804505 |
| Argmax mean: 283 (no offset)|
| -x- Diff Information Gain: 0.351104167164     |
--------------------------------------------
()
---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.3511041671638484
-----------------------------------

[94m[AS-INFO] Starting exploratory action.[0m
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621542087.254981]: Sending pose...
[INFO] [1621542087.323952]: Waiting for pose to finish...
[INFO] [1621542087.930675]: Sending pose...
[INFO] [1621542087.953299]: Waiting for pose to finish...
[INFO] [1621542089.003858]: Sending the gripper position...
[INFO] [1621542090.030978]: Sending pose...
[INFO] [1621542090.053332]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.16393089294
Reading number 10 : -9.19170570374
Reading number 20 : -9.15724372864
Reading number 30 : -9.19222927094
Reading number 40 : -9.17281246185
[94mTorque is: -9.17578697205[0m
[92mSo the estimated weight is 0.0785790584475[0m
[INFO] [1621542102.130741]: Sending pose...
[INFO] [1621542102.161724]: Waiting for pose to finish...
[INFO] [1621542104.280909]: Sending pose...
[INFO] [1621542104.311093]: Waiting for pose to finish...
[INFO] [1621542105.631385]: Sending the gripper position...
[INFO] [1621542106.654047]: Sending pose...
[INFO] [1621542106.680439]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621542108.229781]: Sending pose...
[INFO] [1621542108.254855]: Waiting for pose to finish...
[INFO] [1621542108.806799]: Sending pose...
[INFO] [1621542108.829252]: Waiting for pose to finish...
[INFO] [1621542109.878347]: Sending the gripper position...
[INFO] [1621542110.904165]: Sending pose...
[INFO] [1621542110.928593]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.16783332825
Reading number 10 : -9.21670627594
Reading number 20 : -9.19070529938
Reading number 30 : -9.16631984711
Reading number 40 : -9.17238140106
[94mTorque is: -9.17578990936[0m
[92mSo the estimated weight is 0.0785800111064[0m
[INFO] [1621542122.833553]: Sending pose...
[INFO] [1621542122.853319]: Waiting for pose to finish...
[INFO] [1621542125.007149]: Sending pose...
[INFO] [1621542125.028256]: Waiting for pose to finish...
[INFO] [1621542126.353382]: Sending the gripper position...
[INFO] [1621542127.379181]: Sending pose...
[INFO] [1621542127.403516]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621542128.954157]: Sending pose...
[INFO] [1621542128.978334]: Waiting for pose to finish...
[INFO] [1621542129.532578]: Sending pose...
[INFO] [1621542129.553467]: Waiting for pose to finish...
[INFO] [1621542130.603399]: Sending the gripper position...
[INFO] [1621542131.628963]: Sending pose...
[INFO] [1621542131.653432]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.14378929138
Reading number 10 : -9.19569587708
Reading number 20 : -9.19456195831
Reading number 30 : -9.17344665527
Reading number 40 : -9.14798736572
[94mTorque is: -9.17698003769[0m
[92mSo the estimated weight is 0.0789660049728[0m
[INFO] [1621542143.929510]: Sending pose...
[INFO] [1621542143.953671]: Waiting for pose to finish...
[INFO] [1621542146.079467]: Sending pose...
[INFO] [1621542146.104687]: Waiting for pose to finish...
[INFO] [1621542147.428643]: Sending the gripper position...
[INFO] [1621542148.460927]: Sending pose...
[INFO] [1621542148.479867]: Waiting for pose to finish...
Added density to the meas. database.
Added density to the meas. database.
()
CLOSED-FORM solution via value ----------------------------------------
Material:       Kwhite | Probabilty: 0.0000 | Cond. likelihood: 0.00000012
Material:        joker | Probabilty: 0.3972 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:       Kblack | Probabilty: 0.6028 | Cond. likelihood: 0.00967232
MODE 2 was chosen --> optimizing was done for differential information gain.
The argmax mean for elasticity : 10551
The argmax mean for density : 283

Finished action selection!
