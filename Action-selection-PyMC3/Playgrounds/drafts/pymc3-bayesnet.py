"""
'pip3 install pymc3' <-- newest version: 3.10.0
'pip3 install arviz==0.11.0' 
<-- older but compatible version due to python 3.6 not 3.8

python 3.6.9 
"""

from scipy import stats
from scipy.stats import norm  

import numpy as np
import arviz as az
import pymc3 as pm
import theano
import seaborn as sn
import matplotlib.pyplot as plt
from bayesNetFramework import BayesNet, json2dict
import utility as ut


# az.style.use("arviz-darkgrid")
np.random.seed(123)

BN = BayesNet("BN")
BN.load_from_yaml("graph-def.yaml")
materials = json2dict("./materials.json")  # new iterable json
VOLUME_MUS = np.array([])
VOLUME_SIGS = np.array([])
ELASTICITY_MUS = np.array([])
ELASTICITY_SIGS = np.array([])
DENSITY_MUS = np.array([])
DENSITY_SIGS = np.array([])
MASS_SIGMA = 100  # 100 [g] error for mass measurement

# extract parameters from the materials dictionary
for mat in materials:
    VOLUME_MUS = np.append(VOLUME_MUS, float(mat["volume"]["mean"]))
    VOLUME_SIGS = np.append(VOLUME_SIGS, float(mat["volume"]["sigma"]))
    ELASTICITY_MUS = np.append(ELASTICITY_MUS, float(mat["elasticity"]["mean"]))
    ELASTICITY_SIGS = np.append(ELASTICITY_SIGS, float(mat["elasticity"]["sigma"]))
    DENSITY_MUS = np.append(DENSITY_MUS, float(mat["density"]["mean"]))
    DENSITY_SIGS = np.append(DENSITY_SIGS, float(mat["density"]["sigma"]))

axes = ut.create_axes(materials)
ut.plot_materials(axes, materials)

# plastic
measurements = {#"density": [norm.rvs(loc=1380, scale=400, size=50), norm.rvs(loc=1390, scale=400, size=50),
                 #          norm.rvs(loc=1340, scale=400, size=50)],
                "elasticity": [norm.rvs(loc=2, scale=30, size=5), norm.rvs(loc=5, scale=30, size=5), # TODO maybe only positive numbers?
                            norm.rvs(loc=3, scale=30, size=5)],
                "friction": [norm.rvs(loc=0.7, scale=0.45, size=5), norm.rvs(loc=0.6, scale=0.45, size=5),
                            norm.rvs(loc=0.5, scale=0.45, size=5)],
                "mass": [norm.rvs(loc=150, scale=50, size=5), norm.rvs(loc=200, scale=50, size=5),
                         norm.rvs(loc=170, scale=50, size=5)]}

meas_param = {#"density": [[1380, 400], [1390, 400], [1340, 400]],
              "elasticity": [[2, 30], [5, 30], [3, 30]],
              "friction": [[0.7, 0.45], [0.6, 0.45], [0.5, 0.45]],
              "mass": [[150, 50],[200, 50],[170, 50]]}

# this creates
meas_param_instances = {}
for key in meas_param:
    meas_param_instances[key] = []
    for params in meas_param[key]:
        meas_param_instances[key].append(norm.pdf(axes[key], params[0], params[1]))

# ut.diff_ent_simulated_meas(axes, materials, [], 'density', 400, plot=True)


observations = {
    'friction': np.array([]),
    'elasticity': np.array([]),
    'density': np.array([]),
    'vision-material':np.array([]),
    'vision-category':np.array([]),
    'sound-material':np.array([]),
}

# meas_list = []


# chosen density for updates
chosen_action = 'mass'
for idx, meas in enumerate(measurements[chosen_action]):  # meas is a series of measurement points
    # numerical points of simulated measurement
    observations[chosen_action] = np.append(observations[chosen_action], meas)

    # update beliefs based on the current state of MatCat
    beliefs = ut.create_beliefs(materials, axes)
    current_belief = beliefs[chosen_action]
    bel_fig = ut.plot_beliefs(materials, axes, beliefs)


    # consructed measurement gaussians for a) plot purposes and b) for numerical update of Gaussian Mixture Model
    true_meas_mean = meas_param[chosen_action][idx][0]
    true_meas_sigma = meas_param[chosen_action][idx][1]
    
    # true_meas_y = norm.pdf(axes[chosen_action], true_meas_mean, true_meas_sigma)
    # meas_list.append(true_meas_y)

    # always start from the current belief dictated by the categorical
    # distribution, then implement chain of already done measurements
    posterior = current_belief
    # for m in meas_list:
    #     posterior = posterior * m
    #     posterior = posterior / sum(posterior)
    
    ut.overall_differential_entropy(axes, materials, [])
    BN.plot_graph()
    ut.plot_material_category(materials)


    with pm.Model() as bayesnet:

        # ----- START MODEL DEFINITION -----
        
        w = pm.Dirichlet('w', np.ones(len(materials)))

        mass_prior = pm.Uniform(lower=0, upper=2000)
        obsv_mass = pm.Normal('obs_mass', mu=mass_prior, sigma=MASS_SIGMA, observed=ut.smart_flatten(observations['mass']))
        
        elasticity = pm.NormalMixture('elas', w=w, mu=ELASTICITY_MUS, sigma=ELASTICITY_SIGS, observed=ut.smart_flatten(observations['elasticity']))
        volume = pm.NormalMixture('volu', w=w, mu=VOLUME_MUS, sigma=VOLUME_SIGS, observed=ut.smart_flatten(observations['volume']))

        # density observation is computed based on what measurement is the most recent
        density_obs = None
        if chosen_action == 'mass':
            density_obs = observations['mass'] / ut.argmax_of_prop(materials, 'volume', meas_param['volume'])
        elif chosen_action == 'volume':
            density_obs = 150 / observations['volume']  # TODO 150 is just a place holder, need to add mass to the ut.argmax_of_prop()
        
        if density_obs is not None:
            observations['density'] = np.append(observations['density'], density_obs)
            
        density = pm.NormalMixture('dens', w=w, mu=DENSITY_MUS, sigma=DENSITY_SIGS, observed=observations['density'])

        if observations['vision-material'] != np.array([]):
            vision_material = pm.Multinomial('vision-material', 1, w=w, observed=ut.smart_flatten(observations['vision-material']))

        if observations['vision-category'] != np.array([]):
            vision_category = pm.Multinomial('vision-category', 1, w=w, observed=ut.smart_flatten(observations['vision-category']))

        if observations['sound-material'] != np.array([]):
            sound_material = pm.Multinomial('sound-material', 1, w=w, observed=ut.smart_flatten(observations['sound-material']))

        # ----- END DMODEL DEFINITION -----
        
        trace = pm.sample(5000, tune=5000, target_accept=0.9)
        burnin = 500  
        chain = trace[burnin:]

        # posterior predictive check
        ppc = pm.sample_posterior_predictive(chain, var_names=['matCat'], model=bayesnet)

        # compute mean probabilities (dirichlet returns a number of samples from the posterior predictive distribution)
        for i, samp in enumerate(ppc['matCat'].transpose()):
            prob = np.mean(samp)
            print("Prob for {} is {}.".format(materials[i]['name'], prob))
            materials[i]['prob'] = prob

        beliefs = ut.create_beliefs(materials, axes)
        current_belief = beliefs['density']
        # ut.plot_posterior_sequence(axes['density'], meas_list, current_belief)

        pm.traceplot(trace, var_names=['matCat', 'fric', 'elas'])
        pm.plot_posterior(chain, var_names=['matCat', 'fric', 'elas'])
        pm.autocorrplot(chain)
        plt.show()

