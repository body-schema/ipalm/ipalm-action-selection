/*
 * KINOVA (R) KORTEX (TM)
 *
 * Copyright (c) 2019 Kinova inc. All rights reserved.
 *
 * This software may be modified and distributed
 * under the terms of the BSD 3-Clause license.
 *
 * Refer to the LICENSE file for details.
 *
 */
#include <thread>

#include "ros/ros.h"
#include <kortex_driver/Base_ClearFaults.h>
#include <kortex_driver/PlayCartesianTrajectory.h>
#include <kortex_driver/CartesianSpeed.h>
#include <kortex_driver/BaseCyclic_Feedback.h>
#include <kortex_driver/ReadAction.h>
#include <kortex_driver/ExecuteAction.h>
#include <kortex_driver/PlayJointTrajectory.h>
#include <kortex_driver/SetCartesianReferenceFrame.h>
#include <kortex_driver/CartesianReferenceFrame.h>
#include <kortex_driver/SendGripperCommand.h>
#include <kortex_driver/GetMeasuredGripperMovement.h>
#include <kortex_driver/GripperRequest.h>
#include <kortex_driver/GripperMode.h>

#include <cstdlib>
#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <iostream>
#include <fstream>
#include <string> 
#define HOME_ACTION_IDENTIFIER 2
using namespace std;


bool is_gripper_present = true;
bool should_continue = true;
bool logging_enabled=false;

std::string robot_name = "my_gen3";
ofstream file;
int counter=0;
string buffer="// Time Position Current \n";
double startTime;

float actual_gripper_position;
float actual_gripper_current;
float actual_gripper_voltage;
float actual_gripper_temp;


void feedbackCallback(const kortex_driver::BaseCyclic_Feedback::ConstPtr& feedback)
{
  counter++;
  
  /** Getting the stats we want   */ 
  actual_gripper_position = (1-(feedback->interconnect.oneof_tool_feedback.gripper_feedback[0].motor[0].position) / 100.0)*85;
  actual_gripper_current = feedback->interconnect.oneof_tool_feedback.gripper_feedback[0].motor[0].current_motor;
  actual_gripper_voltage = feedback->interconnect.oneof_tool_feedback.gripper_feedback[0].motor[0].voltage;
  actual_gripper_temp = feedback->interconnect.oneof_tool_feedback.gripper_feedback[0].motor[0].temperature_motor;
  
  
  
  
}

void clear_faults(ros::NodeHandle n, std::string robot_name)
{
  ros::ServiceClient service_client_clear_faults = n.serviceClient<kortex_driver::Base_ClearFaults>("/" + robot_name + "/base/clear_faults");
  kortex_driver::Base_ClearFaults service_clear_faults;

  //service_client_clear_faults.call(service_clear_faults);
  // Clear the faults
  if (service_client_clear_faults.call(service_clear_faults))
  {ROS_INFO("Faults cleared all good.");}
  else
  {
    std::string error_string = "Failed to clear the faults";
    ROS_ERROR("%s", error_string.c_str());
    throw new std::runtime_error(error_string);
  }

  // Wait a bit
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
}



void send_gripper_command_force(ros::NodeHandle n, std::string robot_name, double value)
{
  // Initialize the ServiceClient
  ros::ServiceClient service_client_send_gripper_command = n.serviceClient<kortex_driver::SendGripperCommand>("/" + robot_name + "/base/send_gripper_command");
  kortex_driver::SendGripperCommand service_send_gripper_command;

  // Initialize the request
  kortex_driver::Finger finger;
  finger.finger_identifier = 0;
  finger.value = value;
  service_send_gripper_command.request.input.gripper.finger.push_back(finger);
  service_send_gripper_command.request.input.mode = kortex_driver::GripperMode::GRIPPER_FORCE;
  ros::Duration(0.5).sleep();
  if (service_client_send_gripper_command.call(service_send_gripper_command))  
  {
    ROS_INFO("The gripper force command was sent to the robot.");
  }
  else
  {
    std::string error_string = "Failed to call SendGripperCommand with force";
    ROS_ERROR("%s", error_string.c_str());
    throw new std::runtime_error(error_string);
  }
}

void send_gripper_command_position(ros::NodeHandle n, std::string robot_name, double value)
{
  // Initialize the ServiceClient
  ros::ServiceClient service_client_send_gripper_command = n.serviceClient<kortex_driver::SendGripperCommand>("/" + robot_name + "/base/send_gripper_command");
  kortex_driver::SendGripperCommand service_send_gripper_command;

  // Initialize the request
  kortex_driver::Finger finger;
  finger.finger_identifier = 0;
  finger.value = value;
  service_send_gripper_command.request.input.gripper.finger.push_back(finger);
  //service_send_gripper_command.request.input.duration=10;
  service_send_gripper_command.request.input.mode = kortex_driver::GripperMode::GRIPPER_POSITION;

  if (service_client_send_gripper_command.call(service_send_gripper_command))  
  {
    ROS_INFO("The gripper position command was sent to the robot.");
  }
  else
  {
    std::string error_string = "Failed to call SendGripperCommand position";
    ROS_ERROR("%s", error_string.c_str());
    throw new std::runtime_error(error_string);
  }
}

void send_gripper_command_speed(ros::NodeHandle n, std::string robot_name, double value, double sleep)
{
  // Initialize the ServiceClient
  ros::ServiceClient service_client_send_gripper_command = n.serviceClient<kortex_driver::SendGripperCommand>("/" + robot_name + "/base/send_gripper_command");
  kortex_driver::SendGripperCommand service_send_gripper_command;

  // Initialize the request
  kortex_driver::Finger finger;
  finger.finger_identifier = 0;
  finger.value = value;
  service_send_gripper_command.request.input.gripper.finger.push_back(finger);
  //service_send_gripper_command.request.input.duration=2;
  service_send_gripper_command.request.input.mode = kortex_driver::GripperMode::GRIPPER_SPEED;
  ros::Duration(sleep).sleep();
  if (service_client_send_gripper_command.call(service_send_gripper_command))  
  {
    ROS_INFO("The gripper speed command was sent to the robot.");
  }
  else
  {
    std::string error_string = "Failed to call SendGripperCommand speed";
    ROS_ERROR("%s", error_string.c_str());
    throw new std::runtime_error(error_string);
  }
}
void send_gripper_command_measure(ros::NodeHandle n, std::string robot_name)
{
  // Initialize the ServiceClient
  ros::ServiceClient service_client = n.serviceClient<kortex_driver::GetMeasuredGripperMovement>("/" + robot_name + "/base/get_measured_gripper_movement");
  kortex_driver::GetMeasuredGripperMovement service_send_gripper_command;

  // Initialize the request
  //kortex_driver::GripperRequest request;
  //request.mode=kortex_driver::GripperMode::GRIPPER_SPEED;
  
  service_send_gripper_command.request.input.mode = kortex_driver::GripperMode::GRIPPER_POSITION;
  //service_send_gripper_command.request.input.duration=2;
  //service_send_gripper_command.request.input.mode = kortex_driver::GripperMode::GRIPPER_SPEED;
  //ros::Duration(wait).sleep();
  if (service_client.call(service_send_gripper_command))  
  {
    //ROS_INFO("The gripper measure command was sent to the robot.");
    ROS_INFO("Gripper feedback %f",service_send_gripper_command.response.output.finger[0].value);
  }
  else
  {
    std::string error_string = "Failed to call GetMeasuredGripperMovement";
    ROS_ERROR("%s", error_string.c_str());
    //throw new std::runtime_error(error_string);
  }
}

void mySigintHandler(int sig)
{
	/** Do some custom action.
	* In my case save and close the file
	*/
	/*
	file << buffer;
	file.close();*/
	// All the default sigint handler does is call shutdown()
	should_continue = false;
}

void grippingThread(ros::NodeHandle n, std::string robot_name, double value){
	int flag=0;
	send_gripper_command_speed(n, robot_name, value,1.0);
	while(should_continue){
		//send_gripper_command_measure(n, robot_name);
		if(ros::Time::now().toSec()-startTime>4 and flag==0){
			send_gripper_command_speed(n, robot_name, 1.0,0.0);
			flag=1;
		}
		if(ros::Time::now().toSec()-startTime>8 and flag==1){
			send_gripper_command_speed(n, robot_name, value,0.0);
			flag=2;
		}
		if(ros::Time::now().toSec()-startTime>12 and flag==2){
			send_gripper_command_speed(n, robot_name, 1.0,1.0);
			flag=3;
		}
		if(ros::Time::now().toSec()-startTime>16 and flag==3){
			send_gripper_command_speed(n, robot_name, value,0.0);
			flag=4;
		}
		if(ros::Time::now().toSec()-startTime>20 and flag==4){
			should_continue=false;
		}
	}
}

int main(int argc, char **argv)
{
	
	ros::init(argc, argv, "kinova_gripping_node",ros::init_options::NoSigintHandler);

	/*******************************************************************************/
	// ROS Parameters
	ros::NodeHandle n;
	std::thread gripping;
	int degrees_of_freedom = 7;
	int spinCounter=0;
	double gripper_speed=0.0;
	string object_name="";
	

	// Parameter robot_name
	if (!ros::param::get("~robot_name", robot_name))
	{
		std::string error_string = "Parameter robot_name was not specified, defaulting to " + robot_name + " as namespace";
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using robot_name " + robot_name + " as namespace";
		ROS_INFO("%s", error_string.c_str());
	}

	// Parameter degrees_of_freedom
	if (!ros::param::get("/" + robot_name + "/degrees_of_freedom", degrees_of_freedom))
	{
		std::string error_string = "Parameter /" + robot_name + "/degrees_of_freedom was not specified, defaulting to " + std::to_string(degrees_of_freedom) + " as degrees of freedom";
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using degrees_of_freedom " + std::to_string(degrees_of_freedom) + " as degrees_of_freedom";
		ROS_INFO("%s", error_string.c_str());
	}

	// Parameter is_gripper_present
	if (!ros::param::get("/" + robot_name + "/is_gripper_present", is_gripper_present))
	{
		std::string error_string = "Parameter /" + robot_name + "/is_gripper_present was not specified, defaulting to " + std::to_string(is_gripper_present);
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using is_gripper_present " + std::to_string(is_gripper_present);
		ROS_INFO("%s", error_string.c_str());
	}
	  
	// Parameter gripper_speed
	if (!ros::param::get("~gripper_speed", gripper_speed))
	{
		std::string error_string = "Parameter gripper_speed was not specified, defaulting to " + std::to_string(gripper_speed) + " as a gripper speed";
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using gripper_speed " + std::to_string(gripper_speed) + " as as a gripper speed";
		ROS_INFO("%s", error_string.c_str());
	}
	if (!ros::param::get("~object_name", object_name))
	{
		std::string error_string = "Parameter object_name was not specified, defaulting to " + object_name + " as an object name";
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using object_name " + object_name + " as an object name";
		ROS_INFO("%s", error_string.c_str());
	}
	if (!ros::param::get("~logging_enabled", logging_enabled))
	{
		std::string error_string = "Parameter logging_enabled was not specified, defaulting to \"" + std::to_string(logging_enabled) + "\" as logging enabled";
		ROS_WARN("%s", error_string.c_str());
	}
	else 
	{
		std::string error_string = "Using object_name " + object_name + " as an object name";
		ROS_INFO("%s", error_string.c_str());
	}
	
	/********************************************************************************/
	
	/** Reading the current time so we can name the textfile accordingly. 	 */
	if(logging_enabled){
		time_t now = time(0);
		tm *ltm = localtime(&now);	
		
		string filename="Kinova-"+to_string(1900 + ltm->tm_year)+"-";
					
		if(1+ltm->tm_mon<10){
			filename+="0";
		}
		filename+=to_string(1 + ltm->tm_mon)+"-";
		if(ltm->tm_mday<10){
			filename+="0";
		}
		filename+=to_string(ltm->tm_mday)+"-";
		if(ltm->tm_hour<10){
			filename+="0";
		}
		filename+=to_string(ltm->tm_hour)+"-";
		if(ltm->tm_min<10){
			filename+="0";
		}
		filename+=to_string(ltm->tm_min);
		if(object_name!=""){
			filename+="-";
			filename+=object_name;
		}
		// Opening the filestream
		file.open("KinovaLogs/"+filename+".txt",ios::in | ios::trunc);

		//Checking if the process was successful
		if (file.is_open()){
			ROS_INFO("Output operation successfully performed\n");
		}else{
			ROS_INFO("Error opening file");
			ros::shutdown();
			return 1;
		}  
	}
	
	/*******************************************************************************
	* Make sure to clear the robot's faults else it won't move if it's already in fault
	* */
	clear_faults(n, robot_name);
	/*******************************************************************************/

	 
	
	
	ros::param::del("~gripper_speed");
	startTime = ros::Time::now().toSec();
  /** Subscribing to the topic	 */
	ros::Subscriber sub = n.subscribe("/my_gen3/base_feedback",3, feedbackCallback);

	/** setting the sigint handler  */
	signal(SIGINT, mySigintHandler);
	
	
	if (is_gripper_present)	{	
		//send_gripper_command_speed(n, robot_name, gripper_speed);    
		//send_gripper_command_force(n, robot_name, gripper_speed);
		//gripping=std::thread(grippingThread, n, robot_name, gripper_speed);
		gripping=std::thread(send_gripper_command_speed,n, robot_name, gripper_speed,1.0);
	}
	ros::Rate r(100);
	while(should_continue){
		double t= ros::Time::now().toSec()-startTime;
		ROS_INFO("Position: %f Current: %f Voltage: %f Temp: %f Time: %f",actual_gripper_position,actual_gripper_current,actual_gripper_voltage,actual_gripper_temp,t);
		buffer+=to_string(t)+" "+to_string(actual_gripper_position)+" "+to_string(actual_gripper_current)+"\n";			
					
		if(counter==50){
			if(logging_enabled){
				file << buffer;
			}
			buffer="";
			counter=0;
		}			
		ros::spinOnce();
		r.sleep();
		/**here we can check for the position*/
		/**maybe if the speed is 0.0 then stop*/
	}
	/** here save the log*/
	if(logging_enabled){
		file << buffer;
		file.close();
	}
	ros::param::del("~object_name");
	ros::param::del("~logging_enabled");
	
	gripping.join();
	
	if (is_gripper_present)
	{
		if(gripper_speed!=0.0){
			send_gripper_command_speed(n, robot_name, 1.0,0.0);   
		}		
	}
	
	ROS_INFO("Shutting down");
	ros::shutdown();

	return 0;
}
