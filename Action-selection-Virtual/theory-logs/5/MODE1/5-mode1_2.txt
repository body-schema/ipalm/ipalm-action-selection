Cleaning the previous 'meas.json' file.
----------------------
| ITERATION NUMBER: 0 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.4490 | Cond. likelihood: None
Material:   ceramic | Probabilty: 0.3500 | Cond. likelihood: None
Material:   plastic | Probabilty: 0.1000 | Cond. likelihood: None
Material: aluminium | Probabilty: 0.1000 | Cond. likelihood: None
Material:     joker | Probabilty: 0.0010 | Cond. likelihood: None
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 10.830366873442362 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.7231424935670523   |
| The average sampled Entropy is: 0.512109565574982   |
| -OUT- Discrete Information Gain: 1.2110329279920702   |
| -info- AVERAGE sampled Information Gain: 1.21103292799207 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 10.830366873442362 |
| This is the Emulated Entropy for density: 10.476452484004698 |
| Argmax mean: 2300 (no offset)|
| -x- Diff Information Gain: 0.35391438943766396     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.7231424935670523   |
| The average sampled Entropy is: 0.9767643437967142   |
| -OUT- Discrete Information Gain: 0.7463781497703381   |
| -info- AVERAGE sampled Information Gain: 0.7463781497703382 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  1.2110329279920702
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2686 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.7160 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0184 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.2576 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0080 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 1 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.7160 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0184 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.2576 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0080 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.045710542614522 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0110562276018884   |
| The average sampled Entropy is: 0.862040415818179   |
| -OUT- Discrete Information Gain: 0.14901581178370937   |
| -info- AVERAGE sampled Information Gain: 0.1490158117837094 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.045710542614522 |
| This is the Emulated Entropy for density: 8.84039110462788 |
| Argmax mean: 2304 (no offset)|
| -x- Diff Information Gain: 0.20531943798664187     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0110562276018884   |
| The average sampled Entropy is: 0.9182361105128826   |
| -OUT- Discrete Information Gain: 0.09282011708900584   |
| -info- AVERAGE sampled Information Gain: 0.0928201170890056 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.14901581178370937
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2696 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.6672 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0015 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3022 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0291 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 2 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.6672 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0015 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3022 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0291 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.050385897806926 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0741455380041622   |
| The average sampled Entropy is: 0.9191674607989223   |
| -OUT- Discrete Information Gain: 0.15497807720523993   |
| -info- AVERAGE sampled Information Gain: 0.1549780772052396 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.050385897806926 |
| This is the Emulated Entropy for density: 8.927853385239976 |
| Argmax mean: 2308 (no offset)|
| -x- Diff Information Gain: 0.1225325125669503     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0741455380041622   |
| The average sampled Entropy is: 0.9547205738777069   |
| -OUT- Discrete Information Gain: 0.11942496412645531   |
| -info- AVERAGE sampled Information Gain: 0.11942496412645529 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.15497807720523993
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2696 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.5744 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0001 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3277 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0978 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 3 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.5744 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0001 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3277 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0978 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.139284997020425 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.3164083155993322   |
| The average sampled Entropy is: 0.9617022287189408   |
| -OUT- Discrete Information Gain: 0.3547060868803914   |
| -info- AVERAGE sampled Information Gain: 0.35470608688039124 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.139284997020425 |
| This is the Emulated Entropy for density: 9.04902982095341 |
| Argmax mean: 2311 (no offset)|
| -x- Diff Information Gain: 0.09025517606701605     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.3164083155993322   |
| The average sampled Entropy is: 1.1261652288911908   |
| -OUT- Discrete Information Gain: 0.1902430867081415   |
| -info- AVERAGE sampled Information Gain: 0.1902430867081417 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.3547060868803914
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2686 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.4196 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3014 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.2790 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 4 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.4196 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.3014 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.2790 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.232245882669368 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.5611546791640851   |
| The average sampled Entropy is: 1.0251711703957933   |
| -OUT- Discrete Information Gain: 0.5359835087682918   |
| -info- AVERAGE sampled Information Gain: 0.5359835087682923 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.232245882669368 |
| This is the Emulated Entropy for density: 9.166022297750708 |
| Argmax mean: 2315 (no offset)|
| -x- Diff Information Gain: 0.0662235849186601     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.5611546791640851   |
| The average sampled Entropy is: 1.2466779764065263   |
| -OUT- Discrete Information Gain: 0.3144767027575588   |
| -info- AVERAGE sampled Information Gain: 0.314476702757559 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.5359835087682918
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2706 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.2222 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.2009 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.5769 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 5 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.2222 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.2009 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.5769 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.312173301257838 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.4052409417504363   |
| The average sampled Entropy is: 0.9438809717788333   |
| -OUT- Discrete Information Gain: 0.461359969971603   |
| -info- AVERAGE sampled Information Gain: 0.4613599699716031 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.312173301257838 |
| This is the Emulated Entropy for density: 9.265640198629677 |
| Argmax mean: 2318 (no offset)|
| -x- Diff Information Gain: 0.04653310262816035     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.4052409417504363   |
| The average sampled Entropy is: 1.0902923754931608   |
| -OUT- Discrete Information Gain: 0.3149485662572755   |
| -info- AVERAGE sampled Information Gain: 0.31494856625727574 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.461359969971603
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2706 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0814 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0927 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.8258 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 6 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0814 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0927 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.8258 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.372976439336195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8408219133193136   |
| The average sampled Entropy is: 0.5941712197852621   |
| -OUT- Discrete Information Gain: 0.24665069353405145   |
| -info- AVERAGE sampled Information Gain: 0.2466506935340517 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.372976439336195 |
| This is the Emulated Entropy for density: 9.340914325383649 |
| Argmax mean: 2321 (no offset)|
| -x- Diff Information Gain: 0.03206211395254677     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8408219133193136   |
| The average sampled Entropy is: 0.7588582864545569   |
| -OUT- Discrete Information Gain: 0.08196362686475667   |
| -info- AVERAGE sampled Information Gain: 0.0819636268647567 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.24665069353405145
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2689 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0238 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0341 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.9421 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 7 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0238 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0341 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.9421 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.412279880719503 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.3756018513894724   |
| The average sampled Entropy is: 0.28250102979968417   |
| -OUT- Discrete Information Gain: 0.09310082158978822   |
| -info- AVERAGE sampled Information Gain: 0.0931008215897883 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.412279880719503 |
| This is the Emulated Entropy for density: 9.388721342432074 |
| Argmax mean: 2325 (no offset)|
| -x- Diff Information Gain: 0.023558538287428377     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.3756018513894724   |
| The average sampled Entropy is: 0.28633853368950135   |
| -OUT- Discrete Information Gain: 0.08926331769997103   |
| -info- AVERAGE sampled Information Gain: 0.08926331769997109 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.09310082158978822
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2723 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0063 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0115 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.9822 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 8 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0063 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0115 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.9822 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.429972381003061 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.14572711875333594   |
| The average sampled Entropy is: 0.1079687716879576   |
| -OUT- Discrete Information Gain: 0.03775834706537834   |
| -info- AVERAGE sampled Information Gain: 0.03775834706537837 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.429972381003061 |
| This is the Emulated Entropy for density: 9.409237230479834 |
| Argmax mean: 2328 (no offset)|
| -x- Diff Information Gain: 0.02073515052322783     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.14572711875333594   |
| The average sampled Entropy is: 0.14020830225814332   |
| -OUT- Discrete Information Gain: 0.005518816495192619   |
| -info- AVERAGE sampled Information Gain: 0.005518816495192663 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.03775834706537834
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2706 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0016 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0037 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.9946 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 9 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0016 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.0037 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.9946 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.427608768597029 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.05315182923951739   |
| The average sampled Entropy is: 0.046703195274927875   |
| -OUT- Discrete Information Gain: 0.006448633964589515   |
| -info- AVERAGE sampled Information Gain: 0.0064486339645894895 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.427608768597029 |
| This is the Emulated Entropy for density: 9.405222709447871 |
| Argmax mean: 2331 (no offset)|
| -x- Diff Information Gain: 0.02238605914915759     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.05315182923951739   |
| The average sampled Entropy is: 0.0432670737743338   |
| -OUT- Discrete Information Gain: 0.009884755465183588   |
| -info- AVERAGE sampled Information Gain: 0.009884755465183597 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.974289922868459 |
| This is the Emulated Entropy for elasticity: 6.936666868475972 |
| Argmax mean: 199 (no offset)|
| -x- Diff Information Gain: 0.03762305439248692     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.009884755465183588
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 165 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.0040 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.0108 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.9853 | Cond. likelihood: 0.00217391
MODE 1 was chosen --> optimizing was done for discrete information gain 
 computed from the PMF.

