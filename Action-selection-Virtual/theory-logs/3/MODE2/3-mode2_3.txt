Cleaning the previous 'meas.json' file.
----------------------
| ITERATION NUMBER: 0 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.2000 | Cond. likelihood: None
Material:   ceramic | Probabilty: 0.2000 | Cond. likelihood: None
Material:   plastic | Probabilty: 0.2000 | Cond. likelihood: None
Material: aluminium | Probabilty: 0.2000 | Cond. likelihood: None
Material:     joker | Probabilty: 0.2000 | Cond. likelihood: None
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 12.03286228523066 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 2.5   |
| The average sampled Entropy is: 0.8076154176173208   |
| -OUT- Discrete Information Gain: 1.692384582382679   |
| -info- AVERAGE sampled Information Gain: 1.6923845823826797 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 12.03286228523066 |
| This is the Emulated Entropy for density: 11.00726463807476 |
| Argmax mean: 2302 (no offset)|
| -x- Diff Information Gain: 1.0255976471558998     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 2.5   |
| The average sampled Entropy is: 1.3107897168482068   |
| -OUT- Discrete Information Gain: 1.1892102831517932   |
| -info- AVERAGE sampled Information Gain: 1.1892102831517928 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
| This is the Emulated Entropy for elasticity: 7.344186158558585 |
| Argmax mean: 29 (no offset)|
| -x- Diff Information Gain: 0.6045627505276103     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  1.0255976471558998
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2687 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1401 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0006 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.2956 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.5636 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 1 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1401 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0006 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.2956 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.5636 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.671075678416331 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.3901060621875785   |
| The average sampled Entropy is: 0.6298872318675699   |
| -OUT- Discrete Information Gain: 0.7602188303200086   |
| -info- AVERAGE sampled Information Gain: 0.7602188303200087 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.671075678416331 |
| This is the Emulated Entropy for density: 9.565458737595518 |
| Argmax mean: 2316 (no offset)|
| -x- Diff Information Gain: 0.10561694082081274     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.3901060621875785   |
| The average sampled Entropy is: 0.835123844798404   |
| -OUT- Discrete Information Gain: 0.5549822173891745   |
| -info- AVERAGE sampled Information Gain: 0.5549822173891746 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.948748909086195 |
| This is the Emulated Entropy for elasticity: 7.344186158558585 |
| Argmax mean: 29 (no offset)|
| -x- Diff Information Gain: 0.6045627505276103     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.6045627505276103
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 170 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.1501 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0002 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.6182 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.2315 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 2 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.1501 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0002 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.6182 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.2315 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.671075678416331 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.3304023943013976   |
| The average sampled Entropy is: 0.7562096994170823   |
| -OUT- Discrete Information Gain: 0.5741926948843153   |
| -info- AVERAGE sampled Information Gain: 0.574192694884315 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.671075678416331 |
| This is the Emulated Entropy for density: 9.565458737595518 |
| Argmax mean: 2316 (no offset)|
| -x- Diff Information Gain: 0.10561694082081274     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.722276465519388 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.3304023943013976   |
| The average sampled Entropy is: 0.8475079419971787   |
| -OUT- Discrete Information Gain: 0.4828944523042189   |
| -info- AVERAGE sampled Information Gain: 0.4828944523042192 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.722276465519388 |
| This is the Emulated Entropy for elasticity: 6.521772869758855 |
| Argmax mean: 67 (no offset)|
| -x- Diff Information Gain: 0.20050359576053367     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.20050359576053367
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 178 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.1038 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.8347 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0614 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 3 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.1038 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.8347 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0614 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.671075678416331 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8045298355346351   |
| The average sampled Entropy is: 0.49349204324606233   |
| -OUT- Discrete Information Gain: 0.31103779228857276   |
| -info- AVERAGE sampled Information Gain: 0.3110377922885729 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.671075678416331 |
| This is the Emulated Entropy for density: 9.565458737595518 |
| Argmax mean: 2316 (no offset)|
| -x- Diff Information Gain: 0.10561694082081274     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.360219850319139 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8045298355346351   |
| The average sampled Entropy is: 0.5611636565909388   |
| -OUT- Discrete Information Gain: 0.24336617894369628   |
| -info- AVERAGE sampled Information Gain: 0.2433661789436963 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.360219850319139 |
| This is the Emulated Entropy for elasticity: 6.19228302025247 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.1679368300666697     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.1679368300666697
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 173 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0591 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9275 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0134 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 4 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0591 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9275 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0134 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.671075678416331 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.4254268115820276   |
| The average sampled Entropy is: 0.30944498454881497   |
| -OUT- Discrete Information Gain: 0.11598182703321264   |
| -info- AVERAGE sampled Information Gain: 0.11598182703321255 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.671075678416331 |
| This is the Emulated Entropy for density: 9.565458737595518 |
| Argmax mean: 2316 (no offset)|
| -x- Diff Information Gain: 0.10561694082081274     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.098153243326616 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.4254268115820276   |
| The average sampled Entropy is: 0.3117427508088993   |
| -OUT- Discrete Information Gain: 0.11368406077312831   |
| -info- AVERAGE sampled Information Gain: 0.11368406077312829 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.098153243326616 |
| This is the Emulated Entropy for elasticity: 5.950536817841457 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.1476164254851584     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.1476164254851584
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 179 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0315 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9657 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0027 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 5 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0315 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9657 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0027 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.671075678416331 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.22920276764635844   |
| The average sampled Entropy is: 0.16651627018471246   |
| -OUT- Discrete Information Gain: 0.06268649746164598   |
| -info- AVERAGE sampled Information Gain: 0.06268649746164598 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.671075678416331 |
| This is the Emulated Entropy for density: 9.565458737595518 |
| Argmax mean: 2316 (no offset)|
| -x- Diff Information Gain: 0.10561694082081274     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.894262985638561 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.22920276764635844   |
| The average sampled Entropy is: 0.19610872225221423   |
| -OUT- Discrete Information Gain: 0.033094045394144206   |
| -info- AVERAGE sampled Information Gain: 0.033094045394144116 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.894262985638561 |
| This is the Emulated Entropy for elasticity: 5.765105709058087 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.12915727658047427     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.12915727658047427
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 166 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0164 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9830 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0005 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 6 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0164 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9830 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0005 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.671075678416331 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.12770089116848746   |
| The average sampled Entropy is: 0.11759525746299611   |
| -OUT- Discrete Information Gain: 0.010105633705491349   |
| -info- AVERAGE sampled Information Gain: 0.010105633705491385 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.671075678416331 |
| This is the Emulated Entropy for density: 9.565458737595518 |
| Argmax mean: 2316 (no offset)|
| -x- Diff Information Gain: 0.10561694082081274     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.729612365519966 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.12770089116848746   |
| The average sampled Entropy is: 0.11852448075120395   |
| -OUT- Discrete Information Gain: 0.00917641041728351   |
| -info- AVERAGE sampled Information Gain: 0.009176410417283539 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.729612365519966 |
| This is the Emulated Entropy for elasticity: 5.6173175669475714 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.11229479857239433     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.11229479857239433
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 170 (contains offset=100) and sigma: 30 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0085 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9914 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0001 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 7 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000270
Material:   ceramic | Probabilty: 0.0085 | Cond. likelihood: 0.00566858
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00133701
Material: aluminium | Probabilty: 0.9914 | Cond. likelihood: 0.01106467
Material:     joker | Probabilty: 0.0001 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.671075678416331 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.07223791859494623   |
| The average sampled Entropy is: 0.05932114357834981   |
| -OUT- Discrete Information Gain: 0.012916775016596417   |
| -info- AVERAGE sampled Information Gain: 0.012916775016596431 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.671075678416331 |
| This is the Emulated Entropy for density: 9.565458737595518 |
| Argmax mean: 2316 (no offset)|
| -x- Diff Information Gain: 0.10561694082081274     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.59364764779227 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.07223791859494623   |
| The average sampled Entropy is: 0.06353522751070612   |
| -OUT- Discrete Information Gain: 0.008702691084240113   |
| -info- AVERAGE sampled Information Gain: 0.008702691084240129 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.59364764779227 |
| This is the Emulated Entropy for elasticity: 5.495809770862241 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.09783787693002921     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.10561694082081274
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2724 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0040 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.9957 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0002 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 8 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0040 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.9957 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0002 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.516726786666963 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.04082347316277237   |
| The average sampled Entropy is: 0.031339351846704576   |
| -OUT- Discrete Information Gain: 0.009484121316067795   |
| -info- AVERAGE sampled Information Gain: 0.0094841213160678 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.516726786666963 |
| This is the Emulated Entropy for density: 9.39152833123676 |
| Argmax mean: 2698 (no offset)|
| -x- Diff Information Gain: 0.12519845543020303     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.59364764779227 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.04082347316277237   |
| The average sampled Entropy is: 0.03631226748778484   |
| -OUT- Discrete Information Gain: 0.0045112056749875284   |
| -info- AVERAGE sampled Information Gain: 0.004511205674987532 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.59364764779227 |
| This is the Emulated Entropy for elasticity: 5.495809770862241 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.09783787693002921     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.12519845543020303
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2716 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0019 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.9977 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0004 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 9 |
| MODE NUMBER: 2 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0019 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.9977 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0004 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 300
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.309911354938958 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.025166982081476184   |
| The average sampled Entropy is: 0.017552955175736552   |
| -OUT- Discrete Information Gain: 0.007614026905739632   |
| -info- AVERAGE sampled Information Gain: 0.007614026905739631 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.309911354938958 |
| This is the Emulated Entropy for density: 9.161199565925674 |
| Argmax mean: 2702 (no offset)|
| -x- Diff Information Gain: 0.14871178901328364     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 30
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 5.59364764779227 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.025166982081476184   |
| The average sampled Entropy is: 0.020998562609096317   |
| -OUT- Discrete Information Gain: 0.0041684194723798675   |
| -info- AVERAGE sampled Information Gain: 0.004168419472379877 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 5.59364764779227 |
| This is the Emulated Entropy for elasticity: 5.495809770862241 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.09783787693002921     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.14871178901328364
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2708 (contains offset=0) and sigma: 300 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.0009 | Cond. likelihood: 0.00054049
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00000248
Material: aluminium | Probabilty: 0.9983 | Cond. likelihood: 0.00114030
Material:     joker | Probabilty: 0.0008 | Cond. likelihood: 0.00217391
MODE 2 was chosen --> optimizing was done for differential information gain.
The argmax mean for density : 2702
The argmax mean for elasticity : 69

