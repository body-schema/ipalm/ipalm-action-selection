args Namespace(grasp='off', grid=0.055, mes='1', stiff='off', stiffrep='off', vis='off', wght=None)
[94m[INFO] Starting program in MEASURE mode with 1.0 as the max current[0m
[94m[INFO] Weight estimation mode ON.[0m
[94m[INFO] Grid Measuring is ON, with CLOSING_SPEED at 0.055.[0m
Starting to clear
Waiting for service
Clearing faults
[INFO] [1621531845.458477]: Activating the action notifications...
[INFO] [1621531845.460198]: Successfully activated the Action Notifications!
/home/robot3/vision_ws/src/ipalm_control
Cleaning the previous '/home/robot3/vision_ws/src/ipalm_control/meas.json' file.
-----------------Current PMF-----------------
Material:       Kwhite | Probabilty: 0.2500 | Cond. likelihood: None
Material:        joker | Probabilty: 0.2500 | Cond. likelihood: None
Material: SpongeYellow | Probabilty: 0.2500 | Cond. likelihood: None
Material:       Kblack | Probabilty: 0.2500 | Cond. likelihood: None
---------------------------------------------
[INFO] [1621531847.150131]: Sending the gripper position...
[INFO] [1621531847.652643]: 0.0
----------------------
| ITERATION NUMBER: 0 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:       Kwhite | Probabilty: 0.2500 | Cond. likelihood: None
Material:        joker | Probabilty: 0.2500 | Cond. likelihood: None
Material: SpongeYellow | Probabilty: 0.2500 | Cond. likelihood: None
Material:       Kblack | Probabilty: 0.2500 | Cond. likelihood: None
---------------------------------------------
_____________________
('Property: ', u'Elasticity')
Measurement:
('    Meas. sigma:', 4091)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 2.0   |
| The average sampled Entropy is: 1.35846535495   |
| -OUT- Discrete Information Gain: 0.641534645053   |
| -info- AVERAGE sampled Information Gain: 0.641534645053 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for elasticity: 16.1393706678 |
| This is the Emulated Entropy for elasticity: 15.0711244424 |
| Argmax mean: 10159 (no offset)|
| -x- Diff Information Gain: 1.06824622534     |
--------------------------------------------
()
_____________________
('Property: ', u'Density')
Measurement:
('    Meas. sigma:', 29.64)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 2.0   |
| The average sampled Entropy is: 0.585032006246   |
| -OUT- Discrete Information Gain: 1.41496799375   |
| -info- AVERAGE sampled Information Gain: 1.41496799375 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for density: 9.22313511481 |
| This is the Emulated Entropy for density: 8.51304684736 |
| Argmax mean: 25 (no offset)|
| -x- Diff Information Gain: 0.710088267443     |
--------------------------------------------
()
---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  1.4149679937542072
-----------------------------------

[94m[AS-INFO] Starting exploratory action.[0m
[94m[INFO] Zero weight calibration in progress.[0m
[INFO] [1621531851.674423]: Sending the gripper position...
Zero payload torque: 
Reading number 0 : -8.86622047424
Reading number 10 : -8.86518859863
Reading number 20 : -8.90424537659
Reading number 30 : -8.88805675507
Reading number 40 : -8.91145038605
[94mTorque is: -8.90383581161[0m
[92mGripper zero weight is 2.88777767452[0m
[INFO] [1621531863.849696]: Sending the gripper position...
[92m[INFO] Weight calibration successful.[0m
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621531864.902571]: Sending pose...
[INFO] [1621531864.927310]: Waiting for pose to finish...
[INFO] [1621531867.050395]: Sending pose...
[INFO] [1621531867.074564]: Waiting for pose to finish...
[INFO] [1621531868.125867]: Sending the gripper position...
[INFO] [1621531869.150086]: Sending pose...
[INFO] [1621531869.177382]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.13913345337
Reading number 10 : -9.17224693298
Reading number 20 : -9.16516304016
Reading number 30 : -9.12606334686
Reading number 40 : -9.14724063873
[94mTorque is: -9.15240032196[0m
[92mSo the estimated weight is 0.0806168328841[0m
[INFO] [1621531881.051256]: Sending pose...
[INFO] [1621531881.074933]: Waiting for pose to finish...
[INFO] [1621531883.200443]: Sending pose...
[INFO] [1621531883.224611]: Waiting for pose to finish...
[INFO] [1621531884.549438]: Sending the gripper position...
[INFO] [1621531885.574963]: Sending pose...
[INFO] [1621531885.602105]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621531887.152096]: Sending pose...
[INFO] [1621531887.176290]: Waiting for pose to finish...
[INFO] [1621531887.725861]: Sending pose...
[INFO] [1621531887.759151]: Waiting for pose to finish...
[INFO] [1621531888.800299]: Sending the gripper position...
[INFO] [1621531889.827526]: Sending pose...
[INFO] [1621531889.849320]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.15286922455
Reading number 10 : -9.13465881348
Reading number 20 : -9.1550655365
Reading number 30 : -9.1550655365
Reading number 40 : -9.1550655365
[94mTorque is: -9.15672632217[0m
[92mSo the estimated weight is 0.0820198828843[0m
[INFO] [1621531901.824913]: Sending pose...
[INFO] [1621531901.849455]: Waiting for pose to finish...
[INFO] [1621531903.980747]: Sending pose...
[INFO] [1621531903.999710]: Waiting for pose to finish...
[INFO] [1621531905.299482]: Sending the gripper position...
[INFO] [1621531906.326442]: Sending pose...
[INFO] [1621531906.401214]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621531907.951361]: Sending pose...
[INFO] [1621531907.975016]: Waiting for pose to finish...
[INFO] [1621531908.533393]: Sending pose...
[INFO] [1621531908.550516]: Waiting for pose to finish...
[INFO] [1621531909.599610]: Sending the gripper position...
[INFO] [1621531910.624813]: Sending pose...
[INFO] [1621531910.649384]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.15000343323
Reading number 10 : -9.14070510864
Reading number 20 : -9.12944889069
Reading number 30 : -9.17776298523
Reading number 40 : -9.1713552475
[94mTorque is: -9.1512745285[0m
[92mSo the estimated weight is 0.0802517047214[0m
[INFO] [1621531922.525464]: Sending pose...
[INFO] [1621531922.549508]: Waiting for pose to finish...
[INFO] [1621531924.675077]: Sending pose...
[INFO] [1621531924.699780]: Waiting for pose to finish...
[INFO] [1621531925.999379]: Sending the gripper position...
[INFO] [1621531927.024755]: Sending pose...
[INFO] [1621531927.050455]: Waiting for pose to finish...
Added density to the meas. database.
Added density to the meas. database.
()
CLOSED-FORM solution via value ----------------------------------------
Material:       Kwhite | Probabilty: 0.0000 | Cond. likelihood: 0.00000003
Material:        joker | Probabilty: 0.0960 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:       Kblack | Probabilty: 0.9040 | Cond. likelihood: 0.00941798
----------------------
| ITERATION NUMBER: 1 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:       Kwhite | Probabilty: 0.0000 | Cond. likelihood: 0.00000003
Material:        joker | Probabilty: 0.0960 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:       Kblack | Probabilty: 0.9040 | Cond. likelihood: 0.00941798
---------------------------------------------
_____________________
('Property: ', u'Elasticity')
Measurement:
('    Meas. sigma:', 4091)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 0.456201524185   |
| The average sampled Entropy is: 0.262532190906   |
| -OUT- Discrete Information Gain: 0.193669333279   |
| -info- AVERAGE sampled Information Gain: 0.193669333279 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for elasticity: 16.1393706678 |
| This is the Emulated Entropy for elasticity: 15.0711244424 |
| Argmax mean: 10159 (no offset)|
| -x- Diff Information Gain: 1.06824622534     |
--------------------------------------------
()
_____________________
('Property: ', u'Density')
Measurement:
('    Meas. sigma:', 29.64)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 0.456201524185   |
| The average sampled Entropy is: 0.168358742924   |
| -OUT- Discrete Information Gain: 0.287842781261   |
| -info- AVERAGE sampled Information Gain: 0.287842781261 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for density: 6.91142350606 |
| This is the Emulated Entropy for density: 6.56314375211 |
| Argmax mean: 282 (no offset)|
| -x- Diff Information Gain: 0.348279753943     |
--------------------------------------------
()
---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.28784278126098317
-----------------------------------

[94m[AS-INFO] Starting exploratory action.[0m
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621531932.075919]: Sending pose...
[INFO] [1621531932.101886]: Waiting for pose to finish...
[INFO] [1621531932.653120]: Sending pose...
[INFO] [1621531932.675528]: Waiting for pose to finish...
[INFO] [1621531933.752417]: Sending the gripper position...
[INFO] [1621531934.775064]: Sending pose...
[INFO] [1621531934.799272]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.13821029663
Reading number 10 : -9.17201423645
Reading number 20 : -9.16743469238
Reading number 30 : -9.17096805573
Reading number 40 : -9.15942955017
[94mTorque is: -9.16155487061[0m
[92mSo the estimated weight is 0.0835859241563[0m
[INFO] [1621531946.675051]: Sending pose...
[INFO] [1621531946.699173]: Waiting for pose to finish...
[INFO] [1621531948.826660]: Sending pose...
[INFO] [1621531948.849257]: Waiting for pose to finish...
[INFO] [1621531950.149314]: Sending the gripper position...
[INFO] [1621531951.175613]: Sending pose...
[INFO] [1621531951.199490]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621531952.749859]: Sending pose...
[INFO] [1621531952.774345]: Waiting for pose to finish...
[INFO] [1621531953.325001]: Sending pose...
[INFO] [1621531953.349089]: Waiting for pose to finish...
[INFO] [1621531954.399267]: Sending the gripper position...
[INFO] [1621531955.424642]: Sending pose...
[INFO] [1621531955.449140]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.14569377899
Reading number 10 : -9.14311790466
Reading number 20 : -9.1243391037
Reading number 30 : -9.15731048584
Reading number 40 : -9.16300392151
[94mTorque is: -9.16234178543[0m
[92mSo the estimated weight is 0.0838411439417[0m
[INFO] [1621531967.330810]: Sending pose...
[INFO] [1621531967.349459]: Waiting for pose to finish...
[INFO] [1621531969.499704]: Sending pose...
[INFO] [1621531969.524141]: Waiting for pose to finish...
[INFO] [1621531970.824654]: Sending the gripper position...
[INFO] [1621531971.849871]: Sending pose...
[INFO] [1621531971.873991]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621531973.426379]: Sending pose...
[INFO] [1621531973.456814]: Waiting for pose to finish...
[INFO] [1621531974.003141]: Sending pose...
[INFO] [1621531974.024202]: Waiting for pose to finish...
[INFO] [1621531975.075277]: Sending the gripper position...
[INFO] [1621531976.099703]: Sending pose...
[INFO] [1621531976.130057]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.11901855469
Reading number 10 : -9.1268453598
Reading number 20 : -9.1495885849
Reading number 30 : -9.14506816864
Reading number 40 : -9.16659927368
[94mTorque is: -9.15259841919[0m
[92mSo the estimated weight is 0.0806810816829[0m
[INFO] [1621531988.001790]: Sending pose...
[INFO] [1621531988.026936]: Waiting for pose to finish...
[INFO] [1621531990.150435]: Sending pose...
[INFO] [1621531990.178370]: Waiting for pose to finish...
[INFO] [1621531991.474632]: Sending the gripper position...
[INFO] [1621531992.501910]: Sending pose...
[INFO] [1621531992.532806]: Waiting for pose to finish...
Added density to the meas. database.
Added density to the meas. database.
()
CLOSED-FORM solution via value ----------------------------------------
Material:       Kwhite | Probabilty: 0.0000 | Cond. likelihood: 0.00000001
Material:        joker | Probabilty: 0.0122 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:       Kblack | Probabilty: 0.9878 | Cond. likelihood: 0.00863215
----------------------
| ITERATION NUMBER: 2 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:       Kwhite | Probabilty: 0.0000 | Cond. likelihood: 0.00000001
Material:        joker | Probabilty: 0.0122 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:       Kblack | Probabilty: 0.9878 | Cond. likelihood: 0.00863215
---------------------------------------------
_____________________
('Property: ', u'Elasticity')
Measurement:
('    Meas. sigma:', 4091)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 0.0947377352716   |
| The average sampled Entropy is: 0.0526208534839   |
| -OUT- Discrete Information Gain: 0.0421168817877   |
| -info- AVERAGE sampled Information Gain: 0.0421168817877 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for elasticity: 16.1393706678 |
| This is the Emulated Entropy for elasticity: 15.0711244424 |
| Argmax mean: 10159 (no offset)|
| -x- Diff Information Gain: 1.06824622534     |
--------------------------------------------
()
_____________________
('Property: ', u'Density')
Measurement:
('    Meas. sigma:', 29.64)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 0.0947377352716   |
| The average sampled Entropy is: 0.0278102272357   |
| -OUT- Discrete Information Gain: 0.0669275080359   |
| -info- AVERAGE sampled Information Gain: 0.0669275080359 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for density: 6.51773684553 |
| This is the Emulated Entropy for density: 6.29109950928 |
| Argmax mean: 299 (no offset)|
| -x- Diff Information Gain: 0.226637336243     |
--------------------------------------------
()
---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.06692750803591622
-----------------------------------

[94m[AS-INFO] Starting exploratory action.[0m
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621531997.500614]: Sending pose...
[INFO] [1621531997.524782]: Waiting for pose to finish...
[INFO] [1621531998.076166]: Sending pose...
[INFO] [1621531998.100025]: Waiting for pose to finish...
[INFO] [1621531999.148933]: Sending the gripper position...
[INFO] [1621532000.180340]: Sending pose...
[INFO] [1621532000.199589]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.1518611908
Reading number 10 : -9.14575195312
Reading number 20 : -9.17282295227
Reading number 30 : -9.17259788513
Reading number 40 : -9.16797447205
[94mTorque is: -9.15806425095[0m
[92mSo the estimated weight is 0.0824538128129[0m
[INFO] [1621532012.078555]: Sending pose...
[INFO] [1621532012.100810]: Waiting for pose to finish...
[INFO] [1621532014.228150]: Sending pose...
[INFO] [1621532014.257422]: Waiting for pose to finish...
[INFO] [1621532015.554348]: Sending the gripper position...
[INFO] [1621532016.574761]: Sending pose...
[INFO] [1621532016.599022]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621532018.158678]: Sending pose...
[INFO] [1621532018.175591]: Waiting for pose to finish...
[INFO] [1621532018.725012]: Sending pose...
[INFO] [1621532018.752969]: Waiting for pose to finish...
[INFO] [1621532019.801410]: Sending the gripper position...
[INFO] [1621532020.824915]: Sending pose...
[INFO] [1621532020.848996]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.15154266357
Reading number 10 : -9.15291690826
Reading number 20 : -9.14616680145
Reading number 30 : -9.15064811707
Reading number 40 : -9.19050216675
[94mTorque is: -9.15812147141[0m
[92mSo the estimated weight is 0.0824723711025[0m
[INFO] [1621532032.725473]: Sending pose...
[INFO] [1621532032.751461]: Waiting for pose to finish...
[INFO] [1621532034.877864]: Sending pose...
[INFO] [1621532034.900794]: Waiting for pose to finish...
[INFO] [1621532036.199192]: Sending the gripper position...
[INFO] [1621532037.224896]: Sending pose...
[INFO] [1621532037.249778]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621532038.810189]: Sending pose...
[INFO] [1621532038.824315]: Waiting for pose to finish...
[INFO] [1621532039.375277]: Sending pose...
[INFO] [1621532039.401832]: Waiting for pose to finish...
[INFO] [1621532040.454587]: Sending the gripper position...
[INFO] [1621532041.474593]: Sending pose...
[INFO] [1621532041.498961]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -9.16278839111
Reading number 10 : -9.16276550293
Reading number 20 : -9.14692115784
Reading number 30 : -9.14339733124
Reading number 40 : -9.18171596527
[94mTorque is: -9.15577062607[0m
[92mSo the estimated weight is 0.0817099223306[0m
[INFO] [1621532054.399379]: Sending pose...
[INFO] [1621532054.424643]: Waiting for pose to finish...
[INFO] [1621532056.549816]: Sending pose...
[INFO] [1621532056.575890]: Waiting for pose to finish...
[INFO] [1621532057.874481]: Sending the gripper position...
[INFO] [1621532058.903174]: Sending pose...
[INFO] [1621532058.923898]: Waiting for pose to finish...
Added density to the meas. database.
Added density to the meas. database.
()
CLOSED-FORM solution via value ----------------------------------------
Material:       Kwhite | Probabilty: 0.0000 | Cond. likelihood: 0.00000001
Material:        joker | Probabilty: 0.0014 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:       Kblack | Probabilty: 0.9986 | Cond. likelihood: 0.00889897
MODE 1 was chosen --> optimizing was done for discrete information gain 
 computed from the PMF.

Finished action selection!
