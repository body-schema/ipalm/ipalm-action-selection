from __future__ import print_function
import ast
import numpy
import os


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


bop2ycb_dict = {
    '1': '002_master_chef_can',
    '2': '003_cracker_box',
    '3': '004_sugar_box',
    '4': '005_tomato_soup_can',
    '5': '006_mustard_bottle',
    '6': '007_tuna_fish_can',
    '7': '008_pudding_box',
    '8': '009_gelatin_box',
    '9': '010_potted_meat_can',
    '10': '011_banana',
    '11': '019_pitcher_base',
    '12': '021_bleach_cleanser',
    '13': '024_bowl',
    '14': '025_mug',
    '15': '035_power_drill',
    '16': '036_wood_block',
    '17': '037_scissors',
    '18': '040_large_marker',
    '19': '051_large_clamp',
    '20': '052_extra_large_clamp',
    '21': '061_foam_brick',
}


# TODO: not used for now, only using the "Top" orientation for now
def classify_grasp(gripper_from_file_orientation, obj_orientation):
    top_threshold = -0.65
    bottom_threshold = 0.65
    z_axis = [0, 0, 1]

    # obj_orientation = numpy.identity(3)  # R b->o, gripper_orientation: o->gripper

    # Change to obj_orientation=source of object orientation matrix
    # wrt means with relation to
    # gripper_wrt_ground = obj_orientation * gripper_orientation
    # gripper orientation is the gripper -> object
    print("obj_orientation: ", obj_orientation)
    print("gripper_from_file_orientation", gripper_from_file_orientation)

    gripper_wrt_ground = numpy.matmul(obj_orientation, gripper_from_file_orientation)  # TODO maybe change the order
    gripper_point_axis = [gripper_wrt_ground[0][0], gripper_wrt_ground[1][0], gripper_wrt_ground[2][0]]
    print("RESULT OF MATMUL: gripper_point_axis: ", gripper_point_axis)
    theta_cos = numpy.dot(gripper_point_axis, z_axis)
    print("theta_cos", theta_cos)

    if theta_cos < top_threshold:
        typename = "Top"
    elif (theta_cos > top_threshold) and (theta_cos < bottom_threshold):
        typename = "Side"
    else:
        typename = "Bottom"

    print("returning the typename: ", typename)
    return typename
0

def get_bop2ycb(objectID):
    # TODO if object not in the dict
    try:
        ycb_grasp_file_name = bop2ycb_dict[str(objectID)]  # + ".npy"
        return ycb_grasp_file_name
    except (NameError, KeyError):
        return None


def gimme_grasp_backup(obj2base_mat, objectID):
    object_YCB_file_name = get_bop2ycb(objectID)
    if object_YCB_file_name:
        object_YCB_file_name += ".txt"
    else:
        print(bcolors.WARNING + "[WARN] Name not in list." + bcolors.ENDC)
        return None

    grasp_list = ['Grasp1', 'Grasp2', 'Grasp3', 'Grasp4', 'Grasp5']
    final_grasp = None
    file_dir = '/home/andrej/Documents/internship2020/ipalm_control/YCB_top_grasps/'

    with open(os.path.join(file_dir, object_YCB_file_name), "r") as f1:
        data = f1.read()
        info = ast.literal_eval(data)
        for grasp_number in grasp_list:
            try:
                grasp = info[grasp_number]["Type"]
                print("grasp read from dict: ", grasp)
            except (NameError, KeyError):
                print(bcolors.WARNING + "[WARN] No convenient way to grasp object {}".format(objectID) + bcolors.ENDC)
                return None
            if grasp == "Top":
                return grasp


def gimme_grasp(obj_orientation, objectID):
    """

    :param obj_orientation: 6D pose of ycb object
    :param objectID: BOP YCBV object ID, https://bop.felk.cvut.cz/datasets/
    :return: product of object orientation matrix and the gripper orientation matrix
    """

    object_YCB_file_name = get_bop2ycb(objectID)
    if object_YCB_file_name:
        object_YCB_file_name += ".txt"
    else:
        print(bcolors.WARNING + "[WARN] Name not in list." + bcolors.ENDC)
        return None

    """
        Choosing just the first possible grasp as the best for now.
        # grasp_list = ['Grasp1', 'Grasp2', 'Grasp3', 'Grasp4', 'Grasp5']
    """

    # file_dir = '/home/andrej/Documents/internship2020/ipalm_control/YCB_just_grasps/'
    file_dir = '/home/robot3/vision_ws/src/ipalm_control/YCB_top_grasps/'
    with open(os.path.join(file_dir, object_YCB_file_name), "r") as f1:
        data = f1.read()
        info = ast.literal_eval(data)
        chosen_grasp = None
        chosen_grasp = info["Grasp1"]
        print("INFORMATIONS:")
        print("info[Grasp1]: ", info["Grasp1"])

        if not chosen_grasp:
            print("ASD")

            for g in info:  # g = Grasp1 - 6, info is dictionary; info behaves like set when iterated through
                # chosen_grasp = info[g]  # uncomment to stop choosing first grasp !!!
                # break
                # print(classify_grasp(info[g]["Rotation"], obj_orientation))
                print("info[g][Rotation]", info[g]["Rotation"])
                if classify_grasp(info[g]["Rotation"], obj_orientation) == "Top":
                    print("info[g][Rotation]", info[g]["Rotation"])
                    chosen_grasp = info[g]
                    break
        if not chosen_grasp:
            print("FGW")
            for g in info:
                if classify_grasp(info[g]["Rotation"], obj_orientation) == "Side":
                    print("info[g][Rotation]", info[g]["Rotation"])
                    chosen_grasp = info[g]
                    print(
                        bcolors.WARNING + "[WARN] Grasping pose might be dangerous; object {}".format(objectID) + bcolors.ENDC)
                    break

        # if not chosen_grasp:
        #     print(bcolors.WARNING + "[WARN] No convenient way to grasp object {}".format(objectID) + bcolors.ENDC)
        #     try:
        #         chosen_grasp = info["Grasp1"]
        #     except (NameError, KeyError):
        #         print(bcolors.WARNING + "[WARN] No convenient way to grasp object {}".format(objectID) + bcolors.ENDC)
        #         return None
        #     return None

        if not chosen_grasp:
            print(bcolors.WARNING + "[WARN] No convenient way to grasp object {}".format(objectID) + bcolors.ENDC)
            print(bcolors.FAIL + "RAGEQUITTING" + bcolors.ENDC)
            exit()

    best_gripper_rot = chosen_grasp["Rotation"]
    best_gripper_trans = chosen_grasp["Translation"]
    return (best_gripper_trans, best_gripper_rot)

    # return numpy.matmul(obj_orientation, best_gripper_trans), numpy.matmul(obj_orientation, best_gripper_rot)
    # return numpy.matmul(best_gripper_trans, obj_orientation), numpy.matmul(best_gripper_rot, obj_orientation )


def gimme_all_grasps(obj_orientation, objectID):
    """
    :param obj_orientation: 6D pose of ycb object
    :param objectID: BOP YCBV object ID, https://bop.felk.cvut.cz/datasets/
    :return: product of object orientation matrix and the gripper orientation matrix
    """

    object_YCB_file_name = get_bop2ycb(objectID)
    if object_YCB_file_name:
        object_YCB_file_name += ".txt"
    else:
        print(bcolors.WARNING + "[WARN] Name not in list." + bcolors.ENDC)
        return None

    """
        Choosing just the first possible grasp as the best for now.
        # grasp_list = ['Grasp1', 'Grasp2', 'Grasp3', 'Grasp4', 'Grasp5']
    """

    # file_dir = '/home/andrej/Documents/internship2020/ipalm_control/YCB_just_grasps/'
    file_dir = '/home/robot3/vision_ws/src/ipalm_control/YCB_just_grasps/'
    with open(os.path.join(file_dir, object_YCB_file_name), "r") as f1:
        data = f1.read()
        info = ast.literal_eval(data)
        chosen_grasp = None
        # print(info)
        all_grasps = []
        for k in range(5):
            try:
                prompt = "Grasp" + "{}".format(k + 1)
                current_grasp = info[prompt]
                all_grasps.append(current_grasp)
            except (NameError, KeyError):
                break

    return all_grasps
    # return numpy.matmul(obj_orientation, best_gripper_trans), numpy.matmul(obj_orientation, best_gripper_rot)

def generate_files(file_dir):
    file_list = [f for f in os.listdir(file_dir) if os.path.isfile(os.path.join(file_dir, f))]

    print(file_dir)

    for file_name in file_list:

        print(file_name)
        f1 = open(file_dir + file_name, "r")
        data = f1.read()
        f1.close()
        try:
            info = ast.literal_eval(data)
        except (IndentationError):
            print("Indentation Error")

        for i in range(1, len(info) + 1):
            graspname = "Grasp" + str(i)
            translation = numpy.asarray(info[graspname]['Translation'])
            dist_from_origin = numpy.linalg.norm(translation)

            rotation = numpy.asarray(info[graspname]['Rotation'])
            info[graspname]['Type'] = classify_grasp(rotation, numpy.identity(3, 3))

        f1 = open(file_dir + file_name, "w")
        f1.write(str(info))
        f1.close()


if __name__ == '__main__':
    # file_dir = '/home/andrej/Documents/internship2020/ipalm_control/YCB_just_grasps/'
    # generate_files(file_dir)

    for i in range(28):
        print("object num: ", i)
        res = gimme_grasp(4, str(i))
        print("result is: ", res)
