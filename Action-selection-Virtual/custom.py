import json
import copy
import random

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

import numpy as np 
from scipy.stats import norm  

from ipalm_dataset.measurements import ObjectMeasurements
#from ipalm_dataset.model import Model
import utils

ax = np.linspace(0,100,100)
y_uniform = utils.uniform(ax)

# plt.plot(ax, y_uniform)
# plt.show()


ground_dict = utils.json2dict("./ground.json")

orig_probs = utils.assign_equal_probs(ground_dict, joker=True)
orig_probs["ceramic"]["prob"] = 0.2
orig_probs["aluminium"]["prob"] = 0.2
orig_probs["plastic"]["prob"] = 0.2
orig_probs["steel"]["prob"] = 0.2
orig_probs["joker"]["prob"] = 0.2

# orig_probs["ceramic"]["prob"] = 0.05
# orig_probs["aluminium"]["prob"] = 0.05
# orig_probs["plastic"]["prob"] = 0.05
# orig_probs["steel"]["prob"] = 0.05
# orig_probs["joker"]["prob"] = 0.8

print(orig_probs)

print(utils.compute_entropy(orig_probs))

bin_size = 1



ax = np.linspace(0,100,100)
y_uniform = utils.uniform(ax)

diff_prop_entropy = -sum(bin_size * y_uniform * np.log2(y_uniform))
print("Differential entropy of uniform @ 100: ", diff_prop_entropy)

ax = np.linspace(0,1000,1000)
y_uniform = utils.uniform(ax)

diff_prop_entropy = -sum(bin_size * y_uniform * np.log2(y_uniform))
print("Differential entropy of uniform @ 1000: ", diff_prop_entropy)

ax = np.linspace(0,10000,10000)
y_uniform = utils.uniform(ax)

diff_prop_entropy = -sum(bin_size * y_uniform * np.log2(y_uniform))
print("Differential entropy of uniform @ 10000: ", diff_prop_entropy)