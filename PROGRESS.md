# Progress on the matter of Bayesian Network using PyMC3
The work related to this Progress and TODOs file is related to the [3rd point of the 6.Project](https://docs.google.com/document/d/12aqpOSlD5RfDiqqg1MHmd2hbbNcoumM8F13T2BTn6ek/edit#heading=h.sf4jnpsbzhbb) of summer 2021/2022 CTU internship.

### The startup
- 1st terminal: `deact`, `ipalm` then `kortex` (it doesnt really matter where to start the `kortex`)
    - press `ctrl + shift + e`
- 2nd terminal: `deact`, `ipalm` then  `cd src/ipalm_control/` then `python action-selection-master.py` 

# Quote: Jan Behrens:
*computer has a quintilion of base*
After this project is done, the computre should go to rehab.

# The Progress

- after this project is finished, one should consider upgrading to PyMC v4 on `JAX` backend which supports GPU acceleration, in comparison to `Theano` which does not support GPU. [Link Here](https://www.pymc-labs.io/blog-posts/pymc-stan-benchmark/)

# 22/04/2022
- currently rewriting the action selection and inference modules to accomodate the changes for discrete measruements:
    - instead of doing inference on discrete node (material/category) and rebuilding GMMs based on the changes, the inference on discrete node shall be done, but the rebuilt GMM shall be used as a message for the continuous nodes and next step of inference shall be done: `posterior := (prior * message) / sum(prior * message)`
    - `model_specs` have a new entry called `message` for every node, it can contain a `functional_measurement` array or a `vector` vector containing the "message passing measurement" to be used for the message passing. `vector` is `None` for continuous nodes and `functional_measurement` is `None` for discrete nodes
- [ ] TODO each node should have its own piecewise function only for updating, this shall be reused for inference as well as action selection measurement emulation
- [ ] TODO there shall be some higher level function that tackles the action in the correct order, supplementing for the complicated-ass `NetworkX` thing

# 26/03/2022 & 28/03/2022
## Important:
- [X] TODO hybrid measurement of the elasticity
    - [X] TODO needs testing - looks ok
- [X] TODO sound inference should move faster
- [X] TODO test the action selection performance
- [X] TODO also test the action selection performance of discrete entropy
- [X] TODO debug the butler
- [X] TODO WHY DOES DENSITY INFERENCE ALLWAYS END UP AS CERAMICS? 
    - basically a problem of setting up the referece and measurement errors correctly
- [X] TODO make the vectors not have 0s. if there is a 0.00 prob in box category and vision sees a box, it will not move from the 0.00.
- [X] TODO would it be beneficial to widen the current belief before any measurement?
- [X] TODO there are possibly three modes possible for action selection, eg. discrete (in the categoricale nodes), half discrete - half differential -> where you take a look at the differential entropy of the measured property, where the simulated update was done element wise. After that looking at how other continuous properties change based on the change of categorical nodes like material and category. This makes it a mode between truly differential and truly discrete modes. And finally there is the purely differential, where the entropy of only the measured property is taken into account.

## Misc:
- [X] TODO log action selection information
- [X] TODO print out Action Selection checkopint results
- [X] TODO video of each action
- [X] TODO photos for paper of each action

# 25/03/2022
- `rosbag play out.bag --loop` in the `~/vision_ws/src/ipalm_control` running this simulates the output of CosyPose

# 22/03/2022
- [X] TODO is this part ok? Should it be ms_temp.material['probs'].ravel() * _mat_posterior_probs.ravel() and normalized?
    - yeh its ok good fam

# 21/03/2022
- [X] TODO functions inside of the `pymc3_utility` not finished and not up to date with `BN_networkx_concept.ipynb`
- [X] TODO test the actions
- [ ] TODO action selection
    - [X] Started remaking actions selection
    - [ ] TODO try out the special PMF estimation from the PDF where you take the value at the reference point and just normalize it
- [ ] TODO setup with `execnet`
- [ ] TODO elasticity - uninformative prior after measurement out of bounds


# 20/03/2022
- [ ] TODO try using `execnet` for calling of python3 functions
    - if unssucessful create a script that would accept either arguments or flags inside of the `mod_specs` class 


# 18/03/2022
Just realised that the whole action selection thing is running on Python2 and PyMC3 that is available for python2 was released in 2016. Nevertheless, installing it looks like no go?
possible solution could be [this stackoverflow post](https://stackoverflow.com/questions/27863832/calling-python-2-script-from-python-3)
- seems like `pip install pymc3==3.6 --user` works? 
    - [ ] TODO needs testing of PyMC3 functions WAAAAAA

# 09/03/2022
### Main TODO
- [X] TODO create a very easy 2 cat, 3 mat (or vice versa) BN for testing purposes
- [ ] TODO strange things:
    - the category node dirichlet does not seem to fit well after a posterior? Find out why.
    - the whole PDF for elasticity does not seem to be sampled well due to its sheer width. Made a blog post regarding this fact [here](https://discourse.pymc.io/t/sampling-from-a-normalmixture-with-wide-spread-of-parameters/9003?u=dhajnes)
- now needed to connect it with the NetworkX
- [ ] TODO think through how to connect NetworkX and PyMC3
    - do it just lik [here](https://gist.github.com/tbsexton/1349864212b25cce91dbe5e336d794b4)

# 08/03/2022
- finished the model of BN in the PyMC3

- [ ] TODO implement the threshold for squeezing action
- [ ] TODO create a better reference model for elasticity with hard and soft plastic
- [ ] TODO try out how great of an impact does number of *observations* make on the inference with PyMC3

# 07/03/2022
- it looks like the alphas are initialized jsut good enough with `pmf * lambda` where `lambda > 10`, and the performance is really close to actually fitting the dirichlet alphas to the sampled multinomial distribution
- volume prior normalization

# 04/03/2022
- [ ] TODO test whether its possible to initialize model first with `with pm.Model() as model1` and then using the initialized model after measurements with only a `with model1:`
    - in this way it should be possible to just open the model again? not sure though
- made a [blogpost](https://discourse.pymc.io/t/connecting-two-dirichlet-distributions-through-a-probability-correlation-matrix/8967?u=dhajnes) on PyMC Discourse for the correct implementation of correlation matrix between two Dirichlets

# 03/03/2022
- [X] Dummy material PMF node


# 01/03/2022

- [X] TODO start pymc3 implementation for actual robot use

- [X] TODO make the `(C.T T N) .* M` operation into a function
    - did not make it into a function, but rather created the transition matrices that are saved as `mat_given_cat.csv` and `cat_given_mat.csv`, both also available in `.npy` binary formats
    - [X] TODO transition function for actual use in the BN
    - rest is done using `(C.T T1 N) .* M` or `(M.T T2 N) .* C`, depending on which side are you traversing the connection between category and material node, the notation used: `C - category`, `C.T - category transposed`, `T1 - mat_given_cat transition matrix`, `T2 - cat_given_mat transition matrix`, `N - noise matrix`, `M -  material`, `M.T - material transposed`
> Older TODOs



# 28/02/2022
- Started with the construction of the PyMC3 pipeline for the BN
    - looks like the arrow between `category` and `material` should be considered a two-way link or a weak link, not a causal link. This makes modeling easier as the `category` can be now modeled as a subnode of `material`


# 25/02/2022
- [X] TODO make the volume prior into a function (there might be another module for volume estimation in the future)
    - `volume_object_prior.json` with all available information on volume (and also additionally material) from the london dataset web crawling
    
# 24/02/2022
- [X] TODO problem with density not being gaussian mixture, but rather a `1/x` and a `GMM` combination
    - chosen an approach where both probability distributions of mass and volume are sampled and then divided element-wise as `rho = mass / volume`
    - How to model this in PyMC3?
        - [X] TODO literally just whatever works, fit with gaussian / gaussian mixture model
- [X] - it should be possible to setup the whole BN with yaml file
- [X] TODO finish the cheking for fulfilling the prerequisities

# 23/02/2022
- [X] TODO test the logarithmic correction for squeezing
    - [X] TODO experimentally find the threshold
    - corrected the squeezing estimation, the threshold seems to be `85 [kPa]`, the average STD of the gripper is `4526.47 [Pa]`


# 22/02/2022
- dataset gathering setup for sound recognition
    - gathered cca half of the dataset, available here with addmission: [dataset](https://drive.google.com/drive/folders/1cQG1rJi7i5PWIlCfE4ID1sDc9EAaXuyR?usp=sharing)
- tried another fitting of squeezing data, found out that logarithmic fit provides much better results than plain linear regression

# 21/02/2022
- [X] TODO how to generally take into account that without knowing volume the density cannot be computed?
    - handled by shape of probability distributions (priors when nothing is known = uniform distribution or uninformative prior )
- handled by some structural data of BN powered by custom implementation with NetworkX
    - in other words a utility function will be needed for checking the fullfilment of prerequsities
    - [X] TODO implement prerequsities for each action into the BN framework

# 18/02/2022
- [X] TODO utility function for links between category and material
- [X] TODO test weighing


# 17/02/2022
- [X] TODO try out the density computing with only one or two objects (one mass source and one volume source)
    - mass measurement has very small variance, so it is ok to assume that the ratio of two normally distributed (or distributed as GMM) variables will still end up in a normally distributed (or GMM distributed) variable
    - just fit the density with `n` number of components, perhaps only use scikit learn?


# 16/02/2022
- lots of reading on CrossValidated on the matter of distribution of product of two random variables
    - wrote a [post at CrossValidated](https://stats.stackexchange.com/q/564591/314258) about the issue I am having
    - turns out that the ratio of the normal distributions may be modeled as Cauchy disribution givne the mean is at 0. The mean here  provides information therefore it is not ok to just move it to zero and then model the posterior as Cauchy distribution
    - after using logarithm to get rid of the division (division translates to subtraction of `log(A/B) = \log(A)-\log(B)`

# 15/02/2022
- [X] TODO Check on Shubhans progress on diagnosing the sound module
    -   still not done

# 14/02/2022

- measured the weighing STD with 5 different object, 5 iteration with 50 samples per iteration
    - the average STD considering weighing of objects with different masses: 
    ```
    Average STD: 0.0059919 ~ 0.006 kg
    The 95% confidence interval is therefore +- 0.012 kg (+- 12g).
    ```

# 11/02/2022
- [X] TODO sketch of new bayesian network
- [X] TODO Get back into the context of PyMC3 inference

# 10/02/2022
- solved the TODOs from 09/02/2022 and infered volume from london data in `volume_prior.py`

# 09/02/2022
- [X] TODO some assumption about volume 
    - either measure volume of all objects experimentally or create some adhoc assumption
    - experimental assumption is that 5 - 15 % of fullbody volume is the volume of actual container material
    - started a volume appxotimation algorithm in Action selection playground
- [X] TODO there is some problem with the function `self.send_joint_angles_fct()` it sometimes break suddenly ans skips the moevement entirely, sometimes resulting in collisions etc.
    -    solved by additional `rospy.sleep(0.5)` before and after the `self.send_joint_angles_fct()` function


# 08/02/2022
- diagnostic data for *sound module* for @Shubhan
- tested vision, works independently from sound, the problem with versionclash of torch was resolved with virtual env conda
    - [X] TODO: The clash in version for detectron2 might be due to soundmodule and vision module which both use `torchvision`
- [X] TODO output also the category and material, it is now possible to also export the category bounding boxes
- [ ] TODO create a mask or any other way to extract only one single object that preferably lies on the table



# 07/02/2022
- [X] TODO resolve how to run conda with OS
```
CommandNotFoundError: Your shell has not been properly configured to use 'conda activate'.
To initialize your shell, run

    $ conda init <SHELL_NAME>

Currently supported shells are:
  - bash
  - fish
  - tcsh
  - xonsh
  - zsh
  - powershell

See 'conda init --help' for more information and options.

IMPORTANT: You may need to close and restart your shell after running 'conda init'.
```

- just needed to source the shell, the newly created shell by `os.system()` call did not know that some `anaconda` exists
    - solved with `&& . /home/robot3/anaconda3/etc/profile.d/conda.sh && conda activate audio_env` where the command `.` is used instead of the command`source` for dash types of shells used in ubuntu
- the gripper can estimate, when is the object too stiff (ad hoc threshold for deformation)
- [X] TODO the modulus needs to be returned as None or some **negative number** in order to signalize too stiff 
- [X] TODO possibly refit with linear regression
- [X] TODO compute average standard deviation for the gripper (std for each object and then average from those stds)

# 04/02/2022
- [X] - contact detection done
- [X] - stopping detection done
    - the gripper can now estimate the width of the object automatically, given the object is centered under the gripper
    object
- [X] - Measured each soft object (except *Blue Die*) from the dataset used for comparison with professional setup
    - details can be found in `ipalm_kruzliak_squeezing_calibration.ods`, the results are worse than when the sides are known

- [X] TODO test other actions, such as weighing again
- [X] TODO test the whole experimental pipeline, look for possible flaws
that would create process noise (measurement noise is expected)
> Older TODOs:
- [X] - TODO also return the category, not only material for vision
- [X] - TODO since I have the actions functional with measurement objects work on purely action selection is possible

# 03/02/2022
- contact detection needed

# 02/02/2022
- [X] TODO recalibration for the stiffness is needed @Shubhan
    - The correction line found with linear regression over measured and true data: `y = mx + c` where `m = 0.6651185` and `c = 3755.54693`. To obtain corrected values `corrected = measured * m + c`

# 01/02/2022
- [X] TODO stiffness hybrid estimation (squeezable/unsqueezable)  
    - an example of such measurement can be seen in `hybrid_meas_playground.py`
- [X] - TODO implement also london data as an action
    - probably wont be used as an action


# 10/12/2021
- actions working, returning measurements
- `__repr__` for object meas

# 03/12/2021
- implemented complex saving mechanism for measurements `meas_data_utils.py`
- implemented all physical actions 
    
- [X] TODO testing of each action
- [X] TODO create a logging mechanism NOW, or get lost later :)
    -   delegated to Jiri Hartvich
- [X] TODO create a demo, with every action and use the data from the logging mechanism to show everything
    - kind of done, logger left for jiri



# 29/11/2021
- implemented whole side-movement function for the arm
- [X] TODO possibly needs a speed adjustment and other general small adjustments
- [X] TODO last needed part is the actual inference from recorded data
    - inference_results.txt
    - [X] TODO should be json


# 27/11/2021
- `python3 -m pip install sounddevice` for the sound module
- functional module for sound recording, needs to be connected with the inference module though
- implemented movement function for the arm that should nudge the object to make a sound
    
- [X] TODO implementation of sound modules
    - module is implemented virtually, needed implementation with actual robotic movement
    

# 26/11/2021
- completed London data usage, the action is complete with additional Standard deviation for each property as well as utility functions
- working camera module with mobilenet and detectron2
- [ ] TODO setup of the whole final algorithm, sketch is started in `main_vitual.py`
    - the idea is such that the pipeline first be tested virtually because it's fast and then deployed for the real arm


# 17/11/2021
- Repaired Shubhan's module for squeezing (min/max line instead of Linear Regression which is not robust against outliers)
- recalibration of data in `reference.json`
- preparation for the presentation
### The startup:
- 1st terminal: `deact`, `ipalm` then `kortex` (it doesnt really matter where to start the `kortex`)
- 2nd termina: `deact`, `ipalm` then  `cd src/ipalm_control/` then `python action-selection-master.py` 


# 12/11/2021
- Shubhans sound module testing
- 'story' of the algorithm creation (iPad)

# 05/11/2021
- Thought through how to do the error addition into the prediction:
    -
    ```
    normalized(noisy_bel .* bel)
    ```  

     where: `noisy_bel = np.matmul(conf_noise_mat, bel)`
- [X] TODO implementation of vision module
    - module from Jiri Hartvich uploaded as a draft to github [here](https://github.com/Hartvi/Detectron2-mobilenet)  

# 29/10/2021
- Implemented the inverse bayes rule for web crawler.
- [X] TODO actual implementation as an action of the the inverse bayes rule for web crawler.
    - for entropy calculation an emulation of PMF shall be made using the inference noised by the noise matrix (using 1 - precision), as the measurement is not a simple gaussian.

### notes from Jiri
detectron2 installation:
```
git clone https://github.com/facebookresearch/detectron2.git 
python -m pip install -e detectron2 --user
module load torchvision/0.9.1-fosscuda-2019b-PyTorch-1.8.0
module load OpenCV/3.4.8-fosscuda-2019b-Python-3.7.4
module load scikit-learn/0.21.3-fosscuda-2019b-Python-3.7.4
module load scikit-image/0.16.2-fosscuda-2019b-Python-3.7.4
```
Tak jsem nahral na github novou verzi, kde tu custom slozku jsem prejmenoval: `patch_material_based_recognition` => `ipalm` .
V tom ipalm je slozka models, kam patri tyhle dva modely: https://drive.google.com/drive/folders/1iu_YYbbb6iiyAiCXsjhjMEMQx-KsLr7y?usp=sharing
Takze `ipalm/models/saved_model.pth` a `ipalm/models/model_final.pth`
Pro tebe stacit jen to, co je v if `name == "__main __"`:
```
from detectron2.ipalm.andrej_logic import CatmatPredictor
megapredictor = CatmatPredictor(0.6)
list_of_andrej_dicts: List[Dict] = megapredictor.get_andrej(Union[file_path: str, image_array: np.ndarray])
```


# 22/10/2021
- parsed London Imperial data succesfully
- completed query function (known category - queried material)
    - [X] TODO do this also in reverese (known material - query the category probs)

> Older TODOS:
- [X] TODO - how to translate information from one discrete PMF block *(object category)* to other discrete PMF block *(material category)* ?
    - in case of using the GT data, I do not need to use any measurement error, because it is directly the measurement. Although it should be noised with a noise matrix just to account for statistical inconsistencies.


## 18/10/2021
- parsing the london data through regular expressions (not working yet, jesus christ)
- created a data and object baseline for those london things in `.json`
- [X] TODO - make the regex work and extract at least the arrays
    - parsed manually - way faster (AAAAAAAAAAAAAAAA)

### 15/10/2021
- implemented confusion noise matrix as Ville Kyrki suggested
- implemented dirichlet PMF fitting for the direct prior setup as a module into the `pymc3-bayesnet.py`
- [X] TODO - creation of measurement or a "prior measurement" to use info from web-crawler by Krystian

### 08/10/2021
- trying to figure out whether fitting the alphas for the dirichlet distribution and doing 
inference iteratively is equivalent to just appending new observations and fitting it whole again from uniformly set alphas 
- should I weigh the whole dataset in order to create a reference
- [X] TODO - still missing **module** for direct input of Dirichlet distribution
- [X] TODO - created observations should be added to an observation pool `observataions` in `pymc3-bayesnet.py`
- The whole model is now put into the `pymc3-bayesnet.py` script

### 04/10/2021
- fitting the hierarchical model on the dirichlet is done, although it takes 15 seconds, which takes approximately 15 - 150 times more than I would like...
    - looking for faster alternatives, although not sure wheteher with much luck
- [X] TODO how to learn from discrete outputs?
    - just 1000 or so samples from the probability mass function and then tadaaah?
        - yes, i think a good rule of thumb is: `n_samples = n_categories * 100` and use those to fit Multinomial to the observation
- [X] TODO how to represent the standard deviation and so on in the discrete probabilistic outputs from the vision and 
the sound modules?
- [X] TODO the standard deviation or precision will be also needed for the entropy estimation of the measurement
    - the key is to use a *general noise confusion matrix* in front of the PMF measurement vector (if the vector looks like this: `[0.8, 0,01, 0.05, 0.05]^T`) or the *actual confusion matrix* in front of the classification argmax output (if the vector looks like this: `[0, 0, 0, 1]^T`), where the 1 stands for the classified category

### 01/10/2021
- maybe try out documentation with [Sphinx](https://opensource.com/article/19/11/document-python-sphinx)
- install [Zeal Documentation manager](https://zealdocs.org/) for future purposes.
- implementing the 'direct' input of probabilities to the dirichlet model (see [this post](https://stackoverflow.com/questions/52429587/pymc3-dirichlet-with-multidimensional-concentration-factor))
    - a test example is in `pymc3-playground.py`, kinda working
- solved some bugs with the `argmax()` of a distribution for density estimation


### 28/09/2021
- IPALM meeting (also recorded)
    - transition between PMF and Dirichlet as an optimization problem (that can be just randomly set)
    - ideal solution for unmeasurable properties is split between GMM and a uniform distribution (or a Normal and Uniform)
- managed to get to almost working version of the BN in PyMC3
- [X] TODO fix the argmax mean returning some strange number
    - the problem was that I was not accounting for the "placement of zero" on the axis. In the minimal working examples, the axis was from 0 to N and ergo it worked. In the current setting, the axis is set according to the standard deviations and means and hence can range also into the negative part of the axis. So this offset needed to be corrected for.
### 24/09/2021
- maybe using `pm.Deterministic()` is not useful after all. Maybe even incorrect. It is usually used for Linear regression and other related regression tasks. In the setting of Bayesian Network, there arose a problem where I cannot combine the gaussian mixture (connected to the dirichlet distribution via `weights`) and the `pm.Deterministic()`. The density should be determined by the observations on both volume and mass, although the limitations of the `PyMC3` direct me into the direct 'creation' of density observations based on the currenct most believed values of volume and mass.
- created new pseudocode for actionselection with iterational adding of observations
- [X] TODO need to finish the `build_and_infer_from_model()` function

### 20/09/2021
- as for Dirichlet distribution `pm.Dirichlet()` and `pm.find_MAP()` and divergence:
> Quick update It looks like the function pm.find_MAP() employes a sort of gradient descent optimisation. This does not take into account the constraint resulting from the fact that a vector representing a draw from a Dirichlet distribution is a probability mass function (its values should be positive and their sum should be one). This constraint is apparently not enforced at the sampling stage of the algorithm either, and causes convergence problems as the precision of the likelihood distribution drifts towards zero.
- maybe the iterative nature of the model can be solved by "creating" new and new observational nodes, which are bound to the same variable.
    - this needs to be tested tho, but it seems to 'kinda' work
    - [ ] TODO test this!
    - see an example below:
```
with pm.Model() as mini:
    mass = pm.Deterministic('mass', volume * density)  # TODO shouldnt the deterministic be the density?
    obsv_mass = pm.Normal('obs_mass', mu=mass, sigma=100, observed=ob)
    ...
    # inference
    ...
with mini:
    obsv_mass = pm.Normal('obs_mass2', mu=mass, sigma=100, observed=ob2)
```  
- realized that the `parameter dimension` clash was due to the `mass = pm.Deterministic('mass', volume * density)`, which is obvious now, but wasn't sooner :D
    - this means, that the iterative adding of observations should work, if the observations are not intertwined by a `pm.Deterministic()` link


### 17/09/2021
- fit with `sklearn.mixture` very precise
- need to find out how to update the distribution after refitting

### 16/09/2021
- trying to fit a gaussian mixture to obtain parameters for easier iterative inference, although it is very imprecise still
    - probably doing something working
- this [link](https://discourse.pymc.io/t/performance-speedup-for-updating-posterior-with-new-data/1097) seems promising tho
- trials can be seen in `fit_example.py`
- made another [community question post](https://discourse.pymc.io/t/gaussian-mixture-model-fitting/8036) at discourse pymc3 forum

### 15/09/2021
- made a [community question post](https://discourse.pymc.io/t/meaning-of-various-posterior-predictions-with-multiple-observations-bayesian-network/8026) at `discourse.pymc.io`.
- realised that the iterative PyMC3 approach does not take advantage of previous measurements, it simply fits the distribution all over again based on the current measurement. 
    - trying to solve this problem by stacking observations on top of each other after each iteration
    - although this is quite risky, after many iterations the inference might take a long time if no precaution (e.g. restricting the amount of datapoints) is taken
    - maybe `pm.Categorical()` may be of some use
- created a sketch of Deterministic modelling of the triplet **mass, density** and **volume** in `pymc3-playground.py`

### 14/09/2021
- IPALM meeting

### 10/09/2021
- addressing the problem of various materials not having some material properties (e.g. paper for elasticity or foam for sound) by putting them all far away away from the measurable spectrum

### 09/09/2021
- finished volume prior setup
- more general `materials.json`, removed also the friction, because it is not measurable youtube
- [X] - need to fix the bug with dimension mismatch, probably because of the `None` in the elasticity part of `paper` material
    - the bug can be perhaps fixed by setting the elasticity to really small number close to zero, or perhaps zero

### 08/09/2021
- solved the volume reference bank by just multiplying by a constant `0.1` to assuming a thin wall only (paper box, metal can and so on)
- created new `materials_temp.json` and added `ycb_volume2.ods` spreadsheet

### 01/09 & 07/09/2021
- debugging of the gaussian fitting of the soundscape --> postponed  
Here is the result of the fitting. The dark-blue is the starting iteration of 50 steps and goes up to 5950 steps - turqoise.  
With more iterations of the Metropolis algorithm, the fit is less and less tight:
![Fitting of data with a GMM](/Action-selection-PyMC3/Playgrounds/Theoretical-notes/spectrogram-GMMs50-5950.gif "Fitting of data with a GMM")
- Possible solution, maybe the Observed data should be normalized (with normalization it works better)
    - but still smaller number of samples is better than larger, that is strange
    - also if `tune` is more than 500 the precision starts decreasing
- made a different design choice in `materials.json` and needed to backtrack bugs in action selection
    - action selection is working again



### 31/08/2021
- fixing bugs and still trying to fit the spectrogram correctly with a Gaussian Mixture Model based on PyMC3
- IPALM meeting

### 30/08/2021
- found a dead end with the volume estimation, as of the volume estimation cannot be done with the jugs and pitchers and other stuff that is fillable
- estimating of Gaussian Mixture Model from the spectrogram

### 27/08/2021
- added spectrograms, in the `.gitignore`

### 26/08/2021
- functional action selection virtual with pymc3, needs more thorough testing
- solved some bugs, it is important to normalize the gaussians, if the bin size is not one. It worked previously only on accident, because my binsize was always 1. Not it is not, and every `y = norm.pdf(axis, mean, sigma)` needs to be multiplied by `bin_size`
- also the middle of the axis problem with convolution is solved

### 24/08/2021
- integration with entropy estimation done, may start with experiments

### 23/08/2021
- the long sampling times were due to the large number of data in `observation=data`. When
lowering the amount of observation data, the inference runs faster. The amoung of observation data
is optional, due to the fact that for my application I only need to simulate a measurement with known mean and known variance, but such format is not supported by PyMC3
- started integrating the entropy estimation stuff

### 20/08/2021
- pymc3 iterative inference with argmax measurement keeping
- debugging of speed of the samplers, `NUTS` is > 2 minutes per inference, Metropolis is ~ 30 seconds per inference. Should try to find the lower bound for useful steps. (e.g. start from 10 steps, which is ridiculously low)

### 19/08/2021
- wrote whole `utility.py` module with utilitary code for plotting and other distribution-related things
- start of integration of pymc3 with action-selection algorithm based on my bc. thesis

### 18/08/2021
- it is now possible to declare the network via `yaml` file
- integration of the framework with pymc3
- done pymc3 inference with all 3 different gaussian mixtures (friction, elasticity, density) it is possible to build the model iteratively after each new measurement, also multiple observational data are possible simultaneously, which should be a big plus

### 17/08/2021
- integartion of the bayesian network framework with NetworkX for graph structure visualization
- IPALM meeting, gather insights into the intuition behind belief propagation and measurement nodes

### 16/08/2021
- framework for bayesian network, decided to write own, other nets have scarce or none continuous variable support at all

### 13/08/2021
- reading the Chapter 1 (Probability and Inference), Chap. 3 (Introduction to Multiparameter Models), Chap. 5 (Hierarchical Models) of Andrew Gelman's **Bayesian Data Analysis** available on [gdrive](https://drive.google.com/file/d/1ipF3ZCGroBvc79mzKlnnAqUU4AkjqGE_/view?usp=sharing)
- reading a github blogpost by the Stanford University by *Volodymyr Kuleshov* and *Stefano Ermon* on [Belief Propagation](https://ermongroup.github.io/cs228-notes/inference/jt/) as message passing and [Variable Elimination](https://ermongroup.github.io/cs228-notes/inference/ve/) as message passing.
- studied [this](https://towardsdatascience.com/belief-propagation-in-bayesian-networks-29f51fdc839c) blogpost from *towardsdatascience* on **Belief Propagation in Bayesian Networks** by *Phillip Wenig*

### 12/08/2021
- MWE for dirichlet regression, left some TODOs, e.g. how to extract the hyperparameters of dirichlet distribution after it's been fit to the data based on a Gaussian Mixture Model

### 11/08/2021
- MWE for multivariate infrence and a simple gaussian inference

### 10/08/2021
- half-working minimum working example of a simple inference in PyMC3 in a Bayes network
- better understanding of the Dirichlet distribution thanks to [this thread](https://stats.stackexchange.com/questions/81136/the-meaning-of-representing-the-simplex-as-a-triangle-surface-in-dirichlet-distr#_=_)

**A question**:
How to generalize the learning of the theta variables from the posterior mixed normal distribution. The dirichlet distribution currently only captures the probabilities of the discrete nodes, but I need the posterior to model the means and deviations of the underlying GMM.

### 09/08/2021
- watched the lecture on [PyMC3 vs Stan for hierarchical models](https://www.youtube.com/watch?v=Jb9eklfbDyg&t=2132s)
    - the adjacent github to the lecture can be found [here](https://github.com/jonsedar/pymc3_vs_pystan)

### 06/08/2021
- commited a draft of Bayesian Newtork, although many questionmarks must be resolved for it to work

### 04 & 05/08/2021
- 4th chapter

### 02 & 03/08/2021
- reading 1-3rd chapter of [Bayesian Analysis with Python by *Osvaldo Martin*](https://drive.google.com/file/d/1PdTyGIoCraV4WEyaSUNNVB2QTxjJJPRl/view?usp=sharing) (link to PDF @ Google Drive)
(it is really meaty)  
- trial code running the correct versions of `PyMC3` with the combination of `ARviz`  
(not to be donfused with `RViz` for robotics visualization) and `matplotlib`
    - details on that can be found in the sketch `pymc3-playground.py`


## Related
- [Applet for illustration of different MCMC samplers](https://chi-feng.github.io/mcmc-demo/app.html), such as NUTS, HamiltionianMC, Gibbs Sampling, SVGD etc.
- A Use Case scenario of fitting a Gaussian Mixture Model in PyMC3 [here](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiVx_DVqKnzAhWDMewKHXKqD_gQFnoECBkQAQ&url=https%3A%2F%2Fpsyarxiv.com%2Faes5f%2Fdownload&usg=AOvVaw22G1T0HRf-Yc31LT_Jhk7L)
- Useful overall datascience "documentation" also using PyMC3 but also other stuff [here](https://people.duke.edu/~ccc14/sta-663-2020/index.html)
- Useful PyMC3 detailed explanation from Medium [here](https://towardsdatascience.com/hands-on-bayesian-statistics-with-python-pymc3-arviz-499db9a59501)
- Introduction [Jupyter notebook to PyMC3](https://notebook.community/molgor/spystats/notebooks/Sandboxes/MCMC/.ipynb_checkpoints/Introduction%20to%20PyMC3-checkpoint)
- Pycon 2017 in-depth introduction into Bayesian modelin in PyMC3 [here](https://www.youtube.com/watch?v=TMmSESkhRtI&ab_channel=PyCon2017)
- Machine Learning Workshop 2018 - Advanced Bayesian Modelling in PyMC3 [here](https://slideslive.com/ceai/machine-learning-workshop-2018)
    - and also supplementary code can be found [here](https://github.com/junpenglao/advance-bayesian-modelling-with-PyMC3)
- An applet for [Beta distribution visualization](https://www.desmos.com/calculator/wwcykmxpza)
- Dirichlet process and stick-breaking [here](http://blog.shakirm.com/2015/12/machine-learning-trick-of-the-day-6-tricks-with-sticks/)
- example of a Bayesian Network using PyMC3 [at this link](https://gist.github.com/tbsexton/1349864212b25cce91dbe5e336d794b4)
- crossroads of [reading sources for Bayesian Inference](https://eigenfoo.xyz/bayesian-inference-reading/)
- promising [repo on github](https://github.com/FirstHandScientist/amp) about advanced message passing algorithms
- Doing Bayesian Data Analysis with *John K. Kruschke* - a book [at gdrive](https://drive.google.com/file/d/1uY7n4TkC8doTXK7b7JE4eqyeoX3ovPFV/view?usp=sharing) with great visualisations and explanations on bayesian probability 
- course on DataScience with Python [here](https://github.com/americofmoliveira/Applied_Data_Science_with_Python)
- reading of Kruschke's [book](https://drive.google.com/file/d/1uY7n4TkC8doTXK7b7JE4eqyeoX3ovPFV/view?usp=sharing)    <--- currently reading
- check [KEDRO](https://kedro.readthedocs.io/en/stable/) pipeline for DataScience

## Old TODOS
- [X] - understanding of the usage of hierarchical models for modeling bayesian networks
- [X] TODO try to swap the deterministic to be the `density` not `mass`, because the density is determined, the volume and mass are measured...
    - didnt work, as the density ought to be a GMM and in this way it would be deterministic and not with learnable parameters
- [ ] TODO try out the iterative observations of various sizes, not taking into account the deterministic link of mass, density and volume, as mentioned on the **20/09/21**
- [X] - `pm.Deterministic()` try out the volume to density thing - 
While these transformations work seamlessly, their results are not stored automatically. Thus, if you want to keep track of a transformed variable, you have to use pm.Deterministic. Question remains, which of those nodes shall be considered determined? Is it the density? Volume and mass are measurable, density is not directly observable.
- [X] - consider adding iteratively observations all together into the model and also rescaling the number of observation points to an upper limit?
- [X] - try to fit a simple gaussian mixture with a GMM with PyMC3 to see, whether this thing is usable
    - seems yet unusable, moore lightweight fit with `sklearn` possible
- [X] - dont use joker, instead check whether the argmax fits with the mean of chosen matCat
- [ ] - need to fix the expected information gain which is sampled from the whole disribution.
    - it either needs a custom sampler or I need to supplement the unmeasruable part of the distribution with zeros or numbers really  close to zero
- [X] - try out the `pm.Categorical()`, it may be perhaps useful for hyperparameters saving or something of that nature
    - not really
- [X] - generalize the MWE of multivariate inference for more steps, to see wheteher it is really working
- [X] - should be the entropy computation restricted to a non-negative support? <- No
- [X] - find the lower bound for the Metropolis / NUTS sampler, reasearch on which sampler might be the most useful and try those out
- [X] - read about *Pearl's belief propagation algorithm* [here](http://www.cse.unsw.edu.au/~cs9417ml/Bayes/Pages/PearlPropagation.html)
- [X] - minimum working example (**MWE**) of a simple inference for educational purposes
- [X] - MWE of a multivariate inference 

