import json

grounds = {
    "materials": {
        "steel": {
            "density":{
                "mean":7900,
                "sigma":250,
                "unit":"kg.m^{-3}"
            },
            "elasticity":{
                "mean":200,
                "sigma":10,
                "unit":"GPa"
            }
        },
        "ceramic": {
            "density":{
                "mean":2300,
                "sigma":50,
                "unit":"kg.m^{-3}"
            },
            "elasticity":{
                "mean":30,
                "sigma":10,
                "unit":"GPa"
            }
        },
        "plastic": {
            "density":{
                "mean":1350,
                "sigma":250,
                "unit":"kg.m^{-3}"
            },
            "elasticity":{
                "mean":3,
                "sigma":10,
                "unit":"GPa"
            }
        },
        "aluminium": {
            "density":{
                "mean":	2705,
                "sigma":180,
                "unit":"kg.m^{-3}"
            },
            "elasticity":{
                "mean":70,
                "sigma":20,
                "unit":"GPa"
            }
        }
    }
}


with open('ground.json', 'w') as fp:
    json.dump(grounds, fp, indent=4)

