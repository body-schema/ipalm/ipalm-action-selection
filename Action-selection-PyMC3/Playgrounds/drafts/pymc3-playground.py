"""
'pip3 install pymc3' <-- newest version: 3.10.0
'pip3 install arviz==0.11.0' 
<-- older but compatible version due to python 3.6 not 3.8

python 3.6.9 
"""
import copy
from numpy.core.fromnumeric import mean

import scipy
from scipy import stats
import theano
import seaborn as sn
import arviz as az
from scipy.stats import norm
import numpy as np
import pymc3 as pm
import matplotlib.pyplot as plt

from bayesNetFramework import BayesNet, json2dict
import utility as ut

materials = json2dict("./materials.json")
VOLUME_MUS = np.array([10000, 15000])  # cm^3
VOLUME_SIGS = np.array([100, 100])

DENSITY_MUS = np.array([0.1, 0.3])
DENSITY_SIGS = np.array([0.3, 0.25])

# ob = norm.rvs(loc=1000, scale=100, size=50)


x = np.linspace(0, 20000, 1000)
v1 = norm.pdf(x, VOLUME_MUS[0], VOLUME_SIGS[0])
v2 = norm.pdf(x, VOLUME_MUS[1], VOLUME_SIGS[1])

# GMM = 0.6 * v1 + 0.4 * v2
# plt.plot(x, GMM, color="tab:blue", label="GMM")
# plt.legend()
# plt.show()
#
# rng = x[-1] - x[0]
# bin_size = rng / len(x)
#
# print("rng: ", rng)
# print("bin_size: ", bin_size)
# print("Sum of GMM: ", sum(GMM*bin_size))
#
# print("argmax: ", np.argmax(GMM) * bin_size )

"""
Imaginary output from Jiri's detectron2 and MobileNet module:
Categorical PMF:
p(x|paper) = 0.1
p(x|metal) = 0.25
p(x|Soft-plastic) = 0.01
p(x|Hard-plastic) = 0.05
p(x|Foam) = 0.4
p(x|Wood) = 0.1
p(x|Ceramic) = 0.09
"""

# # formula I made up: n_categories * 100 - is the resolution needed
# PMF = np.array([0.1, 0.25, 0.01, 0.05, 0.4, 0.1, 0.09])
# # PMF = np.array([0.6, 0.4])
# n_samples = len(PMF) * 100

# obs = ut.sample_PMF(PMF, n_samples, seed=123)


# # N - num of observations, K - number of categories (components)
# N, K = obs.shape

# with pm.Model() as dirTrial:

#     # dirichlet distribution
#     alpha = pm.Gamma('alpha', alpha=2, beta=0.5, shape=K)
#     w = pm.Dirichlet('weights', a=alpha)
#     # y = pm.NormalMixture('GMM', w=w, mu=np.array([10,20]), sigma=np.array([10,10]), observed=None)
#     y = pm.Multinomial('y', 1, w, observed=obs)

#     chain = pm.sample(5000, tune=5000, target_accept=0.9)

#     pm.traceplot(chain)
#     pm.plot_posterior(chain)
#     pm.autocorrplot(chain)
#     plt.show()

# with dirTrial:

PMF = {
    "metal" : 0.5,
    "ceramic" : 0.1,
    "soft-plastic" : 0.1,
    "hard-plastic" : 0.2,
    "wood" : 0.025,
    "paper" : 0.025,
    "foam" : 0.05,
}
pmf_vec = [0.5, 0.1, 0.1, 0.2, 0.025, 0.025, 0.05]

# create noise confusion matrix
error = 0.2  # from 0 (no error) to 1 (max error)
# I = np.eye(len(pmf_vec)) * (1 - error)
# piecewise_error = error / (len(pmf_vec) - 1)
# for r in range(len(pmf_vec)):
#     for c in range(len(pmf_vec)):
#         if r == c: continue
#         I[r,c] = piecewise_error
# print(I)

cnm = ut.confusion_noise_matrix([0.1, 0.5, 0.4], 0.3)
print(cnm)

confused_vec = ut.confuse_PMF([0.1, 0.5, 0.4], 0.3)
print(confused_vec)
print(sum(confused_vec))


# ut.set_material_dir_alphas(materials, PMF)
    

exit()

# premade observation (just like setting the same seed...)
ob_precise = np.array([994.6198287170249, 994.2129416188493, 998.6818561179906, 988.299465341037, 1006.8872998435216,
                     996.3084517630922, 999.5486258529284, 997.4123064440918, 998.1926106794725, 999.447422388993])

ob = np.array([917.6170611614461, 973.2323972485249, 1063.6159604187371, 1046.943399077979,
               1033.7699116847784, 987.5210851403585, 1010.1465176042838, 949.503407770969,
               1086.638588055573, 1231.4332372650315, 921.5288869128243, 709.1463607429466,
               900.5172111553644, 1158.4844734001979, 1119.898001196967, 1200.403742765686,
               875.1849438726385, 887.2441101392271, 1009.2307860991181, 799.0985030135088,
               1159.3886607467596, 846.5394344570578, 1098.37881976902, 1035.7107362565787,
               999.4430534831018, 998.0825379915192, 1006.3814474292918, 997.5514200225701,
               1092.3884079060626, 854.3105954222207, 916.8133588234401, 865.9673794712753,
               824.7378810367892, 877.1755714475967, 1048.0470070916128, 1014.7037735381675,
               1112.2935385755939, 953.3288073865842, 1208.8618788067865, 1045.7044172193582,
               1104.8968798189132, 972.8411860414091, 987.3828277124911, 1053.0864056059363,
               855.6711552225519, 716.9299313084961, 893.9983553703589, 1060.9383400420245,
               794.0870099416602, 920.6491254403616])

ob2 = np.array([956.5648724381483, 1220.5930082725456, 1218.6786088973786, 1100.4053897878878,
                1038.6186399174856, 1073.7368575896242, 1149.0732028150799, 906.4166131597609,
                1117.5829044782104, 874.6119332250987, 936.224849754659, 1090.7105195800302,
                857.1319299774032, 985.9931279811334, 913.8245104140315, 974.4380629469404,
                720.1410894539276, 822.8466895490153, 930.0122765402083, 1092.7462431758581,
                982.6364317209784, 1000.2845915896811, 1068.8222711102285, 912.0463656990948,
                1028.362732380729, 919.4633481934384, 827.2330505879393, 960.9100206244899])
                # 1057.3805862405059, 1033.8589050999801, 998.816950552118, 1239.2365265937726,
                # 1041.2912160308779, 1097.8736005937346, 1223.8143338497953, 870.5914676838752,
                # 896.1211789795046, 1174.3712225122931, 920.1937264758938, 1002.9683230303331,
                # 1106.931596942435, 1089.070639129317, 1175.488618198111, 1149.5644137033469,
                # 1106.9392669705737, 922.7291285752808, 1079.4862667793218, 1031.4271994506867,
                # 867.3734540059544, 1141.7299046476853])
# ob_vol = norm.rvs(loc=10000, scale=1000, size=15)

with pm.Model() as mini:

    mat_cat = pm.Dirichlet('matCat', np.ones(2))

    mass_meas_error = 10
    # mass_prior = pm.Normal('mass_mu', mu=500, sigma=150)
    mass_prior = pm.Uniform(lower=0, upper=2000)
    obsv_mass = pm.Normal('obs_mass', mu=mass_prior, sigma=mass_meas_error, observed=ob_precise)

    volume = pm.NormalMixture('volu', w=mat_cat, mu=VOLUME_MUS, sigma=VOLUME_SIGS, observed=None)

    currently_observed = 'mass'
    density_obs = None
    if currently_observed == 'mass':
        density_obs = ob_precise / 9800  # argmax_mean_volume
    # elif currently_observed == 'volume':
    #     density_obs = argmax_mean_mass / ob_vol

    print("density_obs: ", density_obs)
    density = pm.NormalMixture('dens', w=mat_cat, mu=DENSITY_MUS, sigma=DENSITY_SIGS, observed=density_obs)

    # the inference
    step = pm.Metropolis()
    trace = pm.sample(5000, step=step)
    burn_in = 500  # skips the n iterations, where the MCMC is just wandering
    chain = trace[burn_in:]
    ppc = pm.sample_posterior_predictive(chain, var_names=['matCat'], model=mini)
    names = ["A", "B"]
    for i, samp in enumerate(ppc['matCat'].transpose()):
        prob = np.mean(samp)
        print("Prob for {} is {}.".format(names[i], prob))

    pm.traceplot(trace)
    pm.plot_posterior(chain)
    pm.autocorrplot(chain)
    plt.show()

    # saved_trace = pm.save(trace)

with mini:
    obsv_mass = pm.Normal('obs_mass2', mu=mass_prior, sigma=mass_meas_error, observed=ob2)
    step = pm.Metropolis()
    trace = pm.sample(5000, step=step)
    burn_in = 500  # skips the n iters, where the MCMC is just wandering
    chain = trace[burn_in:]
    ppc = pm.sample_posterior_predictive(chain, var_names=['matCat'], model=mini)
    names = ["A", "B"]
    for i, samp in enumerate(ppc['matCat'].transpose()):
        prob = np.mean(samp)
        print("Prob for {} is {}.".format(names[i], prob))

    pm.traceplot(trace)
    pm.plot_posterior(chain)
    pm.autocorrplot(chain)
    plt.show()








# az.style.use("arviz-darkgrid")
#
# np.random.seed(123)
# n_experiments = 20
# theta_real = 0.35
# data = stats.bernoulli.rvs(p=theta_real, size=n_experiments)
# print("this is the data", data)
# # np.array([1, 0, 0, 0])
#
# with pm.Model() as first_model:
#     theta_real = 0.35
#     theta = pm.Beta("theta", alpha=1, beta=1)
#     y = pm.Bernoulli('y', p=theta, observed=data)   # likelihood
#     start = pm.find_MAP()
#     step = pm.Metropolis()
#     trace = pm.sample(1000, step=step, start=start)
#     burnin = 500  # skips the n iters, where the MCMC is just wandering
#     chain = trace[burnin:]
#
#     # this lacks plotting of the lines because of some difficult lately updated
#     # syntax...
#     # pm.traceplot(trace, varnames=['theta'], lines={'theta':theta_real})
#     pm.plot_posterior(chain, ref_val=theta_real)
#     plt.show()

# np.random.seed(123)
#
# mean = 500
# std = 101
# ax = np.linspace(0, 1000, 1000)
# y = norm.pdf(ax, mean, std)
# mmd = std/4
# rand_means = norm.rvs(loc=mean, scale=mmd, size=100)
# plt.plot(ax, y, color='tab:blue', lw=2)
#
# for mn in rand_means:
#     y_n = norm.pdf(ax, mn, std)
#     plt.plot(ax, y_n, color="grey", lw=1.5, alpha=0.1)
#
# plt.show()
#
# exit()









#
# ax = np.linspace(-200, 1000, 1200)
# y_1 = norm.pdf(ax, 100, 100)
# y_2 = norm.pdf(ax, 300, 50)
#
# y_mix = y_1 * 0.5 + y_2 * 0.5
#
# y_trun = copy.deepcopy(y_mix)
# y_trun[:200] = 0.00000000001
# y_trun = y_trun/sum(y_trun)
# # print(y_trun)
# mix_ent = - sum(1 * y_mix * np.log2(y_mix))
# print("mix_ent: ", mix_ent)
# y_trun = np.where(y_trun > 0, y_trun, 10 ** -10)
# trun_ent = - sum(1 * y_trun * np.log2(y_trun))
# print("trun_ent: ", trun_ent)
#
# plt.plot(ax, y_mix, label="Whole H(X) = {:.5f}".format(mix_ent), lw=2)
# plt.plot(ax, y_trun, label="Truncated H(X) = {:.5f}".format(trun_ent), lw=2, ls="--")
# plt.legend()
# plt.title("Before Measurement")
# plt.show()
#
# print("-------")
#
# super_sharp_meas = norm.pdf(ax, 50, 800)
#
# post = y_trun * super_sharp_meas
# post = post / sum(post)
#
# post[:200] = 0.00000000001
# post = post / sum(post)
#
#
# print("mix_ent: ", mix_ent)
# post = np.where(post > 0, post, 10 ** -10)
# trun_ent = - sum(1 * post * np.log2(post))
# print("trun_ent: ", trun_ent)
#
# plt.plot(ax, y_mix, label="Whole H(X) = {:.5f}".format(mix_ent), lw=2)
# plt.plot(ax, post, label="Truncated H(X) = {:.5f}".format(trun_ent), lw=2, ls="--")
# plt.plot(ax, super_sharp_meas, label="Measurement", lw=1, ls=":")
# plt.legend()
# plt.title("After Measurement")
# plt.show()


#
# middle = int((len(ax)-1)/2)
# y_meas = norm.pdf(ax, middle, 100)
#
# # the emulation
# y_wide = np.convolve(y_mix, y_meas, mode='same')
# post = y_trun * y_wide
# post = post / sum(post)
#
# y_wide[:200] = 10 ** -10
# print("---------------\n")
#
# mix_ent = - sum(1 * y_mix * np.log2(y_mix))
# print("mix_ent: ", mix_ent)
#
# y_wide = np.where(y_wide > 0, y_wide, 10 ** -10)
# trun_ent = - sum(1 * y_wide * np.log2(y_wide))
# print("trun_ent: ", trun_ent)
#
# plt.plot(ax, y_mix, label="Whole", lw=2)
# plt.plot(ax, y_wide, label="Truncated", lw=2, ls="--")
# plt.legend()
# plt.show()



