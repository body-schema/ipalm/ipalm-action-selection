Cleaning the previous 'meas.json' file.
----------------------
| ITERATION NUMBER: 0 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0490 | Cond. likelihood: None
Material:   ceramic | Probabilty: 0.3500 | Cond. likelihood: None
Material:   plastic | Probabilty: 0.1000 | Cond. likelihood: None
Material: aluminium | Probabilty: 0.5000 | Cond. likelihood: None
Material:     joker | Probabilty: 0.0010 | Cond. likelihood: None
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 10.414960097409153 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.585461851850611   |
| The average sampled Entropy is: 1.0233496511895066   |
| -OUT- Discrete Information Gain: 0.5621122006611043   |
| -info- AVERAGE sampled Information Gain: 0.562112200661104 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 10.414960097409153 |
| This is the Emulated Entropy for density: 9.806238954684412 |
| Argmax mean: 2302 (no offset)|
| -x- Diff Information Gain: 0.6087211427247414     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.0613755325850995 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.585461851850611   |
| The average sampled Entropy is: 1.3583750691235648   |
| -OUT- Discrete Information Gain: 0.2270867827270462   |
| -info- AVERAGE sampled Information Gain: 0.2270867827270461 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.0613755325850995 |
| This is the Emulated Entropy for elasticity: 6.819421225502525 |
| Argmax mean: 31 (no offset)|
| -x- Diff Information Gain: 0.24195430708257426     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  1.1708333433858458
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2699 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.3526 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0091 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.6343 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0039 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 1 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.3526 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0091 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.6343 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0039 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.70496916485969 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0398661320954854   |
| The average sampled Entropy is: 0.9183388035794318   |
| -OUT- Discrete Information Gain: 0.12152732851605363   |
| -info- AVERAGE sampled Information Gain: 0.12152732851605329 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.70496916485969 |
| This is the Emulated Entropy for density: 9.622091402839947 |
| Argmax mean: 2306 (no offset)|
| -x- Diff Information Gain: 0.08287776201974317     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 7.0613755325850995 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 1.0398661320954854   |
| The average sampled Entropy is: 0.9314339719339307   |
| -OUT- Discrete Information Gain: 0.10843216016155466   |
| -info- AVERAGE sampled Information Gain: 0.10843216016155473 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 7.0613755325850995 |
| This is the Emulated Entropy for elasticity: 6.819421225502525 |
| Argmax mean: 31 (no offset)|
| -x- Diff Information Gain: 0.24195430708257426     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.3503864672441289
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 162 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.3155 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0055 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.6776 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0014 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 2 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.3155 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0055 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.6776 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0014 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.70496916485969 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.9605237281357512   |
| The average sampled Entropy is: 0.8611642683298519   |
| -OUT- Discrete Information Gain: 0.09935945980589922   |
| -info- AVERAGE sampled Information Gain: 0.0993594598058989 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.70496916485969 |
| This is the Emulated Entropy for density: 9.622091402839947 |
| Argmax mean: 2306 (no offset)|
| -x- Diff Information Gain: 0.08287776201974317     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.804843374294029 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.9605237281357512   |
| The average sampled Entropy is: 0.8929361340924816   |
| -OUT- Discrete Information Gain: 0.06758759404326953   |
| -info- AVERAGE sampled Information Gain: 0.06758759404326958 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.804843374294029 |
| This is the Emulated Entropy for elasticity: 6.691958066577149 |
| Argmax mean: 32 (no offset)|
| -x- Diff Information Gain: 0.11288530771687988     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.1822372218256424
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2688 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.2686 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0004 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.7262 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0048 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 3 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.2686 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0004 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.7262 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0048 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.648444596169053 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8863435688273241   |
| The average sampled Entropy is: 0.786989056224844   |
| -OUT- Discrete Information Gain: 0.09935451260248007   |
| -info- AVERAGE sampled Information Gain: 0.0993545126024799 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.648444596169053 |
| This is the Emulated Entropy for density: 9.607596191268312 |
| Argmax mean: 2310 (no offset)|
| -x- Diff Information Gain: 0.04084840490074093     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.804843374294029 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8863435688273241   |
| The average sampled Entropy is: 0.8226925572859578   |
| -OUT- Discrete Information Gain: 0.06365101154136632   |
| -info- AVERAGE sampled Information Gain: 0.0636510115413656 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.804843374294029 |
| This is the Emulated Entropy for elasticity: 6.691958066577149 |
| Argmax mean: 32 (no offset)|
| -x- Diff Information Gain: 0.11288530771687988     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.1765363192582462
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 171 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.2360 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0003 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.7620 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0017 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 4 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.2360 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0003 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.7620 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0017 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.648444596169053 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8094097762056066   |
| The average sampled Entropy is: 0.7310086926744921   |
| -OUT- Discrete Information Gain: 0.07840108353111452   |
| -info- AVERAGE sampled Information Gain: 0.07840108353111454 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.648444596169053 |
| This is the Emulated Entropy for density: 9.607596191268312 |
| Argmax mean: 2310 (no offset)|
| -x- Diff Information Gain: 0.04084840490074093     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.681089263291646 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.8094097762056066   |
| The average sampled Entropy is: 0.7352166133989612   |
| -OUT- Discrete Information Gain: 0.07419316280664545   |
| -info- AVERAGE sampled Information Gain: 0.07419316280664534 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.681089263291646 |
| This is the Emulated Entropy for elasticity: 6.605672021936567 |
| Argmax mean: 34 (no offset)|
| -x- Diff Information Gain: 0.07541724135507888     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.14961040416172433
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 176 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.2058 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0001 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.7934 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0006 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 5 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.2058 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0001 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.7934 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0006 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.648444596169053 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.7428281614231494   |
| The average sampled Entropy is: 0.6647001865167578   |
| -OUT- Discrete Information Gain: 0.07812797490639167   |
| -info- AVERAGE sampled Information Gain: 0.07812797490639195 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.648444596169053 |
| This is the Emulated Entropy for density: 9.607596191268312 |
| Argmax mean: 2310 (no offset)|
| -x- Diff Information Gain: 0.04084840490074093     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.5909571268723 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.7428281614231494   |
| The average sampled Entropy is: 0.6877906567538103   |
| -OUT- Discrete Information Gain: 0.0550375046693391   |
| -info- AVERAGE sampled Information Gain: 0.05503750466933905 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.5909571268723 |
| This is the Emulated Entropy for elasticity: 6.526514218629828 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.06444290824247201     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.1194804129118111
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 173 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.1784 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0001 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.8212 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0002 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 6 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.1784 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0001 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.8212 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0002 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.648444596169053 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.6809086475726502   |
| The average sampled Entropy is: 0.6347119772396176   |
| -OUT- Discrete Information Gain: 0.04619667033303265   |
| -info- AVERAGE sampled Information Gain: 0.046196670333032694 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.648444596169053 |
| This is the Emulated Entropy for density: 9.607596191268312 |
| Argmax mean: 2310 (no offset)|
| -x- Diff Information Gain: 0.04084840490074093     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.510872494366293 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.6809086475726502   |
| The average sampled Entropy is: 0.6095766172109102   |
| -OUT- Discrete Information Gain: 0.07133203036174007   |
| -info- AVERAGE sampled Information Gain: 0.07133203036174018 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.510872494366293 |
| This is the Emulated Entropy for elasticity: 6.451436975695203 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.05943551867108976     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.13076754903282983
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 179 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.1540 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0001 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.8459 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0001 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 7 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.1540 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0001 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.8459 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0001 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.648444596169053 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.621611703219951   |
| The average sampled Entropy is: 0.5406072080530807   |
| -OUT- Discrete Information Gain: 0.08100449516687025   |
| -info- AVERAGE sampled Information Gain: 0.08100449516687024 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.648444596169053 |
| This is the Emulated Entropy for density: 9.607596191268312 |
| Argmax mean: 2310 (no offset)|
| -x- Diff Information Gain: 0.04084840490074093     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.436118053046115 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.621611703219951   |
| The average sampled Entropy is: 0.6037757702627646   |
| -OUT- Discrete Information Gain: 0.01783593295718633   |
| -info- AVERAGE sampled Information Gain: 0.01783593295718629 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.436118053046115 |
| This is the Emulated Entropy for elasticity: 6.37988511868924 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.05623293435687504     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.12185290006761118
-----------------------------------

Added density to the meas. database.
..........................................................
'Real' measurement mean: 2688 (contains offset=0) and sigma: 540 for property: density
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1263 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.8735 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0003 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 8 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
Material:   ceramic | Probabilty: 0.1263 | Cond. likelihood: 0.00055662
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00005017
Material: aluminium | Probabilty: 0.8735 | Cond. likelihood: 0.00070087
Material:     joker | Probabilty: 0.0003 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.619825455857814 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.5504924880666308   |
| The average sampled Entropy is: 0.5438890611572303   |
| -OUT- Discrete Information Gain: 0.006603426909400412   |
| -info- AVERAGE sampled Information Gain: 0.006603426909400581 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.619825455857814 |
| This is the Emulated Entropy for density: 9.58335577904034 |
| Argmax mean: 2314 (no offset)|
| -x- Diff Information Gain: 0.036469676817473484     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.436118053046115 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.5504924880666308   |
| The average sampled Entropy is: 0.4883566682385389   |
| -OUT- Discrete Information Gain: 0.062135819828091854   |
| -info- AVERAGE sampled Information Gain: 0.06213581982809163 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.436118053046115 |
| This is the Emulated Entropy for elasticity: 6.37988511868924 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.05623293435687504     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.1183687541849669
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 164 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.1080 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.8919 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0001 | Cond. likelihood: 0.00217391
----------------------
| ITERATION NUMBER: 9 |
| MODE NUMBER: 3 |
----------------------

-----------------Current PMF-----------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.1080 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.8919 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0001 | Cond. likelihood: 0.00217391
---------------------------------------------
_____________________
Property:  Density
Measurement:
    Meas. sigma: 540
_____________________

------------------PART A----------------------------
| This is the H(prop) for density: 9.619825455857814 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.49515332836622306   |
| The average sampled Entropy is: 0.47497714383338574   |
| -OUT- Discrete Information Gain: 0.020176184532837316   |
| -info- AVERAGE sampled Information Gain: 0.02017618453283714 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for density: 9.619825455857814 |
| This is the Emulated Entropy for density: 9.58335577904034 |
| Argmax mean: 2314 (no offset)|
| -x- Diff Information Gain: 0.036469676817473484     |
--------------------------------------------

_____________________
Property:  Elasticity
Measurement:
    Meas. sigma: 60
_____________________

------------------PART A----------------------------
| This is the H(prop) for elasticity: 6.365459601580717 |
----------------------------------------------------

---------------------PART B-----------------------
| Discrete Material entropy before: 0.49515332836622306   |
| The average sampled Entropy is: 0.4746578755447954   |
| -OUT- Discrete Information Gain: 0.020495452821427673   |
| -info- AVERAGE sampled Information Gain: 0.02049545282142779 |
--------------------------------------------------

------------------PART F--------------------
| This is the H(prop) for elasticity: 6.365459601580717 |
| This is the Emulated Entropy for elasticity: 6.311631536073324 |
| Argmax mean: 69 (no offset)|
| -x- Diff Information Gain: 0.0538280655073935     |
--------------------------------------------

---------Planned action------------
Property chosen to be measured is :
    >  elasticity
With expected information gain : 
    >  0.07432351832882117
-----------------------------------

Added elasticity to the meas. database.
..........................................................
'Real' measurement mean: 164 (contains offset=100) and sigma: 60 for property: elasticity
..........................................................


CLOSED-FORM solution via value ----------------------------------------
Material:     steel | Probabilty: 0.0000 | Cond. likelihood: 0.00066830
Material:   ceramic | Probabilty: 0.0921 | Cond. likelihood: 0.00528333
Material:   plastic | Probabilty: 0.0000 | Cond. likelihood: 0.00357566
Material: aluminium | Probabilty: 0.9079 | Cond. likelihood: 0.00630783
Material:     joker | Probabilty: 0.0000 | Cond. likelihood: 0.00217391
MODE 3 was chosen --> optimizing was done for hybrid sum of 
 differential and discrete information gain.
The argmax mean for density : 2314
The argmax mean for elasticity : 69

