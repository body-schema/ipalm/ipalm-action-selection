import json
import numpy as np
from bayesNetFramework import BayesNet, json2dict
import re
import utility as ut

objs = json2dict("./crawl-data.json")

# parse the goddamned .txt file and extract gt for each property of each object

object_names = ['bakingtray', 'bottle', 'bowl', 'fork', 'glass', 'hammer', 'kettle', 'knife', 'mug', 'pan', 'plate', 'sodacan', 'spoon', 'wineglass']
prop_list = ['Material', 'Colour', 'Weight', 'Volume', 'Length', 'Width', 'Height', 'Functionality']  # shape?, not very informative
bool_questions = ['button', 'lid', 'fillable', 'movable', 'washable', 'dissasemblable', 'handle']
questions_full_name = ['Does it has a button?', 'Does it has a lid?', 'Could it be filled?', 'Could it be moved?', 'Could it be washed?', 'Could it be dismantled?', 'Does it has a handle?']

ipalm_mat_names = ["ceramic", "metal", "soft-plastic", "hard-plastic", "wood", "paper", "foam"]

# query: p(material|mug)
# mat_names = ["ceramic", "glass", "metal", "other", "plastic", "silicone", "stone", "wood"]
# query_cat = "mug"
# probs = {}

# if query_cat not in object_names:
#     print("Unknown query object.")
#     exit()

# obj_query = None
# prop_query = None
# for obj in objs:
#     if obj["name"] == query_cat:
#         obj_query = obj
#         break
# for prop in obj_query['properties']:
#     # print("prop: ", prop)
#     if prop["prop_name"] == "Material":
#         prop_query = prop
#         break

# n_obs = sum(prop["ground"])
# for i, m in enumerate(mat_names):
#     print("i: {}, mat: {}".format(i, m))
#     probs[m] = prop["ground"][i] / n_obs
    
# print("probs: ", probs)
T = np.zeros([len(object_names), len(ipalm_mat_names)])  # transition matrix category -> material
for i, name in enumerate(object_names):
    mats = ut.mat_given_cat(objs, name)
    print("-------------")
    print("Object name: ", name)
    print("Adjacent probabilities:")
    cum = 0
    for mat in mats:
        cum += mats[mat]
    if cum < 1:
        cum2 = 0
        for mat in mats:
            mats[mat] = mats[mat] / cum
            cum2 += mats[mat]
        print("Summation of mat probs: ", cum2)
    column = []
    for mat in mats:
        print("{}: {:.3f}".format(mat, mats[mat]))
        column.append((mats[mat]))
    if cum == 0:
        print("Summation of mat probs: ", cum)
    print("")
    T[i, :] = np.array(column)  # transition matrix category -> material

np.set_printoptions(precision=3)
C = np.ones([len(object_names), 1]) / len(object_names)  # category
M = np.ones([len(ipalm_mat_names), 1]) / len(ipalm_mat_names)  # material
N = ut.confusion_noise_matrix(len(ipalm_mat_names), 0.2)  # confusion noise matrix
posterior_mat = (C.T @ T @ N).reshape(-1,) * M.reshape(-1,)
posterior_mat_normalized = posterior_mat / sum(posterior_mat)
# print("M: ", M)
# print(C.T @ T @ N)
print("Category vector C:\n{}, \nMaterial vector M:\n{},\nTransition matrix T:\n{},\n\nNoise Matrix N:\n{},\nPosterior Material vector:\n{},\nNormalized Material vector:\n{}".format(
    C, M, T, N, posterior_mat, posterior_mat_normalized))

# print(ut.cat_given_mat(objs, "ceramic"))




# for name in ipalm_mat_names:
#     print("\n--------------------")
#     print("For material: ", name)
#     print(ut.cat_given_mat(objs, name))
#     print()




# for obj in objs:
#     props = obj['properties']
#     for prop in props:
#         if not(len(prop['bins']) == len(prop['ground']) == len(prop['prediction'])):
#             print("problem at prop: {} in obj: {}".format(prop['prop_name'], obj['name']))

# print("Test complete.")

# [out]: 
# {
#     "category":{
#         "metrics":{
#             "precision":0.7,
#             "confidence":0.2
#         },
#         "names": ["name1", "name2", ..., "namen"],
#         "prediction":{
#             "name1": p(name1),
#             "name2": p(name2),
#             ...,
#             "namen": p(namen)
#         }
#     },
#     "material":{
#         "metrics":{
#             "precision":0.7,
#             "confidence":0.2
#         },
#         "names": ["name1", "name2", ..., "namen"],
#         "prediction":{
#             "name1": p(name1),
#             "name2": p(name2),
#             ...,
#             "namen": p(namen)
#         }
#     }
# }
