
def iteration_number(i, vb=True):
    if not vb:
        return
    print("----------------------")
    print("| ITERATION NUMBER: {} |".format(i))
    print("----------------------")
    print()


def pmf(materials, vb=True):
    if not vb:
        return
    longest_str = 0
    for mat in materials:
        name = mat['name']
        if len(name) > longest_str:
            longest_str = len(name)

    print("-----------------Current PMF-----------------")
    for mat in materials:
        padded_mat_name = mat['name'].rjust(longest_str)
        print("| Material: {} | Probabilty: {:.4f} |".format(padded_mat_name, mat['prob']))
    print("---------------------------------------------")
    print()


def planned_action(planned_action, info_gain, vb=True):
    if not vb:
        return
    print("---------Planned action------------")
    print("| Property chosen to be measured is :")
    print("|    > ", planned_action)
    print("| With expected information gain : ")
    print("|    > ", info_gain)
    print("-----------------------------------")
    print()


def mode(mode, argmax_mean_dict, vb=True):
    if not vb:
        return
    if mode == 1:
        print("MODE 1 was chosen --> optimizing was done for discrete information gain \n computed from the PMF.")
    else:
        if mode == 2:
            print("MODE 2 was chosen --> optimizing was done for differential information gain.")
        elif mode == 3:
            print(
                "MODE 3 was chosen --> optimizing was done for hybrid sum of \n differential and discrete information gain.")
        for prop in ['density', 'elasticity', 'friction']:
            print("The argmax mean for {} : {}".format(prop, argmax_mean_dict[prop]))
    print()

def random_action(rand_choice, vb=True):
    if not vb:
        return
    if rand_choice:
        print()
        print("--------------RANDOM--------------")
        print("|  Action is planned RANDOMLY!   |")
        print("--------------RANDOM--------------")
        print()

def forced_action(forced_action, vb=True):
    if not vb:
        return
    print()
    print("------FORCING ACTION------")
    print("|    > {} |".format(forced_action))
    print("------FORCING ACTION------")
    print()