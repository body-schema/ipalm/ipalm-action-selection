from __future__ import print_function

import scipy.io, scipy.signal, scipy.fftpack
# from sklearn.neighbors import BallTree
import copy
import numpy as np
import matplotlib.pyplot as plt
import sys
import os



class ModLine:
    def __init__(self, poi=None, limit=None, *args):
        self.poi = poi
        self.limit = limit
        if len(args) == 1:
            self.mod1 = args[0][0]
            self.mod0 = args[0][1]
        elif len(args) == 2:
            self.mod1 = args[0]
            self.mod0 = args[1]
        if poi is not None and limit is not None:
            self.x0 = self.poi - self.limit
            self.x1 = self.poi + self.limit
            self.y0 = self.x0 * self.mod1 + self.mod0
            self.y1 = self.x1 * self.mod1 + self.mod0

    def get_xy_xy(self):
        return (self.x0, self.y0), (self.x1, self.y1)

    def get_xx_yy(self):
        return (self.x0, self.x1), (self.y0, self.y1)

    def set_xy_xy(self, p1, p2):
        self.x0 = p1[0]
        self.y0 = p1[1]
        self.x1 = p2[0]
        self.y1 = p2[1]


class GrippingSessions:
    def __init__(self, mat_files=None, txt_files=None, npy_files=None):
        self.mat_files = mat_files if mat_files else []
        self.txt_files = txt_files if txt_files else []
        self.npy_files = npy_files if npy_files else []
        self.files = self.mat_files + self.txt_files + self.npy_files
        self.gripper_area = 825e-6
        self.strain_limit = 2.85e5
        self.force_limit = 235
        self.AF_const = self.force_limit / self.gripper_area
        self.last_pos = None
        self.last_cur = None
        self.last_poi = None
        self.last_limit = None
        self.last_id = None
        self.file_num = 0
        self.loaded_grips = 0
        self.grips = []
        self.grips = []
        self.ball_tree = None
        self.window_size = 10
        self.update_filter(self.window_size)
        self.max_cur = 1.0
        self.current_tolerance = 0.90
        self.limit = 0.1
        self.resolution = 10.0
        self.slope = 7.5e4
        self.last_global_mods = []
        self.last_line = None
        self.f1s = [self.get_rising_point]
        self.f2s = [self.get_risen_point, self.get_max_point]
        self.line_40 = None
        # if self.mat_files:
        #     self.load_mats()

    def load_mats(self, mat_files=None):
        # ret = True
        for mat_file in (mat_files if mat_files else self.mat_files):
            self.grips.append(self.mat2grip(mat_file))
            self.file_num += 1
            self.mat_files.append(mat_file)  # e.g. mat_file = "ipalm_control/saved_data/sponge1.mat"
            self.files.append(mat_file)
            # print(self.file_num)
            # print(ret)
        # print("loaded mats")
        # return ret

    @staticmethod
    def mat2grip(mat_file):
        """
        Convert the given .mat file to a grip dictionary.

        :param mat_file: path to .mat file
        :return: grip dictionary with "current", "position", "normal_pos" - relative to l0, "object_id", "closing_speed"
        """
        # print(mat_file)
        try:
            mat = scipy.io.loadmat(mat_file)
        except NotImplementedError as e:
            print("[ERROR] mat2grip: ", )
            print(e)
            return None
        temp = dict()
        temp['current'] = np.array(mat['current']).flatten()  # [[num],[num]]
        temp['position'] = np.array(mat['position']).flatten()
        # temp['filter_current'] = scipy.signal.lfilter(self.b, self.a, temp['current'])
        # print(temp['position'])
        temp['object_width'] = mat['object_width'][0][0]  # [[num]]
        temp['closing_speed'] = mat['closing_speed'][0][0]
        temp['normal_pos'] = temp['object_width'] - (1 - temp['position']) * 85
        temp['object_id'] = mat_file.split("-")[-3]
        temp['young_calc'] = []
        # print(temp['closing_speed'])
        # print(temp['normal_pos'])
        # print(temp['object_id'])
        # self.grips.append(temp)
        # print(self.grips[-1], self.file_num)
        # self.file_num += 1
        return temp

    def load_txts(self, txt_files=None):
        """
        Load txt files and add grip dictionaries to list of grips

        :param txt_files: list, paths to text files
        :return: None
        """
        for txt_file in (txt_files if txt_files else self.txt_files):
            self.grips.append(self.txt2grip(txt_file))
            self.file_num += 1
            self.txt_files.append(txt_file)
            self.files.append(txt_file)

    @staticmethod
    def txt2grip(txt_file):
        """
        Converts txt file to grip dictionary

        :param txt_file: text file to convert to grip dict
        :return: grip dictionary with "current", "position", "normal_pos" - relative to l_0, "object_id", "closing_speed"
        """
        try:
            with open(txt_file, 'r') as txt:
                line1 = txt.readline()
                # print("line1: ", line1)
                ordering = dict()
                order_i = 0
                for el in line1.split():
                    if "position" in el.lower():
                        ordering["position"] = order_i
                        order_i += 1
                        # print(order_i)
                    elif "current" in el.lower():
                        ordering["current"] = order_i
                        order_i += 1
                        # print(order_i)
                    elif "time" in el.lower():
                        ordering["time"] = order_i
                        order_i += 1
                        # print(order_i)

                data = np.loadtxt(txt_file, skiprows=1)
                temp = dict()
                temp['position'] = data[..., ordering["position"]]
                temp['current'] = data[..., ordering["current"]]
                # temp['filter_current'] = scipy.signal.lfilter(self.b, self.a, temp['current'])
                temp['object_width'] = int(txt_file.split('-')[-2])
                temp['normal_pos'] = temp['object_width'] - (1.0 - temp['position']) * 85
                temp['closing_speed'] = float(txt_file.split('-')[-1].split('.')[0]) / 10.0  # raw: e.g. 05, 10, 00
                temp['object_id'] = txt_file.split("-")[-3]
                # print(temp['closing_speed'], txt_file.split('-')[-1].split('.')[0])
                # print(temp['object_id'], temp['object_width'], temp['normal_pos'])
                # temp['object_width'] = mat['object_width'][0][0]  # [[num]]
                # temp['closing_speed'] = mat['closing_speed'][0][0]
                # temp['object_id'] = mat_file.split("-")[-3]
                temp['young_calc'] = []
                # print(temp['normal_pos'])
                # print(temp['object_id'])
                # print(self.grips[-1], self.file_num)
                # self.file_num += 1
                return temp
                # print(temp["position"])
                # lines = txt.readlines()
                # print("lines: ", lines)

        except IOError as e:
            print("[ERROR] txt2grip: ", )
            print(e)
            return None

    def load_npys(self, npy_files):
        """
        Load npy files in npy_files

        :param npy_files: list of numpy files
        :return: None
        """
        for npy_file in (npy_files if npy_files else self.npy_files):
            self.npy_files.append(npy_file)
            self.grips.append(self.npy2grip(npy_file))
            self.file_num += 1
            self.npy_files.append(npy_file)
            self.files.append(npy_file)
            # print(self.files)

    def npy2grip(self, npy_file):
        """
        Convert given .npy file to grip dictionary

        :param npy_file: path to .npy file
        :return: file name
        """
        try:
            np_data = np.load(npy_file, allow_pickle=True)
            # print(np_data)
            # print("load npy")
            # exit()
            temp = dict()
            # print(np.array(np_data[0]))
            temp['translation'] = np.ndarray.flatten(np.array(np_data[0]))
            temp['rotation'] = np.ndarray.flatten(np.array(np_data[1]))
            if max(np_data[2]) > 11:
                temp['position'] = np.array(np_data[2]) / 100.0  # percent in [%]
            else:
                temp['position'] = np.array(np_data[2])  # percent in [1]

            temp['current'] = np.array(np_data[3])
            temp['filter_current'] = self.filter_current(temp['current'])
            # temp['object_id'] = npy_file.split("-")[-3]
            # self.file_num += 1
            # self.grips_npy.append(temp)
            return temp
        except (IOError, ValueError) as e:
            print("[ERROR] npy2grip:", e)
            return None

    def load_all(self, mypath=os.getcwd(), extensions=None):
        """
        Load all items in mypath with given extensions.

        :param mypath: path to folder with
        :param extensions: extension to read, otherwise all npy txt mat
        :return:
        """
        # print(os.listdir(mypath))
        # print(mypath)
        if extensions is None:
            tempmat = [os.path.join(mypath, f) for f in os.listdir(mypath)
                       if (os.path.isfile(os.path.join(mypath, f)) and ".mat" in f and f not in self.mat_files)]
            # print(temp)

            self.load_mats(tempmat)
            # print(temp)
            # print("[INFO] load_all: loaded all mats")
            print("loaded mats")

            temptxt = [os.path.join(mypath, f) for f in os.listdir(mypath)
                       if (os.path.isfile(os.path.join(mypath, f)) and ".txt" in f and f not in self.txt_files)]
            # print(temp)
            self.load_txts(temptxt)
            # print(temp)
            print("loaded txts")
            # print("[INFO] load_all: loaded all txts")

            tempnpy = [os.path.join(mypath, f) for f in os.listdir(mypath)
                       if (os.path.isfile(os.path.join(mypath, f)) and ".npy" in f and f not in self.npy_files)]
            # print(temp)
            self.load_npys(tempnpy)
            # print(temp)
            # print("[INFO] load_all: loaded all txts")
            print("loaded npys")
        elif ".mat" in extensions:
            tempmat = [os.path.join(mypath, f) for f in os.listdir(mypath)
                       if (os.path.isfile(os.path.join(mypath, f)) and ".mat" in f and f not in self.mat_files)]
            # print(temp)

            self.load_mats(tempmat)
            # print(temp)
            # print("[INFO] load_all: loaded all mats")
            print("loaded mats")
        if ".txt" in extensions:
            temptxt = [os.path.join(mypath, f) for f in os.listdir(mypath)
                       if (os.path.isfile(os.path.join(mypath, f)) and ".txt" in f and f not in self.txt_files)]
            # print(temp)
            self.load_txts(temptxt)
            # print(temp)
            print("loaded txts")
            # print("[INFO] load_all: loaded all txts")
        if ".npy" in extensions:
            tempnpy = [os.path.join(mypath, f) for f in os.listdir(mypath)
                       if (os.path.isfile(os.path.join(mypath, f)) and ".npy" in f and f not in self.npy_files)]
            # print(temp)
            self.load_npys(tempnpy)
            # print(temp)
            # print("[INFO] load_all: loaded all txts")
            print("loaded npys")

    def get_lin_pol(self, idx=-1, poi=0.4, limit=0.05, pos=None, cur=None):
        """


        :param idx: id of grip to use TODO generalize for object name also
        :param poi: point of interest in the strain interval (0,1)
        :param limit: area around which to calculate the modulus
        :return: youngs modulus in specific strain/stress coordinates
        """
        pol1 = None
        pol0 = None
        if idx != -1:
            self.last_poi = poi
            self.last_limit = limit
            self.last_id = idx  ## HERE ERROR not "last_pos", but regular "position"
            # self.last_pos = self.grips[idx]['normal_pos'][(self.grips[idx]['normal_pos'] >= 0)]
            self.last_pos = self.grips[idx]['position']
            # print(self.grips[idx])
            cur1 = self.grips[idx]['current']

            # self.last_pos = self.last_pos / self.grips[idx]['object_width']
            cur1 = cur1

            # window_size = 10
            # b = (1.0 / window_size) * np.ones(window_size)
            # a = 1
            if cur1.any():
                self.last_cur = cur1  # scipy.signal.lfilter(self.b, self.a, cur1)

                (pol1, pol0) = np.polyfit(
                    self.last_pos[(self.last_pos >= poi - limit) & (self.last_pos <= poi + limit)],
                    self.last_cur[(self.last_pos >= poi - limit) & (self.last_pos <= poi + limit)], 1)

                # print("Young's modulus is %.4f" % pol1)
                # uncomment if you want to save grips
                # temp = {'poi': poi, 'limit': limit, 'youngs_module': pol1}
                # self.grips[idx]['young_calc'].append(temp)
                # print(self.grips[idx]['young_calc'])
            else:
                pass
                # print("[ERROR] get_lin_pol: received empty current vector for polyfit.")
        elif pos is not None and cur is not None:

            self.last_poi = poi
            self.last_limit = limit
            self.last_id = idx
            self.last_pos = pos[(pos >= 0)]
            # print(self.grips[idx])
            # print(cur)
            self.last_cur = cur  # self.filter_current(cur=cur)

            # print("last_cur: "+str(self.last_cur))
            last_cur = cur[(pos >= 0)]

            # self.last_pos = self.last_pos / self.grips[idx]['object_width']
            last_cur = last_cur

            # window_size = 10
            # b = (1 / window_size) * np.ones(window_size)
            # a = 1
            if last_cur.any():
                # last_cur = scipy.signal.lfilter(self.b, self.a, last_cur)
                temp_pos = self.last_pos[(self.last_pos >= poi - limit) & (self.last_pos <= poi + limit)]
                temp_cur = last_cur[(self.last_pos >= poi - limit) & (self.last_pos <= poi + limit)]
                if temp_pos.any() and temp_cur.any():
                    # print(self.last_pos[(self.last_pos >= poi - limit) & (self.last_pos <= poi + limit)])
                    # print(self.last_cur[(self.last_pos >= poi - limit) & (self.last_pos <= poi + limit)])
                    pol1, pol0 = np.polyfit(temp_pos, temp_cur, 1)
                    self.first_rise_1 = pol1
                    self.first_rise_0 = pol0

                    # print("Young's modulus is %.4f" % pol1)
                    # uncomment if you want to save grips
                    # temp = {'poi': poi, 'limit': limit, 'youngs_module': pol1}
                    # self.grips[idx]['young_calc'].append(temp)
                    # print(self.grips[idx]['young_calc'])
                else:
                    # print("last_cur: ", last_cur)
                    # print("last_pos: ", self.last_pos)
                    # print("poi-limit: ", poi-limit, "poi+limit: ", poi+limit)
                    """fitting outside of range of data"""
                    print("[ERROR] get_lin_pol: received empty vector for polyfit.")
            else:
                print("[ERROR] get_lin_pol: received empty current vector for polyfit.")

        else:
            assert pos is not None
        return pol1, pol0

    def filter_current(self, cur):
        """

        :param cur: current to filter
        :return: filtered current using the window filter
        """
        return scipy.signal.lfilter(self.b, self.a, cur)

    def display_raw(self, idx=None, pos=None, cur=None, block=True, keep=False, color=(1, 0, 0), s=1):
        """

        :param idx: id if you want to display known object
        :param pos: position to display as is
        :param cur: current to display as is
        :param block: bool, block execution of script
        :param keep: bool, show window
        :param color: color of data
        :param s: size of dots, area
        :return: None
        """
        if idx:
            # print(self.grips[idx]['object_id'])
            # plt.scatter(self.grips[idx]['normal_pos'], self.grips[idx]['current'], edgecolors="red", s=1)
            plt.scatter(self.grips[idx]['position'], self.grips[idx]['current'], edgecolors=color, s=s,
                        label="Raw data")
            # plt.scatter(self.grips[idx+7]['normal_pos'], self.grips[idx+7]['current'], edgecolors="blue", s=10)
            plt.ylabel('Current [A]')
            plt.xlabel('Strain [1]')
            if not keep:
                plt.show(block=block)
        else:
            plt.scatter(pos, cur, edgecolors=color, s=s, label="Raw data")
            # plt.scatter(self.grips[idx+7]['normal_pos'], self.grips[idx+7]['current'], edgecolors="blue", s=10)
            plt.ylabel('Current [A]')
            plt.xlabel('Strain [1]')
            if not keep:
                plt.show(block=block)

    def display_last_grip(self, keep=False, block=True):
        """
        Display the last get_global_modulus estimate with the last data.

        :param keep: bool, True to show
        :param block: bool, True to block script execution
        :return:
        """
        # print(self.last_pos)
        plt.title(s="Global linear approximation of deformation")
        if self.last_cur is None or self.last_pos is None:
            print(self.last_cur)
            print("[ERROR] display_last_grip: No valid last grip")
            return
        if self.last_line:
            try:
                plt.scatter([self.last_line.x0, self.last_line.x1], [self.last_line.y0, self.last_line.y1],
                            color=(0.0, 0.7, 0.7), label="Object start & deformation", s=20)
            except TypeError as e:
                print(e)
        plt.scatter(self.last_pos, self.last_cur, edgecolors="red", s=3, label="Processed data")
        plt.plot([self.last_poi] * 2, [0, 1], color=(0.3, 0.9, 0.3), linestyle="--")
        plt.plot([self.last_poi + self.last_limit] * 2, [0, 1], color=(0.8, 0.9, 0.0), linestyle="--")
        plt.plot([self.last_poi - self.last_limit] * 2, [0, 1], color=(0.8, 0.9, 0.0), linestyle="--", )
        slope_x0 = self.last_poi - self.last_limit
        slope_x1 = self.last_poi + self.last_limit
        plt.plot([slope_x0, slope_x1], [(self.first_rise_1 * slope_x0 + self.first_rise_0),
                                        (self.first_rise_1 * slope_x1 + self.first_rise_0)], color=(0.1, 0.1, 0.9),
                 label="Rise threshold")
        # plt.plot([self.last_pos[self.last_cur == np.max(self.last_cur)], 1], [self.current_tolerance] * 2, color=(0.8, 0.0, 0.3), label="Maximum current tolerance")
        plt.plot([0, 1], [self.current_tolerance * np.max(self.last_cur)] * 2, color=(0.8, 0.0, 0.3), linestyle="--",
                 label="Top current threshold")

        if self.line_40:
            # slope_x0 = self.line_40[0] - self.line_40[1]
            # slope_x1 = self.line_40[0] + self.line_40[1]

            plt.plot([self.line_40.x0, self.line_40.x1], [self.line_40.y0, self.line_40.y1], color=(0.6, 0.9, 0.33),
                     label="40% modulus")
        plt.ylabel('Current [A]')
        plt.xlabel('Position [1]')
        plt.legend()
        if not keep:
            plt.show(block=block)

    def get_rising_point(self, pos, cur, slope=10e4, resolution=500.0, limit=0.05):
        """
        Gets the point where the current starts rising, taken as the beginning of object

        :param pos: position, cleaned, normalized
        :param cur: current, cleaned, normalized
        :param slope: minimum slope to detect first rise
        :param resolution: number of steps to approx.
        :param limit: one sided pos limit in which to include (pos, cur) points
        :return: (x, y), point where the current seems to rise
        """
        step = 1.0 / resolution
        temp_pos = pos[(pos >= step)]
        temp_cur = cur[(pos >= step)]
        slope = slope / self.AF_const
        # print("temp_pos: "+str(temp_pos))  # python2 OK
        for i in range(int(np.ceil(resolution))):
            poi = limit + i * step
            # check_pos = temp_pos[(temp_pos <= poi)]
            poi_m_lim = poi - limit
            # print("len(temp_pos[(temp_pos <= poi)]): "+str(len(temp_pos[(temp_pos <= poi)]))+"  len(temp_pos[(temp_pos >= poi_m_lim)]): "+str(len(temp_pos[(temp_pos >= poi_m_lim)])))  # OK
            # print(temp_pos)
            if len(temp_pos[(temp_pos <= poi)]) > 2 and len(temp_pos[(temp_pos >= poi_m_lim)]) > 5:
                # if len(temp_pos[(temp_pos <= poi)]) > 20:  # TODO fine tune the 20 to get accurate results
                if temp_pos.any() and temp_cur.any():
                    modul = self.get_lin_pol(pos=temp_pos, cur=temp_cur, poi=poi, limit=limit)  # first derivation

                    # print("modulus: "+str(modul))
                    poi_current = np.average(temp_cur[
                                                 (temp_pos >= poi - limit) & (
                                                         temp_pos <= poi + limit)])
                    # print("poi: ", poi, "limit: ", limit, "average current at current pos: ",
                    #       np.average(cur[(pos >= 0.05) & (pos <= poi + limit)]))
                    if temp_pos.any() and temp_cur.any() and modul[0] is not None and modul[0] > slope and np.average(
                            temp_cur[(temp_pos <= poi)]) < poi_current:
                        self.first_rise_0 = modul[1]
                        self.first_rise_1 = modul[0]
                        print(modul)
                        return poi, poi_current, modul
                    # else:
                    #     pass
                    #     print("[ERROR] get_first_point: cannot find slope: ", " pos.any():", temp_pos.any(), " cur.any():",
                    #           temp_cur.any(), " modul:", modul)
        return None

    @staticmethod
    def get_max_point(pos, cur):
        """
        Get point, where current reaches maximum.

        :param pos: position, cleaned
        :param cur: current, cleaned
        :return: (x, y), where current is max
        """
        poi_i = np.argmax(cur)
        return pos[poi_i], cur[poi_i]

    def density_max_point(self, pos, cur, fp, resolution, density=5):
        """
        Tries to determine point where current stops rising using density.

        :param pos: position, cleaned
        :param cur: current, cleaned
        :param fp: point of begin of rising current
        :param resolution: number of steps in which to split the current vertically (to calculate density)
        :param density: number of points which are needed to consider that the current is continuous
        :return: point, where current seems to stop rising
        """

        def select_pc(p, c, res, dens=5):
            sel_cur = np.array([])
            sel_pos = np.array([])
            focus = np.max(c)
            step = 1.0 / float(res)
            area = step * 2.0
            init_density = (0,) * dens
            while sel_cur.shape[0] == 0 or sel_cur.shape[0] >= init_density:
                # print("Step: ", step, "Area: ", area)
                while sel_cur.shape[0] == 0 or sel_cur.shape[0] >= init_density:
                    sel_cur = np.array([])
                    tmp_foc = focus
                    # print("Trying density: ", density)
                    while sel_cur.shape[0] < dens:
                        sel_cur = c[(tmp_foc - area < c) & (c < tmp_foc + area)]
                        sel_pos = p[(tmp_foc - area < c) & (c < tmp_foc + area)]
                        tmp_foc -= step
                    # print("Number of current values in range: ", sel_cur.shape[0])
                    dens = dens - 1
                    if dens < 1:
                        break
                step *= 0.75
                area *= 0.75
                dens = len(init_density)

            return sel_pos[np.argmax(sel_cur)], np.max(sel_cur)

        sp = self.get_risen_point(pos=pos, cur=cur, rising_pos=fp[0])
        globm = self.AF_const * (sp[1] - fp[1]) / (sp[0] - fp[0])
        locm = self.AF_const * fp[2][0]
        ret = max(globm, locm)

        while ret < 5e5 or 2 * locm > globm:  # TODO 5e5 - unhardcode
            sp = select_pc(p=pos, c=cur, res=resolution, dens=density)
            globm = self.AF_const * (sp[1] - fp[1]) / (sp[0] - fp[0])
            ret = max(globm, locm)
            # print(density)
            # print("local modulus: ", locm, "  global modulus: ", globm, "  global modulus is big enough: ",
            #       2 * locm < globm, "  return is big enough: ", ret > 5e5)
            density -= 1
            if density < 1:
                break
        return sp

    def get_risen_point(self, pos, cur, fp=None, resolution=50, option=0, density=5):
        """
        Function to get the second point used in further calculations for the youngs modulus

        :param pos: position, cleaned
        :param cur: current, cleaned
        :param fp: point of begin of rising current, option 1
        :param resolution: number of steps in which to split the current vertically (to calculate density), option 1
        :param density: number of points which are needed to consider that the current is continuous, option 1
        :return: point, where current seems to stop rising

        """
        if option == 0:
            res_pos = 1.0
            res_cur = 1.0
        elif option == 1:
            res_pos, res_cur = self.get_max_point(pos, cur)
        elif option == 2:
            res_pos, res_cur = self.density_max_point(pos, cur, fp, resolution, density)
        else:
            print("Option ", option, " is invalid")
            res_pos = None
            res_cur = None
        return res_pos, res_cur

    def get_global_modulus(self, pos, cur, limit=0.1, resolution=10, slope=7.5e4, poi_gl=0.40, option=0, local_limit=True):
        """
        self.limit = 0.1
        self.resolution = 10.0
        self.slope = 7.5e4
        Returns None when object has not been squished at least 40% of l_0.

        :param pos: position, cleaned
        :param cur: current, cleaned
        :param limit: one sided limit to include when interpolating
        :param resolution: number of steps in which to split the the min & max position
        :param slope: minimum modulus/slope required to say that the current is rising
        :param poi_gl: point of interest where to calculate modulus in percentage between first rising and second detected rising & risen point
        :param option: 0,1,2 - method to get the second point; \n
        0 = closed gripper position (global), 1 = point of max current, 2 = experimental
        :param local_limit: bool, choose if the limit size should be chosen as percentage of open gripper aperture or of object size, \n
        e.g. local=True: 0.05*(1 - (obj_begin=0.6) = 0.05*0.4) vs. local=False: 0.05*1.0
        :return: modulus or None, None if cannot find modulus or deformation doesn't reach 40% of l_0 or 1st/2nd point
        """
        fp = self.get_rising_point(pos=pos, cur=cur, slope=slope, resolution=resolution, limit=limit)
        # print(pos)
        # print(cur)
        # sp = self.get_max_point(pos, cur)
        sp = self.get_risen_point(pos=pos, cur=cur, option=option)
        if fp is None or sp is None:
            return None
        poi_local = fp[0] + (sp[0] - fp[0]) * poi_gl  # take point 40% between first rise and max point/end
        self.last_line = ModLine()
        self.last_line.set_xy_xy(fp, sp)
        if local_limit:
            limit_local = limit * (sp[0] - fp[0])
            if len(pos[(poi_local - limit_local < pos) & (pos < poi_local + limit_local)]) > 2:  # hardcoded
                ret = self.get_lin_pol(poi=poi_local, limit=limit_local, pos=pos, cur=cur)
                self.line_40 = ModLine(poi_local, limit_local, ret)
                print("linpol with ", len(pos[(poi_local - limit_local < pos) & (pos < poi_local + limit_local)]),
                      "points", ret)
            else:
                print("not enough points: ",
                      len(pos[(poi_local - limit_local < pos) & (pos < poi_local + limit_local)]))
                ret = None
        else:
            ret = self.get_lin_pol(poi=poi_local, limit=limit, pos=pos, cur=cur)
            self.line_40 = ModLine(poi_local, limit, ret)
        return ret

    def get_fp_sp(self, pos, cur):
        """

        :param pos: np array, usu. cleaned
        :param cur: np array, usu. cleaned current
        :return: (p1, p2), first point of rising and point where current reaches maximum
        """
        fp = self.get_rising_point(pos=pos, cur=cur, slope=self.slope, resolution=self.resolution, limit=self.limit)
        sp = self.get_max_point(pos=pos, cur=cur)

        self.last_line = ModLine()
        self.last_line.set_xy_xy(fp, sp)
        if fp is None or sp is None:
            return None
        return fp, sp

    @staticmethod
    def normalized_to_max(arr):
        """

        :param arr: array of input (current) values
        :return: arr: arr / np.max(arr)
        """
        return arr / np.max(arr)

    @staticmethod
    def cut_top_n_side(arr_pos, arr_cur, val):  # TODO maybe cut top and the ones to the left of the last point cut off
        """

        :param arr_pos: array of positions to match arr_cur
        :param arr_cur: array of currents from which to cut > val
        :param val: value above which to cut off
        :return: pos and cur arrays, where cur values above val and pos values from the middle of the corresponding cur values are cut off and with max value 1
        """
        max_arr = arr_pos[arr_cur > val]
        if max_arr.any():
            mp = (min(max_arr) + max(max_arr)) / 2
        else:
            mp = 1.01
        ret_cur = arr_cur[(arr_cur <= val) & (arr_pos < mp)]
        ret_pos = arr_pos[(arr_cur <= val) & (arr_pos < mp)]
        return ret_pos, ret_cur

    def update_filter(self, window_size):
        """

        :param window_size: for filtering, usually 1-10, amount of smoothing
        :return: None
        """
        if window_size < 1:
            window_size = 1
        self.window_size = window_size
        self.b = (1.0 / self.window_size) * np.ones(int(self.window_size))
        self.a = 1.0

    def init_clean_data(self, max_cur=1.0, limit=0.05, resolution=500, slope=8e4, window_size=1, closing_speed=0.055,
                        filter=False, show=False, block=False):
        """

        :param max_cur: 0.0-1.0, maximum current used during grasping/measurement
        :param limit: usu. 0.05, one-sided limit to use when linearly interpolating
        :param resolution: usu. 500, number of steps to take along the whole range of positions
        :param slope: usu. 80-100k, minimum slope required to detect initial rise in current
        :param window_size: usu. 1-10, windows size used for the filter
        :param closing_speed: 0.0-1.0, closing speed of the gripper used during grasping/measurement
        :param filter: bool, determines if the filtered current should also be created
        :param show: bool, display all cleaned data
        :param block: bool, should window block program execution
        :return: None
        """
        max_cur = min(max(0.0, max_cur), 1.0)
        self.max_cur = max_cur
        self.limit = limit
        self.resolution = resolution
        self.slope = slope
        self.update_filter(window_size)

        for grip in self.grips:
            uni_pos, uni_cur = self.get_combed_lists(grip["position"], grip["current"])
            # self.display_raw(pos=uni_pos, cur=uni_cur, keep=True, color=(0.0,0.0,1.0), s=20)
            # c, p = self.get_combed_lists(uni_cur, uni_pos)
            # self.display_raw(pos=p, cur=c)
            if filter:
                grip["filter_current"] = self.filter_current(grip["current"])
                uni_cln_pos, uni_cln_cur = self.get_combed_lists(uni_pos, self.filter_current(uni_cur))
            if self.get_data_density(uni_pos) > 1.3 and uni_cur.any():
                grip["clean_pos"] = uni_pos
                grip["clean_cur"] = uni_cur
                if filter:
                    grip["clean_filter_pos"] = uni_cln_pos
                    grip["clean_filter_cur"] = uni_cln_cur
                grip["max_cur"] = max_cur
                grip["closing_speed"] = closing_speed
                grip["proper"] = np.max(uni_cur) >= self.current_tolerance * self.max_cur
            else:
                print("init_clean_data: empty position and/or current")
            if show and uni_cur.any():
                self.display_raw(pos=uni_pos, cur=uni_cur, keep=True, color=(0.1, 0.2, 0.2))
        plt.show(block=block)

    def init_linear_class(self, filter=False, show=False, option=0, local_limit=True):
        """

        :param filter: bool, use filtered data
        :param show: bool, show color coded classification of stiffnesses
        :param option: 0,1,2 - method to get the second point; \n
        0 = closed gripper position (global), 1 = point of max current, 2 = experimental
        :param local_limit: bool, use local limit or global limit, \n
        e.g. local=True: 0.05*(1 - (obj_begin=0.6) = 0.05*0.4) vs. local=False: 0.05*1.0
        :return:
        """
        grip_diff = len(self.grips) - len(self.last_global_mods)
        if grip_diff > 0:
            self.last_global_mods.extend([None] * grip_diff)
        j = 0
        if not filter:
            for i, grip in enumerate(self.grips):
                if "clean_pos" in grip and "clean_cur" in grip and grip["proper"]:
                    globm = self.get_global_modulus(grip["clean_pos"], grip["clean_cur"],
                                                    self.limit, self.resolution, self.slope, option=option, local_limit=local_limit)
                    if globm:
                        self.last_global_mods[j] = (i, globm[0])
                        grip["l_mod"] = self.last_global_mods[j]
                        j += 1
                    else:
                        del self.last_global_mods[j]
                else:
                    del self.last_global_mods[j]
            self.last_global_mods.sort(key=lambda l: l[1], reverse=True)
            len_mod = len(self.last_global_mods)
            for i, el in enumerate(self.last_global_mods):
                color = (1.0 - 1.0 * i / len_mod, 1.0 * i / len_mod, 0.0)
                if "clean_pos" in self.grips[el[0]] and self.grips[el[0]]["proper"] and show:
                    self.display_raw(pos=self.grips[el[0]]["clean_pos"], cur=self.grips[el[0]]["clean_cur"], keep=True,
                                     color=color)
            if show:
                plt.show()
        else:
            for i, grip in enumerate(self.grips):
                if "clean_filter_pos" in grip and "clean_filter_cur" in grip and grip["proper"]:
                    globm = self.get_global_modulus(grip["clean_filter_pos"], grip["clean_filter_cur"],
                                                    self.limit, self.resolution, self.slope, option=option)
                    if globm:
                        self.last_global_mods[j] = (i, globm)
                        grip['l_mod'] = self.last_global_mods[j]
                        j += 1
                    else:
                        del self.last_global_mods[j]
                else:
                    del self.last_global_mods[j]
            self.last_global_mods.sort(key=lambda l: l[1], reverse=True)
            # print(self.last_global_mods)
            len_mod = len(self.last_global_mods)
            for i, el in enumerate(self.last_global_mods):
                color = (1.0 - 1.0 * i / len_mod, 1.0 * i / len_mod, 0.0)
                if "clean_filter_pos" in self.grips[el[0]] and self.grips[el[0]]["proper"] and show:
                    self.display_raw(pos=self.grips[el[0]]["clean_filter_pos"],
                                     cur=self.grips[el[0]]["clean_filter_cur"], keep=True, color=color)
            if show:
                plt.show()

    def get_linear_rank(self, pos, cur, index=None, filter=False, option=0, local_limit=True):
        """
        returns None when object has not been squished at least 40% of l_0

        :param pos: position, cleaned
        :param cur: current, cleaned
        :param index: use if you want to know the rank of an object relative to closest neighbors
        :param filter: bool, use filtered data or nay
        :param option: 0,1,2 - method to get the second point; \n
        0 = closed gripper position (global), 1 = point of max current, 2 = experimental
        :param local_limit: bool, use local limit or global limit, \n
        e.g. local=True: 0.05*(1 - (obj_begin=0.6) = 0.05*0.4) vs. local=False: 0.05*1.0
        :return: stiffness rank relative to the loaded grips
        """
        uni_pos, uni_cur = self.get_combed_lists(pos, cur)
        if filter:
            uni_cur = self.filter_current(uni_cur)
        # uni_pos, uni_cur = self.cut_top_n_side(uni_pos, uni_cur, self.max_cur)

        glom = self.get_global_modulus(uni_pos, uni_cur, self.limit, self.resolution, self.slope, option=option,
                                       local_limit=local_limit)
        if glom is None:
            return None
        print("global modulus of new grip: ", glom)
        if index is None:
            for i, ig in enumerate(self.last_global_mods):
                if ig[1] < glom[0]:
                    return i
            return len(self.last_global_mods)
        else:
            pass
            for i, el in enumerate(self.last_global_mods):
                if el[1] < glom:
                    try:
                        stiff_avg = 0.0
                        num_of_neighbors = 3
                        # for j in range(max(0, i-(num_of_neighbors)/2), min(len(self.last_global_mods)-1, i+(num_of_neighbors+1)/2)):
                        #     stiff_avg += float(self.files[self.last_global_mods[j][0]].split("-")[-index])
                        margin = 1e4
                        filtered_moduli = filter(lambda x: abs(x[1] - glom) < margin, self.last_global_mods)
                        while len(filtered_moduli) < num_of_neighbors:
                            margin += 1e4
                            filtered_moduli = filter(lambda x: abs(x[1] - glom) < margin, self.last_global_mods)
                        print(filter(lambda x: abs(x[1] - glom) < margin, self.last_global_mods))
                        # 12 items by stiffness
                        for j in filtered_moduli:
                            stiff_avg += float(self.files[j[0]].split("-")[-index])
                        stiff_avg = stiff_avg / (1.0 * len(filtered_moduli))
                        if stiff_avg <= 2.0:
                            print("stiffness of object is 0 & " + str(stiff_avg))
                            return 0
                        elif stiff_avg <= 4.0:
                            print("stiffness of object is 1 & " + str(stiff_avg))
                            return 1
                        elif stiff_avg <= 7.0:
                            print("stiffness of object is 2 & " + str(stiff_avg))
                            return 2
                        elif stiff_avg <= 11.0:
                            print("stiffness of object is 3 & " + str(stiff_avg))
                            return 3
                        # 7 cubes
                        if stiff_avg <= 1.0:
                            print("stiffness of object is 0 & " + str(stiff_avg))
                            return 0
                        elif stiff_avg <= 3.0:
                            print("stiffness of object is 1 & " + str(stiff_avg))
                            return 1
                        elif stiff_avg <= 5.0:
                            print("stiffness of object is 2 & " + str(stiff_avg))
                            return 2
                        elif stiff_avg <= 7.0:
                            print("stiffness of object is 3 & " + str(stiff_avg))
                            return 3
                        print("stiffness of object:" + self.files[el[0]].split("-")[-index])
                        # print("stiffness of object:" + str(self.files[el[0]].split("-")[-index]))
                    except ValueError as e:
                        print("[ERROR] get_linear_rank:" + e.args[0].encode("utf-8"))
                    break
        return stiff_avg

    @staticmethod
    def get_high_index(p, c):
        """

        :param p: position, should all be the same value
        :param c: current
        :return: get index of item with same position but higher current than the ones at the same position
        """
        return np.argsort(np.stack((p, c), axis=1), axis=0)[-1][-1]

    def get_suspicious_indices(self, p, c):
        """

        :param p: position
        :param c: current
        :return: Indices like np.unique(), but with always the one with higher current when positions are the same
        """
        uni_p, inds = np.unique(p, return_index=True)
        inds_len = len(inds)
        ilm1 = inds_len - 1
        for i in range(ilm1):
            ip1 = i + 1
            if inds[ip1] - inds[i] > 1:
                # print("i:", i, inds[ip1], inds[i])
                inds[i] = inds[i] + self.get_high_index(p[inds[i]:inds[ip1]], c[inds[i]:inds[ip1]])
        if inds[ilm1] != len(p) - 1:
            inds[ilm1] = inds[ilm1] + self.get_high_index(p[inds[ilm1]:len(p)], c[inds[ilm1]:len(p)])
        return inds

    def get_combed_lists(self, p, c):
        """

        :param p: position
        :param c: current
        :return: take currents left of 0.X * max(c) & unique with highest values
        """
        inds = self.get_suspicious_indices(p, c)
        p_ret = p[inds]
        c_ret = c[inds]
        c_max = np.max(c)
        c_maxm10 = self.current_tolerance * c_max  # #  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        where_is_it = np.min(np.where((c_ret >= c_maxm10)))
        # print(np.where((c_ret >= c_maxm10)))
        # print(where_is_it)
        # print(len(p), len(p[0:where_is_it+1]))
        return p_ret[:where_is_it + 1], c_ret[:where_is_it + 1]

    @staticmethod
    def get_data_density(arr, anchors=3):
        if not arr.any():
            return 0.0
        if anchors < 2:
            anchors = 2
        ret = 0.0
        arr_min = min(arr)
        arr_max = max(arr)
        arr_step = arr_min + (1.0 * arr_max - arr_min) / (anchors - 1.0)
        pts = []
        for i in range(anchors - 2):
            pts.append(arr_min + arr_step)
        pts = np.array(pts)
        for el in arr:
            ret += min(el - arr_min, arr_max - el, abs(pts - el))
        # print("\033[F"+"density: "+str(ret / (arr_max-arr_min))+"\r")
        # print(ret)
        return ret / (arr_max - arr_min)

    def plot_moduli(self):
        """
        Plot the estimated youngs moduli.

        :return:
        """
        for i, grip in enumerate(self.grips):
            if "l_mod" in grip:
                print(grip["l_mod"][0], grip["l_mod"][1])
                plt.scatter(grip["l_mod"][0], grip["l_mod"][1], s=40,
                            c=(min(1.0, i * 1.00 / len(self.grips)), min(1.0, 1 - i * 1.00 / len(self.grips)), 0.0),
                            edgecolors=(
                                min(1.0, 1 - grip["l_mod"][1] / 6.1e6), min(1.0, 1 - grip["l_mod"][1] / 6.1e6), 0.0))
                # min(1.0, grip["l_mod"][1] / 6.1e6)))
        plt.show()

    def get_anchor_rank(self, pos, cur, option=0, local_limit=True):
        """

        :param pos: position, raw
        :param cur: current, raw
        :param option: 0,1,2 - method to get the second point; \n
        0 = closed gripper position (global), 1 = point of max current, 2 = experimental
        :param local_limit: bool, choose if the limit size should be chosen as percentage of open gripper aperture or of object size, \n
        e.g. local=True: 0.05*(1 - (obj_begin=0.6) = 0.05*0.4) vs. local=False: 0.05*1.0
        :return: stiffness rank from 0-3 in relation to hardcoded anchor values
        """
        uni_pos, uni_cur = self.get_combed_lists(pos, cur)

        global_modulus = self.get_global_modulus(uni_pos, uni_cur, self.limit, self.resolution, self.slope, option, local_limit)
        anchors = [4.2e6, 3.0e6, 1.2e6, 0.8e6]
        min_dist = float("Inf")
        min_i = 0
        for i in range(len(anchors)):
            tmp = abs(anchors[i] - global_modulus[0])
            if tmp < min_dist:
                min_i = i
                min_dist = tmp
        return min_i

    def get_repeatability(self, folders, reference_results=None, id_index=-2, extensions=(".npy",), show=False):
        """

        :param folders: list(str), folders to go through and categorize the files of
        :param reference_results: dict, results you want to compare
        :param id_index: index from the end of the file where the id of the object is located separated by dash
        :param extensions: list(str), extensions to load from folders
        :param show: show the repeatability graph
        :return: returns list of categorizations for each item
        """
        if reference_results is None:  # Matej categorized the 20 objects
            reference_results = {10: 1, 5: 1, 18: 1, 15: 3, 3: 0, 9: 2, 12: 2, 19: 3, 2: 1, 0: 0, 14: 3, 6: 1, 11: 2,
                                 1: 0, 7: 1, 17: 3, 13: 3, 16: 3, 8: 1, 4: 1}

        for folder in folders:
            self.load_all(folder, extensions)
        yint = range(0, 4)
        xint = range(0, 20)
        plt.yticks(yint)
        plt.xticks(xint)
        arm_classes = []
        tmp_class = dict()
        for i, g in enumerate(self.grips):
            cut_name = self.files[i]
            # cut_name = "lel.npy"
            for ext in extensions:
                if ext in cut_name:
                    cut_name.replace(ext, "")
            idx = int(cut_name.split("-")[id_index])
            # print(idx)
            r = self.get_anchor_rank(g["position"], g["current"])

            gf = len(self.grips) / len(folders)
            lg = len(self.grips)

            tmp_class[idx] = r
            if i % gf == 19:
                if tmp_class:
                    arm_classes.append(tmp_class)

                tmp_class = dict()
            # print(gf)
            # igf = i % gf
            # print(igf, gf)
            sz = 20
            # print(i/lf)
            if i / gf == 0:
                sz = 75
            elif i / gf == 1:
                sz = 30
            else:
                sz = 5

            if show:
                if i < lg / 3.0:
                    plt.scatter(idx, r, color=(1.0 - i * 1.0 / (2.0 * lg), i * 1.0 / (2.0 * lg), 0.0), s=sz)
                elif i < lg / 1.50:
                    plt.scatter(idx, r, color=(0.0, 1.0 - i * 1.0 / (2.0 * lg), i * 1.0 / (2.0 * lg)), s=sz)
                else:
                    plt.scatter(idx, r, color=(0.0, 0.0, 1.0 - i * 1.0 / (2.0 * lg)), s=sz)
        ret = list()
        ret.append([reference_results[i] + 1 for i in range(20)])
        for arm in arm_classes:
            ret.append([arm[i] + 1 for i in range(20)])
        # print("matej_classes: ", matej_classes, "\narm_classes: ", arm_classes)
        if show:
            plt.show()
        # print(ret)
        return ret


if __name__ == "__main__":
    args = sys.argv
    file_args = sys.argv[1:]
    grip = GrippingSessions()

    # folders = [r"C:/Users/Hartvi/PycharmProjects/ipalm/ipalm_control/2020_09_04_0055",r"C:/Users/Hartvi/PycharmProjects/ipalm/ipalm_control/2020_09_04_02_10_again",r"C:/Users/Hartvi/PycharmProjects/ipalm/ipalm_control/2020_09_04_strawberry_cube_reverse"]
    # grip.display_repeatability(folders, None)

    folders = ["ipalm_control/2020_09_04_0055", "ipalm_control/2020_09_04_02_10_again",
               "ipalm_control/2020_09_04_strawberry_cube_reverse"]
    # grip.get_repeatability(folders, None, show=True)

    # example code how to run the text reading part
    # grip.load_all("txts/")
    # print("loaded txts")
    # grip.load_all("ipalm-grasping/Kinova_Gen3/Measurements/2f-85/2020_08_28_ref/")
    # grip.load_all("ipalm-grasping/Kinova_Gen3/Measurements/2f-85/2020_09_01_7cubes/")
    # grip.load_all("ipalm-grasping/Kinova_Gen3/Measurements/2f-85/2020_08_31/")
    # grip.load_all("ipalm_control/2020_09_04_0055/", ext=".npy")
    grip.load_all("ipalm_control/2020_09_04_02_10_again/", extensions=".npy")
    # print("loaded npys")
    # grip.load_all("mats/")  # huge
    # print("loaded mats")
    name_list = ["ipalm-grasping/Kinova_Gen3/Measurements/2f-85/2020_08_24/2020-Aug-24-12-59--1-0.npy",
                 "ipalm-grasping/Kinova_Gen3/Measurements/2f-85/2020_08_24/2020-Aug-24-13-01--1-4.npy",
                 "ipalm_control/2020_09_04_0055/2020-Sep-04-08-23-13-0.npy",
                 "ipalm-grasping/Kinova_Gen3/Measurements/2f-85/2020_08_28_ref/2020-Aug-28-13-09-0-3.npy",
                 "ipalm-grasping/Kinova_Gen3/Measurements/2f-85/2020_08_28_ref/2020-Aug-28-13-10-2-1.npy",
                 "ipalm_control/2020_09_04_strawberry_cube_reverse/2020-Sep-04-09-32-14-0.npy"]

    # matt2 = grip.npy2grip(grip_name2)
    # matt3 = grip.npy2grip(grip_name3)
    # grip.display_raw(pos=matt["position"], cur=matt["current"], s=40, color=(1.0,0.0,0.0))
    # grip.display_raw(pos=matt2["position"], cur=matt2["current"], keep=True, s=20, color=(0.0,1.0,0.0))
    # grip.display_raw(pos=matt3["position"], cur=matt3["current"], keep=False, s=2, color=(0.0,0.0,1.0))
    grip.init_clean_data(max_cur=1.00, slope=1e5, resolution=500, limit=0.05, window_size=1, filter=False,
                         show=False)  # good values
    grip.init_linear_class(show=True, filter=False, option=1, local_limit=False)

    # grip.display_raw(idx=18, keep=True)  # 18, 37
    # grip.display_raw(pos=grip.grips[18]["clean_pos"], cur=grip.grips[18]["clean_cur"], s=20, color=(0.0, 0.5, 1.0))

    # stiff_rank = grip.get_linear_rank(matt["position"], matt["current"], index=2)
    grip.plot_moduli()
    for i in range(len(grip.grips)):
        # grip.display_raw(pos=grip.grips[i]["clean_pos"], cur=grip.filter_current(grip.grips[i]["clean_cur"]), keep=False, color=(0.3,0.3,1.0), s=20)
        grip.display_raw(pos=grip.grips[i]["position"], cur=grip.grips[i]["current"], keep=True, color=(0.6, 0.9, 0.3),
                         s=20)
        stiff_rank = grip.get_linear_rank(pos=grip.grips[i]["position"], cur=grip.grips[i]["current"], filter=True,
                                          option=1)
        print("obj id: ", i, "stiffness rank: ", stiff_rank)
        # print("stiffness rank of " + grip_name + " is " + str(stiff_rank+1) + "/" + str(len(grip.last_global_mods)))
        # grip.display_raw(pos=matt["position"], cur=matt["current"], keep=True, s=9, color="green")
        grip.display_last_grip()

    # for grip_name in name_list:
    #     matt = grip.npy2grip(grip_name)
    #
    #     # stiff_rank = grip.get_anchor_rank(matt["position"], matt["filter_current"])
    #     stiff_rank = grip.get_linear_rank(pos=matt["position"], cur=matt["filter_current"])
    #     print(stiff_rank)
    #     # print("stiffness rank of " + grip_name + " is " + str(stiff_rank+1) + "/" + str(len(grip.last_global_mods)))
    #     # grip.display_raw(pos=matt["position"], cur=matt["current"], keep=True, s=9, color="green")
    #     grip.display_last_grip()


"""
# TODO proposals:
* Create a class for each grip so the members are visible by the IDE
* Create a method to classify objects based on
  * youngs modulus if object has been squished at least 40% -> get_global_modulus does not return None, args: option=1
  * if get_global_modulus returns None, use custom method to classify objects
    * calculate modulus from point of contact to maximum squished position - args: option 0
    * calculate the distance pressed
    * etc...
* Add parameter, e.g. *comb=True/False* to turn off list combing inside get_anchor_rank, get_linear_rank, init_clean_data,

## possible grip dictionary members

position - positions of gripper 0-1, open-closed
current - 0-1, min to max
From Pavel only so far
normal_pos - position normalized to the size of object - when loading txt or mat from Pavel's files
object_id - name of object - .mat/.txt
object_width - object width - .mat/.txt
closing_speed - used gripper closing speed - .mat/.txt

clean_cur - current cleaned using the get_combed_lists method
clean_pos - positions corresponding to clean_cur
filter_current - current filtered using the GrippingSessions.update_filter(), then .filter_current() method
proper - true if current reaches at least .current_tolerance*gripper_max_current
l_mod - last calculated modulus in .init_linear_class(); display using .plot_moduli()

# Examples
1) Loading files
`grip = GrippingSessions()  # instance to save the grips`

`grip.load_all(path_to_folder, extensions=(".mat", ".txt", ".npy"))`
Loads all files with extensions given in the *extensions* argument

`grip.load_mats(list_of_mat_files)`
`grip.load_txts(list_of_txt_files)`
`grip.load_npys(list_of_npy_files)`
Loads files and save in grip.grips list of grip dictionaries.
Modifies grip.files, grip.file_num, grip.mat_files, grip.txt_files, grip.npy_files.

`grip.mat2grip(mat_file)`
`grip.txt2grip(txt_file)`
`grip.npy2grip(npy_file)`
These return dictionary of single grip

2) Initialization
`grip = GrippingSessions()  # instance to save the grips`

`grip.init_clean_data(max_cur=1.00, slope=1e5, resolution=500, limit=0.05, window_size=1, filter=False,
                         show=False)  # good values`
filter = bool, use filter on data - only good for high closing speeds, e.g. >0.2, for closing speed 0.5 ~ possible filter up to 10
show = bool, show graph with all loaded data
`grip.init_linear_class(show=True, filter=False, option=1, local_limit=False)`
* Configurations:
  * option=0 and local_limit=True: scientifically correct way to do it, modulus at 40% deformation - should return `None` if it fails bad enough
  * option=1 and local_limit=True: take modulus as 40% *distance* from beginning of object to stopped gripper and set the limit as, e.g. 0.05\* *distance*

3) Ranking
`grip.get_linear_rank(position, current)` - see TODO proposals
Return rank relative to the other loaded files based on the linear estimate of the youngs modulus.

`grip.get_anchor_rank(position, current)`
Return rank relative to hardcoded 'anchor' values; fickle.

4) Displaying
a) Repeatability
```
folders = [folder1,folder2]
grip.get_repeatability(folders, None)
```

`def get_repeatability(self, folders, reference_results=None, id_index=-2, extensions=(".npy",), show=False):`
folders = list of paths to folders
reference_results = dictionary where keys are integer ids of objects and values are the estimated stiffness categories 0-3 hard to soft
id_index = python-way index of id of object
* e.g. id here is 13, so id_index = -2 "ipalm_control/2020_09_04_0055/2020-Sep-04-08-23-13-0.npy"<br>
show = bool, show a simple graph with the results
Returns a list with the ranks for each object according to ID with the reference results at index 0.

b) Debug & presentation
```
matt=grip.npy2grip(npy_file)
keep=True  # doesn't show the window yet, and block doesn't do anything
grip.display_raw(pos=matt["position"], cur=matt["current"], color=(1.0, 1.0, 0.0), s=20, keep=True)

# option=1, local_limit=False is technically incorrect. option=0, local_limit=True is the correct configuration.
grip.get_anchor_rank(pos=matt["position"], cur=matt["current"], option=1, local_limit=False)
# keep is False, so the window gets displayed
# block is False, so the execution of the program continues even after this line
grip.display_last_grip(keep=False, block=False)
```


## Useful Numpy tricks
arr[...,:] = create array from last dimension
arr[(condition1) & (condition2)] = same as in matlab; choose elements that satisfy condition
"""


