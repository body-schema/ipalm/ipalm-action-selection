from operator import pos
import numpy as np
from scipy.stats import norm  
import matplotlib.pyplot as plt
import utility as ut
from sklearn.mixture import GaussianMixture

mats = [('A', 250, 80), ('B', 500, 60), ('C', 700, 50), ('D', 850, 50)]
x = np.linspace(0,1000,1000)
prior_probs = [0.25, 0.25, 0.25, 0.25]

GMM = None
for i, mat in enumerate(mats):
    # print(mat[1])
    if GMM is None:
        GMM = norm.pdf(x, mat[1], mat[2]) * prior_probs[i]
    else:
        GMM += norm.pdf(x, mat[1], mat[2]) * prior_probs[i]

# GMM = GMM / sum(GMM)  # works only because bin size in x = np.linspace(0,1000,1000) is one

y_meas = norm.pdf(x, 500, 100)

likelihood = np.convolve(GMM, y_meas, mode='same')

posterior = (GMM * likelihood) / sum(GMM * likelihood)

plt.plot(x,GMM, label="GMM", lw=2)
plt.plot(x,y_meas, label="Meas", lw=2)
plt.plot(x,likelihood, label="likelihood", lw=2)
plt.legend()
plt.show()

plt.plot(x,GMM, label="GMM", lw=2)
plt.plot(x,posterior, label="Posterior", lw=2)
plt.legend()
plt.show()

# take the GMM probability at each mean
pA = posterior[250]
pB = posterior[500]
pC = posterior[700]
pD = posterior[850]

probs = np.array([pA, pB, pC, pD])
probs = probs / sum(probs)
print("posterior probs: ", probs)
print("prior probs: ", prior_probs)

# data needs sampling

data = np.array(ut.sampler(x, posterior, 1000))
real_mus = np.array([[250],[500],[700],[850]])
data = data.reshape(-1, 1)
gm = GaussianMixture(n_components=len(mats), random_state=0).fit(data)




n = len(data)
bins = int(np.sqrt(n))*5
mus_fit = gm.means_.flatten()
# mus_fit = mus_fit / sum(mus_fit)

print("gm.means_ : ", gm.means_)
print("real mus: ", real_mus)
print(gm.weights_)

sigs_fit = np.sqrt(gm.covariances_.flatten())
w_fit = gm.weights_.flatten()
x = np.linspace(0, 1000, 1000)

for i in range(len(mus_fit)):
    if i == 0:
        y = norm.pdf(x, mus_fit[i], sigs_fit[i]) * w_fit[i]
    else:
        y += norm.pdf(x, mus_fit[i], sigs_fit[i]) * w_fit[i]

y *= 6000
plt.plot(x, y, label="Rec. GMM", color="tab:red", lw=2, ls="--")
plt.hist(data, bins=bins, label="Data")
plt.legend()
plt.show()

