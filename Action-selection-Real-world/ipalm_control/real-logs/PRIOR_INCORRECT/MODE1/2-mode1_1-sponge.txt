args Namespace(grasp='off', grid=0.055, mes='1', stiff='off', stiffrep='off', vis='off', wght=None)
[94m[INFO] Starting program in MEASURE mode with 1.0 as the max current[0m
[94m[INFO] Weight estimation mode ON.[0m
[94m[INFO] Grid Measuring is ON, with CLOSING_SPEED at 0.055.[0m
Starting to clear
Waiting for service
Clearing faults
[INFO] [1621530528.644267]: Activating the action notifications...
[INFO] [1621530528.647069]: Successfully activated the Action Notifications!
/home/robot3/vision_ws/src/ipalm_control
Cleaning the previous '/home/robot3/vision_ws/src/ipalm_control/meas.json' file.
-----------------Current PMF-----------------
Material:       Kwhite | Probabilty: 0.1000 | Cond. likelihood: None
Material:        joker | Probabilty: 0.0500 | Cond. likelihood: None
Material: SpongeYellow | Probabilty: 0.2500 | Cond. likelihood: None
Material:       Kblack | Probabilty: 0.6000 | Cond. likelihood: None
---------------------------------------------
[INFO] [1621530530.355158]: Sending the gripper position...
[INFO] [1621530530.858372]: 0.0
----------------------
| ITERATION NUMBER: 0 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:       Kwhite | Probabilty: 0.1000 | Cond. likelihood: None
Material:        joker | Probabilty: 0.0500 | Cond. likelihood: None
Material: SpongeYellow | Probabilty: 0.2500 | Cond. likelihood: None
Material:       Kblack | Probabilty: 0.6000 | Cond. likelihood: None
---------------------------------------------
_____________________
('Property: ', u'Elasticity')
Measurement:
('    Meas. sigma:', 4091)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 1.49046857073   |
| The average sampled Entropy is: 1.22041410147   |
| -OUT- Discrete Information Gain: 0.270054469261   |
| -info- AVERAGE sampled Information Gain: 0.270054469261 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for elasticity: 15.7999997855 |
| This is the Emulated Entropy for elasticity: 15.10476329 |
| Argmax mean: 9541 (no offset)|
| -x- Diff Information Gain: 0.695236495521     |
--------------------------------------------
()
_____________________
('Property: ', u'Density')
Measurement:
('    Meas. sigma:', 29.64)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 1.49046857073   |
| The average sampled Entropy is: 0.198873739779   |
| -OUT- Discrete Information Gain: 1.29159483095   |
| -info- AVERAGE sampled Information Gain: 1.29159483095 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for density: 8.37967730393 |
| This is the Emulated Entropy for density: 7.38907320074 |
| Argmax mean: 450 (no offset)|
| -x- Diff Information Gain: 0.990604103195     |
--------------------------------------------
()
---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  1.2915948309536638
-----------------------------------

[94m[AS-INFO] Starting exploratory action.[0m
[94m[INFO] Zero weight calibration in progress.[0m
[INFO] [1621530534.928026]: Sending the gripper position...
Zero payload torque: 
Reading number 0 : -8.88943576813
Reading number 10 : -8.89842796326
Reading number 20 : -8.88069057465
Reading number 30 : -8.92770385742
Reading number 40 : -8.92770385742
[94mTorque is: -8.91954433441[0m
[92mGripper zero weight is 2.89287241373[0m
[INFO] [1621530547.103232]: Sending the gripper position...
[92m[INFO] Weight calibration successful.[0m
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621530548.154740]: Sending pose...
[INFO] [1621530548.177983]: Waiting for pose to finish...
[INFO] [1621530550.303661]: Sending pose...
[INFO] [1621530550.328966]: Waiting for pose to finish...
[INFO] [1621530551.378100]: Sending the gripper position...
[INFO] [1621530552.408851]: Sending pose...
[INFO] [1621530552.428017]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -8.93765449524
Reading number 10 : -8.91784954071
Reading number 20 : -8.9051399231
Reading number 30 : -8.93661880493
Reading number 40 : -8.90561676025
[94mTorque is: -8.93072011948[0m
[92mSo the estimated weight is 0.00362463810967[0m
[INFO] [1621530564.928955]: Sending pose...
[INFO] [1621530564.956762]: Waiting for pose to finish...
[INFO] [1621530567.080651]: Sending pose...
[INFO] [1621530567.110205]: Waiting for pose to finish...
[INFO] [1621530568.404546]: Sending the gripper position...
[INFO] [1621530569.428923]: Sending pose...
[INFO] [1621530569.453874]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621530571.003868]: Sending pose...
[INFO] [1621530571.027932]: Waiting for pose to finish...
[INFO] [1621530571.578551]: Sending pose...
[INFO] [1621530571.603303]: Waiting for pose to finish...
[INFO] [1621530572.653221]: Sending the gripper position...
[INFO] [1621530573.678404]: Sending pose...
[INFO] [1621530573.704160]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -8.93166923523
Reading number 10 : -8.92275333405
Reading number 20 : -8.92463874817
Reading number 30 : -8.88557052612
Reading number 40 : -8.92962551117
[94mTorque is: -8.92731086731[0m
[92mSo the estimated weight is 0.00251891665408[0m
[INFO] [1621530586.205624]: Sending pose...
[INFO] [1621530586.229251]: Waiting for pose to finish...
[INFO] [1621530588.438268]: Sending pose...
[INFO] [1621530588.455500]: Waiting for pose to finish...
[INFO] [1621530589.778130]: Sending the gripper position...
[INFO] [1621530590.837742]: Sending pose...
[INFO] [1621530590.853860]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621530592.403868]: Sending pose...
[INFO] [1621530592.428832]: Waiting for pose to finish...
[INFO] [1621530592.980367]: Sending pose...
[INFO] [1621530593.003517]: Waiting for pose to finish...
[INFO] [1621530594.055146]: Sending the gripper position...
[INFO] [1621530595.078657]: Sending pose...
[INFO] [1621530595.104244]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -8.90476131439
Reading number 10 : -8.92391872406
Reading number 20 : -8.91078186035
Reading number 30 : -8.86277008057
Reading number 40 : -8.91170597076
[94mTorque is: -8.91455238342[0m
[92mSo the estimated weight is -0.00161903756119[0m
[INFO] [1621530607.630600]: Sending pose...
[INFO] [1621530607.660297]: Waiting for pose to finish...
[INFO] [1621530609.780729]: Sending pose...
[INFO] [1621530609.808955]: Waiting for pose to finish...
[INFO] [1621530611.129426]: Sending the gripper position...
[INFO] [1621530612.154360]: Sending pose...
[INFO] [1621530612.182192]: Waiting for pose to finish...
Added density to the meas. database.
Added density to the meas. database.
()
CLOSED-FORM solution via value ----------------------------------------
Material:       Kwhite | Probabilty: 0.0000 | Cond. likelihood: 0.00000001
Material:        joker | Probabilty: 0.0216 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.9784 | Cond. likelihood: 0.00904703
Material:       Kblack | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
----------------------
| ITERATION NUMBER: 1 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:       Kwhite | Probabilty: 0.0000 | Cond. likelihood: 0.00000001
Material:        joker | Probabilty: 0.0216 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.9784 | Cond. likelihood: 0.00904703
Material:       Kblack | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
---------------------------------------------
_____________________
('Property: ', u'Elasticity')
Measurement:
('    Meas. sigma:', 4091)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 0.15050301441   |
| The average sampled Entropy is: 0.0473313620272   |
| -OUT- Discrete Information Gain: 0.103171652382   |
| -info- AVERAGE sampled Information Gain: 0.103171652382 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for elasticity: 15.7999997855 |
| This is the Emulated Entropy for elasticity: 15.10476329 |
| Argmax mean: 9541 (no offset)|
| -x- Diff Information Gain: 0.695236495521     |
--------------------------------------------
()
_____________________
('Property: ', u'Density')
Measurement:
('    Meas. sigma:', 29.64)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 0.15050301441   |
| The average sampled Entropy is: 0.0367031087165   |
| -OUT- Discrete Information Gain: 0.113799905693   |
| -info- AVERAGE sampled Information Gain: 0.113799905693 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for density: 6.19044393581 |
| This is the Emulated Entropy for density: 5.9609667769 |
| Argmax mean: -135 (no offset)|
| -x- Diff Information Gain: 0.229477158905     |
--------------------------------------------
()
---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.11379990569308215
-----------------------------------

[94m[AS-INFO] Starting exploratory action.[0m
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621530617.229457]: Sending pose...
[INFO] [1621530617.252809]: Waiting for pose to finish...
[INFO] [1621530617.805209]: Sending pose...
[INFO] [1621530617.830655]: Waiting for pose to finish...
[INFO] [1621530618.877985]: Sending the gripper position...
[INFO] [1621530619.906324]: Sending pose...
[INFO] [1621530619.932396]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -8.8999671936
Reading number 10 : -8.93000030518
Reading number 20 : -8.93431472778
Reading number 30 : -8.93261623383
Reading number 40 : -8.93456268311
[94mTorque is: -8.92813325882[0m
[92mSo the estimated weight is 0.00278564257902[0m
[INFO] [1621530632.455478]: Sending pose...
[INFO] [1621530632.478374]: Waiting for pose to finish...
[INFO] [1621530634.629210]: Sending pose...
[INFO] [1621530634.655031]: Waiting for pose to finish...
[INFO] [1621530635.955188]: Sending the gripper position...
[INFO] [1621530636.980073]: Sending pose...
[INFO] [1621530637.004049]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621530638.577265]: Sending pose...
[INFO] [1621530638.603463]: Waiting for pose to finish...
[INFO] [1621530639.155688]: Sending pose...
[INFO] [1621530639.180921]: Waiting for pose to finish...
[INFO] [1621530640.227877]: Sending the gripper position...
[INFO] [1621530641.255222]: Sending pose...
[INFO] [1621530641.277786]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -8.93243122101
Reading number 10 : -8.9211025238
Reading number 20 : -8.94607925415
Reading number 30 : -8.93321990967
Reading number 40 : -8.94853973389
[94mTorque is: -8.93041934967[0m
[92mSo the estimated weight is 0.00352708955318[0m
[INFO] [1621530653.778822]: Sending pose...
[INFO] [1621530653.803025]: Waiting for pose to finish...
[INFO] [1621530655.930297]: Sending pose...
[INFO] [1621530655.953373]: Waiting for pose to finish...
[INFO] [1621530657.279330]: Sending the gripper position...
[INFO] [1621530658.303512]: Sending pose...
[INFO] [1621530658.330241]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621530659.880530]: Sending pose...
[INFO] [1621530659.903019]: Waiting for pose to finish...
[INFO] [1621530660.453810]: Sending pose...
[INFO] [1621530660.480325]: Waiting for pose to finish...
[INFO] [1621530661.528752]: Sending the gripper position...
[INFO] [1621530662.553750]: Sending pose...
[INFO] [1621530662.578137]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -8.9387216568
Reading number 10 : -8.91840362549
Reading number 20 : -8.93600082397
Reading number 30 : -8.89095401764
Reading number 40 : -8.89587593079
[94mTorque is: -8.91966779709[0m
[92mSo the estimated weight is 4.00426029666e-05[0m
[INFO] [1621530675.087353]: Sending pose...
[INFO] [1621530675.103115]: Waiting for pose to finish...
[INFO] [1621530677.253883]: Sending pose...
[INFO] [1621530677.278407]: Waiting for pose to finish...
[INFO] [1621530678.605432]: Sending the gripper position...
[INFO] [1621530679.635100]: Sending pose...
[INFO] [1621530679.653154]: Waiting for pose to finish...
Added density to the meas. database.
Added density to the meas. database.
()
CLOSED-FORM solution via value ----------------------------------------
Material:       Kwhite | Probabilty: 0.0000 | Cond. likelihood: 0.00000002
Material:        joker | Probabilty: 0.0024 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.9976 | Cond. likelihood: 0.00934720
Material:       Kblack | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
----------------------
| ITERATION NUMBER: 2 |
| MODE NUMBER: 1 |
----------------------

-----------------Current PMF-----------------
Material:       Kwhite | Probabilty: 0.0000 | Cond. likelihood: 0.00000002
Material:        joker | Probabilty: 0.0024 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.9976 | Cond. likelihood: 0.00934720
Material:       Kblack | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
---------------------------------------------
_____________________
('Property: ', u'Elasticity')
Measurement:
('    Meas. sigma:', 4091)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 0.0239919347372   |
| The average sampled Entropy is: 0.00915322122757   |
| -OUT- Discrete Information Gain: 0.0148387135096   |
| -info- AVERAGE sampled Information Gain: 0.0148387135096 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for elasticity: 15.7999997855 |
| This is the Emulated Entropy for elasticity: 15.10476329 |
| Argmax mean: 9541 (no offset)|
| -x- Diff Information Gain: 0.695236495521     |
--------------------------------------------
()
_____________________
('Property: ', u'Density')
Measurement:
('    Meas. sigma:', 29.64)
_____________________
()
---------------------PART B-----------------------
| Discrete Material entropy before: 0.0239919347372   |
| The average sampled Entropy is: 0.00613367390026   |
| -OUT- Discrete Information Gain: 0.0178582608369   |
| -info- AVERAGE sampled Information Gain: 0.0178582608369 |
--------------------------------------------------
()
------------------PART F--------------------
| This is the H(prop) for density: 5.73320029054 |
| This is the Emulated Entropy for density: 5.6163712886 |
| Argmax mean: -137 (no offset)|
| -x- Diff Information Gain: 0.116829001939     |
--------------------------------------------
()
---------Planned action------------
Property chosen to be measured is :
    >  density
With expected information gain : 
    >  0.01785826083689824
-----------------------------------

[94m[AS-INFO] Starting exploratory action.[0m
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621530684.628869]: Sending pose...
[INFO] [1621530684.653219]: Waiting for pose to finish...
[INFO] [1621530685.205602]: Sending pose...
[INFO] [1621530685.228306]: Waiting for pose to finish...
[INFO] [1621530686.277945]: Sending the gripper position...
[INFO] [1621530687.303600]: Sending pose...
[INFO] [1621530687.328342]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -8.93430900574
Reading number 10 : -8.92928314209
Reading number 20 : -8.93620681763
Reading number 30 : -8.91762161255
Reading number 40 : -8.92063903809
[94mTorque is: -8.92344623566[0m
[92mSo the estimated weight is 0.00126550214337[0m
[INFO] [1621530699.829488]: Sending pose...
[INFO] [1621530699.853430]: Waiting for pose to finish...
[INFO] [1621530701.980080]: Sending pose...
[INFO] [1621530702.003533]: Waiting for pose to finish...
[INFO] [1621530703.328686]: Sending the gripper position...
[INFO] [1621530704.353859]: Sending pose...
[INFO] [1621530704.378625]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621530705.929133]: Sending pose...
[INFO] [1621530705.970084]: Waiting for pose to finish...
[INFO] [1621530706.503788]: Sending pose...
[INFO] [1621530706.527801]: Waiting for pose to finish...
[INFO] [1621530707.588150]: Sending the gripper position...
[INFO] [1621530708.603603]: Sending pose...
[INFO] [1621530708.628518]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -8.9274520874
Reading number 10 : -8.93577957153
Reading number 20 : -8.93094348907
Reading number 30 : -8.90772819519
Reading number 40 : -8.94769859314
[94mTorque is: -8.92894231796[0m
[92mSo the estimated weight is 0.00304804442246[0m
[INFO] [1621530721.129721]: Sending pose...
[INFO] [1621530721.176541]: Waiting for pose to finish...
[INFO] [1621530723.303937]: Sending pose...
[INFO] [1621530723.327587]: Waiting for pose to finish...
[INFO] [1621530724.628164]: Sending the gripper position...
[INFO] [1621530725.655879]: Sending pose...
[INFO] [1621530725.678301]: Waiting for pose to finish...
[94m[INFO] Starting the weighing sequence.[0m
[INFO] [1621530727.230318]: Sending pose...
[INFO] [1621530727.252880]: Waiting for pose to finish...
[INFO] [1621530727.806777]: Sending pose...
[INFO] [1621530727.828303]: Waiting for pose to finish...
[INFO] [1621530728.977864]: Sending the gripper position...
[INFO] [1621530730.004372]: Sending pose...
[INFO] [1621530730.028174]: Waiting for pose to finish...
Payload torque: 
Reading number 0 : -8.9217004776
Reading number 10 : -8.88788223267
Reading number 20 : -8.91571044922
Reading number 30 : -8.9018497467
Reading number 40 : -8.94808006287
[94mTorque is: -8.9155619812[0m
[92mSo the estimated weight is -0.00129159509862[0m
[INFO] [1621530742.529236]: Sending pose...
[INFO] [1621530742.553243]: Waiting for pose to finish...
[INFO] [1621530744.678919]: Sending pose...
[INFO] [1621530744.703083]: Waiting for pose to finish...
[INFO] [1621530746.003218]: Sending the gripper position...
[INFO] [1621530747.028573]: Sending pose...
[INFO] [1621530747.052902]: Waiting for pose to finish...
Added density to the meas. database.
Added density to the meas. database.
()
CLOSED-FORM solution via value ----------------------------------------
Material:       Kwhite | Probabilty: 0.0000 | Cond. likelihood: 0.00000001
Material:        joker | Probabilty: 0.0003 | Cond. likelihood: 0.00100000
Material: SpongeYellow | Probabilty: 0.9997 | Cond. likelihood: 0.00875889
Material:       Kblack | Probabilty: 0.0000 | Cond. likelihood: 0.00000000
MODE 1 was chosen --> optimizing was done for discrete information gain 
 computed from the PMF.

