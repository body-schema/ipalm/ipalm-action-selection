import json
import numpy as np
import utility as ut
import matplotlib.pyplot as plt
from scipy.stats import norm, gaussian_kde
from sklearn.mixture import GaussianMixture


def json2dict(file_name):
    with open(file_name) as fp:
        data = fp.read()
        a = json.loads(data)
        return a

def flatten(t):
    return [item for sublist in t for item in sublist]

objs = json2dict("./crawl-data.json")

# get a dict consisting of "{material:{'bins':[bins], 'ground':[histogram]}"
object_names = ['bakingtray', 'bottle', 'bowl', 'fork', 'glass', 'hammer', 'kettle', 'knife', 'mug', 'pan', 'plate', 'sodacan', 'spoon', 'wineglass']

fillable = {"bakingtray":True, "bottle":True, "bowl":True,  "fork":False, "glass":True, "hammer":False,
            "kettle":True, "knife":False, "mug":True, "pan":True, "plate":True, "sodacan":False, "spoon":False,  "wineglass":True}

prop_list = ['Material', 'Colour', 'Weight', 'Volume', 'Length', 'Width', 'Height', 'Functionality']  # shape?, not very informative
bool_questions = ['button', 'lid', 'fillable', 'movable', 'washable', 'dissasemblable', 'handle']
questions_full_name = ['Does it has a button?', 'Does it has a lid?', 'Could it be filled?', 'Could it be moved?', 'Could it be washed?', 'Could it be dismantled?', 'Does it has a handle?']

def get_volume_distributions(objs, plot=False):
    volume_object_priors = {}
    for obj in objs:
        name = obj['name']
        print("-------------------------")
        print("Name: ", name)
        print("Object is fillable: {}".format(fillable[name]))
        vol = ut.get_london_property(obj, "Volume")
        bins = vol['bins']
        gt = vol['ground']
        data = []
        for i in range(len(bins)):
            # data.append([(bins[i]+bins[i] for _ in range(gt[i]) if bins[i] > 0])
            if bins[i] > 0:
                if i == len(bins)-1:
                    data.append([(bins[i]+ bins[i] + 500)/2 for _ in range(gt[i])])
                else:
                    data.append([(bins[i]+bins[i+1])/2 for _ in range(gt[i])])
            
        clusters = []
        n_pnts = len(flatten(data))
        k = 0
        for cluster in data:
            temp = {}
            if cluster == []:
                k+=1
                continue
            temp["mean"]= np.mean(cluster)

            # assuming 95% confidence interval
            if k == len(bins)-1:
                temp["std"] = np.sqrt(len(cluster)) * (bins[k] + 500 - bins[k]) / 3.92
            else:
                temp["std"] = np.sqrt(len(cluster)) * (bins[k+1]-bins[k]) / 3.92
            temp['w'] = len(cluster)/n_pnts
            clusters.append(temp)
            k+=1
        print("Data: ", data)
        print("clusters: ", clusters)
        
            
            
        data = flatten(data)
        if len(data) == 0:
            print("No usable data. Skipping.")
            volume_object_priors[name] = {"volume": {"singular_estim":{"mean":None, "std":None}, "clusters":None, "fillable":fillable[name]}, 'material':ut.get_london_property(obj, 'Material')}
            continue
        mean = np.mean(data)
        std = np.std(data)
        ax = np.linspace(0, max(bins) + 1000, max(bins) + 1000)
        if std == 0.0:
            print("Std is 0.0, not showing the gaussian.")
        else:
            
            if plot:
                y = norm.pdf(ax, mean, std)
                plt.plot(ax, y, label="Volume", lw=2, color="tab:blue")
                for idx, pdf in enumerate(clusters):
                    if idx == 0:
                        y_fit = norm.pdf(ax, pdf['mean'], pdf['std']) * pdf['w']
                    else:
                        y_fit += norm.pdf(ax, pdf['mean'], pdf['std']) * pdf['w']

                plt.plot(ax, y_fit.flatten(), label="Volume GMM", color="tab:orange")
                plt.title("Object {}".format(name))
                plt.legend()
                plt.show()
        volume_object_priors[name] = {"volume": {"singular_estim":{"mean":mean, "std":std}, "clusters":clusters, "fillable":fillable[name]}, 'material':ut.get_london_property(obj, 'Material')}
        
        
    return volume_object_priors

volume_object_priors = get_volume_distributions(objs, plot=False)

# print("------------------------")

# print("volume_object_priors: \n", json.dumps(volume_object_priors, indent=2, default=str))

with open('volume_object_priors.json', 'w') as fp:
    json.dump(volume_object_priors, fp, indent=4)

