#!/usr/bin/env python
# python version = 2.7
import time
import threading

import sys
import rospy
import rosservice
import math
import copy
import tf
import numpy as np
import os, signal  # for exiting
import matplotlib.pyplot as plt
import time
import random
import matplotlib.animation as animation
# import socket
# from npsocket.npsocket import SocketNumpyArray
from scipy.spatial import distance

from geometry_msgs.msg import Point, Pose, Twist, Vector3
from std_msgs.msg import Float32, String, Float64, Int32, Bool
from tf2_msgs.msg import TFMessage

from kortex_driver.msg import BaseCyclic_Feedback, ActionType, ActionNotification, ConstrainedPose, ActionEvent, Finger, \
    GripperMode, CartesianSpeed, JointAngle
from kortex_driver.srv import Base_ClearFaults, ExecuteAction, ExecuteActionRequest, SetCartesianReferenceFrame, \
    OnNotificationActionTopic, OnNotificationActionTopicRequest, \
    SendGripperCommand, SendGripperCommandRequest, GetMeasuredCartesianPose, SendTwistCommand, SendTwistCommandRequest, \
    SendWrenchCommand, SendWrenchCommandRequest, Stop, PlayJointTrajectory, PlayJointTrajectoryRequest  # , StringString
from ipalm_control.srv import StringString, \
    StringStringResponse  # not yet needed - good for updating ignored zones or whatever
import yaml

path_to_yaml = "/home/robot3/vision_ws/config/"
with open(os.path.join(path_to_yaml, "kinova_config.yaml"), "r") as file:
    yaml_config = yaml.load(file)
visualize = False  # visualization flag


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class IpalmGrasping:
    # Initialization functions
    def __init__(self):

        self.robot_name = self.check_name()
        if self.robot_name is None:
            rospy.logerr('unrecognized robot name')

        self.OBJ_IDS = yaml_config['obj_ids']  # TODO generalize ???
        self.BASE_FRAME = 1
        self.TOOL_FRAME = 2
        self.OPENED_GRIPPER_POSITION = 0.01
        self.CLOSED_GRIPPER_POSITION = 98.0
        self.GRIPPER_CURRENT_LIMIT = 0.05
        self.gripper_feedback = None

        self.TRANSLATION_SPEED = yaml_config['speed_limit']  # 0.30 seems to be the limit
        self.ORIENTATION_SPEED = 30

        self.BIG_RADIUS = 0.25
        self.MEDIUM_RADIUS = 0.10
        self.SMALL_RADIUS = 0.01
        self.NULL_RADIUS = 0.001
        self.SMALL_RADIUS_ANGLE = 1
        self.ANGULAR_TOLERANCE = 2
        self.ANGULAR_SPEED_LIMIT = 10
        self.restricted_radius = 0.16  # 0.16m from the origin
        self.WORKSPACE_AREA = [0.20, 0.90, -0.40, 0.40]  # list of [x1, x2, y1, y2], 2 coordinates defining the workspace area

        # Grasping speed
        # if visualize:
        #     self.CLOSING_SPEED = -0.0044
        # else:
        #     self.CLOSING_SPEED = -0.006

        self.CLOSING_SPEED = -0.006

        # Object lists
        self.grasping_position = []
        self.current_feedback_list = []
        self.gripper_position_list = []

        # Cartesian poses we need
        self.ignored_areas = []

        # self.pose_starting = Twist(Vector3(0.352, -0.032, 0.410), Vector3(179.900, 0.001, 90.000))
        # print(yaml_config['pose_starting'])
        self.pose_starting = self.coords2twist(yaml_config['pose_starting'])
        self.pose_basket = self.coords2twist(yaml_config['basket_mid'])
        # self.pose_basket = Twist(Vector3(0.0, -0.2, 0.40), Vector3(179.900, 0.001, 90.000)) # Current Basket size is 24 x 24 cm
        # for clarity
        basket_coords = yaml_config['basket_mid'][0]
        # self.set_ignore_area(0-0.14, -0.2-0.14, 0+0.14, -0.2+0.14) # ignores items in basket
        self.set_ignore_area(basket_coords[0] - 0.14, basket_coords[1] - 0.14, basket_coords[0] + 0.14,
                             basket_coords[1] + 0.14)
        self.pose_transition = Twist(Vector3(0.0, -0.2, 0.25), Vector3(179.900, 0.001, 90.000))

        self.marker_trans = [0.352, -0.032, 0.410]
        self.marker_rot = [179.900, 0.001, 90.000]

        # Angular pose we need

        self.pose_angle_starting = yaml_config['start_angles']  # in a seven shape
        self.current_joint_angle = [0.0, 0.0, 180.0, 270.0, 0.0, 270.0, 90.0]

        # Gripper positions we need
        self.gripper_opened = 0.00
        self.gripper_closed = 0.85
        self.did_grip = True  # used if gripper closes emptyhanded
        self.dont_look_ID = "-1"
        # self.set_ignore_area(0.58, -0.19, 0.4, 0.19) # ignores the middle rectangle

        # Statistics
        self.start_time = time.time()
        self.elapsed_time = self.start_time

        # Services
        self.init_services()

        # Subscribers

        self.actual_tool_pose = Twist()
        self.basefeedback = rospy.Subscriber("/" + self.robot_name + "/base_feedback", BaseCyclic_Feedback,
                                             self.base_feedback_callback, buff_size=1)

        self.tf_feedback = rospy.Subscriber("/tf", TFMessage, self.tf_callback)

        self.is_cube_found = False
        self.sub_cube_found = rospy.Subscriber('is_cube_found', Bool, self.is_cube_found_callback, queue_size=1)

        # Events
        self.action_type = ActionType.UNSPECIFIED_ACTION
        self.is_action_successful = False
        self.is_action_completed = threading.Event()
        self.is_action_completed.clear()

        # Subscribe to the ActionNotification
        req = OnNotificationActionTopicRequest()
        rospy.loginfo("Activating the action notifications...")
        try:
            self.activate_publishing_of_action_notification(req)
        except rospy.ServiceException:
            rospy.logerr("Failed to call OnNotificationActionTopic")
        else:
            rospy.loginfo("Successfully activated the Action Notifications!")

        rospy.Subscriber("/" + self.robot_name + "/action_topic", ActionNotification, self.action_notification_callback)

        self.actuator_current_motor = [0 for _ in range(yaml_config['dof'])]

    def coords2twist(self, x, y=None, z=None, rotx=None, roty=None, rotz=None):
        """
        This function takes general input, ideally from the YAML file, either a tuple of tuples, or a simple tuple
        or simply just naked coordinates.

        :param x: coord x
        :param y: coord y
        :param z: coord z
        :param rotx: rot of x
        :param roty: rot of y
        :param rotz: rot of z
        :return: product of Twist fnc
        """
        if y is None:  # then x is a tuple of 2 tuples of 3 floats ((x, y, z), (rotx, roty, rotz)) format from the yaml file
            tempx = x
            x = tempx[0][0]
            y = tempx[0][1]
            z = tempx[0][2]
            rotx = tempx[1][0]
            roty = tempx[1][1]
            rotz = tempx[1][2]
        elif z is None:
            tempx = x
            tempy = y
            x = tempx[0]
            y = tempx[1]
            z = tempx[2]
            rotx = tempy[0]
            roty = tempy[1]
            rotz = tempy[2]

        return Twist(Vector3(x, y, z), Vector3(rotx, roty, rotz))

    def visualize(self, x, y, itemid):

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        # xs = []  # x values array
        # ys = []  # y values array
        ax.clear()
        ax.plot(x, y, label=itemid)
        plt.show(block=False)

        # This function is called periodically from FuncAnimation
        """
        def animate(j, xs, ys):
            lineslist = ser.readlines(32)  # read 32 lines with formatted values
            for line in lineslist:  # loop for splitting each line into columns
                j = j + 1  # sample number
                listdata = line.split(b',')  # split the line - delimiter is comma
                dato = int(listdata[0])  # we are plotting the first column
                xs.append(j)  # add to list X new sample number (1 to N)
                ys.append(dato)  # add to list Y new data
                xs = xs[-100:]  # limit both lists to 100 items
                ys = ys[-100:]
                # Draw x and y lists
            ax.clear()
            ax.plot(xs, ys, label="dato")
        """
        # Set up plot to call animate() function periodically
        # ani = animation.FuncAnimation(fig, animate, fargs=(xs, ys), interval=50, repeat=0)

    def is_in_workspace(self, point):
        if (min(self.WORKSPACE_AREA[0:2]) < point[0] < max(self.WORKSPACE_AREA[0:2])) and (min(self.WORKSPACE_AREA[2:4]) < point[1] < max(self.WORKSPACE_AREA[2:4])):
            return True
        return False

    @staticmethod
    def check_name():
        service_list = rosservice.get_service_list()
        for element in service_list:
            if 'manipulator' in element:
                return 'manipulator'
            elif 'my_gen3' in element:
                return 'my_gen3'
        return None

    def init_services(self):
        # Services
        print("Starting to clear")
        clear_faults_full_name = '/' + self.robot_name + '/base/clear_faults'
        print("Waiting for service")
        rospy.wait_for_service(clear_faults_full_name)
        print("Clearing faults")
        self.clear_faults = rospy.ServiceProxy(clear_faults_full_name, Base_ClearFaults)
        print(self.clear_faults)

        execute_action_full_name = '/' + self.robot_name + '/base/execute_action'
        rospy.wait_for_service(execute_action_full_name)
        self.execute_action = rospy.ServiceProxy(execute_action_full_name, ExecuteAction)
        #
        set_cartesian_reference_frame_full_name = '/' + self.robot_name + '/control_config/set_cartesian_reference_frame'
        rospy.wait_for_service(set_cartesian_reference_frame_full_name)
        self.set_cartesian_reference_frame = rospy.ServiceProxy(set_cartesian_reference_frame_full_name,
                                                                SetCartesianReferenceFrame)
        #
        activate_publishing_of_action_notification_full_name = '/' + self.robot_name + '/base/activate_publishing_of_action_topic'
        rospy.wait_for_service(activate_publishing_of_action_notification_full_name)
        self.activate_publishing_of_action_notification = rospy.ServiceProxy(
            activate_publishing_of_action_notification_full_name, OnNotificationActionTopic)
        #
        send_gripper_command_full_name = '/' + self.robot_name + '/base/send_gripper_command'
        rospy.wait_for_service(send_gripper_command_full_name)
        self.send_gripper_command = rospy.ServiceProxy(send_gripper_command_full_name, SendGripperCommand)
        #
        get_measured_cartesian_pose_full_name = '/' + self.robot_name + '/base/get_measured_cartesian_pose'
        rospy.wait_for_service(get_measured_cartesian_pose_full_name)
        self.get_measured_cartesian_pose = rospy.ServiceProxy(get_measured_cartesian_pose_full_name,
                                                              GetMeasuredCartesianPose)
        #
        stop_action_full_name = '/' + self.robot_name + '/base/stop'
        rospy.wait_for_service(stop_action_full_name)
        self.stop_the_robot = rospy.ServiceProxy(stop_action_full_name, Stop)
        #
        self.twist_command = SendTwistCommandRequest()
        send_twist_command_full_name = '/' + self.robot_name + '/base/send_twist_command'
        rospy.wait_for_service(send_twist_command_full_name)
        self.send_twist_command = rospy.ServiceProxy(send_twist_command_full_name, SendTwistCommand)
        #
        self.wrench_command = SendWrenchCommandRequest()
        send_wrench_command_full_name = '/' + self.robot_name + '/base/send_wrench_command'
        rospy.wait_for_service(send_wrench_command_full_name)
        self.send_wrench_command = rospy.ServiceProxy(send_wrench_command_full_name, SendWrenchCommand)

        play_joint_trajectory_full_name = '/' + self.robot_name + '/base/play_joint_trajectory'
        rospy.wait_for_service(play_joint_trajectory_full_name)
        self.play_joint_trajectory = rospy.ServiceProxy(play_joint_trajectory_full_name, PlayJointTrajectory)

        # self.obj_feedback = rospy.Service("/" + self.robot_name + "/ignored_ids", StringString, self.id_handler) # a Service instance for string comm

        # Callbacks

    """
    # defintion of the StringString comm service - useful for updating ignored zones etc. between goes_to_object.py and epos_camera_stream.py
        
    def id_handler(self, arg_str):
        # add/remove ids depending on if we see it
        ret = []
        return StringStringResponse(ret)

    # Calling our service
    def id_handler_client(self, arg_str):
        rospy.wait_for_service("/" + self.robot_name + "/ignored_ids")
        try:
            strstr = rospy.ServiceProxy("/" + self.robot_name + "/ignored_ids", StringString)
            resp = strstr(arg_str)
            return resp.output  # ? equivalent to the int64 sum
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)
    """

    def tf_callback(self, feedback):
        """
        This function is called everytime there is a change in the /tf, listen to the transform between the grasping pose
        and the base of the robot, this pose can be send directly to the reach_cartesian_pose() function. The tfs can be
        visualize in the Terminal with the command `rostopic echo tf` when the kortex is launched
        Args:
            feedback: class with all the tfs, it is better to use the listener

        Returns: self.grasping_position is filled with poses
        """
        self.grasping_position = []  # a LIST OF TUPLES [(twist, obj_id), ... ]
        for object_id in self.OBJ_IDS:
            # not going for the object, if it is being ignored, might be solved with "all knowing" self.is_ignored

            try:
                (self.marker_trans, self.marker_rot) = listener.lookupTransform('base_link',
                                                                                '/grasping_object_{}'.format(object_id),
                                                                                rospy.Time(
                                                                                    0))  # rotation output in quaternions
                # Converts quaternions to degrees
                rotation_rad = tf.transformations.euler_from_quaternion(self.marker_rot)
                rotation = tuple([x * 180 / math.pi for x in rotation_rad])
                # print("obj_{}".format(object_id), self.marker_trans)  # reading OK
                # Fill the object list
                if not (self.is_ignored(self.marker_trans)):
                    self.grasping_position.append((Twist(
                        Vector3(self.marker_trans[0], self.marker_trans[1], self.marker_trans[2]),
                        Vector3(rotation[0], rotation[1], rotation[2])), object_id))
                else:
                    print(bcolors.OKBLUE + "[INFO] This object {} is in an IGNORED AREA".format(object_id) + bcolors.ENDC)


            except(tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                pass

    def set_ignore_area(self, xpos1, ypos1, xpos2, ypos2):
        self.ignored_areas.append((xpos1, xpos2, ypos1, ypos2))

    def is_ignored(self, place):
        for area in self.ignored_areas:
            if (min(area[0:2]) < place[0] < max(area[0:2])) and (min(area[2:4]) < place[1] < max(area[2:4])):
                return True
        return False

    def action_notification_callback(self, notif):
        if self.action_type == notif.handle.action_type:
            if notif.action_event == ActionEvent.ACTION_END:
                self.is_action_completed.set()
                self.is_action_successful = True
            elif notif.action_event == ActionEvent.ACTION_ABORT:
                self.is_action_completed.set()
                self.is_action_successful = False

    def is_in_restricted_area(self, x_m, y_m, z_m):
        """
        Function that returns True, if the point set by the manipulator is in the restricted area.
        Args:
            x_m: x coord of the manipulator
            y_m: y coord of the manipulator
            z_m: z coord of the manipulator (for future uses)

        Returns:
            bool: True if the point is restricted
                : False otherwise
        """

        if distance.sqeuclidean((x_m, y_m), (0, 0)) <= self.restricted_radius**2:  # 0.17:
            return True
        return False

    def base_feedback_callback(self, feedback):
        """
        This function returns the feedback from topic manipulator/base, can be visualized in the terminal using the
        command `rostopic echo manipulator/base_feedback` when the kortex is launched. Used to know the current gripper
        position and current, and the current joints angle
        Args:
            feedback: class with all the attributes from base_feedback
        """
        """
        if self.is_restricted(self.actual_tool_pose.linear.x, self.actual_tool_pose.linear.y, self.actual_tool_pose.linear.z):
            print("[WARN] The manipulator is in restricted area, emergency stop in progress!")
            for i in range(7):
                temp_joint_angles = []
                for i in range(7):
                    temp_joint_angles.append(feedback.actuators[i].position)
                self.current_joint_angle = temp_joint_angles
            # exit()
        """

        self.actual_tool_pose.linear.x = feedback.base.tool_pose_x
        self.actual_tool_pose.linear.y = feedback.base.tool_pose_y
        self.actual_tool_pose.linear.z = feedback.base.tool_pose_z
        self.actual_tool_pose.angular.x = feedback.base.tool_pose_theta_x
        self.actual_tool_pose.angular.y = feedback.base.tool_pose_theta_y
        self.actual_tool_pose.angular.z = feedback.base.tool_pose_theta_z
        self.gripper_feedback = feedback.interconnect.oneof_tool_feedback.gripper_feedback
        if self.gripper_feedback is not None:
            self.gripper_current = self.gripper_feedback[0].motor[0].current_motor
        self.current_gripper_position = self.gripper_feedback[0].motor[0].position

        # self.current_joint_angle = []
        temp_joint_angles = []
        for i in range(7):
            temp_joint_angles.append(feedback.actuators[i].position)
        self.current_joint_angle = temp_joint_angles

        # for i in range(7):
        #   self.actuator_current_motor[i] = feedback.actuators[i].current_motor  #should be in feedback for actuator not base

        # self.actual_tool_pose is the 6D orientation

    def is_cube_found_callback(self, msg):
        self.is_cube_found = msg.data

    def get_current_base_frame_pose(self):
        # Get the pose from the arm
        try:
            response = self.get_measured_cartesian_pose()
        except rospy.ServiceException:
            rospy.logerr("Failed to get measured cartesian pose")

        current_base_frame_pose = Twist()

        # This condition is to work around a bug where the linear and angular values are sometimes mixed up
        # Won't be needed in the near future
        if abs(response.output.x) < 1.5 and abs(response.output.y) < 1.5 and abs(response.output.z) < 3 \
                and (abs(response.output.theta_x) > 1 or abs(response.output.theta_y) > 1 or abs(
            response.output.theta_z) > 1):

            current_base_frame_pose.linear.x = response.output.x
            current_base_frame_pose.linear.y = response.output.y
            current_base_frame_pose.linear.z = response.output.z

            current_base_frame_pose.angular.x = response.output.theta_x
            current_base_frame_pose.angular.y = response.output.theta_y
            current_base_frame_pose.angular.z = response.output.theta_z

        else:
            current_base_frame_pose.linear.x = response.output.theta_x
            current_base_frame_pose.linear.y = response.output.theta_y
            current_base_frame_pose.linear.z = response.output.theta_z

            current_base_frame_pose.angular.x = response.output.x
            current_base_frame_pose.angular.y = response.output.y
            current_base_frame_pose.angular.z = response.output.z

        return current_base_frame_pose

    def calculate_camera_x_y(self, z_cloud):
        # Linear regression found experimentally
        # Probably needs to be altered for different setups
        x = 0.00004 * (z_cloud * z_cloud) + 0.00001 * z_cloud + 0.317
        y = 0.00018 * (z_cloud * z_cloud * z_cloud) - 0.00333 * (z_cloud * z_cloud) + 0.009 * z_cloud + 0.385

        return (x, y)

    def get_camera_target_point(self):
        coordinates = self.srv_get_xyz_coordinates().point
        x_pixels = coordinates.x
        y_pixels = coordinates.y
        z_cloud = coordinates.z

        # Convert the pixels
        # This could be much much better
        x = x_pixels * 0.001
        y = y_pixels * 0.001

        return (x, y, z_cloud)

    def calculate_delta_pose(self, target_pose, reference_frame):

        if reference_frame == self.TOOL_FRAME:

            (x_target, y_target, z_cloud) = self.get_camera_target_point()
            (x_camera, y_camera) = self.calculate_camera_x_y(z_cloud)

            delta_x = x_camera - x_target
            delta_y = y_camera - y_target
            delta_z = (
                        z_cloud - 0.195)  # we want to end up at 19.5cm from the cube

            delta_xtheta = 0
            delta_ytheta = 0
            delta_ztheta = 0

        elif reference_frame == self.BASE_FRAME:

            current_base_frame_pose = self.get_current_base_frame_pose()

            delta_x = target_pose.linear.x - current_base_frame_pose.linear.x
            delta_y = target_pose.linear.y - current_base_frame_pose.linear.y
            delta_z = target_pose.linear.z - current_base_frame_pose.linear.z

            delta_xtheta = target_pose.angular.x - current_base_frame_pose.angular.x
            delta_ytheta = target_pose.angular.y - current_base_frame_pose.angular.y
            delta_ztheta = target_pose.angular.z - current_base_frame_pose.angular.z

        else:  # TODO we should deal with an erroneous frame here
            pass

        return (delta_x, delta_y, delta_z, delta_xtheta, delta_ytheta, delta_ztheta)

    def normalize_vector3(self, x, y, z):

        temp = (x * x) + (y * y) + (z * z)

        if temp == 0:
            temp = 0.001

        norm = math.sqrt(temp)

        x_normalized = x / norm
        y_normalized = y / norm
        z_normalized = z / norm

        return (x_normalized, y_normalized, z_normalized)

    def is_position_reached(self, target_pose, radius, reference_frame):

        # Get feedback depending on frame
        if reference_frame == self.TOOL_FRAME:
            feedback = self.actual_tool_pose
            (target_pose.linear.x, target_pose.linear.y, feedback.linear.z) = self.get_camera_target_point()
            target_pose.linear.z = 0.195
            (feedback.linear.x, feedback.linear.y) = self.calculate_camera_x_y(feedback.linear.z)
        elif reference_frame == self.BASE_FRAME:
            feedback = self.get_current_base_frame_pose()
        else:  # TODO handle error if frame is not base or tool
            pass

        # Check if we reached the position with a certain offset
        radius_theta = radius * 100
        if (target_pose.linear.x - radius) < feedback.linear.x and feedback.linear.x < (
                target_pose.linear.x + radius) and \
                (target_pose.linear.y - radius) < feedback.linear.y and feedback.linear.y < (
                target_pose.linear.y + radius) and \
                (target_pose.linear.z - radius) < feedback.linear.z and feedback.linear.z < (
                target_pose.linear.z + radius) and \
                (target_pose.angular.x - radius_theta) < feedback.angular.x and feedback.angular.x < (
                target_pose.angular.x + radius_theta) and \
                (target_pose.angular.y - radius_theta) < feedback.angular.y and feedback.angular.y < (
                target_pose.angular.y + radius_theta) and \
                (target_pose.angular.z - radius_theta) < feedback.angular.z and feedback.angular.z < (
                target_pose.angular.z + radius_theta):
            return True
        else:
            return False

    def is_twist_command_null(self):
        return self.twist_command.input.twist.linear_x == 0 and \
               self.twist_command.input.twist.linear_y == 0 and \
               self.twist_command.input.twist.linear_z == 0 and \
               self.twist_command.input.twist.angular_x == 0 and \
               self.twist_command.input.twist.angular_y == 0 and \
               self.twist_command.input.twist.angular_z == 0

    def process_and_send_twist_to_robot(self, twist, use_normalized_twist, translation_speed_limit,
                                        orientation_speed_limit, reference_frame):

        if use_normalized_twist:
            (direction_x, direction_y, direction_z) = self.normalize_vector3(twist.linear.x, twist.linear.y,
                                                                             twist.linear.z)
            (direction_x_theta, direction_y_theta, direction_z_theta) = self.normalize_vector3(twist.angular.x,
                                                                                               twist.angular.y,
                                                                                               twist.angular.z)
            target = Twist(Vector3(direction_x, direction_y, direction_z),
                           Vector3(direction_x, direction_y_theta, direction_z_theta))
        else:
            target = twist
            p_gain = 1
            minimum_translation_speed = 0.01
            minimum_orientation_speed = 1

        if use_normalized_twist:
            pass
        else:
            # Validation: put to 0 if lower than minimum, cap at max if above max, multiply by a proportional gain
            target.linear.x *= p_gain
            target.linear.x = target.linear.x if abs(target.linear.x) > minimum_translation_speed else 0.0
            target.linear.x = math.copysign(min(abs(target.linear.x), translation_speed_limit), target.linear.x)
            target.linear.y *= p_gain
            target.linear.y = target.linear.y if abs(target.linear.y) > minimum_translation_speed else 0.0
            target.linear.y = math.copysign(min(abs(target.linear.y), translation_speed_limit), target.linear.y)
            target.linear.z *= p_gain
            target.linear.z = target.linear.z if abs(target.linear.z) > minimum_translation_speed else 0.0
            target.linear.z = math.copysign(min(abs(target.linear.z), translation_speed_limit), target.linear.z)
            target.angular.z *= p_gain
            target.angular.z = target.angular.z if abs(target.angular.z) > minimum_orientation_speed else 0.0
            target.angular.z = math.copysign(min(abs(target.angular.z), orientation_speed_limit), target.angular.z)
            target.angular.z *= -1  # hardcoded -1 here

        # Fill the twist command for the arm
        self.twist_command.input.twist.linear_x = target.linear.x
        self.twist_command.input.twist.linear_y = target.linear.y
        self.twist_command.input.twist.linear_z = target.linear.z
        self.twist_command.input.twist.angular_x = 0.0
        self.twist_command.input.twist.angular_y = 0.0
        self.twist_command.input.twist.angular_z = target.angular.z

        # Send the command to the arm
        self.twist_command.input.reference_frame = reference_frame
        self.twist_command.input.duration = 0
        self.send_twist_command(self.twist_command)

    def move_toward_pose(self, target_pose, translation_speed, orientation_speed, radius, reference_frame):

        # Find the delta between current pose and target pose
        (delta_x, delta_y, delta_z, delta_xtheta, delta_ytheta, delta_ztheta) = self.calculate_delta_pose(target_pose,
                                                                                                          reference_frame)
        delta_pose = Twist(Vector3(delta_x, delta_y, delta_z), Vector3(delta_xtheta, delta_ytheta, delta_ztheta))

        (direction_x, direction_y, direction_z) = self.normalize_vector3(delta_pose.linear.x, delta_pose.linear.y,
                                                                         delta_pose.linear.z)
        (direction_x_theta, direction_y_theta, direction_z_theta) = self.normalize_vector3(delta_pose.angular.x,
                                                                                           delta_pose.angular.y,
                                                                                           delta_pose.angular.z)
        target = Twist(Vector3(direction_x, direction_y, direction_z),
                       Vector3(direction_x, direction_y_theta, direction_z_theta))

        if self.is_position_reached(target_pose, radius, reference_frame):
            if self.is_position_reached(target_pose, self.MEDIUM_RADIUS, reference_frame):
                if self.is_position_reached(target_pose, 0.07, reference_frame):
                    self.process_and_send_twist_to_robot(delta_pose, False, 0.08, 30, reference_frame)
                else:
                    self.process_and_send_twist_to_robot(delta_pose, False, 0.12, 30, reference_frame)
            else:
                self.process_and_send_twist_to_robot(delta_pose, False, 0.16, 30, reference_frame)
        else:
            self.process_and_send_twist_to_robot(delta_pose, False, 0.20, 30, reference_frame)

        return self.is_twist_command_null()

    # Demo steps

    def rotate_camera(self, translation_speed, orientation_speed, radius):

        widths = []
        zthetas = []
        previous_width = 9999
        rotation = 10  # deg

        i = 0
        # Rotate one way
        while not rospy.is_shutdown():
            # Get the width of the square
            w = self.srv_get_cube_width()
            if i == 0:
                previous_width = w.data

            # Change angular z target
            target_pose = copy.deepcopy(self.actual_tool_pose)
            target_pose.angular.z -= rotation

            # Move until you find the minimum width
            while not (self.actual_tool_pose.angular.z < (target_pose.angular.z + 7)) and not rospy.is_shutdown():
                self.move_toward_pose(target_pose, translation_speed, orientation_speed, radius, self.BASE_FRAME)

            # If the width grows bigger two times in a row, break from the loop
            w = self.srv_get_cube_width()
            if previous_width < w.data:
                i = i + 1
                if i > 2:
                    break
            else:
                i = 0

        # Rotate the other way
        previous_width = 9999
        while not rospy.is_shutdown():
            # Get the width of the square
            w = self.srv_get_cube_width()
            if i == 0:
                previous_width = w.data

            # Change angular z target
            target_pose = copy.deepcopy(self.actual_tool_pose)
            target_pose.angular.z += rotation

            widths.append(w.data)
            zthetas.append(self.actual_tool_pose.angular.z)

            # Move until you find the minimum width
            while not (self.actual_tool_pose.angular.z > (target_pose.angular.z - 7)) and not rospy.is_shutdown():
                self.move_toward_pose(target_pose, translation_speed, orientation_speed, radius, self.BASE_FRAME)

            # If the width grows bigger two times in a row, break from the loop
            w = self.srv_get_cube_width()
            if previous_width < w.data:
                i = i + 1
                if i > 2:
                    break
            else:
                i = 0

        # Find where the minimum width was and go there
        minwidth = min(widths)
        index = widths.index(minwidth)
        target_ztheta = zthetas[index]

        target_pose = copy.deepcopy(self.get_current_base_frame_pose())
        target_pose.angular.z = target_ztheta
        self.reach_cartesian_pose_and_wait_for_completion(target_pose)

        return True

    def approach(self, translation_speed, orientation_speed, radius):

        # Approach the cube broadly and reach the good XY coordinates
        while not rospy.is_shutdown():

            curr = self.get_current_base_frame_pose()
            target_pose = copy.deepcopy(curr)

            # If we see the cube
            if self.is_cube_found:
                (target_pose.linear.x, target_pose.linear.y, target_pose.linear.z) = self.get_camera_target_point()
                self.move_toward_pose(target_pose, translation_speed, orientation_speed, radius, self.TOOL_FRAME)
                print('target: ', target_pose.linear.z)
                print('current: ', curr.linear.z)
                print('current: ', self.srv_get_xyz_coordinates())
                # we approached the cube enough
                if 0.205 >= target_pose.linear.z and target_pose.linear.z >= 0.185 and self.is_twist_command_null():
                    break

            # If we don't see the cube
            else:
                self.stop_the_robot()
                rospy.sleep(0.1)
                while not rospy.is_shutdown():
                    self.move_toward_pose(self.pose_starting, translation_speed, orientation_speed, radius,
                                          self.BASE_FRAME)
                    # Look for the cube
                    if self.is_cube_found:
                        # We found the cube
                        break

        # Approach the cube more precisely in Z
        target_pose = copy.deepcopy(self.get_current_base_frame_pose())

        target_pose.linear.z -= 0.07  # approach the cube by 7mm
        start = time.time()
        time_elapsed = 0  # seconds
        while not rospy.is_shutdown():
            time_elapsed = time.time() - start
            if self.move_toward_pose(target_pose, 0.03, 0, 0.02, self.BASE_FRAME) or time_elapsed > 5:
                # if self.move_toward_pose(target_pose, 0.03, 0, 0.02, self.TOOL_FRAME) or time_elapsed > 5:
                break

        self.stop_the_robot()

    def send_gripper_command_speed(self, value, current):
        """
        Send speed to open or close the gripper using SendGripperCommand service
        Args:
            value: float, speed in m/s, the gripper open when > 0 and closes when < 0
            current: float, current gripper current in self.gripper current
        Returns: bool, False if the object is grasped
        """
        # Initialize request
        req = SendGripperCommandRequest()
        temp_finger = Finger()
        temp_finger.finger_identifier = 0
        if current < self.GRIPPER_CURRENT_LIMIT:  # 0.05 to grip an object is enough
            temp_finger.value = value
        else:
            temp_finger.value = 0
        req.input.gripper.finger.append(temp_finger)
        req.input.mode = GripperMode.GRIPPER_SPEED
        # Try to send request
        try:
            self.send_gripper_command(req)
        except rospy.ServiceException:
            rospy.logerr("Failed to call SendGripperCommand")

        rospy.sleep(0.01)
        if current < self.GRIPPER_CURRENT_LIMIT:
            return True
        else:
            return False

    def send_gripper_command_and_wait_for_completion(self, value):
        """
        Move the gripper to an opening position
        Args:
            value: float, relative opening of the gripper, opened when = 0 and closed when = 1
        """
        # Create the gripper command request
        req = SendGripperCommandRequest()
        finger = Finger()
        finger.finger_identifier = 0.0
        finger.value = value
        req.input.gripper.finger.append(finger)
        req.input.mode = GripperMode.GRIPPER_POSITION
        # Call the service
        try:
            self.send_gripper_command(req)
        except rospy.ServiceException:
            rospy.logerr("Failed to call SendGripperCommand")
            return False
        else:
            rospy.loginfo("Sending the gripper position...")

            # Wait a bit
        rospy.sleep(0.5)
        return True

    def send_joint_angles_fct(self):
        """
        Move the joints of the robot to the starting position, can unstick the robot after multiple movements
        """
        # Initialize request
        req = PlayJointTrajectoryRequest()
        for i in range(7):
            temp_angle = JointAngle()
            temp_angle.joint_identifier = i
            temp_angle.value = self.pose_angle_starting[i]  # [0,0,0,0,0,0,0] = robot vertical position
            req.input.joint_angles.joint_angles.append(temp_angle)
        succeded = False
        # Wait for completion
        while not succeded:
            try:
                # while len(self.current_joint_angle) < 7:
                #     pass
                self.play_joint_trajectory(req)
                succeded = True
                # Compare old and new value of angles
                a = np.array(self.current_joint_angle)
                b = np.array(self.pose_angle_starting)
                count = 0
                c = np.sum(np.abs(a - b)) % 360
                while c > self.ANGULAR_TOLERANCE * 2 and not c > 360.0 - self.ANGULAR_TOLERANCE and count < 30:
                    count += 1
                    rospy.sleep(0.2)
                    a = np.array(self.current_joint_angle)
                    c = np.sum(np.abs(a - b)) % 360

                if count > 30:
                    succeded = False

            except rospy.ServiceException:
                rospy.logerr("Failed to send pose")
                rospy.sleep(0.5)
                succeded = False

    def line_from_points(self, P, Q):
        """

        Args:
            P: [x,y] point1
            Q: [x,y] point2

        Returns:
            three element tuple returning coeffs of a line in a form ax + by + c = 0
            from a previous computation result ax + by = c, thats why the c in return is -c

        """
        a = Q[1] - P[1]
        b = P[0] - Q[0]
        c = a * (P[0]) + b * (P[1])

        return (a, b, -c)

    def will_cross_restricted_area(self, a, b, c, radius, x=0, y=0):
        """
        Args:
            The following are parametres for a general line equation ax + by + c = 0
            a: (a*x)
            b: (b*y)
            c: (c)
            radius: circular distance from the NONO point
            x: x of the NONO point
            y: y of the NONO point

        Returns:
            bool: True if the line crosses a restricted area

        """
        dist = ((abs(a * x + b * y + c)) /
                math.sqrt(a * a + b * b))

        if dist <= radius:
            print(bcolors.WARNING + "[WARN] Planned trajectory intersects a restricted area!" + bcolors.ENDC)
            return True

        print(bcolors.OKBLUE + "[INFO] Planned trajectory should be safe." + bcolors.ENDC)

        return False

    def reach_cartesian_pose_and_wait_for_completion(self, target_pose, trans_speed):
        """
        Move the robot to a cartesian pose with any mean possible
        Args:
            target_pose: Twist(), Target pose
            trans_speed: float, translation speed

        Returns: bool, True if action successful
        """
        # Clear the faults
        self.clear_faults()
        rospy.sleep(0.5)

        pose_speed = CartesianSpeed()
        pose_speed.translation = trans_speed
        pose_speed.orientation = self.ORIENTATION_SPEED

        # Create the pose and the request
        my_constrained_pose = ConstrainedPose()
        # https://github.com/Kinovarobotics/ros_kortex/tree/kinetic-devel/kortex_examples/src/full_arm
        my_constrained_pose.constraint.oneof_type.speed.append(pose_speed)

        lastPoint = [self.actual_tool_pose.linear.x, self.actual_tool_pose.linear.y]
        plannedPoint = [target_pose.linear.x, target_pose.linear.y]
        currentLine = self.line_from_points(lastPoint, plannedPoint)

        # TODO first check whether are both points in the workspace area
        print("lastPoint", lastPoint)
        print("is lastPoint in WS? ", self.is_in_workspace(lastPoint))
        print("plannedPoint", plannedPoint)
        print("is plannedPoint in WS? ", self.is_in_workspace(plannedPoint))

        if not (self.is_in_workspace(lastPoint) and self.is_in_workspace(plannedPoint)):
            if self.will_cross_restricted_area(currentLine[0], currentLine[1], currentLine[2], self.restricted_radius):
                print(bcolors.WARNING + "[WARN] Trajectory on a collision course, halting." + bcolors.ENDC)

                return False
        else:
            print(bcolors.OKBLUE + "[INFO] Trajectory OK, both points in the workspace area." + bcolors.ENDC)


        my_constrained_pose.target_pose.x = target_pose.linear.x
        my_constrained_pose.target_pose.y = target_pose.linear.y
        my_constrained_pose.target_pose.z = target_pose.linear.z
        my_constrained_pose.target_pose.theta_x = target_pose.angular.x
        my_constrained_pose.target_pose.theta_y = target_pose.angular.y
        my_constrained_pose.target_pose.theta_z = target_pose.angular.z

        req = ExecuteActionRequest()
        req.input.oneof_action_parameters.reach_pose.append(my_constrained_pose)

        # Tell the callback to look for a REACH_POSE action type
        self.action_type = ActionType.REACH_POSE

        # Send the pose
        rospy.loginfo("Sending pose...")
        try:
            self.execute_action(req)
        except rospy.ServiceException:
            rospy.logerr("Failed to send pose")
        else:
            rospy.loginfo("Waiting for pose to finish...")

        # Wait for the pose to finish

        success = self.is_action_completed.wait()

        # Clear the notifications variables afterwards
        self.is_action_completed.clear()
        self.action_type = ActionType.UNSPECIFIED_ACTION

        # Return if action was successful or aborted
        return self.is_action_successful

    def print_pos(self):

        # rospy.loginfo(self.actual_tool_pose)
        # rospy.loginfo("arm_current =")
        # rospy.loginfo(self.arm_current)
        # rospy.loginfo("arm_voltage =")
        # rospy.loginfo(self.arm_voltage)
        # rospy.loginfo("actuator_current_motor =")
        #
        # for i in range(7):
        #     rospy.loginfo(self.actuator_current_motor[i])
        if self.gripper_feedback is not None:
            rospy.loginfo(self.gripper_feedback[0].motor[0].current_motor)

    def fill_current_and_gripper_list(self):
        """
        Fill arrays for grasping measurement
        """
        self.current_feedback_list.append(self.gripper_current)
        self.gripper_position_list.append(self.current_gripper_position)

    def save_data(self, object_id, order_id):
        """
        Save datas in .npy files of relative current and gripper position for grasping measurements
        Args:
            object_id: int, ID of object
            order_id: int, order id in running
        """
        # print(object_id)
        print(bcolors.OKBLUE + "[INFO] saving data" + bcolors.ENDC)
        while True:
            try:  # TODO assuming perfect execution
                (trans, rot) = listener.lookupTransform('/robotiq_arg2f_base_link',
                                                        '/grasping_object_{}'.format(object_id),
                                                        rospy.Time(
                                                            0))  # rotation output in quaternions
                # Converts quaternions to degrees
                rotation_rad = tf.transformations.euler_from_quaternion(rot)
                rot = tuple([x * 180 / math.pi for x in rotation_rad])
                # print("obj_{}".format(object_id), self.marker_trans)  # reading OK
                break
            except(tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
                # print(e)
                pass

        with open('saved_data/' + time.strftime("%Y-%b-%d-%H-%M", time.gmtime()) + '-{}-{}.npy'.format(object_id,
                                                                                                       order_id),
                  'wb') as f:  # TODO correct saving format
            # np.save(f, np.transpose(np.array(self.gripper_position_list)))
            # np.save(f, np.transpose(np.array(self.current_feedback_list)))
            np.save(f, np.array((trans, rot, self.gripper_position_list, self.current_feedback_list)))
            print(np.array((trans, rot, self.gripper_position_list, self.current_feedback_list)))
            print(bcolors.OKBLUE + "[INFO] successfully saved data" + bcolors.ENDC)
        x = self.gripper_position_list
        y = self.current_feedback_list
        # for _ in range(10):
        #     x.append(random.randrange(10))
        #     y.append(random.randrange(10))
        if visualize:
            self.visualize(x, y, object_id)
        print("#######################")
        print("Printing object properties")
        print(np.transpose(np.array(self.gripper_position_list)))
        print(np.transpose(np.array(self.current_feedback_list)))
        print("#######################")

    def clear_files(self):
        """
        Clear the .npy files for grasping measurements
        """
        print(os.getcwd())
        with open('saved_data/size_object.txt', 'w') as f:
            txt = ''
            f.write(txt)

    def calculate_near_pose(self, pose, distance):
        """
        Calculate a position near the grasping point with the same orientation and at a certain distance
        Args:
            pose: Twist(), grasping point of the object
            distance: float, distance from the grasping point
        Returns: Twist(), near pose
        """
        near_pose = Twist()
        near_pose.linear.x = pose.linear.x
        near_pose.linear.y = pose.linear.y
        near_pose.linear.z = pose.linear.z + distance
        near_pose.angular.x = pose.angular.x  # -177.40
        near_pose.angular.y = pose.angular.y  # -0.05
        near_pose.angular.z = pose.angular.z  # -179.34
        return near_pose

    def main(self):
        """
        Demo to pick up objects seen with infer.py and epos_camera_stream.py
        """
        self.clear_files()
        # Start the demo
        while not rospy.is_shutdown():
            # Initialize to starting position
            # print("Going to the initial position...")
            self.send_joint_angles_fct()  # reset joint position instead of cartesian so that the robot doesnt get stuck
            # print("Initial position reached.")
            self.print_pos()
            object_count = 0
            # Grasp objects
            print(self.grasping_position)

            # self.grasping_position is a list of tuples: [(twist, object_id), ], where positions is a twist((x,z,y)(rotx,roty,rotz))
            while self.grasping_position:
                grasp_pos = self.grasping_position.pop()
                object_grasp = grasp_pos[0]  # select the first object seen
                curr_object_id = grasp_pos[1]  # select the first object's ID
                # Goes to starting position
                self.send_joint_angles_fct()
                # Open Gripper
                self.send_gripper_command_and_wait_for_completion(self.gripper_opened)
                print("Going to the object")
                # Calculate and go to near pose
                near_pose = self.calculate_near_pose(object_grasp, 0.1)
                self.reach_cartesian_pose_and_wait_for_completion(near_pose, self.TRANSLATION_SPEED / 1.5)
                # Goes to object
                self.reach_cartesian_pose_and_wait_for_completion(object_grasp,
                                                                  self.TRANSLATION_SPEED / 5)  # speed must be lower when distance is small

                # Initialize object grasping and measurement lists
                no_object = True
                self.dont_look_ID = "-1"
                self.current_feedback_list = []
                self.gripper_position_list = []
                # Grasp object
                print('Closing Gripper...')
                while no_object and self.current_gripper_position < self.CLOSED_GRIPPER_POSITION:
                    if self.current_gripper_position < self.CLOSED_GRIPPER_POSITION:
                        self.did_grip = False
                    self.dont_look_ID = str(curr_object_id)  # get ID of object being grabbed for EPOS
                    # self.curr_ignored_IDS.add(self.dont_look_ID)

                    no_object = self.send_gripper_command_speed(self.CLOSING_SPEED / 1.1, self.gripper_current)
                    # is_object = self.send_gripper_command_speed(self.CLOSING_SPEED/1.52, self.gripper_current)

                    self.fill_current_and_gripper_list()

                listener.clear()  # clear the buffer of the subscriber
                # Goes to Basket
                if not no_object:
                    # Save data in .npy files only if caught object
                    self.save_data(curr_object_id, object_count)
                    object_count += 1
                    self.reach_cartesian_pose_and_wait_for_completion(self.pose_basket, self.TRANSLATION_SPEED)
                    self.send_gripper_command_and_wait_for_completion(self.gripper_opened)
                else:
                    self.send_gripper_command_and_wait_for_completion(self.gripper_opened)
                    self.reach_cartesian_pose_and_wait_for_completion(self.pose_starting, self.TRANSLATION_SPEED)
                # Release object
                self.did_grip = True


def shut_down_node(sig=2, frame=None):
    """
    Close the ros node
    """
    print(sig)
    nodes = os.popen("rosnode list").readlines()
    for i, node in enumerate(nodes):
        nodes[i] = nodes[i].replace("\n", "")
        if nodes[i] == node_name:
            os.system("rosnode kill " + node)
    exit()


signal.signal(signal.SIGINT, shut_down_node)  # when receiving ctrl+c
signal.signal(signal.SIGTERM, shut_down_node)  # idk what this does exactly
signal.signal(20, shut_down_node)  # when receiving ctrl+z
# ctrl+z - https://stackoverflow.com/questions/11886812/what-is-the-difference-between-sigstop-and-sigtstp

node_name = 'pickup_object'

if __name__ == '__main__':
    if (len(sys.argv) == 1):
        print(bcolors.OKBLUE + "[INFO] Running in speechless mode." + bcolors.ENDC)
        visualize = False
    elif (len(sys.argv) == 2):
        if sys.argv[1] == '--mes':
            print(bcolors.OKBLUE + "[INFO] Running in MEASURE mode with visualization." + bcolors.ENDC)
            visualize = True

    rospy.init_node()
    listener = tf.TransformListener()
    rate = rospy.Rate(10)  # the rate changes the amount of measurements for grasping

    # Create the experiment object
    experiment = IpalmGrasping()
    try:
        experiment.main()
    except KeyboardInterrupt:
        print("Shutting down")
    # demo.stop_the_robot()
    # cv2.destroyAllWindows()
    shut_down_node()
